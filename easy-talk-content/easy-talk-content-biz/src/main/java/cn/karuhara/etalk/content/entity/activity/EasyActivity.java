package cn.karuhara.etalk.content.entity.activity;

import cn.karuhara.etalk.content.enums.ActivityPushEnum;
import cn.karuhara.etalk.content.enums.ActivityStatusEnum;
import cn.karuhara.etalk.framework.mybatis.core.dataobject.BaseDO;
import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 活动
 * @TableName easy_activity
 */
@EqualsAndHashCode(callSuper = true)
@TableName(value ="easy_activity",autoResultMap = true)
@Data
public class EasyActivity extends BaseDO implements Serializable {

    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 活动封面
     */
    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<String> coverImageUrl;

    /**
     * 名称
     */
    private String name;

    /**
     * 简介
     */
    private String intro;

    /**
     * 介绍图 例如 活动的海报
     */
    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<String> imgs;

    /**
     * 活动创建人
     */
    private Integer userId;

    /**
     * 报名开始时间
     */
    private LocalDateTime applyStart;

    /**
     * 活动结束时间
     */
    private LocalDateTime applyEnd;

    /**
     * 开始时间
     */
    private LocalDateTime startTime;

    /**
     * 结束时间
     */
    private LocalDateTime endTime;

    /**
     * 人数限制
     */
    private Integer peopleNum;

    /**
     * 地点纬度  例如 39.90420000（代表北京的大致纬度）
     */
    private Double latitude;

    /**
     * 地点经度  例如 116.40740000（代表北京的大致经度）
     */
    private Double longitude;

    /**
     * 地点位置  例如 北京市朝阳区 XX 街道 XX 号
     */
    private String address;

    /**
     * 地点名称  例如 XX 公园”“XX 商场
     */
    private String placeName;

    /**
     * 浏览
     */
    private Integer browse;

    /**
     * 首页推送：0 否、1 是
     */
    private ActivityPushEnum push;

    /**
     * 状态：1 报名中、2 进行中、3 已结束、4 已下线
     */
    private ActivityStatusEnum status;

    @TableLogic
    private Integer deleted;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}