package cn.karuhara.etalk.content.controller.app.dynamic.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "发表评论的VO")
@Data
public class CommentPushVO {
    @Schema(description = "评论者的id")
    private Integer userId;

    @Schema(description = "动态id")
    private Integer dynamicId;

    @Schema(description = "回复的评论ID")
    private Integer replyCommentId;

    @Schema(description = "回复内容")
    private String content;
}
