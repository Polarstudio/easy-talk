package cn.karuhara.etalk.content.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 评论点赞状态枚举
 */
@Getter
@AllArgsConstructor
public enum CommentEnum {
    LIKE("0","点赞"),
    UNLIKE("1","点踩");
    @EnumValue
    @JsonValue
    private final String code;

    private final String desc;
}
