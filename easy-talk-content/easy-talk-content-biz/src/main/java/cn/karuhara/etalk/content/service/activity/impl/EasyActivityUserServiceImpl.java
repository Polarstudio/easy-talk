package cn.karuhara.etalk.content.service.activity.impl;

import cn.karuhara.etalk.content.controller.app.activity.vo.ActivityAppApplyDetailVO;
import cn.karuhara.etalk.content.controller.app.activity.vo.ActivityAppApplyPageReqVO;
import cn.karuhara.etalk.content.controller.app.activity.vo.ActivityUserInfoVO;
import cn.karuhara.etalk.content.entity.activity.EasyActivity;
import cn.karuhara.etalk.content.enums.ActivityUserStatus;
import cn.karuhara.etalk.content.service.activity.EasyActivityService;
import cn.karuhara.etalk.content.service.activity.EasyActivityUserService;
import cn.karuhara.etalk.framework.common.enums.GlobalCodeEnum;
import cn.karuhara.etalk.framework.common.exception.ServiceException;
import cn.karuhara.etalk.framework.common.pojo.CommonResult;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import cn.karuhara.etalk.member.api.MemberApi;
import cn.karuhara.etalk.member.api.dto.MemberBaseInfoDTO;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.karuhara.etalk.content.entity.activity.EasyActivityUser;
import cn.karuhara.etalk.content.mapper.activity.EasyActivityUserMapper;
import io.micrometer.common.util.StringUtils;
import jakarta.annotation.Resource;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
* @author atom
* @description 针对表【easy_activity_user(用户参与活动)】的数据库操作Service实现
* @createDate 2024-11-19 10:49:07
*/
@Service
public class EasyActivityUserServiceImpl extends ServiceImpl<EasyActivityUserMapper, EasyActivityUser>
    implements EasyActivityUserService {

    @Resource
    private MemberApi memberApi;

    @Resource
    private EasyActivityUserMapper activityUserMapper;

    @Resource
    private EasyActivityService activityService;

    @Resource
    private EasyActivityService easyActivityService;

    /**
     * 分页查询活动的报名用户
     *
     * @param pageReqVO 分页参数
     * @return 分页数据
     */
    @Override
    public PageResult<ActivityAppApplyDetailVO> getList(ActivityAppApplyPageReqVO pageReqVO) {
        PageResult<ActivityAppApplyDetailVO> pageResult = new PageResult<>();
        LambdaQueryWrapper<EasyActivityUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyActivityUser::getActivityId,pageReqVO.getActivityId());
        if (StringUtils.isNotBlank(pageReqVO.getPhone())) {
            //通过手机号找到用户
            MemberBaseInfoDTO member = memberApi.getMemberByPhone(pageReqVO.getPhone());
            if (member == null) {
                return PageResult.empty();
            }
            //如果存在在查看是否已经报名
            queryWrapper.eq(EasyActivityUser::getUserId,member.getId());
            EasyActivityUser one = getOne(queryWrapper);
            if (one == null) {
                return PageResult.empty();
            }
            ActivityAppApplyDetailVO detailVO = new ActivityAppApplyDetailVO();
            detailVO.setAvatar(member.getAvatar());
            detailVO.setId(member.getId());
            detailVO.setNikeName(member.getNikeName());
            detailVO.setPhone(member.getPhone());
            List<ActivityAppApplyDetailVO> detailVOS = new ArrayList<>();
            detailVOS.add(detailVO);
            pageResult.setList(detailVOS);
            pageResult.setTotal(1L);
            return pageResult;
        }
        Page<EasyActivityUser> page = new Page<>(pageReqVO.getPageNo(), pageReqVO.getPageSize());
        Page<EasyActivityUser> activityUserPage = activityUserMapper.selectPage(page, queryWrapper);
        if (activityUserPage.getTotal() == 0) {
            return PageResult.empty();
        }
        //获取到参与活动的用户id
        List<EasyActivityUser> records = activityUserPage.getRecords();
        List<Integer> userIds = records.stream().map(EasyActivityUser::getUserId).collect(Collectors.toList());
        List<MemberBaseInfoDTO> members = memberApi.getByIds(userIds);
        List<ActivityAppApplyDetailVO> detailVOS = new ArrayList<>();
        members.forEach(member -> {
            ActivityAppApplyDetailVO detailVO = new ActivityAppApplyDetailVO();
            detailVO.setAvatar(member.getAvatar());
            detailVO.setId(member.getId());
            detailVO.setNikeName(member.getNikeName());
            detailVO.setPhone(member.getPhone());
            detailVOS.add(detailVO);
        });
        return new PageResult<>(detailVOS, page.getPages());
    }

    /**
     * 移除报名的用户
     *
     * @param activityId 活动id
     * @param userId     用户id
     */
    @Override
    public void delete(Integer activityId, Integer userId) {
        LambdaQueryWrapper<EasyActivityUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyActivityUser::getActivityId,activityId);
        queryWrapper.eq(EasyActivityUser::getUserId,userId);
        remove(queryWrapper);
    }

    /**
     * 报名活动
     *
     * @param activityId 活动id
     * @param userId     用户id
     */
    @Override
    public void applyActivity(Integer activityId, Integer userId) {
        //先查询用户是否已经报名
        LambdaQueryWrapper<EasyActivityUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyActivityUser::getActivityId,activityId);
        queryWrapper.eq(EasyActivityUser::getUserId,userId);
        EasyActivityUser one = getOne(queryWrapper);
        if (one != null) {
            //取消报名
            remove(queryWrapper);
            return;
        }
        //否则报名
        //只能在报名后和开始前报名
        EasyActivity activity = activityService.getById(activityId);
        LocalDateTime applyStart = activity.getApplyStart();
        LocalDateTime applyEnd = activity.getApplyEnd();
        LocalDateTime now = LocalDateTime.now();
        if (now.isBefore(applyStart)){
            throw new ServiceException(GlobalCodeEnum.FAIL.getCode(),"报名还没开始~");
        }
        if (now.isAfter(applyEnd)){
            throw new ServiceException(GlobalCodeEnum.FAIL.getCode(),"报名已经结束喽~");
        }
        //查看报名已满员
        //查询已经报名的人数
        LambdaQueryWrapper<EasyActivityUser> activityUserQuery = new LambdaQueryWrapper<>();
        activityUserQuery.eq(EasyActivityUser::getActivityId, activityId);
        long count = count(activityUserQuery);
        if (count == activity.getPeopleNum()){
            throw new ServiceException(GlobalCodeEnum.FAIL.getCode(),"报名人数已满~");
        }
        EasyActivityUser activityUser = new EasyActivityUser();
        activityUser.setActivityId(activityId);
        activityUser.setUserId(userId);
        activityUser.setStatus(ActivityUserStatus.NOTATTEND);
        activityUser.setUuid(UUID.randomUUID().toString());
        save(activityUser);
    }

    /**
     * 查询用户是否参与活动
     *
     * @param activityId 活动id
     * @param userId     用户id
     * @return 是否报名
     */
    @Override
    public Boolean checkApply(Integer activityId, Integer userId) {
        LambdaQueryWrapper<EasyActivityUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyActivityUser::getActivityId,activityId);
        queryWrapper.eq(EasyActivityUser::getUserId,userId);
        EasyActivityUser one = getOne(queryWrapper);
        return one != null;
    }

    /**
     * 获取用户报名的相关信息
     *
     * @param activityId
     * @param userId
     * @return
     */
    @Override
    public ActivityUserInfoVO getActivityUserInfo(Integer activityId, Integer userId) {
        LambdaQueryWrapper<EasyActivityUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyActivityUser::getActivityId,activityId);
        queryWrapper.eq(EasyActivityUser::getUserId,userId);
        EasyActivityUser activityUser = getOne(queryWrapper);
        //获取活动信息
        EasyActivity activity = activityService.getById(activityId);
        ActivityUserInfoVO activityUserInfoVO = new ActivityUserInfoVO();
        BeanUtils.copyProperties(activity, activityUserInfoVO);
        activityUserInfoVO.setUuid(activityUser.getUuid());
        activityUserInfoVO.setStatus(activityUser.getStatus());
        return activityUserInfoVO;
    }

    /**
     * 核销活动报名二维码
     *
     * @param activityId
     * @param uuid
     * @return
     */
    @Override
    public CommonResult<Boolean> checkQr(Integer activityId, String uuid) {
        LambdaQueryWrapper<EasyActivityUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyActivityUser::getUuid,uuid);
        EasyActivityUser activityUser = getOne(queryWrapper);
        if (activityUser == null) {
            return CommonResult.error("活动码无效",false);
        }
        if (!activityUser.getActivityId().equals(activityId)) {
            return CommonResult.error("无权操作",false);
        }
        if(activityUser.getStatus() == ActivityUserStatus.ATTEND){
            return CommonResult.error("重复核验",false);
        }
        activityUser.setStatus(ActivityUserStatus.ATTEND);
        saveOrUpdate(activityUser);
        return CommonResult.success(true);
    }

}




