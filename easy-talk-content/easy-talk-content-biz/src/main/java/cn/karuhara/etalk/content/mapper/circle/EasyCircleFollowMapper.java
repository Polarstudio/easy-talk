package cn.karuhara.etalk.content.mapper.circle;

import cn.karuhara.etalk.content.entity.circle.EasyCircleFollow;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author atom
* @description 针对表【easy_circle_follow(圈子用户)】的数据库操作Mapper
* @createDate 2024-11-17 20:25:04
* @Entity cn.karuhara.etalk.content.entity.circle.EasyCircleFollow
*/
public interface EasyCircleFollowMapper extends BaseMapper<EasyCircleFollow> {

}




