package cn.karuhara.etalk.content.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 动态状态枚举
 */
@Getter
@AllArgsConstructor
public enum DynamicStatusEnum {
    DRAFT("0", "草稿"),
    PENDING_REVIEW("1", "待审核"),
    DISPLAY("2", "展示"),
    NOT_DISPLAY("3", "不展示"),
    REJECTED("4", "驳回"),
    USER_DELETED("5", "用户删除");

    @EnumValue
    @JsonValue
    private final String code;

    private final String desc;
}
