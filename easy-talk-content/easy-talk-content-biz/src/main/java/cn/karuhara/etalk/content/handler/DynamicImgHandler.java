package cn.karuhara.etalk.content.handler;

import cn.karuhara.etalk.content.controller.app.dynamic.vo.DynamicAppAttachmentVO;
import cn.karuhara.etalk.content.entity.dynamic.EasyDynamicImg;
import cn.karuhara.etalk.content.enums.DynamicTypeEnum;
import cn.karuhara.etalk.content.service.dynamic.EasyDynamicImgService;
import cn.karuhara.etalk.framework.common.context.LoginContextHolder;
import cn.karuhara.etalk.framework.common.enums.GlobalCodeEnum;
import cn.karuhara.etalk.framework.common.exception.ServiceException;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Description: 动态图文策略类
 * @author: 轻语
*/
@Component
public class DynamicImgHandler implements DynamicTypeHandler{

    @Resource
    private EasyDynamicImgService imgService;

    @Override
    public DynamicTypeEnum getDynamicType() {
        return DynamicTypeEnum.PICTURE_TEXT;
    }

    @Override
    public void sava(DynamicAppAttachmentVO attachmentVO) {
        Integer userId = LoginContextHolder.get();
        if (userId == null) {
            throw new ServiceException(GlobalCodeEnum.LOGIN_AUTH);
        }
        List<EasyDynamicImg> dynamicImgs = new ArrayList<>();
        List<String> images = attachmentVO.getImages();
        images.forEach(image -> {
            EasyDynamicImg easyDynamicImg = new EasyDynamicImg();
            easyDynamicImg.setImgUrl(image);
            easyDynamicImg.setUserId(userId);
            easyDynamicImg.setDynamicId(attachmentVO.getDynamicId());
            dynamicImgs.add(easyDynamicImg);
        });
        imgService.saveBatch(dynamicImgs);
    }

    @Override
    public DynamicAppAttachmentVO get(Integer dynamicId) {
        LambdaQueryWrapper<EasyDynamicImg> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyDynamicImg::getDynamicId, dynamicId);
        List<EasyDynamicImg> list = imgService.list(queryWrapper);
        List<String> imgs = list.stream().map(EasyDynamicImg::getImgUrl).collect(Collectors.toList());
        DynamicAppAttachmentVO dynamicAppAttachmentVO = new DynamicAppAttachmentVO();
        dynamicAppAttachmentVO.setImages(imgs);
        return dynamicAppAttachmentVO;
    }

    @Override
    public void update(DynamicAppAttachmentVO attachmentVO) {
        //先把之前的给删除了
        delete(attachmentVO.getDynamicId());
        sava(attachmentVO);
    }

    @Override
    public void delete(Integer dynamicId) {
        LambdaQueryWrapper<EasyDynamicImg> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyDynamicImg::getDynamicId, dynamicId);
        imgService.remove(queryWrapper);
    }
}
