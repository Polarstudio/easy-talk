package cn.karuhara.etalk.content.service.dynamic;

import cn.karuhara.etalk.content.controller.admin.dynamic.vo.DynamicDeleteVO;
import cn.karuhara.etalk.content.controller.admin.dynamic.vo.DynamicUpdateVO;
import cn.karuhara.etalk.content.controller.app.dynamic.vo.DynamicAddVO;
import cn.karuhara.etalk.content.controller.app.dynamic.vo.DynamicDetailVO;
import cn.karuhara.etalk.content.controller.app.dynamic.vo.DynamicAppLikePageReqVO;
import cn.karuhara.etalk.content.controller.app.dynamic.vo.DynamicPageReqVO;
import cn.karuhara.etalk.content.entity.dynamic.EasyDynamic;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author atom
* @description 针对表【easy_dynamic(动态)】的数据库操作Service
* @createDate 2024-11-26 16:51:19
*/
public interface EasyDynamicService extends IService<EasyDynamic> {

    /**
     * 管理后台 - 分页获取动态
     * @param pageReqVO 分页条件
     * @return 动态详情列表
     */
    PageResult<DynamicDetailVO> getAdminList(DynamicPageReqVO pageReqVO);

    /**
     * APP - 分页获取动态
     * @param pageReqVO 分页条件
     * @return 动态详情列表
     */
    PageResult<DynamicDetailVO> getList(DynamicPageReqVO pageReqVO);

    /**
     * 获取动态的详情
     * @param id 动态的id
     * @return 动态的详细信息
     */
    DynamicDetailVO getDetail(Integer id);

    /**
     * 发布或修改动态
     * @param appAddVO 动态信息
     */
    void pushDynamic(DynamicAddVO appAddVO);

    /**
     * 获取用户的点赞总数
     * @param userId 用户ID
     * @return 获赞总数
     */
    Long getUserLinkedCount(Integer userId);

    /**
     * 取消关联话题、圈子、活动
     * @param circleId 圈子id
     * @param topicId 话题id
     * @param activityId 活动id
     */
    void canalRelevance(Integer circleId,Integer topicId,Integer activityId);

    /**
     * 删除动态
     */
    void delete(DynamicDeleteVO deleteV);

    /**
     * 获取用户喜欢的动态
     * @return 动态分页结果集
     */
    PageResult<DynamicDetailVO> like(DynamicPageReqVO pageReqVO);

    /**
     * 修改动态
     * @param dynamicUpdateVO 修改动态的数据
     */
    void updateDynamic(DynamicUpdateVO dynamicUpdateVO);

    /**
     * 获取关注用户的动态
     * @param pageReqVO 分页参数
     * @return 分页数据
     */
    PageResult<DynamicDetailVO> getFollowerList(DynamicPageReqVO pageReqVO);

    /**
     * 获取用户发布的动态
     * @param pageReqVO 分页参数
     * @return 动态详情
     */
    PageResult<DynamicDetailVO> getUserDynamicList(DynamicPageReqVO pageReqVO);
}
