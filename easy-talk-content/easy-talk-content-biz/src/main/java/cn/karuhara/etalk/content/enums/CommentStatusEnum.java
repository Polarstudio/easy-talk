package cn.karuhara.etalk.content.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CommentStatusEnum {
    REVIEWED("0", "待审核"),
    NORMAL("1", "正常"),
    REJECTED("2", "驳回");

    @EnumValue
    @JsonValue
    private final String code;

    private final String desc;
}
