package cn.karuhara.etalk.content.controller.app.circle.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(description = "App - 推荐圈子的信息")
public class CircleAppRecommendVO {

    @Schema(description = "圈子ID")
    private Integer id;

    @Schema(description = "圈子名称")
    private String circleName;

    @Schema(description = "圈子封面")
    private String coverImg;

    @Schema(description = "圈子的动态数量")
    private Long dynamicCount;
}
