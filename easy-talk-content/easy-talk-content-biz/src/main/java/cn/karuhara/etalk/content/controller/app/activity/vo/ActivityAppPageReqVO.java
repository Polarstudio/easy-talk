package cn.karuhara.etalk.content.controller.app.activity.vo;

import cn.karuhara.etalk.framework.common.pojo.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Schema(description = "App - 分页查询参数")
@Data
public class ActivityAppPageReqVO extends PageParam {

    @Schema(description = "活动名称",requiredMode = Schema.RequiredMode.REQUIRED,example = "排球运动")
    private String name;

    @Schema(description = "用户id,用于获取用户发布的活动")
    private Integer userId;

}
