package cn.karuhara.etalk.content.controller.admin.dynamic;

import cn.karuhara.etalk.content.controller.admin.dynamic.vo.DynamicDeleteVO;
import cn.karuhara.etalk.content.controller.admin.dynamic.vo.DynamicUpdateVO;
import cn.karuhara.etalk.content.controller.app.dynamic.vo.DynamicDetailVO;
import cn.karuhara.etalk.content.controller.app.dynamic.vo.DynamicPageReqVO;
import cn.karuhara.etalk.content.service.dynamic.EasyDynamicService;
import cn.karuhara.etalk.framework.common.pojo.CommonResult;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Tag(name = "管理后台 - 动态模块")
@RestController
@RequestMapping("/content/dynamic/admin")
@Validated
public class DynamicController {
    @Resource
    private EasyDynamicService dynamicService;

    @Operation(summary = "分页获取数据")
    @PostMapping("/page")
    public CommonResult<PageResult<DynamicDetailVO>> getList(@RequestBody DynamicPageReqVO pageReqVO){
        PageResult<DynamicDetailVO> pageResult = dynamicService.getAdminList(pageReqVO);
        return CommonResult.success(pageResult);
    }

    @Operation(summary = "修改动态")
    @PutMapping
    public CommonResult<Void> update(@RequestBody DynamicUpdateVO dynamicUpdateVO){
        dynamicService.updateDynamic(dynamicUpdateVO);
        return CommonResult.success();
    }

    @Operation(summary = "删除动态")
    @DeleteMapping
    public CommonResult<Void> delete(@RequestBody DynamicDeleteVO deleteVO){
        dynamicService.delete(deleteVO);
        return CommonResult.success();
    }
}
