package cn.karuhara.etalk.content.entity.dynamic;

import cn.karuhara.etalk.content.enums.CommentStatusEnum;
import cn.karuhara.etalk.framework.mybatis.core.dataobject.BaseDO;
import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 动态评论
 * @TableName easy_dynamic_comment
 */
@TableName(value ="easy_dynamic_comment")
@Data
public class EasyDynamicComment implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 动态ID
     */
    private Integer dynamicId;

    /**
     * 回复的评论ID
     */
    private Integer replyCommentId;

    /**
     * 回复内容
     */
    private String content;

    /**
     * 状态：0 待审核、1 正常、2 驳回
     */
    private CommentStatusEnum status;

    /**
     * 评论时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime date;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}