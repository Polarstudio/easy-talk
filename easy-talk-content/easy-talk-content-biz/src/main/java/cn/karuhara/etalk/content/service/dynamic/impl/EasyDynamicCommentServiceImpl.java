package cn.karuhara.etalk.content.service.dynamic.impl;

import cn.karuhara.etalk.content.controller.admin.dynamic.vo.CommentAdminDetailVO;
import cn.karuhara.etalk.content.controller.admin.dynamic.vo.CommentDeleteVO;
import cn.karuhara.etalk.content.controller.admin.dynamic.vo.CommentUpdateVO;
import cn.karuhara.etalk.content.controller.admin.dynamic.vo.CommonAdminPageReqVO;
import cn.karuhara.etalk.content.controller.app.dynamic.vo.CommentDetailVO;
import cn.karuhara.etalk.content.controller.app.dynamic.vo.CommentPageReqVO;
import cn.karuhara.etalk.content.controller.app.dynamic.vo.CommentPushVO;
import cn.karuhara.etalk.content.convert.CommentConvert;
import cn.karuhara.etalk.content.detection.tencent.DetectionEntity;
import cn.karuhara.etalk.content.detection.tencent.DetectionHandler;
import cn.karuhara.etalk.content.entity.dynamic.EasyDynamicCommentLike;
import cn.karuhara.etalk.content.enums.CommentEnum;
import cn.karuhara.etalk.content.enums.CommentStatusEnum;
import cn.karuhara.etalk.content.enums.DetectionTypeEnum;
import cn.karuhara.etalk.content.mq.product.CommentProducer;
import cn.karuhara.etalk.content.service.dynamic.EasyDynamicCommentLikeService;
import cn.karuhara.etalk.framework.common.context.LoginContextHolder;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import cn.karuhara.etalk.member.api.MemberApi;
import cn.karuhara.etalk.member.api.dto.MemberBaseInfoDTO;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.karuhara.etalk.content.entity.dynamic.EasyDynamicComment;
import cn.karuhara.etalk.content.service.dynamic.EasyDynamicCommentService;
import cn.karuhara.etalk.content.mapper.dynamic.EasyDynamicCommentMapper;
import jakarta.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

/**
* @author atom
* @description 针对表【easy_dynamic_comment(动态评论)】的数据库操作Service实现
* @createDate 2024-11-26 21:21:49
*/
@Service
public class EasyDynamicCommentServiceImpl extends ServiceImpl<EasyDynamicCommentMapper, EasyDynamicComment>
    implements EasyDynamicCommentService{
    @Resource
    private EasyDynamicCommentMapper commentMapper;

    @Resource
    private MemberApi memberApi;

    @Resource
    private EasyDynamicCommentLikeService commentLikeService;

    @Resource
    private CommentProducer commentProducer;

    @Resource
    private DetectionHandler detectionHandler;

    @Override
    public Long getCommentCount(Integer dynamicId) {
        LambdaQueryWrapper<EasyDynamicComment> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyDynamicComment::getDynamicId, dynamicId);
        return count(queryWrapper);
    }

    /**
     * 获取评论的列表
     *
     * @param pageReqVO 分页参数
     * @return 分页数据
     */
    @Override
    public PageResult<CommentDetailVO> getList(CommentPageReqVO pageReqVO) {
        LambdaQueryWrapper<EasyDynamicComment> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyDynamicComment::getDynamicId, pageReqVO.getDynamicId());
        queryWrapper.eq(EasyDynamicComment::getReplyCommentId,pageReqVO.getReplyCommentID());
        queryWrapper.orderByDesc(EasyDynamicComment::getStatus);
        //只获取通过审核的动态
        queryWrapper.eq(EasyDynamicComment::getStatus,CommentStatusEnum.NORMAL);
        Page<EasyDynamicComment> page=  new Page<>(pageReqVO.getPageNo(), pageReqVO.getPageSize());
        Page<EasyDynamicComment> commentPage = commentMapper.selectPage(page, queryWrapper);
        if(commentPage.getTotal() == 0){
            return PageResult.empty();
        }
        List<EasyDynamicComment> comments = commentPage.getRecords();
        List<CommentDetailVO> commentDetailVOS = CommentConvert.INSTANT.commentsToDetailVO(comments);
        commentDetailVOS.forEach(this::fillData);
        List<CommentDetailVO> sortedCommentDetailVOS = commentDetailVOS.stream()
                .sorted(Comparator.comparing(CommentDetailVO::getLikeCount).reversed())
                .collect(Collectors.toList());
        return new PageResult<>(sortedCommentDetailVOS, commentPage.getPages());
    }

    /**
     * 发表评论
     *
     * @param commentPushVO 评论参数
     */
    @Override
    public void push(CommentPushVO commentPushVO) {
        EasyDynamicComment easyDynamicComment = CommentConvert.INSTANT.pushVOToComment(commentPushVO);
        if (commentPushVO.getReplyCommentId() == null){
            easyDynamicComment.setReplyCommentId(0);
        }
        easyDynamicComment.setDate(LocalDateTime.now());
        easyDynamicComment.setStatus(CommentStatusEnum.REVIEWED);
        save(easyDynamicComment);
        //异步消息进行评论审核
        CompletableFuture.runAsync(()->{
            DetectionEntity detectionEntity = new DetectionEntity();
            //设置用户的openId
            MemberBaseInfoDTO memberBaseInfo = memberApi.getMemberBaseInfo(commentPushVO.getUserId());
            detectionEntity.setWxOpenid(memberBaseInfo.getWxOpenid());
            //设置评论id
            detectionEntity.setPostId(easyDynamicComment.getId());
            //设置评论内容
            List<String> texts = new ArrayList<>();
            texts.add(easyDynamicComment.getContent());
            detectionEntity.setTexts(texts);
            detectionEntity.setType(DetectionTypeEnum.COMMENT);
            detectionHandler.detection(detectionEntity);
        });
    }

    /**
     * 删除评论
     */
    @Override
    public void delete(CommentDeleteVO deleteVO) {
        //发送通知给用户
        commentProducer.deleteMessage(deleteVO);
        //最后在删除评论
        removeById(deleteVO.getId());
        //获取到所有的子评论id
        LambdaQueryWrapper<EasyDynamicComment> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyDynamicComment::getReplyCommentId, deleteVO.getId());
        List<EasyDynamicComment> list = list(queryWrapper);
        //用于删除所有的点赞记录
        List<Integer> commentIds = list.stream().map(EasyDynamicComment::getId).collect(Collectors.toList());
        remove(queryWrapper);
        //删除所有的点赞记录,包括子评论的点赞记录
        queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyDynamicComment::getDynamicId,deleteVO.getId());
        commentMapper.delete(queryWrapper);
        if (commentIds.isEmpty()){
            return;
        }
        LambdaQueryWrapper<EasyDynamicCommentLike> likeQueryWrapper = new LambdaQueryWrapper<>();
        likeQueryWrapper.in(EasyDynamicCommentLike::getCommentId,commentIds);
        commentLikeService.remove(likeQueryWrapper);
    }

    /**
     * 获取评论的列表
     *
     * @param pageReqVO 分页参数
     * @return
     */
    @Override
    public PageResult<CommentAdminDetailVO> getCommentList(CommonAdminPageReqVO pageReqVO) {
        LambdaQueryWrapper<EasyDynamicComment> queryWrapper = new LambdaQueryWrapper<>();
        if (pageReqVO.getDynamicId() != null){
            queryWrapper.eq(EasyDynamicComment::getDynamicId, pageReqVO.getDynamicId());
        }
        if (pageReqVO.getUserId() != null){
            queryWrapper.eq(EasyDynamicComment::getUserId, pageReqVO.getUserId());
        }
        if (StringUtils.isNotBlank(pageReqVO.getStatus())){
            queryWrapper.eq(EasyDynamicComment::getStatus, pageReqVO.getStatus());
        }
        if (StringUtils.isNotBlank(pageReqVO.getContent())){
            queryWrapper.like(EasyDynamicComment::getContent, pageReqVO.getContent());
        }
        Page<EasyDynamicComment> page=  new Page<>(pageReqVO.getPageNo(), pageReqVO.getPageSize());
        Page<EasyDynamicComment> commentPage = commentMapper.selectPage(page, queryWrapper);
        if(commentPage.getTotal() == 0){
            return PageResult.empty();
        }
        List<EasyDynamicComment> comments = commentPage.getRecords();
        List<CommentAdminDetailVO> commentAdminDetailVOS = CommentConvert.INSTANT.commentsToAdminDetailVO(comments);
        commentAdminDetailVOS.forEach(this::fillCommentUserData);
        return new PageResult<>(commentAdminDetailVOS, commentPage.getPages());
    }

    /**
     * 修改评论
     *
     * @param updateVO
     */
    @Override
    public void updateComment(CommentUpdateVO updateVO) {
        EasyDynamicComment easyDynamicComment = CommentConvert.INSTANT.updateVOToComment(updateVO);
        updateById(easyDynamicComment);
    }

    public void fillCommentUserData(CommentAdminDetailVO commentDetailVO){
        MemberBaseInfoDTO memberBaseInfo = memberApi.getMemberBaseInfo(commentDetailVO.getUserId());
        commentDetailVO.setAvatar(memberBaseInfo.getAvatar());
        commentDetailVO.setNickname(memberBaseInfo.getNikeName());
        LambdaQueryWrapper<EasyDynamicCommentLike> commentLikeWrapper1 = new LambdaQueryWrapper<>();
        commentLikeWrapper1.eq(EasyDynamicCommentLike::getCommentId,commentDetailVO.getId());
        commentLikeWrapper1.eq(EasyDynamicCommentLike::getType,CommentEnum.LIKE.getCode());
        long count = commentLikeService.count(commentLikeWrapper1);
        commentDetailVO.setLikeCount(count);
    }

    //填充评论详情的内容
    public void fillData(CommentDetailVO commentDetailVO) {
        //获取评论者的信息
        MemberBaseInfoDTO memberBaseInfo = memberApi.getMemberBaseInfo(commentDetailVO.getUserId());
        commentDetailVO.setAvatar(memberBaseInfo.getAvatar());
        commentDetailVO.setNickname(memberBaseInfo.getNikeName());
        commentDetailVO.setPosition(memberBaseInfo.getDependency());
        //查询用户是否点赞该评论
        LambdaQueryWrapper<EasyDynamicCommentLike> commentLikeWrapper = new LambdaQueryWrapper<>();
        commentLikeWrapper.eq(EasyDynamicCommentLike::getCommentId,commentDetailVO.getId());
        commentLikeWrapper.eq(EasyDynamicCommentLike::getUserId,commentDetailVO.getUserId());
        EasyDynamicCommentLike commentLike = commentLikeService.getOne(commentLikeWrapper);
        if (commentLike == null){
            commentDetailVO.setLikeActive(false);
            commentDetailVO.setDislikeActive(false);
        }else if (commentLike.getType() == CommentEnum.LIKE){
            commentDetailVO.setLikeActive(true);
        }else {
            commentDetailVO.setDislikeActive(true);
        }
        //点赞的数量
        LambdaQueryWrapper<EasyDynamicCommentLike> commentLikeWrapper1 = new LambdaQueryWrapper<>();
        commentLikeWrapper1.eq(EasyDynamicCommentLike::getCommentId,commentDetailVO.getId());
        commentLikeWrapper1.eq(EasyDynamicCommentLike::getType,CommentEnum.LIKE.getCode());
        long count = commentLikeService.count(commentLikeWrapper1);
        commentDetailVO.setLikeCount(count);
        //是否允许删除
        Integer userId = LoginContextHolder.get();
        commentDetailVO.setAllowDelete(userId != null && commentDetailVO.getUserId() == userId);
        commentDetailVO.setDisabledReply(false);
    }
}




