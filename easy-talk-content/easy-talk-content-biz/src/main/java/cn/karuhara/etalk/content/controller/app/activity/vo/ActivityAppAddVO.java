package cn.karuhara.etalk.content.controller.app.activity.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Schema(description = "APP - 发布活动VO")
@Data
public class ActivityAppAddVO extends ActivityAppBaseVO{

    @Schema(description = "活动简介",requiredMode = Schema.RequiredMode.REQUIRED,example = "一起来打排球吧！")
    private String intro;

    @Schema(description = "介绍图 例如 活动的海报",requiredMode = Schema.RequiredMode.REQUIRED)
    private List<String> imgs;

    @Schema(description = "地点纬度  例如 39.90420000",requiredMode = Schema.RequiredMode.REQUIRED,example = "39.90420000")
    private Double latitude;

    @Schema(description = "地点经度  例如 116.40740000",requiredMode = Schema.RequiredMode.REQUIRED,example = "116.40740000")
    private Double longitude;

    @Schema(description = "地点位置",requiredMode = Schema.RequiredMode.REQUIRED)
    private String address;

}
