package cn.karuhara.etalk.content.service.dynamic;

import cn.karuhara.etalk.content.entity.dynamic.EasyDynamicImg;
import cn.karuhara.etalk.content.mapper.dynamic.EasyDynamicImgMapper;
import com.baomidou.mybatisplus.extension.service.IService;
import jakarta.annotation.Resource;

/**
* @author atom
* @description 针对表【easy_dynamic_img(动态图片)】的数据库操作Service
* @createDate 2024-11-26 21:21:49
*/
public interface EasyDynamicImgService extends IService<EasyDynamicImg> {


}
