package cn.karuhara.etalk.content.mapper.topic;

import cn.karuhara.etalk.content.entity.topic.EasyTopicFollow;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author atom
* @description 针对表【easy_topic_follow(话题关注表)】的数据库操作Mapper
* @createDate 2024-11-18 16:38:17
* @Entity cn.karuhara.etalk.content.entity.EasyTopicFollow
*/
public interface EasyTopicFollowMapper extends BaseMapper<EasyTopicFollow> {

}




