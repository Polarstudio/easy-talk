package cn.karuhara.etalk.content.mq.consumer;

import cn.karuhara.etalk.content.detection.tencent.DetectionResult;
import cn.karuhara.etalk.content.enums.MsgSecCheckResultEnum;
import cn.karuhara.etalk.content.mq.product.CommentProducer;
import cn.karuhara.etalk.content.mq.product.DynamicProducer;
import cn.karuhara.etalk.framework.common.constants.RedisConstant;
import cn.karuhara.etalk.framework.kafka.core.Event;
import cn.karuhara.etalk.framework.kafka.core.MessageConstants;
import cn.karuhara.etalk.fremawork.redis.core.MyRedisUtil;
import com.alibaba.fastjson2.JSONObject;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

@Component
@Slf4j
public class MediaCheckConsumer {

    @Resource
    MyRedisUtil myRedisUtil;

    @Resource
    DynamicProducer dynamicProducer;

    @Resource
    CommentProducer commentProducer;

    @KafkaListener(topics = MessageConstants.DETECTION_MEDIA_TOPIC)
    public void mediaCheck(ConsumerRecord record, Acknowledgment ack) {
        Event event = JSONObject.parseObject(record.value().toString(), Event.class);
        log.info("收到一条消息：{}", event);
        ack.acknowledge();
        String tradeId = (String) event.getData().get(MessageConstants.TRACE_ID);
        String suggest = (String) event.getData().get(MessageConstants.DETECTION_SUGGEST);
        //通过tradeId从Redis中找到相对应的Post_Id
        String buildKey = MyRedisUtil.buildKey(RedisConstant.DETECTION_MEDIA, RedisConstant.TRACE_ID+":"+tradeId);
        String detectionInfo = (String) myRedisUtil.get(buildKey);
        String[] tradeIdRedis = detectionInfo.split("\\.");
        String postTradeIds = MyRedisUtil.buildKey(RedisConstant.DETECTION_MEDIA, RedisConstant.POST_ID+":"+tradeIdRedis[1]);
        //修改postTrade中的tradeId的状态
        myRedisUtil.hSet(postTradeIds,tradeId,suggest);
        //检查所有postTrade中的所有tradeId的状态
        Map<Object, Object> postTradeIdMap = myRedisUtil.hGetAll(postTradeIds);
        Collection<Object> values = postTradeIdMap.values();
        AtomicBoolean detectionFinish = new AtomicBoolean(true);
        values.forEach((v)->{
            if (v.equals("0")){
                detectionFinish.set(false);
            }
        });
        //如果为false，代表还有其它的内容没有审核完成
        if (!detectionFinish.get()){
            return;
        }
        DetectionResult detectionResult = new DetectionResult();
        detectionResult.setResult(MsgSecCheckResultEnum.PASS);
        detectionResult.setPostId(Integer.parseInt(tradeIdRedis[1]));
        //开始检测结果
        values.forEach((v)->{
            if(v.equals(MsgSecCheckResultEnum.REVIEW.getCode())){
                detectionResult.setResult(MsgSecCheckResultEnum.REVIEW);
            }else if (v.equals(MsgSecCheckResultEnum.RISKY.getCode())){
                detectionResult.setResult(MsgSecCheckResultEnum.RISKY);
            }
        });
        //如果需要复审直接不用进行下一步
        if(detectionResult.getResult() == MsgSecCheckResultEnum.REVIEW) {
            return;
        }
        //必须使用异步的方式，否则会造成重复消费的问题
        Thread thread = new Thread(()->{
            if (Objects.equals(tradeIdRedis[0], "0")){
                log.info("type:{}",tradeIdRedis[0]);
                dynamicProducer.detectionMessage(detectionResult);
            }
        });
        thread.start();
    }
}
