package cn.karuhara.etalk.content.controller.app.activity.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "报名活动的用户信息")
@Data
public class ActivityAppApplyDetailVO {
    @Schema(description = "用户的id")
    private Integer id;

    @Schema(description = "用户昵称")
    private String nikeName;

    @Schema(description = "用户的头像")
    private String avatar;

    @Schema(description = "用户的手机号")
    private String phone;
}
