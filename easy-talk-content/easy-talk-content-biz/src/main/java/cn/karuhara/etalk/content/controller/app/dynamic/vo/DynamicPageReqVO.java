package cn.karuhara.etalk.content.controller.app.dynamic.vo;

import cn.karuhara.etalk.framework.common.pojo.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Schema(description = "分页获取动态VO")
@Data
public class DynamicPageReqVO extends PageParam {
    @Schema(description = "搜索内容")
    private String content;

    @Schema(description = "用户id,用于获取用户发布的动态")
    private Integer userId;

    @Schema(description = "动态类型")
    private String status;

    @Schema(description = "关联圈子ID")
    private Integer circleId;

    @Schema(description = "关联话题ID")
    private Integer topicId;

    @Schema(description = "关联活动ID")
    private Integer activityId;
}
