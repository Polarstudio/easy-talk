package cn.karuhara.etalk.content.convert;

import cn.karuhara.etalk.content.controller.admin.circle.vo.CircleAddReqVO;
import cn.karuhara.etalk.content.controller.admin.circle.vo.CirclePageRespVO;
import cn.karuhara.etalk.content.controller.admin.circle.vo.CircleUpdateReqVO;
import cn.karuhara.etalk.content.controller.app.circle.vo.CircleAppBriefVO;
import cn.karuhara.etalk.content.controller.app.circle.vo.CircleAppDetailVO;
import cn.karuhara.etalk.content.controller.app.circle.vo.CircleAppPushVO;
import cn.karuhara.etalk.content.controller.app.circle.vo.CircleAppRecommendVO;
import cn.karuhara.etalk.content.entity.circle.EasyCircle;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface CircleConvert {
    CircleConvert INSTANCE = Mappers.getMapper(CircleConvert.class);

    EasyCircle addVOToCircle(CircleAddReqVO circleAddVO);

    List<CirclePageRespVO> circleListToPageRespVOList(List<EasyCircle> circleList);

    EasyCircle updateVOToCircle(CircleUpdateReqVO circleUpdateReqVO);

    List<CircleAppDetailVO> circleListToAppCircleDetailVOs(List<EasyCircle> circle);

    List<CircleAppPushVO> circleListToAppCirclePushVOList(List<EasyCircle> circle);

    List<CircleAppRecommendVO> circleListToAppCircleRecommend(List<EasyCircle> circles);

    CircleAppDetailVO circleToDetailVO(EasyCircle circle);

    List<CircleAppBriefVO> circleListToAppBriefVOList(List<EasyCircle> circle);
}
