package cn.karuhara.etalk.content.controller.app.activity;

import cn.karuhara.etalk.content.controller.app.activity.vo.ActivityAppApplyDetailVO;
import cn.karuhara.etalk.content.controller.app.activity.vo.ActivityAppApplyPageReqVO;
import cn.karuhara.etalk.content.controller.app.activity.vo.ActivityUserInfoVO;
import cn.karuhara.etalk.content.service.activity.EasyActivityUserService;
import cn.karuhara.etalk.framework.common.pojo.CommonResult;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/content/activity/app/apply")
@Tag(name = "APP - 报名活动")
public class ActivityAppApplyController {

    @Resource
    private EasyActivityUserService activityUserService;

    @Operation(summary = "分页查询活动的报名用户")
    @PostMapping("/list")
    public CommonResult<PageResult<ActivityAppApplyDetailVO>> list(@RequestBody ActivityAppApplyPageReqVO pageReqVO){
        PageResult<ActivityAppApplyDetailVO> pageResult = activityUserService.getList(pageReqVO);
        return CommonResult.success(pageResult);
    }

    @Operation(summary = "移除报名的用户")
    @DeleteMapping("/{activityId}/{userId}")
    public CommonResult<Void> delete(@PathVariable("activityId") Integer activityId, @PathVariable("userId") Integer userId){
        activityUserService.delete(activityId,userId);
        return CommonResult.success();
    }

    @Operation(summary = "报名或取消活动")
    @PostMapping("/{activityId}/{userId}")
    public CommonResult<Void> applyActivity(@PathVariable("activityId") Integer activityId, @PathVariable("userId") Integer userId){
        activityUserService.applyActivity(activityId,userId);
        return CommonResult.success();
    }

    @Operation(summary = "查询用户是否参与活动")
    @GetMapping("/check/{activityId}/{userId}")
    public CommonResult<Boolean> check(@PathVariable("activityId") Integer activityId, @PathVariable("userId") Integer userId){
        return CommonResult.success(activityUserService.checkApply(activityId,userId));
    }

    @Operation(summary = "获取用户报名的相关信息")
    @GetMapping("/{activityId}/{userId}")
    public CommonResult<ActivityUserInfoVO> getActivityUserInfo(@PathVariable("activityId") Integer activityId, @PathVariable("userId") Integer userId){
        return CommonResult.success(activityUserService.getActivityUserInfo(activityId,userId));
    }

    @Operation(summary = "核销活动报名二维码")
    @PutMapping("/qr/{activityId}/{uuid}")
    public CommonResult<Boolean> checkUserQr(@PathVariable("activityId") Integer activityId, @PathVariable("uuid") String uuid){
        return activityUserService.checkQr(activityId,uuid);
    }

}
