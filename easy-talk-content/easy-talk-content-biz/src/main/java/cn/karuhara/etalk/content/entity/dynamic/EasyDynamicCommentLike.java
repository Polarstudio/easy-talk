package cn.karuhara.etalk.content.entity.dynamic;

import cn.karuhara.etalk.content.enums.CommentEnum;
import cn.karuhara.etalk.content.enums.CommentLikeTypeEnum;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName easy_dynamic_comment_like
 */
@TableName(value ="easy_dynamic_comment_like")
@Data
public class EasyDynamicCommentLike implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 动态id
     */
    private Integer dynamicId;
    /**
     * 评论id
     */
    private Integer commentId;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 类型（0点赞 1 点踩）
     */
    private CommentEnum type;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}