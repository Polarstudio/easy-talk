package cn.karuhara.etalk.content.handler;

import cn.karuhara.etalk.content.enums.DynamicTypeEnum;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description: 动态类型工厂类
 * @author: 轻语
*/
@Component
public class DynamicHandlerFactory implements InitializingBean {

    @Resource
    private List<DynamicTypeHandler> typeHandlerList;

    /**
     * 用户存储不同类型的处理器
     */
    private final Map<DynamicTypeEnum,DynamicTypeHandler> dynamicTypeHandlerMap = new HashMap<>();

    /**
     * 根据枚举获取对应的处理器
     * @param dynamicTypeEnum 类型枚举
     * @return 处理器
     */
    public DynamicTypeHandler getDynamicTypeHandler(DynamicTypeEnum dynamicTypeEnum) {
        return dynamicTypeHandlerMap.get(dynamicTypeEnum);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        for (DynamicTypeHandler dynamicTypeHandler : typeHandlerList) {
            dynamicTypeHandlerMap.put(dynamicTypeHandler.getDynamicType(), dynamicTypeHandler);
        }
    }
}
