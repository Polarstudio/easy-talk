package cn.karuhara.etalk.content.controller.app.dynamic.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Schema(description = "动态评论的详情")
@Data
public class CommentDetailVO extends CommentBaseVO{

    @Schema(description = "回复的评论ID")
    private Integer replyCommentId;

    @Schema(description = "用户头像")
    private String avatar;

    @Schema(description = "用户昵称")
    private String nickname;

    @Schema(description = "评论时用户的地点")
    private String position;

    @Schema(description = "是否点赞该评论")
    private Boolean likeActive;

    @Schema(description = "点赞的数量")
    private Long likeCount;

    @Schema(description = "是否点踩该评论")
    private Boolean dislikeActive;

    @Schema(description = "是否允许删除该评论")
    private Boolean allowDelete;

    @Schema(description = "禁止评论")
    private Boolean disabledReply;
}
