package cn.karuhara.etalk.content.controller.admin.activity.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "修改活动")
public class ActivityUpdateReqVO extends ActivityBaseVO{

}
