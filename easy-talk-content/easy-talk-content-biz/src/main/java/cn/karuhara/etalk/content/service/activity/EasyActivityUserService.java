package cn.karuhara.etalk.content.service.activity;

import cn.karuhara.etalk.content.controller.app.activity.vo.ActivityAppApplyDetailVO;
import cn.karuhara.etalk.content.controller.app.activity.vo.ActivityAppApplyPageReqVO;
import cn.karuhara.etalk.content.controller.app.activity.vo.ActivityAppDetailVO;
import cn.karuhara.etalk.content.controller.app.activity.vo.ActivityUserInfoVO;
import cn.karuhara.etalk.content.entity.activity.EasyActivityUser;
import cn.karuhara.etalk.framework.common.pojo.CommonResult;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author atom
 * @description 针对表【easy_activity_user(用户参与活动)】的数据库操作Service
 * @createDate 2024-11-19 10:49:07
 */
public interface EasyActivityUserService extends IService<EasyActivityUser> {

    /**
     * 分页查询活动的报名用户
     * @param pageReqVO 分页参数
     * @return 分页数据
     */
    PageResult<ActivityAppApplyDetailVO> getList(ActivityAppApplyPageReqVO pageReqVO);

    /**
     * 移除报名的用户
     * @param activityId 活动id
     * @param userId 用户id
     */
    void delete(Integer activityId, Integer userId);

    /**
     * 报名活动
     * @param activityId 活动id
     * @param userId 用户id
     */
    void applyActivity(Integer activityId, Integer userId);

    /**
     * 查询用户是否参与活动
     * @param activityId 活动id
     * @param userId 用户id
     * @return 是否报名
     */
    Boolean checkApply(Integer activityId, Integer userId);

    /**
     * 获取用户报名的相关信息
     * @param activityId
     * @param userId
     * @return
     */
    ActivityUserInfoVO getActivityUserInfo(Integer activityId, Integer userId);

    /**
     * 核销活动报名二维码
     * @param activityId
     * @param uuid
     * @return
     */
    CommonResult<Boolean> checkQr(Integer activityId, String uuid);
}
