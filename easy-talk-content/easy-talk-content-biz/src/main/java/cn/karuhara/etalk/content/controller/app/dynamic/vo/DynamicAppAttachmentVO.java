package cn.karuhara.etalk.content.controller.app.dynamic.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

@Schema(description = "动态附件信息")
@Data
public class DynamicAppAttachmentVO {

    @Schema(description = "动态的id")
    private Integer dynamicId;

    @Schema(description = "图片列表")
    private List<String> images;

    @Schema(description = "音频封面")
    private String audioCover;

    @Schema(description = "音频地址")
    private String audioUrl;

    @Schema(description = "音频名称")
    private String audioName;

    @Schema(description = "音频介绍")
    private String audioIntro;

    @Schema(description = "视频封面")
    private String videoCover;

    @Schema(description = "视频高度")
    private Integer height;

    @Schema(description = "视频宽度")
    private Integer width;

    @Schema(description = "视频地址")
    private String videoUrl;
}
