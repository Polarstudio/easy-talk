package cn.karuhara.etalk.content.mq.product;

import cn.karuhara.etalk.content.controller.admin.dynamic.vo.CommentDeleteVO;
import cn.karuhara.etalk.content.controller.admin.dynamic.vo.DynamicDeleteVO;
import cn.karuhara.etalk.content.detection.tencent.DetectionResult;
import cn.karuhara.etalk.content.entity.dynamic.EasyDynamic;
import cn.karuhara.etalk.content.entity.dynamic.EasyDynamicComment;
import cn.karuhara.etalk.content.enums.CommentStatusEnum;
import cn.karuhara.etalk.content.enums.MsgSecCheckResultEnum;
import cn.karuhara.etalk.content.service.dynamic.EasyDynamicCommentService;
import cn.karuhara.etalk.framework.kafka.core.Event;
import cn.karuhara.etalk.framework.kafka.core.EventProducer;
import cn.karuhara.etalk.framework.kafka.core.MessageConstants;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

@Component
public class CommentProducer {
    @Resource
    private EventProducer eventProducer;

    @Resource
    private EasyDynamicCommentService commentService;

    public void deleteMessage(CommentDeleteVO deleteVO){
        EasyDynamicComment comment = commentService.getById(deleteVO.getId());
        Event event = new Event();
        //设置消息类型
        event.setTopic(MessageConstants.COMMENT_DELETE_TOPIC);
        //设置通知的用户
        if (deleteVO.getUserId()!=null){
            event.setToUserId(deleteVO.getUserId());
        }
        //设置消息的内容
        if (deleteVO.getReason()!=null){
            event.setContent(deleteVO.getReason());
        }
        //这是其它的额外扩展的参数
        event.setDataMap(MessageConstants.POST_ID,deleteVO.getId());
        event.setDataMap(MessageConstants.COMMENT_CONTENT,comment.getContent());
        eventProducer.fireEvent(event);
    }

    public void detectionMessage(DetectionResult detectionResult){
        if (detectionResult.getResult() == MsgSecCheckResultEnum.REVIEW){
            return;
        }
        EasyDynamicComment comment = commentService.getById(detectionResult.getPostId());
        if (detectionResult.getResult() == MsgSecCheckResultEnum.RISKY){
            comment.setStatus(CommentStatusEnum.REJECTED);
        }else {
            comment.setStatus(CommentStatusEnum.NORMAL);
        }
        //更新审核的动态
        commentService.updateById(comment);
        //发送通知
        Event event = new Event();
        event.setTopic(MessageConstants.COMMENT_DETECTION_TOPIC);
        event.setToUserId(comment.getUserId());
        event.setContent(MessageConstants.CONTENT_VIOLATIONS);
        event.setDataMap(MessageConstants.COMMENT_CONTENT,comment.getContent());
        event.setDataMap(MessageConstants.POST_ID,comment.getId());
        event.setDataMap(MessageConstants.DYNAMIC_ID,comment.getDynamicId());
        event.setDataMap(MessageConstants.DETECTION_RESULT,detectionResult.getResult().getCode());
        eventProducer.fireEvent(event);
    }
}
