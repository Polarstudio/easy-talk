package cn.karuhara.etalk.content.service.topic.impl;

import cn.karuhara.etalk.content.service.topic.EasyTopicService;
import cn.karuhara.etalk.framework.common.constants.KafkaStreamConstant;
import cn.karuhara.etalk.framework.common.model.mess.UpdateTopicMess;
import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.karuhara.etalk.content.entity.topic.EasyTopicFollow;
import cn.karuhara.etalk.content.service.topic.EasyTopicFollowService;
import cn.karuhara.etalk.content.mapper.topic.EasyTopicFollowMapper;
import jakarta.annotation.Resource;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

/**
* @author atom
* @description 针对表【easy_topic_follow(话题关注表)】的数据库操作Service实现
* @createDate 2024-11-18 16:38:17
*/
@Service
public class EasyTopicFollowServiceImpl extends ServiceImpl<EasyTopicFollowMapper, EasyTopicFollow>
    implements EasyTopicFollowService{

    @Resource
    private KafkaTemplate<String, String> kafkaTemplate;

    @Resource
    private EasyTopicService topicService;
    /**
     * 判断用户是否关注话题
     *
     * @param topicId 话题id
     * @param userId  用户id
     * @return 是否关注
     */
    @Override
    public Boolean followCheck(Integer topicId, Integer userId) {
        LambdaQueryWrapper<EasyTopicFollow> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyTopicFollow::getTopicId, topicId);
        queryWrapper.eq(EasyTopicFollow::getUserId, userId);
        EasyTopicFollow topicFollow = getOne(queryWrapper);
        return topicFollow!=null;
    }

    /**
     * 关注或取消关注话题
     * @param topicId 话题id
     * @param userId 用户id
     */
    @Override
    public void follow(Integer topicId,Integer userId) {
        LambdaQueryWrapper<EasyTopicFollow> followQueryWrapper = new LambdaQueryWrapper<>();
        followQueryWrapper.eq(EasyTopicFollow::getTopicId,topicId);
        followQueryWrapper.eq(EasyTopicFollow::getUserId,userId);
        EasyTopicFollow topicFollow = getOne(followQueryWrapper);
        //实时流，热点计算
        UpdateTopicMess mess = new UpdateTopicMess();
        mess.setTopicId(topicId);
        mess.setType(UpdateTopicMess.UpdateArticleType.FOLLOW);
        //关注
        if(topicFollow==null){
            topicFollow = new EasyTopicFollow();
            topicFollow.setUserId(userId);
            topicFollow.setTopicId(topicId);
            save(topicFollow);
            mess.setAdd(1);
        }else{
            //取消关注
            remove(followQueryWrapper);
            mess.setAdd(-1);
        }
        //发送消息，数据聚合
        kafkaTemplate.send(KafkaStreamConstant.HOT_TOPIC_SCORE_TOPIC, JSON.toJSONString(mess));
    }
}




