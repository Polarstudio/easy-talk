package cn.karuhara.etalk.content.controller.app.dynamic.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "最简单的动态信息 - 提供给圈子使用")
@Data
public class DynamicAppBriefVO {
    @Schema(description = "动态id")
    private Integer id;

    @Schema(description = "动态内容")
    private String content;
}
