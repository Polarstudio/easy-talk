package cn.karuhara.etalk.content.controller.app.activity.vo;

import cn.karuhara.etalk.content.enums.ActivityStatusEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Schema(description = "APP - 活动基本的信息")
public class ActivityAppBaseVO {
    @Schema(description = "活动id",example = "1")
    private Integer id;

    @Schema(description = "活动封面",requiredMode = Schema.RequiredMode.REQUIRED,example = "https://www.karuhara.cn/etalk/activity.jpg")
    private List<String> coverImageUrl;

    @Schema(description = "活动标题",requiredMode = Schema.RequiredMode.REQUIRED,example = "排球运动")
    @NotNull(message = "活动名称不能为空！")
    private String name;

    @Schema(description = "活动创建人",requiredMode = Schema.RequiredMode.REQUIRED,example = "1")
    private Integer userId;

    @Schema(description = "活动开始时间",requiredMode = Schema.RequiredMode.REQUIRED,example = "")
    @NotNull(message = "活动开始时间不能为空！")
    @JsonFormat(pattern = "yyyy/MM/dd HH:mm", timezone = "GMT+8")
    private LocalDateTime startTime;

    @Schema(description = "活动结束时间",requiredMode = Schema.RequiredMode.REQUIRED,example = "")
    @NotNull(message = "活动结束时间不能为空！")
    @JsonFormat(pattern = "yyyy/MM/dd HH:mm", timezone = "GMT+8")
    private LocalDateTime endTime;

    @Schema(description = "报名开始时间",requiredMode = Schema.RequiredMode.REQUIRED,example = "")
    @NotNull(message = "报名开始时间不能为空！")
    @JsonFormat(pattern = "yyyy/MM/dd HH:mm", timezone = "GMT+8")
    private LocalDateTime applyStart;

    @Schema(description = "活动结束时间",requiredMode = Schema.RequiredMode.REQUIRED,example = "")
    @NotNull(message = "活动结束时间不能为空！")
    @JsonFormat(pattern = "yyyy/MM/dd HH:mm", timezone = "GMT+8")
    private LocalDateTime applyEnd;

    @Schema(description = "地点名称",requiredMode = Schema.RequiredMode.REQUIRED,example = "XX 公园 XX 商场")
    @NotNull(message = "地点名称不能为空！")
    private String placeName;

    @Schema(description = "活动的人数限制")
    private Integer peopleNum;

    @Schema(description = "活动状态",requiredMode = Schema.RequiredMode.REQUIRED,example = "0")
    private ActivityStatusEnum status;

    @Schema(description = "状态名称")
    private String statusName;
}
