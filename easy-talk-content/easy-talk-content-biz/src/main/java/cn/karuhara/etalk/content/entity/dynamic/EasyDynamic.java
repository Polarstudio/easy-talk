package cn.karuhara.etalk.content.entity.dynamic;

import cn.karuhara.etalk.content.enums.DynamicStatusEnum;
import cn.karuhara.etalk.content.enums.DynamicTopEnum;
import cn.karuhara.etalk.content.enums.DynamicTypeEnum;
import cn.karuhara.etalk.framework.mybatis.core.dataobject.BaseDO;
import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 动态
 * @TableName easy_dynamic
 */
@EqualsAndHashCode(callSuper = true)
@TableName(value ="easy_dynamic")
@Data
public class EasyDynamic extends BaseDO implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 内容
     */
    private String content;

    /**
     * 地点位置
     */
    private String address;

    /**
     * 地点名称
     */
    private String addressName;

    /**
     * 地点经度
     */
    private Double latitude;

    /**
     * 地点纬度
     */
    private Double longitude;

    /**
     * 圈子ID
     */
    private Integer circleId;

    /**
     * 话题ID
     */
    private Integer topicId;

    /**
     * 活动ID
     */
    private Integer activityId;

    /**
     * 浏览
     */
    private Integer browse;

    /**
     * 置顶:   0不置顶、1置顶
     */
    private DynamicTopEnum top;

    /**
     * 类型：0 图文、1 视频、2 音文
     */
    private DynamicTypeEnum dynamicType;

    /**
     * 状态：0 草稿、1 待审核、2 展示、3 不展示、4 驳回、5 用户删除
     */
    private DynamicStatusEnum status;

    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 最后更新时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 是否删除（1是 0否）
     */
    @TableLogic
    private Integer deleted;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}