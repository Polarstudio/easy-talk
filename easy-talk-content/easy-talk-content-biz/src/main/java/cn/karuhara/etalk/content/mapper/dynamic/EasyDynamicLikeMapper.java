package cn.karuhara.etalk.content.mapper.dynamic;

import cn.karuhara.etalk.content.entity.dynamic.EasyDynamicLike;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author atom
* @description 针对表【easy_dynamic_like(动态点赞)】的数据库操作Mapper
* @createDate 2024-11-26 16:51:20
* @Entity cn.karuhara.etalk.content.dynamic.entity.EasyDynamicLike
*/
public interface EasyDynamicLikeMapper extends BaseMapper<EasyDynamicLike> {

}




