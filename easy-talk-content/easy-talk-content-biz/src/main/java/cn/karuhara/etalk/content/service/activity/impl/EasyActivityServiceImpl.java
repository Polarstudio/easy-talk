package cn.karuhara.etalk.content.service.activity.impl;

import cn.karuhara.etalk.content.controller.admin.activity.vo.*;
import cn.karuhara.etalk.content.controller.app.activity.vo.*;
import cn.karuhara.etalk.content.convert.ActivityConvert;
import cn.karuhara.etalk.content.entity.activity.EasyActivityUser;
import cn.karuhara.etalk.content.enums.ActivityPushEnum;
import cn.karuhara.etalk.content.enums.ActivityStatusEnum;
import cn.karuhara.etalk.content.mapper.activity.EasyActivityUserMapper;
import cn.karuhara.etalk.content.mq.product.ActivityProducer;
import cn.karuhara.etalk.content.service.dynamic.EasyDynamicService;
import cn.karuhara.etalk.framework.common.constants.GlobalConstant;
import cn.karuhara.etalk.framework.common.context.LoginContextHolder;
import cn.karuhara.etalk.framework.common.enums.GlobalCodeEnum;
import cn.karuhara.etalk.framework.common.exception.ServiceException;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import cn.karuhara.etalk.member.api.MemberApi;
import cn.karuhara.etalk.member.api.dto.MemberBaseInfoDTO;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.karuhara.etalk.content.entity.activity.EasyActivity;
import cn.karuhara.etalk.content.service.activity.EasyActivityService;
import cn.karuhara.etalk.content.mapper.activity.EasyActivityMapper;
import jakarta.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import cn.karuhara.etalk.content.service.activity.EasyActivityUserService;
import java.util.List;
import java.util.stream.Collectors;

/**
* @author atom
* @description 针对表【easy_activity(活动)】的数据库操作Service实现
* @createDate 2024-11-19 10:49:07
*/
@Service
public class EasyActivityServiceImpl extends ServiceImpl<EasyActivityMapper, EasyActivity>
    implements EasyActivityService{
    @Resource
    private EasyActivityMapper activityMapper;

    @Resource
    private EasyActivityUserService activityUserService;

    @Resource
    private MemberApi memberApi;

    @Resource
    private EasyActivityUserMapper activityUserMapper;

    @Resource
    private ActivityProducer activityProducer;

    @Resource
    private EasyDynamicService dynamicService;

    @Override
    public void addActivity(ActivityAddReqVO addReqVO) {
        //先判断是否有同名的活动
        LambdaQueryWrapper<EasyActivity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyActivity::getName, addReqVO.getName());
        EasyActivity one = getOne(queryWrapper);
        if (one != null) {
            throw new ServiceException(GlobalCodeEnum.FAIL, GlobalConstant.ACTIVITY_NAME_EXISTS);
        }
        EasyActivity easyActivity = ActivityConvert.INSTANT.addVOToActivity(addReqVO);
        save(easyActivity);
    }

    @Override
    public PageResult<ActivityPageRespVO> getPageList(ActivityPageReqVO pageReqVO) {
        LambdaQueryWrapper<EasyActivity> queryWrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(pageReqVO.getName())){
            queryWrapper.like(EasyActivity::getName, pageReqVO.getName());
        }
        if (StringUtils.isNotBlank(pageReqVO.getPush())){
            queryWrapper.eq(EasyActivity::getPush, pageReqVO.getPush());
        }
        if (StringUtils.isNotBlank(pageReqVO.getStatus())){
            queryWrapper.eq(EasyActivity::getStatus, pageReqVO.getStatus());
        }
        if (StringUtils.isNotBlank(pageReqVO.getPlaceName())){
            queryWrapper.like(EasyActivity::getPlaceName, pageReqVO.getPlaceName());
        }
        Page<EasyActivity> page = new Page<>(pageReqVO.getPageNo(), pageReqVO.getPageSize());
        Page<EasyActivity> iPage= activityMapper.selectPage(page, queryWrapper);
        if (iPage.getTotal() == 0){
            return PageResult.empty();
        }
        //获取活动报名的人数
        List<EasyActivity> records = iPage.getRecords();
        List<ActivityPageRespVO> pageRespVOS = ActivityConvert.INSTANT.activityListToPageRespList(records);
        for (ActivityPageRespVO respVO : pageRespVOS) {
            LambdaQueryWrapper<EasyActivityUser> activityUserQuery = new LambdaQueryWrapper<>();
            activityUserQuery.eq(EasyActivityUser::getActivityId, respVO.getId());
            long count = activityUserService.count(activityUserQuery);
            respVO.setAttendNum(count);
        }
        return new PageResult<>(pageRespVOS, iPage.getPages());
    }

    @Override
    public void updateActivity(ActivityUpdateReqVO updateReqVO) {
        EasyActivity easyActivity = ActivityConvert.INSTANT.updateActivity(updateReqVO);
        activityMapper.updateById(easyActivity);
    }

    /**
     * 分页获取全部的活动
     *
     * @param pageReqVO 分页参数
     * @return 结果集
     */
    @Override
    public PageResult<ActivityAppPageRespVO> getList(ActivityAppPageReqVO pageReqVO) {
        LambdaQueryWrapper<EasyActivity> queryWrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(pageReqVO.getName())){
            queryWrapper.like(EasyActivity::getName, pageReqVO.getName());
        }
        //如果userId存在则代表获取的是用户发布的活动
        if (pageReqVO.getUserId() != null){
            queryWrapper.eq(EasyActivity::getUserId, pageReqVO.getUserId());
        }else {
            queryWrapper.eq(EasyActivity::getStatus,ActivityStatusEnum.REGISTRATION)
                    .or()
                    .eq(EasyActivity::getStatus,ActivityStatusEnum.IN_PROGRESS);
        }
        Page<EasyActivity> page = new Page<>(pageReqVO.getPageNo(), pageReqVO.getPageSize());
        IPage<EasyActivity> iPage= activityMapper.selectPage(page, queryWrapper);
        if (iPage.getTotal() == 0){
            return PageResult.empty();
        }
        List<EasyActivity> records = iPage.getRecords();
        List<ActivityAppPageRespVO> activityAppPageRespVOS = ActivityConvert.INSTANT.activitiesToPageRespList(records);
        activityAppPageRespVOS.forEach(this::fillActivity);
        return new PageResult<>(activityAppPageRespVOS, iPage.getPages());
    }

    /**
     * 分页获取用户参与的活动
     *
     * @return 活动结果集
     */
    @Override
    public PageResult<ActivityAppPageRespVO> getAttendList(ActivityAppPageReqVO pageReqVO) {
        //先判断用户是否以及登录
        Integer userId = LoginContextHolder.get();
        if (userId == null){
            return PageResult.empty();
        }
        //获取用户参与的活动的id
        LambdaQueryWrapper<EasyActivityUser> activityUserQuery = new LambdaQueryWrapper<>();
        activityUserQuery.eq(EasyActivityUser::getUserId, userId);
        List<EasyActivityUser> activityUsers = activityUserService.list(activityUserQuery);
        List<Integer> activityIds = activityUsers.stream().map(EasyActivityUser::getActivityId).collect(Collectors.toList());
        if (activityIds.isEmpty()){
            return PageResult.empty();
        }
        //获取活动的列表
        Page<EasyActivity> page = new Page<>(pageReqVO.getPageNo(), pageReqVO.getPageSize());
        LambdaQueryWrapper<EasyActivity> queryWrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(pageReqVO.getName())){
            queryWrapper.like(EasyActivity::getName, pageReqVO.getName());
        }
        queryWrapper.in(EasyActivity::getId, activityIds);
        IPage<EasyActivity> iPage= activityMapper.selectPage(page, queryWrapper);
        if (iPage.getTotal() == 0){
            return PageResult.empty();
        }
        List<EasyActivity> records = iPage.getRecords();
        List<ActivityAppPageRespVO> activityAppPageRespVOS = ActivityConvert.INSTANT.activitiesToPageRespList(records);
        activityAppPageRespVOS.forEach(this::fillActivity);
        return new PageResult<>(activityAppPageRespVOS, iPage.getPages());
    }

    /**
     * 获取活动的详细信息
     *
     * @param id 活动id
     * @return 活动详细信息
     */
    @Override
    public ActivityAppDetailVO getDetail(Integer id) {
        EasyActivity activity = getById(id);
        if (activity == null){
            return null;
        }
        ActivityAppDetailVO detailVO = ActivityConvert.INSTANT.activityToAppDetailVO(activity);
        Long attendCount = getAttendCount(detailVO.getId());
        detailVO.setAttendNum(attendCount);
        //获取创建者的信息
        MemberBaseInfoDTO memberBaseInfo = memberApi.getMemberBaseInfo(detailVO.getUserId());
        detailVO.setAvatar(memberBaseInfo.getAvatar());
        detailVO.setNikeName(memberBaseInfo.getNikeName());
        detailVO.setCertificateName(memberBaseInfo.getCertificateName());
        detailVO.setStatusName(detailVO.getStatus().getDesc());
        return detailVO;
    }

    /**
     * 发布或修改活动
     *
     * @param appAddVO 活动的参数
     */
    @Override
    public void addAppActivity(ActivityAppAddVO appAddVO) {
        Integer userId = LoginContextHolder.get();
        if (userId == null){
            throw new ServiceException(GlobalCodeEnum.LOGIN_AUTH);
        }
        //如何传入的包含id，那么代表是更新操作
        if (appAddVO.getId() != null){
            //TODO 异步的把oss中之前的封面、图片给删了
        }
        EasyActivity easyActivity = ActivityConvert.INSTANT.appAddVOToActivity(appAddVO);
        easyActivity.setUserId(userId);
        //无论新增还更改，都需要审核
        easyActivity.setStatus(ActivityStatusEnum.REVIEWED);
        saveOrUpdate(easyActivity);
        //TODO 自动审核
    }

    /**
     * 获取活动的tag信息
     *
     * @param pageReqVO 分页参数
     * @return list
     */
    @Override
    public PageResult<ActivityAppBriefVO> getTags(ActivityAppPageReqVO pageReqVO) {
        //获取用户的id，要求用户必须参与活动才能发布活动相关的动态
        Integer userId = LoginContextHolder.get();
        if (userId == null){
            return PageResult.empty();
        }
        //先找到用户参与的活动
        LambdaQueryWrapper<EasyActivityUser> activityUserQuery = new LambdaQueryWrapper<>();
        activityUserQuery.eq(EasyActivityUser::getUserId, userId);
        List<EasyActivityUser> easyActivityUsers = activityUserMapper.selectList(activityUserQuery);
        List<Integer> activities = easyActivityUsers.stream().map(EasyActivityUser::getActivityId).collect(Collectors.toList());
        if (activities.isEmpty()){
            return PageResult.empty();
        }
        //再根据获取到的活动id获取活动的tag
        LambdaQueryWrapper<EasyActivity> queryWrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(pageReqVO.getName())) {
            queryWrapper.like(EasyActivity::getName, pageReqVO.getName());
        }
        queryWrapper.in(EasyActivity::getId, activities);
        Page<EasyActivity> page = new Page<>(pageReqVO.getPageNo(), pageReqVO.getPageSize());
        IPage<EasyActivity> iPage= activityMapper.selectPage(page, queryWrapper);
        if (iPage.getTotal() == 0){
            return PageResult.empty();
        }
        List<EasyActivity> records = iPage.getRecords();
        List<ActivityAppBriefVO> activityAppBriefVOS = ActivityConvert.INSTANT.activitiesToAppBriefVOList(records);
        return new PageResult<>(activityAppBriefVOS, iPage.getPages());
    }

    /**
     * 管理后台 - 获取活动的tag信息
     * @return
     */
    @Override
    public List<ActivityAppBriefVO> getAdminTags() {
        List<EasyActivity> list = list();
        return ActivityConvert.INSTANT.activitiesToAppBriefVOList(list);
    }

    /**
     * 删除活动
     */
    @Override
    public void delete(ActivityDeleteVO deleteVO) {
        //解除动态关联活动
        dynamicService.canalRelevance(null,null,deleteVO.getId());
        //发送通知
        activityProducer.deleteMessage(deleteVO);
        //解除关联
        LambdaQueryWrapper<EasyActivityUser> activityUserQuery = new LambdaQueryWrapper<>();
        activityUserQuery.eq(EasyActivityUser::getActivityId, deleteVO.getId());
        activityUserService.remove(activityUserQuery);
        //在删除活动
        removeById(deleteVO.getId());
    }

    /**
     * 获取推荐的活动
     *
     * @return 推荐的活动
     */
    @Override
    public ActivityAppDetailVO getRecommendActivity() {
        LambdaQueryWrapper<EasyActivity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyActivity::getPush, ActivityPushEnum.PUSH);
        List<EasyActivity> list = activityMapper.selectList(queryWrapper);
        EasyActivity easyActivity = list.get(0);
        ActivityAppDetailVO detailVO = ActivityConvert.INSTANT.activityToAppDetailVO(easyActivity);
        detailVO.setStatusName(detailVO.getStatus().getDesc());
        return detailVO;
    }

    //获取活动的用户参与的人数
    public Long getAttendCount(Integer activityId) {
        LambdaQueryWrapper<EasyActivityUser> activityUserQuery = new LambdaQueryWrapper<>();
        activityUserQuery.eq(EasyActivityUser::getActivityId, activityId);
        return activityUserService.count(activityUserQuery);
    }

    //填充数据
    public void fillActivity(ActivityAppPageRespVO vo) {
        Long attendCount = getAttendCount(vo.getId());
        vo.setAttendNum(attendCount);
        vo.setStatusName(vo.getStatus().getDesc());
        //获取创建者的信息
        MemberBaseInfoDTO memberBaseInfo = memberApi.getMemberBaseInfo(vo.getUserId());
        vo.setAvatar(memberBaseInfo.getAvatar());
        vo.setNikeName(memberBaseInfo.getNikeName());
        vo.setCertificateName(memberBaseInfo.getCertificateName());
    }
}




