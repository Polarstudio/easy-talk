package cn.karuhara.etalk.content.convert;

import cn.karuhara.etalk.content.controller.admin.activity.vo.ActivityAddReqVO;
import cn.karuhara.etalk.content.controller.admin.activity.vo.ActivityPageRespVO;
import cn.karuhara.etalk.content.controller.admin.activity.vo.ActivityUpdateReqVO;
import cn.karuhara.etalk.content.controller.app.activity.vo.*;
import cn.karuhara.etalk.content.entity.activity.EasyActivity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ActivityConvert {
    ActivityConvert INSTANT = Mappers.getMapper(ActivityConvert.class);

    EasyActivity addVOToActivity(ActivityAddReqVO addReqVO);

    List<ActivityPageRespVO> activityListToPageRespList(List<EasyActivity> activityList);

    EasyActivity updateActivity(ActivityUpdateReqVO updateReqVO);

    List<ActivityAppPageRespVO> activitiesToPageRespList(List<EasyActivity> activityList);

    ActivityAppDetailVO activityToAppDetailVO(EasyActivity easyActivity);

    EasyActivity appAddVOToActivity(ActivityAppAddVO addReqVO);

    List<ActivityAppBriefVO> activitiesToAppBriefVOList(List<EasyActivity> easyActivityList);

    ActivityAppBaseVO activityToAppBaseVO(EasyActivity easyActivity);
}
