package cn.karuhara.etalk.content.controller.app.circle.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "最简单的圈子信息 - 提供给发布动态使用")
@Data
public class CircleAppBriefVO {

    @Schema(description = "圈子ID")
    private Integer id;

    @Schema(description = "圈子名称")
    private String circleName;
}
