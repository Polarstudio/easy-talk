package cn.karuhara.etalk.content.entity.dynamic;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.Data;

/**
 * 动态点赞
 * @TableName easy_dynamic_like
 */
@TableName(value ="easy_dynamic_like")
@Data
public class EasyDynamicLike implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 动态ID
     */
    private Integer dynamicId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}