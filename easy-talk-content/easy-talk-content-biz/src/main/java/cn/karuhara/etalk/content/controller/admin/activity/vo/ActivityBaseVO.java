package cn.karuhara.etalk.content.controller.admin.activity.vo;

import cn.karuhara.etalk.content.enums.ActivityPushEnum;
import cn.karuhara.etalk.content.enums.ActivityStatusEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class ActivityBaseVO {

    @Schema(description = "活动id",example = "1")
    private Integer id;

    @Schema(description = "活动封面",requiredMode = Schema.RequiredMode.REQUIRED,example = "https://www.karuhara.cn/etalk/activity.jpg")
    private List<String> coverImageUrl;

    @Schema(description = "活动名称",requiredMode = Schema.RequiredMode.REQUIRED,example = "排球运动")
    @NotNull(message = "活动名称不能为空！")
    private String name;

    @Schema(description = "活动简介",requiredMode = Schema.RequiredMode.REQUIRED,example = "一起来打排球吧！")
    @NotNull(message = "活动简介不能为空！")
    private String intro;

    @Schema(description = "介绍图 例如 活动的海报",requiredMode = Schema.RequiredMode.REQUIRED)
    private List<String> imgs;

    @Schema(description = "活动创建人",requiredMode = Schema.RequiredMode.REQUIRED,example = "1")
    @NotNull(message = "活动创建人不能为空！")
    private Integer userId;

    @Schema(description = "报名开始时间",requiredMode = Schema.RequiredMode.REQUIRED,example = "")
    @NotNull(message = "报名开始时间不能为空！")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private LocalDateTime applyStart;

    @Schema(description = "活动结束时间时间",requiredMode = Schema.RequiredMode.REQUIRED,example = "")
    @NotNull(message = "活动结束时间时间不能为空！")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private LocalDateTime applyEnd;

    @Schema(description = "活动开始时间",requiredMode = Schema.RequiredMode.REQUIRED,example = "")
    @NotNull(message = "活动开始时间不能为空！")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private LocalDateTime startTime;

    @Schema(description = "活动结束时间",requiredMode = Schema.RequiredMode.REQUIRED,example = "")
    @NotNull(message = "活动结束时间不能为空！")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private LocalDateTime endTime;

    @Schema(description = "人数限制 (-1代表无人数限制)",requiredMode = Schema.RequiredMode.REQUIRED,example = "20")
    private Integer peopleNum;

    @Schema(description = "地点纬度  例如 39.90420000",requiredMode = Schema.RequiredMode.REQUIRED,example = "39.90420000")
    @NotNull(message = "地点纬度不能为空！")
    private String latitude;

    @Schema(description = "地点经度  例如 116.40740000",requiredMode = Schema.RequiredMode.REQUIRED,example = "116.40740000")
    @NotNull(message = "地点经度不能为空！")
    private String longitude;

    @Schema(description = "地点位置",requiredMode = Schema.RequiredMode.REQUIRED)
    @NotNull(message = "地点位置不能为空！")
    private String address;

    @Schema(description = "地点名称",requiredMode = Schema.RequiredMode.REQUIRED,example = "XX 公园 XX 商场")
    @NotNull(message = "地点名称不能为空！")
    private String placeName;

    @Schema(description = "首页推送",requiredMode = Schema.RequiredMode.REQUIRED,example = "0")
    private ActivityPushEnum push;

    @Schema(description = "活动状态",requiredMode = Schema.RequiredMode.REQUIRED,example = "0")
    private ActivityStatusEnum status;
}
