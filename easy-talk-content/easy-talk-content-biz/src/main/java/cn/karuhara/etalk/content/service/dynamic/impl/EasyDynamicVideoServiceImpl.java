package cn.karuhara.etalk.content.service.dynamic.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.karuhara.etalk.content.entity.dynamic.EasyDynamicVideo;
import cn.karuhara.etalk.content.service.dynamic.EasyDynamicVideoService;
import cn.karuhara.etalk.content.mapper.dynamic.EasyDynamicVideoMapper;
import org.springframework.stereotype.Service;

/**
* @author atom
* @description 针对表【easy_dynamic_video(动态视频)】的数据库操作Service实现
* @createDate 2024-11-26 21:21:49
*/
@Service
public class EasyDynamicVideoServiceImpl extends ServiceImpl<EasyDynamicVideoMapper, EasyDynamicVideo>
    implements EasyDynamicVideoService{

}




