package cn.karuhara.etalk.content.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 接口请求的平台
 */
@Getter
@AllArgsConstructor
public enum PlaftFormTypeEnum {
    ADMIN("0","管理后台"),
    MINI_PROGRAM("1","小程序");

    @EnumValue
    @JsonValue
    private final String code;

    private final String desc;
}
