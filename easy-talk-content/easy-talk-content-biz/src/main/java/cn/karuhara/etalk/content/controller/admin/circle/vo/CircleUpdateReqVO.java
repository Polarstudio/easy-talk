package cn.karuhara.etalk.content.controller.admin.circle.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "修改圈子业务")
public class CircleUpdateReqVO extends CircleBaseVO{

    @Schema(description = "圈子id",example = "1")
    @NotNull(message = "圈子id不能为空！")
    private Integer id;
}
