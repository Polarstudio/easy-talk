package cn.karuhara.etalk.content.controller.admin.topic.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "用户分页结果")
public class TopicPageRespVO extends TopicBaseVO{

    @Schema(description = "话题关注数")
    private long followNum;

    @Schema(description = "创建时间")
    private LocalDateTime createTime;

}
