package cn.karuhara.etalk.content.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 动态类型枚举类
 */
@Getter
@AllArgsConstructor
public enum DynamicTypeEnum {
    PICTURE_TEXT("0","图文"),
    VIDEO_TEXT("1","视频"),
    AUDIO_TEXT("2","音文");

    @EnumValue
    @JsonValue
    private final String code;

    private final String desc;
}
