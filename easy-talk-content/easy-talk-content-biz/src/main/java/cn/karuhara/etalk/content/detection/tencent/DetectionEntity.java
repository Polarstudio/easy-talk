package cn.karuhara.etalk.content.detection.tencent;

import cn.karuhara.etalk.content.enums.DetectionTypeEnum;
import lombok.Data;
import java.util.List;

/**
 * 待审核的内容
 */
@Data
public class DetectionEntity {

    /**
     * 对应的内容id，如评论id，动态id
     */
    private Integer postId;

    /**
     * 对应的用户的openId,进行内容审核时需要用到
     */
    private String wxOpenid;

    /**
     * 审核的类型
     */
    private DetectionTypeEnum type;

    /**
     * 文本内容
     */
    private List<String> texts;

    /**
     * 图片内容
     */
    private List<String> images;

    /**
     * 视频内容
     */
    private String videos;

    /**
     * 音频内容
     */
    private String audios;

}
