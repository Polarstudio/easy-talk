package cn.karuhara.etalk.content.controller.admin.dynamic.vo;

import cn.karuhara.etalk.content.enums.CommentStatusEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "管理后台 - 修改评论")
@Data
public class CommentUpdateVO {
    @Schema(description = "评论ID")
    private Integer id;
    @Schema(description = "用户id")
    private Integer userId;
    @Schema(description = "评论内容")
    private String content;
    @Schema(description = "评论状态")
    private CommentStatusEnum status;
}
