package cn.karuhara.etalk.content.controller.app.dynamic.vo;

import cn.karuhara.etalk.framework.common.pojo.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Schema(description = "分页获取用户喜欢的动态")
@Data
public class DynamicAppLikePageReqVO extends PageParam {
    @Schema(description = "用户id")
    private Integer userId;

    @Schema(description = "搜索内容")
    private String content;
}
