package cn.karuhara.etalk.content.mapper.dynamic;

import cn.karuhara.etalk.content.entity.dynamic.EasyDynamic;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author atom
* @description 针对表【easy_dynamic(动态)】的数据库操作Mapper
* @createDate 2024-11-26 16:51:19
* @Entity cn.karuhara.etalk.content.dynamic.entity.EasyDynamic
*/
public interface EasyDynamicMapper extends BaseMapper<EasyDynamic> {

}




