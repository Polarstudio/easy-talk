package cn.karuhara.etalk.content.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Description: 审核类型枚举类
 * @author: 轻语
*/
@Getter
@AllArgsConstructor
public enum DetectionTypeEnum {
    DYNAMIC("0", "动态"),
    COMMENT("1", "评论"),
    TOPIC("2", "话题"),
    CIRCLE("3", "圈子"),
    ACTIVITY("4", "活动");

    @EnumValue
    @JsonValue
    private final String code;

    private final String desc;
}
