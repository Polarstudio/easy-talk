package cn.karuhara.etalk.content.controller.app.topic;

import cn.karuhara.etalk.content.controller.admin.topic.vo.TopicDeleteVO;
import cn.karuhara.etalk.content.controller.app.topic.vo.*;
import cn.karuhara.etalk.content.service.topic.EasyTopicFollowService;
import cn.karuhara.etalk.content.service.topic.EasyTopicService;
import cn.karuhara.etalk.framework.common.pojo.CommonResult;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/content/topic/app")
@Tag(name = "APP - 平台话题")
public class TopicAppController {

    @Resource
    private EasyTopicService topicService;

    @Resource
    private EasyTopicFollowService followService;

    @Operation(summary = "获取推荐的话题")
    @GetMapping("/recommend")
    public CommonResult<List<TopicAppBaseVO>> recommend() {
        List<TopicAppBaseVO> vos = topicService.getRecommend();
        return CommonResult.success(vos);
    }

    @Operation(summary = "分页获取话题")
    @PostMapping("/list")
    public CommonResult<PageResult<TopicAppDetailVO>> list(@RequestBody TopicAppPageReqVO pageParam) {
        PageResult<TopicAppDetailVO> pageResult = topicService.gteList(pageParam);
        return CommonResult.success(pageResult);
    }

    @Operation(summary = "创建或编辑话题")
    @PostMapping
    public CommonResult<Void> add(@RequestBody TopicAppBaseVO topicAppBaseVO) {
        topicService.appAddTopic(topicAppBaseVO);
        return CommonResult.success();
    }

    @Operation(summary = "获取用户关注的话题")
    @PostMapping("/list/follow")
    public CommonResult<PageResult<TopicAppDetailVO>> getFollowList(@RequestBody TopicAppPageReqVO pageParam) {
        PageResult<TopicAppDetailVO> pageResult = topicService.getFollowList(pageParam);
        return CommonResult.success(pageResult);
    }

    @Operation(summary = "获取话题的详情")
    @GetMapping("/{id}")
    public CommonResult<TopicAppDetailVO> get(@PathVariable Integer id) {
        TopicAppDetailVO detailVO = topicService.getDetail(id);
        return CommonResult.success(detailVO);
    }

    @Operation(summary = "获取话题的id和名称")
    @PostMapping("/list/tags")
    public CommonResult<PageResult<TopicAppBriefVO>> getTags(@RequestBody TopicAppPageReqVO pageReqVO){
        return CommonResult.success(topicService.getTags(pageReqVO));
    }

    @Operation(summary = "关注或取关话题话题")
    @PutMapping("/follow/{topicId}/{userId}")
    public CommonResult<Void> follow(@PathVariable Integer topicId, @PathVariable Integer userId){
        followService.follow(topicId,userId);
        return CommonResult.success();
    }

    @Operation(summary = "判断用户是否关注话题")
    @GetMapping("/follow/{topicId}/{userId}")
    public CommonResult<Boolean> followCheck(@PathVariable("topicId") Integer topicId, @PathVariable("userId") Integer userId){
        return CommonResult.success(followService.followCheck(topicId,userId));
    }

    @Operation(summary = "删除话题")
    @DeleteMapping
    public CommonResult<Void> delete(@RequestBody TopicDeleteVO topicDeleteVO){
        topicService.delete(topicDeleteVO);
        return CommonResult.success();
    }

    @Operation(summary = "获取话题的热榜")
    @GetMapping("/hot")
    public CommonResult<List<TopicHotVO>> getHotTopic(){
        return CommonResult.success(topicService.getHotTopic());
    }
}
