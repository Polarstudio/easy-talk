package cn.karuhara.etalk.content.service.topic;

import cn.karuhara.etalk.content.entity.topic.EasyTopicFollow;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author atom
* @description 针对表【easy_topic_follow(话题关注表)】的数据库操作Service
* @createDate 2024-11-18 16:38:17
*/
public interface EasyTopicFollowService extends IService<EasyTopicFollow> {

    /**
     * 判断用户是否关注话题
     * @param topicId 话题id
     * @param userId 用户id
     * @return 是否关注
     */
    Boolean followCheck(Integer topicId, Integer userId);

    /**
     * 关注或取关话题话题
     */
    void follow(Integer topicId,Integer userId);
}
