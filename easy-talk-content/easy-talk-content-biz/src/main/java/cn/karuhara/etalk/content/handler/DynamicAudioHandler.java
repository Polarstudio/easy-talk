package cn.karuhara.etalk.content.handler;

import cn.karuhara.etalk.content.controller.app.dynamic.vo.DynamicAppAttachmentVO;
import cn.karuhara.etalk.content.convert.DynamicConvert;
import cn.karuhara.etalk.content.entity.dynamic.EasyDynamicAudio;
import cn.karuhara.etalk.content.entity.dynamic.EasyDynamicVideo;
import cn.karuhara.etalk.content.enums.DynamicTypeEnum;
import cn.karuhara.etalk.content.service.dynamic.EasyDynamicAudioService;
import cn.karuhara.etalk.framework.common.context.LoginContextHolder;
import cn.karuhara.etalk.framework.common.enums.GlobalCodeEnum;
import cn.karuhara.etalk.framework.common.exception.ServiceException;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

/**
 * @Description: 动态音频策略类
 * @author: 轻语
*/
@Component
public class DynamicAudioHandler implements DynamicTypeHandler{

    @Resource
    private EasyDynamicAudioService audioService;

    @Override
    public DynamicTypeEnum getDynamicType() {
        return DynamicTypeEnum.AUDIO_TEXT;
    }

    @Override
    public void sava(DynamicAppAttachmentVO attachmentVO) {
        Integer userId = LoginContextHolder.get();
        if (userId == null) {
            throw new ServiceException(GlobalCodeEnum.LOGIN_AUTH);
        }
    }

    @Override
    public DynamicAppAttachmentVO get(Integer dynamicId) {
        LambdaQueryWrapper<EasyDynamicAudio> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(EasyDynamicAudio::getDynamicId,dynamicId);
        EasyDynamicAudio dynamicAudio = audioService.getOne(wrapper);
        return DynamicConvert.INSTANT.dynamicAudioToAttachmentVO(dynamicAudio);
    }

    @Override
    public void update(DynamicAppAttachmentVO attachmentVO) {

    }

    @Override
    public void delete(Integer dynamicId) {

    }
}
