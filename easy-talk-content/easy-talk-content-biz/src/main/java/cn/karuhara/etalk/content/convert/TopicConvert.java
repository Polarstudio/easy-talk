package cn.karuhara.etalk.content.convert;

import cn.karuhara.etalk.content.controller.admin.topic.vo.TopicBaseVO;
import cn.karuhara.etalk.content.controller.admin.topic.vo.TopicPageRespVO;
import cn.karuhara.etalk.content.controller.app.topic.vo.TopicAppBaseVO;
import cn.karuhara.etalk.content.controller.app.topic.vo.TopicAppBriefVO;
import cn.karuhara.etalk.content.controller.app.topic.vo.TopicAppDetailVO;
import cn.karuhara.etalk.content.entity.topic.EasyTopic;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface TopicConvert {
    TopicConvert INSTANCE = Mappers.getMapper(TopicConvert.class);

    EasyTopic addVOToTopic(TopicBaseVO topicBaseVO);

    TopicPageRespVO topicToPageRespVO(EasyTopic easyTopic);

    List<TopicAppBaseVO> topicToAppBaseVOList(List<EasyTopic> easyTopicList);

    List<TopicAppDetailVO> topicToAppDetailVOList(List<EasyTopic> easyTopicList);

    EasyTopic appBaseVOToTopic(TopicAppBaseVO topicAppBaseVO);

    TopicAppDetailVO topicToAppDetailVO(EasyTopic easyTopic);

    List<TopicAppBriefVO> topicsToAppBriefVOs(List<EasyTopic> easyTopic);
}
