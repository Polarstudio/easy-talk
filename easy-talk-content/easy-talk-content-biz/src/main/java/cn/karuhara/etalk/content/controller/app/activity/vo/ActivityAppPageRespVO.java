package cn.karuhara.etalk.content.controller.app.activity.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Schema(description = "App - 分页查询的结果集")
@Data
public class ActivityAppPageRespVO extends ActivityAppBaseVO{

    @Schema(description = "创建人的昵称")
    private String nikeName;

    @Schema(description = "创建人的头像")
    private String avatar;

    @Schema(description = "活动的参与人数")
    private Long attendNum;

    @Schema(description = "用户认证名称")
    private String certificateName;
}
