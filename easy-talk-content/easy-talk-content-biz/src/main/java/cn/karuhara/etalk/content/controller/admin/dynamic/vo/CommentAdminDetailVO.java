package cn.karuhara.etalk.content.controller.admin.dynamic.vo;

import cn.karuhara.etalk.content.controller.app.dynamic.vo.CommentBaseVO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Schema(description = "管理后台 - 详情信息")
@Data
public class CommentAdminDetailVO extends CommentBaseVO {
    @Schema(description = "用户头像")
    private String avatar;

    @Schema(description = "用户昵称")
    private String nickname;

    @Schema(description = "获赞数量")
    private Long likeCount;


}
