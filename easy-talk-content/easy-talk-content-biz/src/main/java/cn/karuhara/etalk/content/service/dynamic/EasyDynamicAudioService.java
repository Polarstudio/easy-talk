package cn.karuhara.etalk.content.service.dynamic;

import cn.karuhara.etalk.content.entity.dynamic.EasyDynamicAudio;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author atom
* @description 针对表【easy_dynamic_audio(动态音频)】的数据库操作Service
* @createDate 2024-11-26 21:21:49
*/
public interface EasyDynamicAudioService extends IService<EasyDynamicAudio> {

}
