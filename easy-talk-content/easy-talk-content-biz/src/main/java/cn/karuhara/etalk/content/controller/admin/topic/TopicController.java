package cn.karuhara.etalk.content.controller.admin.topic;

import cn.karuhara.etalk.content.controller.admin.topic.vo.TopicBaseVO;
import cn.karuhara.etalk.content.controller.admin.topic.vo.TopicDeleteVO;
import cn.karuhara.etalk.content.controller.admin.topic.vo.TopicPageReqVO;
import cn.karuhara.etalk.content.controller.admin.topic.vo.TopicPageRespVO;
import cn.karuhara.etalk.content.controller.app.topic.vo.TopicAppBriefVO;
import cn.karuhara.etalk.content.controller.app.topic.vo.TopicAppPageReqVO;
import cn.karuhara.etalk.content.entity.topic.EasyTopic;
import cn.karuhara.etalk.content.service.topic.EasyTopicService;
import cn.karuhara.etalk.framework.annotation.WeLog;
import cn.karuhara.etalk.framework.common.pojo.CommonResult;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "管理后台 - 话题模块")
@RestController
@RequestMapping("/content/topic/admin")
@Validated
public class TopicController {

    @Resource
    private EasyTopicService service;

    @PostMapping()
    @WeLog(description = "新增话题操作")
    @Operation(summary = "新增话题")
    public CommonResult<Void> add(@Valid @RequestBody TopicBaseVO topicBaseVO) {
        service.addTopic(topicBaseVO);
        return CommonResult.success();
    }

    @PostMapping("/page")
    @Operation(summary = "分页获取话题")
    public CommonResult<PageResult<TopicPageRespVO>> getPageList(@Valid @RequestBody TopicPageReqVO topicPageReqVO) {
        PageResult<TopicPageRespVO> pageResult =  service.getPageList(topicPageReqVO);
        return CommonResult.success(pageResult);
    }

    @Operation(summary = "修改话题")
    @PutMapping()
    public CommonResult<Void> update(@Valid @RequestBody TopicBaseVO topicBaseVO) {
        service.updateTopic(topicBaseVO);
        return CommonResult.success();
    }

    @Operation(summary = "管理后台 - 获取话题的id和名称")
    @GetMapping("/list/tags")
    public CommonResult<List<TopicAppBriefVO>> getTags(){
        return CommonResult.success(service.getAdminTags());
    }

    @Operation(summary = "删除话题")
    @DeleteMapping
    public CommonResult<Void> delete(@RequestBody TopicDeleteVO topicDeleteVO){
        service.delete(topicDeleteVO);
        return CommonResult.success();
    }
}
