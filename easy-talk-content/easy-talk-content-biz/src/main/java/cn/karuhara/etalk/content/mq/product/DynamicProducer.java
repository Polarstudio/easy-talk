package cn.karuhara.etalk.content.mq.product;

import cn.karuhara.etalk.content.controller.admin.dynamic.vo.DynamicDeleteVO;
import cn.karuhara.etalk.content.detection.tencent.DetectionResult;
import cn.karuhara.etalk.content.entity.dynamic.EasyDynamic;
import cn.karuhara.etalk.content.enums.DetectionTypeEnum;
import cn.karuhara.etalk.content.enums.MsgSecCheckResultEnum;
import cn.karuhara.etalk.content.enums.DynamicStatusEnum;
import cn.karuhara.etalk.content.service.dynamic.EasyDynamicService;
import cn.karuhara.etalk.framework.kafka.core.Event;
import cn.karuhara.etalk.framework.kafka.core.EventProducer;
import cn.karuhara.etalk.framework.kafka.core.MessageConstants;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

@Component
public class DynamicProducer {
    @Resource
    private EventProducer eventProducer;

    @Resource
    private EasyDynamicService dynamicService;

    public void deleteMessage(DynamicDeleteVO deleteVO){
        EasyDynamic dynamic = dynamicService.getById(deleteVO.getId());
        Event event = new Event();
        //设置消息类型
        event.setTopic(MessageConstants.DYNAMIC_DELETE_TOPIC);
        //设置通知的用户
        if (deleteVO.getUserId()!=null){
            event.setToUserId(deleteVO.getUserId());
        }
        //设置消息的内容
        if (deleteVO.getReason()!=null){
            event.setContent(deleteVO.getReason());
        }
        //这是其它的额外扩展的参数
        event.setDataMap(MessageConstants.POST_ID,deleteVO.getId());
        event.setDataMap(MessageConstants.DYNAMIC_CONTENT,dynamic.getContent());
        eventProducer.fireEvent(event);
    }

    public void detectionMessage(DetectionResult detectionResult){
        if (detectionResult.getResult() == MsgSecCheckResultEnum.REVIEW){
            return;
        }
        EasyDynamic dynamic = dynamicService.getById(detectionResult.getPostId());
        //如果该内容（如动态）已经被驳回过，那么就不进行下一步了
        if (dynamic.getStatus() == DynamicStatusEnum.REJECTED){
            return;
        }
        //修改动态的状态
        if (detectionResult.getResult() == MsgSecCheckResultEnum.RISKY){
            //屏蔽
            dynamic.setStatus(DynamicStatusEnum.REJECTED);
        }else {
            //通过
            dynamic.setStatus(DynamicStatusEnum.DISPLAY);
        }
        //更新动态
        dynamicService.updateById(dynamic);
        //发送通知
        Event event = new Event();
        event.setTopic(MessageConstants.DYNAMIC_DETECTION_TOPIC);
        event.setToUserId(dynamic.getUserId());
        event.setContent(MessageConstants.CONTENT_VIOLATIONS);
        event.setDataMap(MessageConstants.POST_ID,dynamic.getId());
        event.setDataMap(MessageConstants.DYNAMIC_CONTENT,dynamic.getContent());
        event.setDataMap(MessageConstants.DETECTION_RESULT,detectionResult.getResult().getCode());
        eventProducer.fireEvent(event);
    }

}
