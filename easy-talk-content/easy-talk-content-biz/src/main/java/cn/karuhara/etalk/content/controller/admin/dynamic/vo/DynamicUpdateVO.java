package cn.karuhara.etalk.content.controller.admin.dynamic.vo;

import cn.karuhara.etalk.content.enums.DynamicStatusEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "编辑话题的VO")
@Data
public class DynamicUpdateVO {

    @Schema(description = "动态id")
    private Integer id;

    @Schema(description = "动态内容")
    private String content;

    @Schema(description = "圈子id")
    private Integer circleId;

    @Schema(description = "话题id")
    private Integer topicId;

    @Schema(description = "活动id")
    private Integer activityId;

    @Schema(description = "状态")
    private DynamicStatusEnum status;
}
