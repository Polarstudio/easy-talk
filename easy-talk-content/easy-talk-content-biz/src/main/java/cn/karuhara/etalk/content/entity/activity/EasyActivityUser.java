package cn.karuhara.etalk.content.entity.activity;

import cn.karuhara.etalk.content.enums.ActivityUserStatus;
import cn.karuhara.etalk.framework.mybatis.core.dataobject.BaseDO;
import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户参与活动
 * @TableName easy_activity_user
 */
@EqualsAndHashCode(callSuper = true)
@TableName(value ="easy_activity_user")
@Data
public class EasyActivityUser extends BaseDO implements Serializable {


    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 活动ID
     */
    private Integer activityId;

    /**
     * UUID
     */
    private String uuid;

    /**
     * 状态
     */
    private ActivityUserStatus status;

    @TableLogic
    private Integer deleted;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}