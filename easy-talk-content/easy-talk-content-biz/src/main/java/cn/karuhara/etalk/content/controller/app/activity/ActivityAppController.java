package cn.karuhara.etalk.content.controller.app.activity;

import cn.karuhara.etalk.content.controller.admin.activity.vo.ActivityDeleteVO;
import cn.karuhara.etalk.content.controller.app.activity.vo.*;
import cn.karuhara.etalk.content.service.activity.EasyActivityService;
import cn.karuhara.etalk.framework.common.pojo.CommonResult;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/content/activity/app")
@Tag(name = "APP - 平台活动")
public class ActivityAppController {

    @Resource
    private EasyActivityService activityService;

    @PostMapping("/list")
    @Operation(summary = "分页获取全部的活动")
    public CommonResult<PageResult<ActivityAppPageRespVO>> list(@RequestBody ActivityAppPageReqVO pageReqVO){
        PageResult<ActivityAppPageRespVO> pageResult = activityService.getList(pageReqVO);
        return CommonResult.success(pageResult);
    }

    @PostMapping("/list/attend")
    @Operation(summary = "分页获取用户参与的活动")
    public CommonResult<PageResult<ActivityAppPageRespVO>> listAttend(@RequestBody ActivityAppPageReqVO pageReqVO){
        PageResult<ActivityAppPageRespVO> pageResult = activityService.getAttendList(pageReqVO);
        return CommonResult.success(pageResult);
    }

    @Operation(summary = "获取活动的详细信息")
    @GetMapping("/{id}")
    public CommonResult<ActivityAppDetailVO> getDetail(@PathVariable("id") Integer id){
        ActivityAppDetailVO detailVO = activityService.getDetail(id);
        return CommonResult.success(detailVO);
    }

    @PostMapping
    @Operation(summary = "发布或修改活动")
    public CommonResult<Void> push(@RequestBody ActivityAppAddVO appAddVO){
        activityService.addAppActivity(appAddVO);
        return CommonResult.success();
    }

    @Operation(summary = "获取活动的tag信息")
    @PostMapping("/list/tags")
    public CommonResult<PageResult<ActivityAppBriefVO>> getTags(@RequestBody ActivityAppPageReqVO pageReqVO){
        PageResult<ActivityAppBriefVO> pageResult = activityService.getTags(pageReqVO);
        return CommonResult.success(pageResult);
    }

    @Operation(summary = "获取推荐的活动")
    @GetMapping("/recommend")
    public CommonResult<ActivityAppBaseVO> getRecommendActivity(){
        return CommonResult.success(activityService.getRecommendActivity());
    }

    @Operation(summary = "删除活动")
    @DeleteMapping
    public CommonResult<Void> delete(@RequestBody ActivityDeleteVO deleteVO){
        activityService.delete(deleteVO);
        return CommonResult.success();
    }

}
