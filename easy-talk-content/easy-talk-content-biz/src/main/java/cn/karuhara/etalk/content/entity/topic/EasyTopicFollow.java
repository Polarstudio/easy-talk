package cn.karuhara.etalk.content.entity.topic;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.Data;

/**
 * 话题关注表
 * @TableName easy_topic_follow
 */
@TableName(value ="easy_topic_follow")
@Data
public class EasyTopicFollow implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 话题ID
     */
    private Integer topicId;

    /**
     * 关注时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime create_time;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}