package cn.karuhara.etalk.content.controller.admin.dynamic.vo;

import cn.karuhara.etalk.content.enums.CommentStatusEnum;
import cn.karuhara.etalk.framework.common.pojo.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Schema(description = "管理后台 - 获取评论分页")
@Data
public class CommonAdminPageReqVO extends PageParam {

    @Schema(description = "用户id")
    private Integer userId;

    @Schema(description = "动态id")
    private Integer dynamicId;

    @Schema(description = "状态")
    private String status;

    @Schema(description = "评论内容")
    private String content;
}
