package cn.karuhara.etalk.content.controller.admin.activity.vo;

import cn.karuhara.etalk.framework.common.pojo.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "分页获取活动参数")
public class ActivityPageReqVO extends PageParam {

    @Schema(description = "活动名称",requiredMode = Schema.RequiredMode.REQUIRED,example = "排球运动")
    private String name;

    @Schema(description = "地点名称",requiredMode = Schema.RequiredMode.REQUIRED,example = "XX 公园 XX 商场")
    private String placeName;

    @Schema(description = "首页推送",requiredMode = Schema.RequiredMode.REQUIRED,example = "0")
    private String push;

    @Schema(description = "活动状态",requiredMode = Schema.RequiredMode.REQUIRED,example = "0")
    private String status;
}
