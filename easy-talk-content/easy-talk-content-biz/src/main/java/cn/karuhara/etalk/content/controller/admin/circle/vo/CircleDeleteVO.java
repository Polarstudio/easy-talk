package cn.karuhara.etalk.content.controller.admin.circle.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "删除圈子")
@Data
public class CircleDeleteVO {
    @Schema(description = "id")
    private Integer id;
    @Schema(description = "用户id")
    private Integer userId;
    @Schema(description = "删除理由")
    private String reason;
}
