package cn.karuhara.etalk.content.controller.app.dynamic;

import cn.karuhara.etalk.content.controller.admin.dynamic.vo.DynamicDeleteVO;
import cn.karuhara.etalk.content.controller.app.dynamic.vo.*;
import cn.karuhara.etalk.content.service.dynamic.EasyDynamicLikeService;
import cn.karuhara.etalk.content.service.dynamic.EasyDynamicService;
import cn.karuhara.etalk.framework.common.pojo.CommonResult;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/content/dynamic/app")
@Tag(name = "APP - 平台动态")
public class DynamicAppController {

    @Resource
    private EasyDynamicService dynamicService;

    @Resource
    private EasyDynamicLikeService likeService;

    @Operation(summary = "获取动态列表，分页")
    @PostMapping("/list/recommend")
    public CommonResult<PageResult<DynamicDetailVO>> getList(@RequestBody DynamicPageReqVO pageReqVO){
        PageResult<DynamicDetailVO> pageResult = dynamicService.getList(pageReqVO);
        return CommonResult.success(pageResult);
    }

    @GetMapping("/{id}")
    @Operation(summary = "获取动态的详情")
    public CommonResult<DynamicDetailVO> getById(@PathVariable Integer id){
        DynamicDetailVO detailVO = dynamicService.getDetail(id);
        return CommonResult.success(detailVO);
    }

    @PostMapping
    @Operation(summary = "发布或修改动态")
    public CommonResult<Void> add(@RequestBody DynamicAddVO appAddVO){
        //TODO 如何位置为空，那么根据ip获取经纬度
        dynamicService.pushDynamic(appAddVO);
        return CommonResult.success();
    }

    @Operation(summary = "获取用户的点赞总数")
    @GetMapping("/getUserLiked")
    public CommonResult<Long> getLiked(@Param("userId") Integer userId){
        Long linkedCount = dynamicService.getUserLinkedCount(userId);
        return CommonResult.success(linkedCount);
    }

    @Operation(summary = "点赞或取消点赞")
    @PutMapping("/like/{dynamicId}/{userId}")
    public CommonResult<Void> like(@PathVariable Integer dynamicId, @PathVariable Integer userId){
        likeService.like(dynamicId,userId);
        return CommonResult.success();
    }

    @Operation(summary = "查询用户是否点赞某个动态")
    @GetMapping("/like/{dynamicId}/{userId}")
    public CommonResult<Boolean> checkLike(@PathVariable Integer dynamicId, @PathVariable Integer userId){
        return CommonResult.success(likeService.checkLike(dynamicId,userId));
    }

    @Operation(summary = "删除动态")
    @DeleteMapping("/{id}")
    public CommonResult<Void> delete(@PathVariable Integer id){
        DynamicDeleteVO deleteVO = new DynamicDeleteVO();
        deleteVO.setId(id);
        dynamicService.delete(deleteVO);
        return CommonResult.success();
    }

    @Operation(summary = "获取用户喜欢的动态")
    @PostMapping("/user/like")
    public CommonResult<PageResult<DynamicDetailVO>> getLikeByUserId(@RequestBody DynamicPageReqVO pageReqVO){
        PageResult<DynamicDetailVO> pageResult = dynamicService.like(pageReqVO);
        return CommonResult.success(pageResult);
    }

    @Operation(summary = "获取关注用户的动态")
    @PostMapping("/list/follow")
    public CommonResult<PageResult<DynamicDetailVO>> getFollowList(@RequestBody DynamicPageReqVO pageReqVO){
        PageResult<DynamicDetailVO> pageResult = dynamicService.getFollowerList(pageReqVO);
        return CommonResult.success(pageResult);
    }

    @Operation(summary = "获取用户发布的动态")
    @PostMapping("/list/user")
    public CommonResult<PageResult<DynamicDetailVO>> getUserDynamicList(@RequestBody DynamicPageReqVO pageReqVO){
        PageResult<DynamicDetailVO> pageResult = dynamicService.getUserDynamicList(pageReqVO);
        return CommonResult.success(pageResult);
    }
}
