package cn.karuhara.etalk.content.convert;

import cn.karuhara.etalk.content.controller.admin.dynamic.vo.CommentAdminDetailVO;
import cn.karuhara.etalk.content.controller.admin.dynamic.vo.CommentUpdateVO;
import cn.karuhara.etalk.content.controller.app.dynamic.vo.CommentDetailVO;
import cn.karuhara.etalk.content.controller.app.dynamic.vo.CommentLikeVO;
import cn.karuhara.etalk.content.controller.app.dynamic.vo.CommentPushVO;
import cn.karuhara.etalk.content.entity.dynamic.EasyDynamicComment;
import cn.karuhara.etalk.content.entity.dynamic.EasyDynamicCommentLike;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface CommentConvert {
    CommentConvert INSTANT = Mappers.getMapper(CommentConvert.class);

    List<CommentDetailVO> commentsToDetailVO(List<EasyDynamicComment> comments);

    EasyDynamicComment pushVOToComment(CommentPushVO commentPushVO);

    List<CommentAdminDetailVO> commentsToAdminDetailVO(List<EasyDynamicComment> comments);

    EasyDynamicComment updateVOToComment(CommentUpdateVO updateVO);

    EasyDynamicCommentLike likeVOToComment(CommentLikeVO likeVO);
}
