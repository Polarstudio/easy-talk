package cn.karuhara.etalk.content.mq.product;

import cn.karuhara.etalk.content.controller.admin.circle.vo.CircleDeleteVO;
import cn.karuhara.etalk.content.entity.circle.EasyCircle;
import cn.karuhara.etalk.content.entity.circle.EasyCircleFollow;
import cn.karuhara.etalk.content.service.circle.EasyCircleFollowService;
import cn.karuhara.etalk.content.service.circle.EasyCircleService;
import cn.karuhara.etalk.framework.kafka.core.Event;
import cn.karuhara.etalk.framework.kafka.core.EventProducer;
import cn.karuhara.etalk.framework.kafka.core.MessageConstants;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CircleProducer {
    @Resource
    private EventProducer eventProducer;

    @Resource
    private EasyCircleFollowService followService;

    @Resource
    private EasyCircleService circleService;

    public void deleteMessage(CircleDeleteVO deleteVO){
        EasyCircle circle = circleService.getById(deleteVO.getId());
        //获取所有关注了这个圈子的用户，提示他们这个圈子被删除了
        LambdaQueryWrapper<EasyCircleFollow> followWrapper = new LambdaQueryWrapper<>();
        followWrapper.eq(EasyCircleFollow::getCircleId,deleteVO.getId());
        List<EasyCircleFollow> circleFollows = followService.list(followWrapper);
        List<Integer> userIds = circleFollows.stream().map(EasyCircleFollow::getUserId).collect(Collectors.toList());
        Event event = new Event();
        //设置话题类型
        event.setTopic(MessageConstants.CIRCLE_DELETE_TOPIC);
        //设置通知的用户
        if (deleteVO.getUserId()!=null){
            event.setToUserId(deleteVO.getUserId());
        }
        //设置消息的内容
        if (deleteVO.getReason()!=null){
            event.setContent(deleteVO.getReason());
        }
        //这是其它的额外扩展的参数
        event.setDataMap(MessageConstants.POST_ID,deleteVO.getId());
        event.setDataMap(MessageConstants.FOLLOW_IDS,userIds);
        event.setDataMap(MessageConstants.CIRCLE_NAME,circle.getCircleName());
        eventProducer.fireEvent(event);
    }
}
