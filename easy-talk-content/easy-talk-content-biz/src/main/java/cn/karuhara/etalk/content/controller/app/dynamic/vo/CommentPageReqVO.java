package cn.karuhara.etalk.content.controller.app.dynamic.vo;

import cn.karuhara.etalk.framework.common.pojo.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Schema(description = "分页获取评论的VO")
@Data
public class CommentPageReqVO extends PageParam {
    @Schema(description = "动态id")
    private Integer dynamicId;

    @Schema(description = "回复的评论id,用于获取子评论")
    private Integer replyCommentID;
}
