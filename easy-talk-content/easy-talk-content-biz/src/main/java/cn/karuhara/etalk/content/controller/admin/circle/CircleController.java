package cn.karuhara.etalk.content.controller.admin.circle;

import cn.karuhara.etalk.content.controller.admin.circle.vo.*;
import cn.karuhara.etalk.content.controller.app.circle.vo.CircleAppBriefVO;
import cn.karuhara.etalk.content.entity.circle.EasyCircle;
import cn.karuhara.etalk.content.mq.product.CircleProducer;
import cn.karuhara.etalk.content.service.circle.EasyCircleService;
import cn.karuhara.etalk.framework.annotation.WeLog;
import cn.karuhara.etalk.framework.common.pojo.CommonResult;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import cn.karuhara.etalk.framework.kafka.core.Event;
import cn.karuhara.etalk.framework.kafka.core.MessageConstants;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "管理后台 - 圈子模块")
@RestController
@RequestMapping("/content/circle/admin")
public class CircleController {

    @Resource
    private EasyCircleService service;

    @Resource
    private CircleProducer circleProducer;

    @Operation(summary = "新增圈子")
    @WeLog(description = "新增圈子操作")
    @PostMapping
    public CommonResult<Void> add(@RequestBody CircleAddReqVO circleAddVO) {
        service.addCircle(circleAddVO);
        return CommonResult.success();
    }

    @Operation(summary = "分页获取圈子")
    @PostMapping("/page")
    public CommonResult<PageResult<CirclePageRespVO>> page(@RequestBody CirclePageReqVO circlePageReqVO) {
        PageResult<CirclePageRespVO> pageList= service.getPageList(circlePageReqVO);
        return CommonResult.success(pageList);
    }

    @Operation(summary = "修改圈子")
    @WeLog(description = "修改圈子操作")
    @PutMapping
    public CommonResult<Void> update(@RequestBody CircleUpdateReqVO circleUpdateReqVO) {
        service.updateCircle(circleUpdateReqVO);
        return CommonResult.success();
    }

    @Operation(summary = "管理后台 - 获取圈子的id和名称")
    @GetMapping("/list/tags")
    public CommonResult<List<CircleAppBriefVO>> getTags(){
        return CommonResult.success(service.getAdminTags());
    }

    @Operation(summary = "删除圈子")
    @DeleteMapping
    public CommonResult<Void> delete(@RequestBody CircleDeleteVO deleteVO) {
        service.delete(deleteVO);
        return CommonResult.success();
    }

    @Operation(summary = "消息发送测试")
    @PostMapping("/message")
    public CommonResult<Void> message(){
        CircleDeleteVO deleteVO = new CircleDeleteVO();
        deleteVO.setId(8);
        deleteVO.setUserId(13);
        deleteVO.setReason("1111111");
        circleProducer.deleteMessage(deleteVO);
        return CommonResult.success();
    }
}
