package cn.karuhara.etalk.content.controller.app.dynamic;

import cn.karuhara.etalk.content.controller.admin.dynamic.vo.CommentDeleteVO;
import cn.karuhara.etalk.content.controller.app.dynamic.vo.CommentDetailVO;
import cn.karuhara.etalk.content.controller.app.dynamic.vo.CommentLikeVO;
import cn.karuhara.etalk.content.controller.app.dynamic.vo.CommentPageReqVO;
import cn.karuhara.etalk.content.controller.app.dynamic.vo.CommentPushVO;
import cn.karuhara.etalk.content.service.dynamic.EasyDynamicCommentLikeService;
import cn.karuhara.etalk.content.service.dynamic.EasyDynamicCommentService;
import cn.karuhara.etalk.framework.common.pojo.CommonResult;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/content/dynamic/app/comment")
@Tag(name = "APP - 动态的评论")
public class CommentAppController {

    @Resource
    private EasyDynamicCommentService commentService;

    @Resource
    private EasyDynamicCommentLikeService commentLikeService;

    @Operation(summary = "获取评论的列表")
    @PostMapping("/list")
    public CommonResult<PageResult<CommentDetailVO>> comment(@RequestBody CommentPageReqVO pageReqVO){
        PageResult<CommentDetailVO> pageResult = commentService.getList(pageReqVO);
        return CommonResult.success(pageResult);
    }

    @Operation(summary = "发表评论")
    @PutMapping
    public CommonResult<Void> push(@RequestBody CommentPushVO commentPushVO){
        commentService.push(commentPushVO);
        return CommonResult.success();
    }

    @Operation(summary = "删除评论")
    @DeleteMapping("/{id}")
    public CommonResult<Void> delete(@PathVariable Integer id){
        CommentDeleteVO commentDeleteVO = new CommentDeleteVO();
        commentDeleteVO.setId(id);
        commentService.delete(commentDeleteVO);
        return CommonResult.success();
    }

    @Operation(summary = "点赞或点踩评论")
    @PostMapping("/like")
    private CommonResult<Void> like(@RequestBody CommentLikeVO commentLikeVO){
        commentLikeService.like(commentLikeVO);
        return CommonResult.success();
    }

}
