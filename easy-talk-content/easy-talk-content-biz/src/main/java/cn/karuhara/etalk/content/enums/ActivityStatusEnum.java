package cn.karuhara.etalk.content.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

/**
 * 活动状态枚举类
 */
@Getter
@AllArgsConstructor
public enum ActivityStatusEnum {
    REGISTRATION("0", "报名中"),
    IN_PROGRESS("1", "进行中"),
    FINISHED("2", "已结束"),
    OFFLINE("3", "已下线"),
    REVIEWED("4","审核中"),
    REJECT("5","驳回");

    @EnumValue
    @JsonValue
    private final String code;

    private final String desc;
}
