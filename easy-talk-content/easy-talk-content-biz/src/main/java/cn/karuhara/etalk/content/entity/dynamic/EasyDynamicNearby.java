package cn.karuhara.etalk.content.entity.dynamic;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 附近动态表
 * @TableName easy_dynamic_nearby
 */
@TableName(value ="easy_dynamic_nearby")
@Data
public class EasyDynamicNearby implements Serializable {
    /**
     * 
     */
    @TableId
    private Integer id;

    /**
     * 动态ID
     */
    private Integer dynamicId;

    /**
     * 经度
     */
    private String latitude;

    /**
     * 维度
     */
    private String longitude;

    @TableLogic
    private Integer deleted;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}