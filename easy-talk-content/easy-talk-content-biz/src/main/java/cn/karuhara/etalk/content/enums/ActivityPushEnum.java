package cn.karuhara.etalk.content.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 活动首页推送枚举类
 */
@Getter
@AllArgsConstructor
public enum ActivityPushEnum {
    NORMAL("0","正常"),
    PUSH("1","推送");

    @EnumValue
    @JsonValue
    private final String code;

    private final String desc;
}
