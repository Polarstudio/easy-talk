package cn.karuhara.etalk.content.mapper.dynamic;

import cn.karuhara.etalk.content.entity.dynamic.EasyDynamicComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author atom
* @description 针对表【easy_dynamic_comment(动态评论)】的数据库操作Mapper
* @createDate 2024-11-26 21:21:49
* @Entity cn.karuhara.etalk.content.entity/dynamic.EasyDynamicComment
*/
public interface EasyDynamicCommentMapper extends BaseMapper<EasyDynamicComment> {

}




