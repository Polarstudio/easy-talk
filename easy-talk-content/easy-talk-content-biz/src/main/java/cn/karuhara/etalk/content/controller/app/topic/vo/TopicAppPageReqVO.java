package cn.karuhara.etalk.content.controller.app.topic.vo;

import cn.karuhara.etalk.framework.common.pojo.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Schema(description = "APP - 话题请求分页参数")
@Data
public class TopicAppPageReqVO extends PageParam {

    @Schema(description = "用户id，用于查询用户关注的话题")
    private Integer userId;

    @Schema(description = "话题名称")
    private String topicName;
}
