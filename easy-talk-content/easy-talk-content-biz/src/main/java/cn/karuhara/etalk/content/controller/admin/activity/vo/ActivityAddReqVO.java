package cn.karuhara.etalk.content.controller.admin.activity.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "管理后台 - 新增活动VO")
public class ActivityAddReqVO extends ActivityBaseVO {
}
