package cn.karuhara.etalk.content.service.dynamic.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.karuhara.etalk.content.entity.dynamic.EasyDynamicNearby;
import cn.karuhara.etalk.content.service.dynamic.EasyDynamicNearbyService;
import cn.karuhara.etalk.content.mapper.dynamic.EasyDynamicNearbyMapper;
import org.springframework.stereotype.Service;

/**
* @author atom
* @description 针对表【easy_dynamic_nearby(附近动态表)】的数据库操作Service实现
* @createDate 2024-11-26 21:21:49
*/
@Service
public class EasyDynamicNearbyServiceImpl extends ServiceImpl<EasyDynamicNearbyMapper, EasyDynamicNearby>
    implements EasyDynamicNearbyService{

}




