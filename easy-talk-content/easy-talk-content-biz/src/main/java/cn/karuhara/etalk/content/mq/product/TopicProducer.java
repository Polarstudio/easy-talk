package cn.karuhara.etalk.content.mq.product;

import cn.karuhara.etalk.content.controller.admin.topic.vo.TopicDeleteVO;
import cn.karuhara.etalk.content.entity.topic.EasyTopic;
import cn.karuhara.etalk.content.entity.topic.EasyTopicFollow;
import cn.karuhara.etalk.content.service.topic.EasyTopicFollowService;
import cn.karuhara.etalk.content.service.topic.EasyTopicService;
import cn.karuhara.etalk.framework.kafka.core.Event;
import cn.karuhara.etalk.framework.kafka.core.EventProducer;
import cn.karuhara.etalk.framework.kafka.core.MessageConstants;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 话题通知相关
 */
@Component
public class TopicProducer {
    @Resource
    private EventProducer eventProducer;

    @Resource
    private EasyTopicService topicService;

    @Resource
    private EasyTopicFollowService followService;

    public void deleteMessage(TopicDeleteVO deleteVO){
        EasyTopic topic = topicService.getById(deleteVO.getId());
        //获取所有关注了这个话题的用户，来提醒它们话题已被删除
        LambdaQueryWrapper<EasyTopicFollow> followQueryWrapper = new LambdaQueryWrapper<>();
        followQueryWrapper.eq(EasyTopicFollow::getTopicId, deleteVO.getId());
        List<EasyTopicFollow> follows = followService.list(followQueryWrapper);
        List<Integer> userIds = follows.stream().map(EasyTopicFollow::getUserId).collect(Collectors.toList());
        Event event = new Event();
        //设置消息类型
        event.setTopic(MessageConstants.TOPIC_DELETE_TOPIC);
        //设置通知的用户
        if (deleteVO.getUserId()!=null){
            event.setToUserId(deleteVO.getUserId());
        }
        //设置消息的内容
        if (deleteVO.getReason()!=null){
            event.setContent(deleteVO.getReason());
        }
        //这是其它的额外扩展的参数
        event.setDataMap(MessageConstants.POST_ID,deleteVO.getId());
        event.setDataMap(MessageConstants.FOLLOW_IDS,userIds);
        event.setDataMap(MessageConstants.TOPIC_NAME,topic.getTopicName());
        eventProducer.fireEvent(event);
    }
}
