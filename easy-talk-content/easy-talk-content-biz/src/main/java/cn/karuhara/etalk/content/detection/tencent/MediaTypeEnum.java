package cn.karuhara.etalk.content.detection.tencent;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 审核的类型
 */
@Getter
@AllArgsConstructor
public enum MediaTypeEnum {
    AUDIO("1","音频"),
    IMAGE("2","图片");

    @EnumValue
    @JsonValue
    private final String code;

    private final String desc;
}
