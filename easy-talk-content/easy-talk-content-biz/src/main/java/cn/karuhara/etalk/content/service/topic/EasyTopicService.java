package cn.karuhara.etalk.content.service.topic;

import cn.karuhara.etalk.content.controller.admin.topic.vo.TopicBaseVO;
import cn.karuhara.etalk.content.controller.admin.topic.vo.TopicDeleteVO;
import cn.karuhara.etalk.content.controller.admin.topic.vo.TopicPageReqVO;
import cn.karuhara.etalk.content.controller.admin.topic.vo.TopicPageRespVO;
import cn.karuhara.etalk.content.controller.app.circle.vo.CircleAppBaseVO;
import cn.karuhara.etalk.content.controller.app.topic.vo.*;
import cn.karuhara.etalk.content.entity.topic.EasyTopic;
import cn.karuhara.etalk.framework.common.model.mess.TopicStreamMess;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author atom
* @description 针对表【easy_topic(话题表)】的数据库操作Service
* @createDate 2024-11-18 16:38:17
*/
public interface EasyTopicService extends IService<EasyTopic> {

    /**
     * 新增话题
     * @param topicBaseVO 话题参数
     */
    void addTopic(TopicBaseVO topicBaseVO);

    /**
     * 分页获取话题
     * @param topicPageReqVO 分页参数
     * @return 话题集合
     */
    PageResult<TopicPageRespVO> getPageList(TopicPageReqVO topicPageReqVO);

    /**
     * 修改菜单那
     * @param topicBaseVO 话题参数
     */
    void updateTopic(TopicBaseVO topicBaseVO);

    /**
     * 获取推荐的话题
     * @return 话题的基本信息
     */
    List<TopicAppBaseVO> getRecommend();

    /**
     * 分页获取话题
     * @param pageParam 分页参数
     * @return 分页结果
     */
    PageResult<TopicAppDetailVO> gteList(TopicAppPageReqVO pageParam);

    /**
     * 创建或编辑话题
     * @param topicAppBaseVO 话题信息
     */
    void appAddTopic(TopicAppBaseVO topicAppBaseVO);

    /**
     * 获取用户关注的话题
     * @return 话题分页信息
     */
    PageResult<TopicAppDetailVO> getFollowList(TopicAppPageReqVO pageParam);

    /**
     * 获取话题的详情
     * @param id 话题id
     * @return 话题详细信息
     */
    TopicAppDetailVO getDetail(Integer id);

    /**
     * 获取话题的id和名称
     * @return list
     */
    PageResult<TopicAppBriefVO> getTags(TopicAppPageReqVO pageReqVO);



    /**
     * 管理后台 - 获取话题的id和名称
     * @return
     */
    List<TopicAppBriefVO> getAdminTags();

    /**
     * 删除话题
     */
    void delete(TopicDeleteVO topicDeleteVO);

    /**
     * 更新话题的分值
     * @param topicStreamMess
     */
    void updateScore(TopicStreamMess topicStreamMess);

    /**
     * 获取热榜话题数据
     * @return 话题数据
     */
    List<TopicHotVO> getHotTopic();
}
