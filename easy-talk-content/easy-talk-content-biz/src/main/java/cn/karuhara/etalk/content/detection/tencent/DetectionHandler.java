package cn.karuhara.etalk.content.detection.tencent;

import cn.karuhara.etalk.content.enums.DetectionTypeEnum;
import cn.karuhara.etalk.content.enums.DynamicTypeEnum;
import cn.karuhara.etalk.content.enums.MsgSecCheckResultEnum;
import cn.karuhara.etalk.content.mq.product.CommentProducer;
import cn.karuhara.etalk.content.mq.product.DynamicProducer;
import cn.karuhara.etalk.framework.common.constants.RedisConstant;
import cn.karuhara.etalk.fremawork.redis.core.MyRedisUtil;
import cn.karuhara.etalk.system.api.ConfigApi;
import cn.karuhara.etalk.system.api.WxApi;
import com.alibaba.fastjson2.JSONObject;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 内容安全审查处理器
 */
@Component
@Slf4j
public class DetectionHandler {

    OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(15, TimeUnit.SECONDS)
            .build();

    @Resource
    private WxApi wxApi;

    @Resource
    MyRedisUtil myRedisUtil;

    @Resource
    private DynamicProducer dynamicProducer;

    @Resource
    private CommentProducer commentProducer;

    public void detection(DetectionEntity detectionEntity) {
        log.info("detection start:{}", JSONObject.toJSONString(detectionEntity));
        DetectionResult detectionResult = new DetectionResult();
        detectionResult.setPostId(detectionEntity.getPostId());
        detectionResult.setResult(MsgSecCheckResultEnum.PASS);
        //审核文字
        List<String> texts =  detectionEntity.getTexts();
        List<MsgSecCheckResult> msgSecCheckResults = textDetection(texts,detectionEntity.getWxOpenid());
        for (MsgSecCheckResult msgSecCheckResult : msgSecCheckResults) {
            if (msgSecCheckResult.getResult().getSuggest() == MsgSecCheckResultEnum.RISKY) {
                detectionResult.setResult(MsgSecCheckResultEnum.RISKY);
                break;
            } else if (msgSecCheckResult.getResult().getSuggest() == MsgSecCheckResultEnum.REVIEW) {
                detectionResult.setResult(MsgSecCheckResultEnum.REVIEW);
            }
        }
        if (detectionResult.getResult() == MsgSecCheckResultEnum.REVIEW){
            return;
        }
        switch (detectionEntity.getType()) {
            case DYNAMIC: dynamicProducer.detectionMessage(detectionResult); break;
            case COMMENT: commentProducer.detectionMessage(detectionResult); break;
        }
        //如果文字审核不通过，那么就直接通知用户并修改动态状态，不需要审核图片或音频了
        if (detectionResult.getResult() != MsgSecCheckResultEnum.PASS || detectionEntity.getImages().isEmpty()){
            return;
        }
        //审核图片
        List<MediaCheckResult> mediaCheckResults = mediaCheckAsync(detectionEntity.getImages(), detectionEntity.getWxOpenid(), MediaTypeEnum.IMAGE);
        //将返回的tradeId存入redis中
        //用于存入该审核（如动态）的媒体文件（如图片、音频等）的所有tradeId
        String postTradeIds = MyRedisUtil.buildKey(RedisConstant.DETECTION_MEDIA, RedisConstant.POST_ID+":"+detectionEntity.getPostId());
        mediaCheckResults.forEach(mediaCheckResult -> {
            String detectionKey = MyRedisUtil.buildKey(RedisConstant.DETECTION_MEDIA, RedisConstant.TRACE_ID+":"+mediaCheckResult.getTraceId());
            myRedisUtil.set(detectionKey, detectionEntity.getType().getCode()+"."+detectionEntity.getPostId(),3600);
            myRedisUtil.hSet(postTradeIds, mediaCheckResult.getTraceId(), "0",3600);
        });
    }

    //文本审核
    public List<MsgSecCheckResult> textDetection(List<String> texts,String openId) {
        //获取access_token,先从从redis中查询
        String accessToken;
        String accessKey = MyRedisUtil.buildKey(RedisConstant.WX_ACCESS_TOKEN);
        Object object = myRedisUtil.get(accessKey);
        if (object != null) {
            accessToken = (String) object;
        }else {
            accessToken = wxApi.getAccessToken();
        }
        List<MsgSecCheckResult> msgSecCheckResults = new ArrayList<>();
        texts.forEach(text->{
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("content",text);
            jsonObject.put("version","2");
            jsonObject.put("scene","2");
            jsonObject.put("openid",openId);
            RequestBody requestBody = RequestBody
                    .create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
            Request request = new Request.Builder()
                    .url("https://api.weixin.qq.com/wxa/msg_sec_check?access_token="+accessToken)
                    .post(requestBody)
                    .build();
            Call call = client.newCall(request);
            Response response;
            String responseString;
            try {
                response = call.execute();
                responseString = response.body().string();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            MsgSecCheckResult msgSecCheckResult = JSONObject.parseObject(responseString, MsgSecCheckResult.class);
            msgSecCheckResults.add(msgSecCheckResult);
        });
        return msgSecCheckResults;
    }

    //审核图片、音频
    public List<MediaCheckResult> mediaCheckAsync(List<String> urls, String openId, MediaTypeEnum typeEnum){
        //获取access_token,先从从redis中查询
        String accessToken;
        String accessKey = MyRedisUtil.buildKey(RedisConstant.WX_ACCESS_TOKEN);
        Object object = myRedisUtil.get(accessKey);
        if (object != null) {
            accessToken = (String) object;
        }else {
            accessToken = wxApi.getAccessToken();
        }
        List<MediaCheckResult> mediaCheckResults = new ArrayList<>();
        //需要提交待审核音频、图片的url
        urls.forEach(url->{
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("media_url",url);
            jsonObject.put("media_type",Integer.valueOf(typeEnum.getCode()));
            jsonObject.put("scene","3");
            jsonObject.put("version",2);
            jsonObject.put("openid",openId);
            RequestBody requestBody = RequestBody
                    .create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
            Request request = new Request.Builder()
                    .url("https://api.weixin.qq.com/wxa/media_check_async?access_token="+accessToken)
                    .post(requestBody)
                    .build();
            Call call = client.newCall(request);
            Response response;
            String responseString;
            try {
                response = call.execute();
                responseString = response.body().string();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            MediaCheckResult mediaCheckResult = JSONObject.parseObject(responseString, MediaCheckResult.class);
            mediaCheckResults.add(mediaCheckResult);
        });
        return mediaCheckResults;
    }
}
