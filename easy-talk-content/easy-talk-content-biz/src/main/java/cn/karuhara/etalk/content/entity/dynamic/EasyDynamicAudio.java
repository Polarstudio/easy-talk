package cn.karuhara.etalk.content.entity.dynamic;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import lombok.Data;

/**
 * 动态音频
 * @TableName easy_dynamic_audio
 */
@TableName(value ="easy_dynamic_audio")
@Data
public class EasyDynamicAudio implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 动态ID
     */
    private Integer dynamicId;

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 音频封面
     */
    private String audioCover;

    /**
     * 音频地址
     */
    private String audioUrl;

    /**
     * 音频名称
     */
    private String audioName;

    /**
     * 介绍
     */
    private String audioIntro;

    /**
     * 大小
     */
    private String audioSize;

    /**
     * 创建时间
     */
    private Long createTime;

    /**
     * 更新时间
     */
    private Long updateTime;

    /**
     * 逻辑删除
     */
    @TableLogic
    private Integer deleted;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}