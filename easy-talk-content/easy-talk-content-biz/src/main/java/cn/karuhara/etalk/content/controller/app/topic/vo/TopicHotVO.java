package cn.karuhara.etalk.content.controller.app.topic.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "话题热度")
@Data
public class TopicHotVO {

    @Schema(description = "Id",example = "1")
    private Integer id;

    @Schema(description = "话题名称",example = "轻语社怎么样")
    private String topicName;

    @Schema(description = "话题热度")
    private Double score;
}
