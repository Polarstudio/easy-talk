package cn.karuhara.etalk.content.controller.app.activity.vo;

import cn.karuhara.etalk.content.enums.ActivityUserStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.time.LocalDateTime;

@Schema(description = "活动报名相关信息")
@Data
public class ActivityUserInfoVO {

    @Schema(description = "UUID")
    private String uuid;

    @Schema(description = "参与情况")
    private ActivityUserStatus status;

    @Schema(description = "活动ID")
    private Integer activityId;

    @Schema(description = "活动标题")
    private String name;

    @Schema(description = "活动开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private LocalDateTime startTime;

    @Schema(description = "活动结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private LocalDateTime endTime;

    @Schema(description = "地点名称")
    private String placeName;
}
