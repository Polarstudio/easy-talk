package cn.karuhara.etalk.content.controller.app.circle.vo;

import cn.karuhara.etalk.content.controller.app.dynamic.vo.DynamicAppBriefVO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "App - 获取推送的圈子信息")
public class CircleAppPushVO extends CircleAppBaseVO {
    @Schema(description = "圈子的动态数量")
    private Long dynamicCount;

    @Schema(description = "圈子的人数")
    private Long memberCount;
}
