package cn.karuhara.etalk.content.convert;

import cn.karuhara.etalk.content.controller.admin.dynamic.vo.DynamicUpdateVO;
import cn.karuhara.etalk.content.controller.app.dynamic.vo.*;
import cn.karuhara.etalk.content.entity.dynamic.EasyDynamic;
import cn.karuhara.etalk.content.entity.dynamic.EasyDynamicAudio;
import cn.karuhara.etalk.content.entity.dynamic.EasyDynamicVideo;
import cn.karuhara.etalk.member.api.dto.MemberDynamicInfoDTO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface DynamicConvert {
    DynamicConvert INSTANT = Mappers.getMapper(DynamicConvert.class);

    DynamicDetailVO dynamicToDetailVO(EasyDynamic easyDynamic);

    void addDynamicInfoToDynamicDetailVO(@MappingTarget DynamicDetailVO dynamicDetailVO, MemberDynamicInfoDTO memberDynamicInfoDTO);

    DynamicAppAttachmentVO dynamicAudioToAttachmentVO(EasyDynamicAudio dynamicAudio);

    DynamicAppAttachmentVO dynamicVideoToAttachmentVO(EasyDynamicVideo dynamicVideo);

    List<DynamicBaseVO> dynamicListToBaseVOs(List<EasyDynamic> dynamics);

    List<DynamicAppBriefVO> dynamicListToBriefList(List<EasyDynamic> dynamics);

    List<DynamicDetailVO> dynamicListToDetailVO(List<EasyDynamic> dynamics);

    EasyDynamic addVOToDynamic(DynamicAddVO appAddVO);

    EasyDynamic updateVOToDynamic(DynamicUpdateVO appUpdateVO);
}
