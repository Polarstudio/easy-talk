package cn.karuhara.etalk.content.mapper.topic;

import cn.karuhara.etalk.content.entity.topic.EasyTopic;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author atom
* @description 针对表【easy_topic(话题表)】的数据库操作Mapper
* @createDate 2024-11-18 16:38:17
* @Entity cn.karuhara.etalk.content.entity.EasyTopic
*/
public interface EasyTopicMapper extends BaseMapper<EasyTopic> {

}




