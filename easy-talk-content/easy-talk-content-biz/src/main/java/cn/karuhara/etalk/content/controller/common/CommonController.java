package cn.karuhara.etalk.content.controller.common;

import cn.karuhara.etalk.framework.common.pojo.CommonResult;
import cn.karuhara.etalk.framework.oss.adapter.StorageAdapter;
import com.tencentcloudapi.common.exception.TencentCloudSDKException;
import com.tencentcloudapi.tms.v20200713.models.TextModerationResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Calendar;
import java.util.TimeZone;

@Tag(name = "公共控制器模块")
@RestController
@RequestMapping("/content/common")
public class CommonController {

    @Resource
    private StorageAdapter storageAdapter;

    @Value("${oss.bucket}")
    private String bucket;

    @PostMapping("/upload")
    @Operation(summary = "文件上传接口")
    public CommonResult<String> upload(@RequestPart MultipartFile file){
        //获取当前日期做目录
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        ZoneId zoneId = ZoneId.ofOffset("GMT", ZoneOffset.ofHours(8));
        TimeZone timeZone = TimeZone.getTimeZone(zoneId);
        df.setTimeZone(timeZone);
        String objectName = df.format(cal.getTime());
        storageAdapter.uploadFile(file,bucket,objectName);
        objectName = objectName + "/" + file.getOriginalFilename();
        return CommonResult.success(storageAdapter.getUrl(bucket, objectName));
    }
}
