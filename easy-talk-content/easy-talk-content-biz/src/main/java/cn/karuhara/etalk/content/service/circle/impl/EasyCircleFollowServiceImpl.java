package cn.karuhara.etalk.content.service.circle.impl;

import cn.karuhara.etalk.content.entity.circle.EasyCircleFollow;
import cn.karuhara.etalk.content.mapper.circle.EasyCircleFollowMapper;
import cn.karuhara.etalk.content.service.circle.EasyCircleFollowService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
* @author atom
* @description 针对表【easy_circle_follow(圈子用户)】的数据库操作Service实现
* @createDate 2024-11-17 20:25:04
*/
@Service
public class EasyCircleFollowServiceImpl extends ServiceImpl<EasyCircleFollowMapper, EasyCircleFollow>
    implements EasyCircleFollowService {

    /**
     * 加入或退出圈子
     *
     * @param circleId 圈子id
     * @param userId   用户id
     */
    @Override
    public void joinOrCanalCircle(Integer circleId, Integer userId) {
        LambdaQueryWrapper<EasyCircleFollow> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(EasyCircleFollow::getCircleId, circleId);
        lambdaQueryWrapper.eq(EasyCircleFollow::getUserId, userId);
        EasyCircleFollow circleFollow = getOne(lambdaQueryWrapper);
        if (circleFollow == null) {
            circleFollow = new EasyCircleFollow();
            circleFollow.setCircleId(circleId);
            circleFollow.setUserId(userId);
            save(circleFollow);
        }else {
            remove(lambdaQueryWrapper);
        }
    }

    /**
     * 查询用户是否加入某个圈子
     *
     * @param circleId 圈子id
     * @param userId   用户id
     * @return 是否加入
     */
    @Override
    public Boolean joinCheck(Integer circleId, Integer userId) {
        LambdaQueryWrapper<EasyCircleFollow> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(EasyCircleFollow::getCircleId, circleId);
        lambdaQueryWrapper.eq(EasyCircleFollow::getUserId, userId);
        EasyCircleFollow easyCircleFollow = getOne(lambdaQueryWrapper);
        return easyCircleFollow != null;
    }
}




