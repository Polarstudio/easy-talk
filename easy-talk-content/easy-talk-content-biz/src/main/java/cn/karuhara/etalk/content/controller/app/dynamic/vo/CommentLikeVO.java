package cn.karuhara.etalk.content.controller.app.dynamic.vo;

import cn.karuhara.etalk.content.enums.CommentEnum;
import cn.karuhara.etalk.content.enums.CommentLikeTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "点赞评论")
@Data
public class CommentLikeVO {
    @Schema(description = "动态的id")
    private Integer dynamicId;
    @Schema(description = "评论ID")
    private Integer commentId;
    @Schema(description = "用户ID")
    private Integer userId;
    @Schema(description = "类型")
    private CommentEnum type;
}
