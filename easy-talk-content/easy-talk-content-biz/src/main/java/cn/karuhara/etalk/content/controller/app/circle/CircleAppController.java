package cn.karuhara.etalk.content.controller.app.circle;

import cn.karuhara.etalk.content.controller.admin.circle.vo.CircleAddReqVO;
import cn.karuhara.etalk.content.controller.admin.circle.vo.CircleDeleteVO;
import cn.karuhara.etalk.content.controller.app.circle.vo.*;
import cn.karuhara.etalk.content.service.circle.EasyCircleFollowService;
import cn.karuhara.etalk.content.service.circle.EasyCircleService;
import cn.karuhara.etalk.framework.common.pojo.CommonResult;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.List;
@Tag(name = "APP - 平台圈子")
@RestController
@Validated
@RequestMapping("/content/circle/app")
public class CircleAppController {

    @Resource
    private EasyCircleService circleService;

    @Resource
    private EasyCircleFollowService followService;

    @Operation(summary = "分页获取圈子")
    @PostMapping("/list")
    public CommonResult<PageResult<CircleAppDetailVO>> getCircleList(@Valid @RequestBody CircleAppPageReqVO appPageReqVO) {
        PageResult<CircleAppDetailVO> pageResult =  circleService.getAppPageList(appPageReqVO);
        return CommonResult.success(pageResult);
    }

    @Operation(summary = "获取所有的推送的圈子")
    @GetMapping("/push")
    public CommonResult<List<CircleAppPushVO>> getPush(){
        List<CircleAppPushVO> pushVOS = circleService.getPushList();
        return CommonResult.success(pushVOS);
    }

    @Operation(summary = "分页获取用户关注的圈子")
    @PostMapping("/list/follow")
    public CommonResult<PageResult<CircleAppDetailVO>> getFollowList(@Valid @RequestBody CircleAppPageReqVO appPageReqVO){
        PageResult<CircleAppDetailVO> pageResult = circleService.getFollowList(appPageReqVO);
        return CommonResult.success(pageResult);
    }

    @Operation(summary = "获取推荐的圈子")
    @GetMapping("/recommend")
    public CommonResult<List<CircleAppRecommendVO>> getRecommend(){
        List<CircleAppRecommendVO> recommendVOS = circleService.getRecommend();
        return CommonResult.success(recommendVOS);
    }

    @Operation(summary = "根据圈子的id获取圈子的信息")
    @GetMapping("/{id}")
    public CommonResult<CircleAppDetailVO> getAppDetail(@PathVariable Integer id){
        CircleAppDetailVO circleAppDetailVO = circleService.getByCircleId(id);
        return CommonResult.success(circleAppDetailVO);
    }

    @Operation(summary = " 添加或编辑圈子")
    @PostMapping
    public CommonResult<Void> add(@Valid @RequestBody CircleAddReqVO addReqVO){
        circleService.addCircleApp(addReqVO);
        return CommonResult.success();
    }

    @Operation(summary = "获取圈子的id和名称")
    @PostMapping("/list/tags")
    public CommonResult<PageResult<CircleAppBriefVO>> getTags(@RequestBody CircleAppPageReqVO appPageReqVO){
        PageResult<CircleAppBriefVO> pageResult = circleService.getTags(appPageReqVO);
        return CommonResult.success(pageResult);
    }

    @Operation(summary = "获取用户加入的圈子的数量")
    @GetMapping("/attend/count")
    public CommonResult<Long> getAttendCount(){
        Long count = circleService.getUserAttendCount();
        return CommonResult.success(count);
    }

    @Operation(summary = "加入或退出圈子")
    @PutMapping("/{circleId}/{userId}")
    public CommonResult<Void> joinOrCanalCircle(@PathVariable("circleId") @Schema(description = "圈子id") Integer circleId,
                                                @PathVariable("userId") @Schema(description = "用户id") Integer userId){
        followService.joinOrCanalCircle(circleId,userId);
        return CommonResult.success();
    }

    @Operation(summary = "查询用户是否加入某个圈子")
    @GetMapping("/join/{circleId}/{userId}")
    public CommonResult<Boolean> joinCheck(@PathVariable("circleId") Integer circleId, @PathVariable("userId") Integer userId){
        return CommonResult.success(followService.joinCheck(circleId,userId));
    }

    @Operation(summary = "删除圈子")
    @DeleteMapping("/{id}")
    public CommonResult<Void> remove(@PathVariable("id") @Schema(description = "圈子id") Integer id){
        CircleDeleteVO circleDeleteVO = new CircleDeleteVO();
        circleDeleteVO.setId(id);
        circleService.delete(circleDeleteVO);
        return CommonResult.success();
    }
}
