package cn.karuhara.etalk.content.entity.dynamic;

import cn.karuhara.etalk.framework.mybatis.core.dataobject.BaseDO;
import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 动态图片
 * @TableName easy_dynamic_img
 */
@EqualsAndHashCode(callSuper = true)
@TableName(value ="easy_dynamic_img")
@Data
public class EasyDynamicImg extends BaseDO implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 动态ID
     */
    private Integer dynamicId;

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 地址
     */
    private String imgUrl;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}