package cn.karuhara.etalk.content.controller.app.topic.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

@Data
@Schema(description = "APP - 话题信息")
public class TopicAppBaseVO {

    @Schema(description = "Id",example = "1")
    private Integer id;

    @Schema(description = "话题名称",example = "轻语社怎么样")
    @NotNull(message = "话题名称不能为空！")
    @Length(max = 20, message = "话题名称不得大于20的字符!")
    private String topicName;

    @Schema(description = "话题描述",example = "这是一个话题描述")
    @NotNull(message = "")
    private String intro;
}
