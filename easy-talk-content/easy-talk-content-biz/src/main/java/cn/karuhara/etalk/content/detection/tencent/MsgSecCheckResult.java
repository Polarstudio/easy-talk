package cn.karuhara.etalk.content.detection.tencent;

import cn.karuhara.etalk.content.enums.MsgSecCheckResultEnum;
import com.alibaba.fastjson2.annotation.JSONField;
import lombok.Data;

import java.util.List;

@Data
public class MsgSecCheckResult {
    // 错误码，用于表示整体检测操作返回的错误代码
    private int errCode;
    // 错误信息，对出现错误情况的具体文字描述
    private String errMsg;
    // 详细检测结果列表，每个元素包含了更细致的检测相关信息
    private List<Detail> detail;

    private Result result;

    // 内部静态类，用于表示详细检测结果中的各项具体内容
    @Data
    public static class Detail {
        // 策略类型，例如可能对应不同的检测策略分类等
        @JSONField(name = "strategy")
        private String strategy;
        // 详细的错误码，仅当该值为0时，该项结果有效（根据给定说明）
        @JSONField(name = "errcode")
        private int errcode;
        // 建议值，有risky、pass、review三种可能的取值，表示对检测内容的相应建议
        @JSONField(name = "suggest")
        private String suggest;
        // 命中标签枚举值，不同数值对应不同的标签分类，如100表示正常，10001表示广告等等
        @JSONField(name = "label")
        private int label;
        // 命中的自定义关键词，即检测出与特定自定义关键词匹配的内容
        @JSONField(name = "keyword")
        private String keyword;
        // 置信度，取值范围在0 - 100之间，数值越高代表越有可能属于当前返回的标签（label）所对应的类别
        @JSONField(name = "prob")
        private double prob;
        // 唯一请求标识，用于标记单次请求，方便追踪和区分不同的检测请求
        @JSONField(name = "trace_id")
        private String traceId;
    }

    // 内部静态类，用于表示综合结果相关的属性信息
    @Data
    public static class Result {
        // 建议，同样有risky、pass、review三种取值，从综合角度给出对检测内容的建议
        @JSONField(name = "suggest")
        private MsgSecCheckResultEnum suggest;
        // 命中标签枚举值，从综合层面判断的命中标签情况，与Detail类中的label类似但代表综合结果
        @JSONField(name = "label")
        private int label;
    }
}
