package cn.karuhara.etalk.content.controller.app.circle.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "圈子详细信息")
public class CircleAppDetailVO extends CircleAppBaseVO {

    @Schema(description = "圈子创建人")
    private String userId;

    @Schema(description = "创建人昵称")
    private String nikeName;

    @Schema(description = "创建人头像")
    private String avatar;

    @Schema(description = "创建人类型")
    private String userType;

    @Schema(description = "圈子的动态数量")
    private Long dynamicCount;

    @Schema(description = "圈子最后更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private LocalDateTime updateDynamicTime;

    @Schema(description = "圈子的人数")
    private Long memberCount;
}
