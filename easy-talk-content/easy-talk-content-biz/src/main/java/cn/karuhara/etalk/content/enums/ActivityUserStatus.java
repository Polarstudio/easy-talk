package cn.karuhara.etalk.content.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 活动首页推送枚举类
 */
@Getter
@AllArgsConstructor
public enum ActivityUserStatus {
    NOTATTEND("0","未参加"),
    ATTEND("1","参加");

    @EnumValue
    @JsonValue
    private final String code;

    private final String desc;
}
