package cn.karuhara.etalk.content.controller.app.circle.vo;

import cn.karuhara.etalk.framework.common.pojo.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Schema(description = "分页获取圈子")
@Data
public class CircleAppPageReqVO extends PageParam {

    @Schema(description = "用户的id,用于获取用户创建的圈子")
    private Integer userId;

    @Schema(description = "状态：0 正常、1 推送、2 隐藏")
    private String status;

    @Schema(description = "圈子名称")
    private String circleName;
}
