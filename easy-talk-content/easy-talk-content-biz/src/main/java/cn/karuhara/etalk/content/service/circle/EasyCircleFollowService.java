package cn.karuhara.etalk.content.service.circle;

import cn.karuhara.etalk.content.entity.circle.EasyCircleFollow;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author atom
* @description 针对表【easy_circle_follow(圈子用户)】的数据库操作Service
* @createDate 2024-11-17 20:25:04
*/
public interface EasyCircleFollowService extends IService<EasyCircleFollow> {

    /**
     * 加入或退出圈子
     * @param circleId 圈子id
     * @param userId 用户id
     */
    void joinOrCanalCircle(Integer circleId, Integer userId);

    /**
     * 查询用户是否加入某个圈子
     * @param circleId 圈子id
     * @param userId 用户id
     * @return 是否加入
     */
    Boolean joinCheck(Integer circleId, Integer userId);
}
