package cn.karuhara.etalk.content.controller.app.dynamic.vo;

import cn.karuhara.etalk.content.enums.DynamicStatusEnum;
import cn.karuhara.etalk.content.enums.DynamicTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "基础动态信息")
@Data
public class DynamicBaseVO {

    @Schema(description = "动态id")
    private Integer id;

    @Schema(description = "创建者ID")
    private Integer userId;

    @Schema(description = "动态内容")
    private String content;

    @Schema(description = "动态关联位置")
    private String address;

    @Schema(description = "位置名称")
    private String addressName;

    @Schema(description = "地点纬度  例如 39.90420000",requiredMode = Schema.RequiredMode.REQUIRED,example = "39.90420000")
    private Double latitude;

    @Schema(description = "地点经度  例如 116.40740000",requiredMode = Schema.RequiredMode.REQUIRED,example = "116.40740000")
    private Double longitude;

    @Schema(description = "关联圈子ID")
    private Integer circleId;

    @Schema(description = "关联话题ID")
    private Integer topicId;

    @Schema(description = "关联活动ID")
    private Integer activityId;

    @Schema(description = "动态类型")
    private DynamicTypeEnum dynamicType;

    @Schema(description = "动态状态")
    private DynamicStatusEnum status;
}
