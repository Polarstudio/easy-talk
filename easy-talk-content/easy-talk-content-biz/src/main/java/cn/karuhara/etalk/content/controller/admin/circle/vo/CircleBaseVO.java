package cn.karuhara.etalk.content.controller.admin.circle.vo;

import cn.karuhara.etalk.content.enums.CircleStatusEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

@Data
public class CircleBaseVO {

    @Schema(description = "圈子id")
    private Integer id;

    @Schema(description = "圈子名称" , requiredMode = Schema.RequiredMode.REQUIRED ,example = "摄影")
    @NotNull(message = "圈子名称不能为空")
    private String circleName;

    @Schema(description = "创建者")
    private Integer userId;

    @Schema(description = "圈子封面",requiredMode = Schema.RequiredMode.REQUIRED ,example = "http://etalk.karuhara.cn/circle/avatar.jpg")
    @URL(message = "圈子头像必须不为空!")
    private String coverImg;

    @Schema(description = "圈子简介",requiredMode = Schema.RequiredMode.REQUIRED ,example = "这是一个圈子简介")
    @NotNull(message = "圈子简介不能为空")
    private String intro;

    @Schema(description = "圈子状态",requiredMode = Schema.RequiredMode.REQUIRED ,example = "1")
    private CircleStatusEnum status;
}
