package cn.karuhara.etalk.content.controller.app.activity.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "最简单的活动信息 - 提供给发布动态使用")
@Data
public class ActivityAppBriefVO {

    @Schema(description = "活动id",example = "1")
    private Integer id;

    @Schema(description = "活动标题")
    private String name;
}
