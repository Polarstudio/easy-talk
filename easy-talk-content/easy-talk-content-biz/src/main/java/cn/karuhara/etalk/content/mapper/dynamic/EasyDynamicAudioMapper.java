package cn.karuhara.etalk.content.mapper.dynamic;

import cn.karuhara.etalk.content.entity.dynamic.EasyDynamicAudio;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author atom
* @description 针对表【easy_dynamic_audio(动态音频)】的数据库操作Mapper
* @createDate 2024-11-26 21:21:49
* @Entity cn.karuhara.etalk.content.entity/dynamic.EasyDynamicAudio
*/
public interface EasyDynamicAudioMapper extends BaseMapper<EasyDynamicAudio> {

}




