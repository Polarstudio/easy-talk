package cn.karuhara.etalk.content.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CommentLikeTypeEnum {
    LIKED("0", "点赞"),
    UNLIKED("1", "点踩");

    @EnumValue
    @JsonValue
    private final String code;

    private final String desc;
}
