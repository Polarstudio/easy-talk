package cn.karuhara.etalk.content.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 圈子状态枚举类
 */
@Getter
@AllArgsConstructor
public enum CircleStatusEnum {
    NORMAL("0","正常"),
    PUSH("1","推送"),
    HIDE("2","隐藏"),
    REVIEWED("3","待审核"),
    REJECT("4","驳回");
    @EnumValue
    @JsonValue
    private final String code;

    private final String desc;

}
