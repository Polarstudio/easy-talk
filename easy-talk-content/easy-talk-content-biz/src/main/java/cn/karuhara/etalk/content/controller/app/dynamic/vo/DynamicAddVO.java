package cn.karuhara.etalk.content.controller.app.dynamic.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Schema(description = "App - 发布动态VO")
@Data
public class DynamicAddVO extends DynamicBaseVO {

    @Schema(description = "动态附件")
    private DynamicAppAttachmentVO attachment;
}
