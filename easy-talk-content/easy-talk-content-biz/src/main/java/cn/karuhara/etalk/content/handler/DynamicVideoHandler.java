package cn.karuhara.etalk.content.handler;

import cn.karuhara.etalk.content.controller.app.dynamic.vo.DynamicAppAttachmentVO;
import cn.karuhara.etalk.content.convert.DynamicConvert;
import cn.karuhara.etalk.content.entity.dynamic.EasyDynamicVideo;
import cn.karuhara.etalk.content.enums.DynamicTypeEnum;
import cn.karuhara.etalk.content.service.dynamic.EasyDynamicVideoService;
import cn.karuhara.etalk.framework.common.context.LoginContextHolder;
import cn.karuhara.etalk.framework.common.enums.GlobalCodeEnum;
import cn.karuhara.etalk.framework.common.exception.ServiceException;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

/**
 * @Description: 动态视频枚举类
 * @author: 轻语
*/
@Component
public class DynamicVideoHandler implements DynamicTypeHandler{

    @Resource
    private EasyDynamicVideoService videoService;

    @Override
    public DynamicTypeEnum getDynamicType() {
        return DynamicTypeEnum.VIDEO_TEXT;
    }

    @Override
    public void sava(DynamicAppAttachmentVO attachmentVO) {
        Integer userId = LoginContextHolder.get();
        if (userId == null) {
            throw new ServiceException(GlobalCodeEnum.LOGIN_AUTH);
        }
        EasyDynamicVideo video = new EasyDynamicVideo();
        video.setUserId(userId);
        video.setDynamicId(attachmentVO.getDynamicId());
        video.setVideoUrl(attachmentVO.getVideoUrl());
        video.setHeight(attachmentVO.getHeight());
        video.setWidth(attachmentVO.getWidth());
        videoService.save(video);
    }

    @Override
    public DynamicAppAttachmentVO get(Integer dynamicId) {
        LambdaQueryWrapper<EasyDynamicVideo> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(EasyDynamicVideo::getDynamicId, dynamicId);
        EasyDynamicVideo dynamicVideo = videoService.getOne(wrapper);
        return DynamicConvert.INSTANT.dynamicVideoToAttachmentVO(dynamicVideo);
    }

    @Override
    public void update(DynamicAppAttachmentVO attachmentVO) {
        //先把之前的给删了
        delete(attachmentVO.getDynamicId());
        sava(attachmentVO);
    }

    @Override
    public void delete(Integer dynamicId) {
        LambdaQueryWrapper<EasyDynamicVideo> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(EasyDynamicVideo::getDynamicId, dynamicId);
        videoService.remove(wrapper);
    }
}
