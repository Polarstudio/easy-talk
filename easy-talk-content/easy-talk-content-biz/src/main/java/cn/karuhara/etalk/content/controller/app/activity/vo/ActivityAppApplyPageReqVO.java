package cn.karuhara.etalk.content.controller.app.activity.vo;

import cn.karuhara.etalk.framework.common.pojo.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Schema(description = "获取活动报名用户的分页参数")
@Data
public class ActivityAppApplyPageReqVO extends PageParam {
    @Schema(description = "活动的id")
    private Integer activityId;

    @Schema(description = "用户的手机号")
    private String phone;
}
