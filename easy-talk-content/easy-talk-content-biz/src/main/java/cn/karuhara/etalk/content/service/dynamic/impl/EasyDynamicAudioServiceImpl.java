package cn.karuhara.etalk.content.service.dynamic.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.karuhara.etalk.content.entity.dynamic.EasyDynamicAudio;
import cn.karuhara.etalk.content.service.dynamic.EasyDynamicAudioService;
import cn.karuhara.etalk.content.mapper.dynamic.EasyDynamicAudioMapper;
import org.springframework.stereotype.Service;

/**
* @author atom
* @description 针对表【easy_dynamic_audio(动态音频)】的数据库操作Service实现
* @createDate 2024-11-26 21:21:49
*/
@Service
public class EasyDynamicAudioServiceImpl extends ServiceImpl<EasyDynamicAudioMapper, EasyDynamicAudio>
    implements EasyDynamicAudioService{

}




