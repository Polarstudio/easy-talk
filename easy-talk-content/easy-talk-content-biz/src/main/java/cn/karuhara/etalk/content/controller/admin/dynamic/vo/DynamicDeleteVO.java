package cn.karuhara.etalk.content.controller.admin.dynamic.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "删除动态")
@Data
public class DynamicDeleteVO {
    @Schema(description = "id")
    private Integer id;
    @Schema(description = "用户id")
    private Integer userId;
    @Schema(description = "删除理由")
    private String reason;
}
