package cn.karuhara.etalk.content.controller.admin.circle.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "管理后台 - 添加圈子VO")
public class CircleAddReqVO extends CircleBaseVO{

}
