package cn.karuhara.etalk.content.stream;

import cn.karuhara.etalk.content.service.topic.EasyTopicService;
import cn.karuhara.etalk.framework.common.constants.KafkaStreamConstant;
import cn.karuhara.etalk.framework.common.model.mess.TopicStreamMess;
import cn.karuhara.etalk.framework.common.model.mess.UpdateTopicMess;
import com.alibaba.fastjson2.JSONObject;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class TopicIncrHandleListener {

    @Resource
    private EasyTopicService topicService;

    @KafkaListener(topics = KafkaStreamConstant.HOT_TOPIC_INCR_HANDLE_TOPIC)
    public void hotIncrHandle(ConsumerRecord<String, String> record, Acknowledgment ack) {
        TopicStreamMess topicStreamMess = JSONObject.parseObject(record.value(), TopicStreamMess.class);
        log.info("topicStreamMess:{}", topicStreamMess);
        topicService.updateScore(topicStreamMess);
        ack.acknowledge();
    }
}
