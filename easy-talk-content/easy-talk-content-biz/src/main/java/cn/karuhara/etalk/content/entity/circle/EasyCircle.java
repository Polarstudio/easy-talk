package cn.karuhara.etalk.content.entity.circle;

import cn.karuhara.etalk.content.enums.CircleStatusEnum;
import cn.karuhara.etalk.framework.mybatis.core.dataobject.BaseDO;
import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 圈子
 * @TableName easy_circle
 */
@EqualsAndHashCode(callSuper = true)
@TableName(value ="easy_circle")
@Data
public class EasyCircle extends BaseDO implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 名称
     */
    private String circleName;

    /**
     * 创建者
     */
    private Integer userId;
    /**
     * 圈子封面
     */
    private String coverImg;

    /**
     * 简介
     */
    private String intro;

    /**
     * 状态：1 正常（首页推送）、2 正常、3 隐藏
     */
    private CircleStatusEnum status;

    @TableLogic
    private Integer deleted;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}