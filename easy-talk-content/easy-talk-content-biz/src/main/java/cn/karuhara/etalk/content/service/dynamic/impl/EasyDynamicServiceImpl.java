package cn.karuhara.etalk.content.service.dynamic.impl;

import cn.karuhara.etalk.content.controller.admin.dynamic.vo.DynamicDeleteVO;
import cn.karuhara.etalk.content.controller.admin.dynamic.vo.DynamicUpdateVO;
import cn.karuhara.etalk.content.controller.app.dynamic.vo.*;
import cn.karuhara.etalk.content.convert.DynamicConvert;
import cn.karuhara.etalk.content.detection.tencent.DetectionEntity;
import cn.karuhara.etalk.content.detection.tencent.DetectionHandler;
import cn.karuhara.etalk.content.entity.activity.EasyActivity;
import cn.karuhara.etalk.content.entity.circle.EasyCircle;
import cn.karuhara.etalk.content.entity.dynamic.EasyDynamicComment;
import cn.karuhara.etalk.content.entity.dynamic.EasyDynamicCommentLike;
import cn.karuhara.etalk.content.entity.dynamic.EasyDynamicLike;
import cn.karuhara.etalk.content.entity.topic.EasyTopic;
import cn.karuhara.etalk.content.enums.DetectionTypeEnum;
import cn.karuhara.etalk.content.enums.DynamicStatusEnum;
import cn.karuhara.etalk.content.handler.DynamicHandlerFactory;
import cn.karuhara.etalk.content.handler.DynamicTypeHandler;
import cn.karuhara.etalk.content.mapper.topic.EasyTopicMapper;
import cn.karuhara.etalk.content.mq.product.DynamicProducer;
import cn.karuhara.etalk.content.service.activity.EasyActivityService;
import cn.karuhara.etalk.content.service.circle.EasyCircleService;
import cn.karuhara.etalk.content.service.dynamic.EasyDynamicCommentLikeService;
import cn.karuhara.etalk.content.service.dynamic.EasyDynamicCommentService;
import cn.karuhara.etalk.content.service.dynamic.EasyDynamicLikeService;
import cn.karuhara.etalk.content.service.topic.EasyTopicService;
import cn.karuhara.etalk.framework.common.constants.KafkaStreamConstant;
import cn.karuhara.etalk.framework.common.model.mess.UpdateTopicMess;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import cn.karuhara.etalk.member.api.MemberApi;
import cn.karuhara.etalk.member.api.dto.MemberBaseInfoDTO;
import cn.karuhara.etalk.member.api.dto.MemberDynamicInfoDTO;
import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.karuhara.etalk.content.entity.dynamic.EasyDynamic;
import cn.karuhara.etalk.content.service.dynamic.EasyDynamicService;
import cn.karuhara.etalk.content.mapper.dynamic.EasyDynamicMapper;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.beans.Transient;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

/**
* @author atom
* @description 针对表【easy_dynamic(动态)】的数据库操作Service实现
* @createDate 2024-11-26 16:51:19
*/
@Service
@Slf4j
public class EasyDynamicServiceImpl extends ServiceImpl<EasyDynamicMapper, EasyDynamic>
    implements EasyDynamicService{

    @Resource
    EasyDynamicMapper dynamicMapper;

    @Qualifier("cn.karuhara.etalk.member.api.MemberApi")
    @Resource
    private MemberApi memberApi;

    @Resource
    private EasyCircleService circleService;

    @Resource
    private EasyTopicService topicService;

    @Resource
    private EasyDynamicLikeService likeService;

    @Resource
    private EasyDynamicCommentService commentService;
    @Resource
    private EasyDynamicCommentLikeService commentLikeService;

    @Resource
    private DynamicHandlerFactory dynamicHandlerFactory;

    @Resource
    private EasyActivityService activityService;

    @Resource
    private DynamicProducer dynamicProducer;

    @Resource
    private EasyTopicMapper topicMapper;

    @Resource
    private DetectionHandler detectionHandler;

    @Resource
    private KafkaTemplate<String, String> kafkaTemplate;

    /**
     * 获取动态的详情
     *
     * @param id 动态的id
     * @return 动态的详细信息
     */
    @Override
    public DynamicDetailVO getDetail(Integer id) {
        EasyDynamic dynamic = getById(id);
        if (dynamic==null){
            return null;
        }
        DynamicDetailVO detailVO = DynamicConvert.INSTANT.dynamicToDetailVO(dynamic);
        fillDetail(detailVO);
        return detailVO;
    }

    /**
     * 发布动态
     *
     * @param appAddVO 动态信息
     */
    @Override
    public void pushDynamic(DynamicAddVO appAddVO) {
        EasyDynamic dynamic = DynamicConvert.INSTANT.addVOToDynamic(appAddVO);
        if (appAddVO.getStatus() != DynamicStatusEnum.DRAFT){
            dynamic.setStatus(DynamicStatusEnum.PENDING_REVIEW);
        }
        dynamicMapper.insertOrUpdate(dynamic);
        //添加附件
        DynamicTypeHandler dynamicTypeHandler = dynamicHandlerFactory.getDynamicTypeHandler(dynamic.getDynamicType());
        DynamicAppAttachmentVO attachment = appAddVO.getAttachment();
        attachment.setDynamicId(dynamic.getId());
        dynamicTypeHandler.update(attachment);
        //如果是新增动态并且不是草稿，关联的也话题不为空，那么增加话题热度
        if (appAddVO.getStatus() != DynamicStatusEnum.DRAFT &&  appAddVO.getTopicId()!=null){
            UpdateTopicMess topicMess  = new UpdateTopicMess();
            topicMess.setType(UpdateTopicMess.UpdateArticleType.DYNAMIC);
            topicMess.setTopicId(appAddVO.getTopicId());
            topicMess.setAdd(1);
            //发送消息，数据聚合
            kafkaTemplate.send(KafkaStreamConstant.HOT_TOPIC_SCORE_TOPIC, JSON.toJSONString(topicMess));
        }
        if (appAddVO.getStatus() == DynamicStatusEnum.DRAFT){
            return;
        }
        //异步进行内容审核
        CompletableFuture.runAsync(()->{
            DetectionEntity detectionEntity = new DetectionEntity();
            //设置用户的openId
            MemberBaseInfoDTO memberBaseInfo = memberApi.getMemberBaseInfo(appAddVO.getUserId());
            detectionEntity.setWxOpenid(memberBaseInfo.getWxOpenid());
            //设置动态id
            detectionEntity.setPostId(dynamic.getId());
            detectionEntity.setType(DetectionTypeEnum.DYNAMIC);
            //设置文本内容
            List<String> texts = new ArrayList<>();
            texts.add(dynamic.getContent());
            detectionEntity.setTexts(texts);
            //设置图片内容
            detectionEntity.setImages(appAddVO.getAttachment().getImages());
            //设置视频内容
            detectionEntity.setVideos(appAddVO.getAttachment().getVideoUrl());
            //设置音频内容
            detectionEntity.setAudios(appAddVO.getAttachment().getAudioUrl());
            //提交审核
            detectionHandler.detection(detectionEntity);
        });
    }

    /**
     * 获取用户的点赞总数
     *
     * @param userId 用户ID
     * @return 获赞总数
     */
    @Override
    public Long getUserLinkedCount(Integer userId) {
        //获取用户发布的动态
        LambdaQueryWrapper<EasyDynamic> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(EasyDynamic::getUserId, userId);
        List<EasyDynamic> list = list(wrapper);
        List<Integer> dynamicIds = list.stream().map(EasyDynamic::getId).collect(Collectors.toList());
        //获取动态的点赞
        if (dynamicIds.isEmpty()){
            return 0L;
        }
        Long linkedCount = 0L;
        for (Integer dynamicId : dynamicIds){
            Long likeCount = likeService.getLikeCount(dynamicId);
            linkedCount += likeCount;
        }
        return linkedCount;
    }

    /**
     * 取消关联话题、圈子、活动
     *
     * @param circleId   圈子id
     * @param topicId    话题id
     * @param activityId 活动id
     */
    @Override
    public void canalRelevance(Integer circleId, Integer topicId, Integer activityId) {
        LambdaUpdateWrapper<EasyDynamic> wrapper = new LambdaUpdateWrapper<>();
        if (circleId!=null){
            wrapper.eq(EasyDynamic::getCircleId, circleId).set(EasyDynamic::getCircleId, null);
        }
        if (topicId!=null){
            wrapper.eq(EasyDynamic::getTopicId, topicId).set(EasyDynamic::getTopicId, null);
        }
        if (activityId!=null){
            wrapper.eq(EasyDynamic::getActivityId, activityId).set(EasyDynamic::getActivityId, null);
        }
        dynamicMapper.update(null,wrapper);
    }

    /**
     * 删除动态
     *
     */
    @Override
    @Transient
    public void delete(DynamicDeleteVO deleteV) {
        //先删除基本动态信息
        EasyDynamic one = getById(deleteV.getId());
        //发送通知给用户
        dynamicProducer.deleteMessage(deleteV);
        //删除动态
        removeById(deleteV.getId());
        //TODO 这些都可以异步的进行
        //在删除附件
        DynamicTypeHandler dynamicTypeHandler = dynamicHandlerFactory.getDynamicTypeHandler(one.getDynamicType());
        dynamicTypeHandler.delete(deleteV.getId());
        //删除动态的点赞数据
        LambdaQueryWrapper<EasyDynamicLike> likeWrapper = new LambdaQueryWrapper<>();
        likeWrapper.eq(EasyDynamicLike::getDynamicId, deleteV.getId());
        likeService.remove(likeWrapper);
        //先获取所有的评论
        LambdaQueryWrapper<EasyDynamicComment> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(EasyDynamicComment::getDynamicId, deleteV.getId());
        List<EasyDynamicComment> comments = commentService.list(wrapper);
        //删除所有的评论
        commentService.remove(wrapper);
        //获取所有的评论id
        List<Integer> commentIds = comments.stream().map(EasyDynamicComment::getId).collect(Collectors.toList());
        //删除评论的点赞信息
        LambdaQueryWrapper<EasyDynamicCommentLike> commentLikeWrapper = new LambdaQueryWrapper<>();
        if (!commentIds.isEmpty()){
            commentLikeWrapper.in(EasyDynamicCommentLike::getCommentId, commentIds);
            commentLikeService.remove(commentLikeWrapper);
        }
    }

    /**
     * 修改动态
     *
     * @param dynamicUpdateVO 修改动态的数据
     */
    @Override
    public void updateDynamic(DynamicUpdateVO dynamicUpdateVO) {
        EasyDynamic easyDynamic = DynamicConvert.INSTANT.updateVOToDynamic(dynamicUpdateVO);
        updateById(easyDynamic);
    }

    /**
     * 获取关注用户的动态
     *
     * @param pageReqVO 分页参数
     * @return 分页数据
     */
    @Override
    public PageResult<DynamicDetailVO> getFollowerList(DynamicPageReqVO pageReqVO) {
        //获取用户关注的用户id
        List<Integer> followIds = memberApi.getFollowIds(pageReqVO.getUserId());
        if (followIds.isEmpty()){
            return PageResult.empty();
        }
        LambdaQueryWrapper<EasyDynamic> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(EasyDynamic::getUserId, followIds);
        wrapper.eq(EasyDynamic::getStatus,DynamicStatusEnum.DISPLAY);
        return handleDynamicPage(pageReqVO,wrapper);
    }

    /**
     * 获取用户发布的动态
     *
     * @param pageReqVO 分页参数
     * @return 动态详情
     */
    @Override
    public PageResult<DynamicDetailVO> getUserDynamicList(DynamicPageReqVO pageReqVO) {
        LambdaQueryWrapper<EasyDynamic> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(EasyDynamic::getUserId, pageReqVO.getUserId());
        wrapper.eq(EasyDynamic::getStatus, DynamicStatusEnum.DISPLAY);
        return handleDynamicPage(pageReqVO,wrapper);
    }

    /**
     * 获取用户喜欢的动态
     * @return 动态分页结果集
     */
    @Override
    public PageResult<DynamicDetailVO> like(DynamicPageReqVO pageReqVO) {
        // 先获取用户点赞的动态id
        LambdaQueryWrapper<EasyDynamicLike> likeWrapper = new LambdaQueryWrapper<>();
        likeWrapper.eq(EasyDynamicLike::getUserId, pageReqVO.getUserId());
        List<EasyDynamicLike> likes = likeService.list(likeWrapper);
        List<Integer> likeIds = likes.stream().map(EasyDynamicLike::getDynamicId).collect(Collectors.toList());
        if (likeIds.isEmpty()) {
            return PageResult.empty();
        }
        LambdaQueryWrapper<EasyDynamic> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(EasyDynamic::getId, likeIds);
        wrapper.eq(EasyDynamic::getStatus, DynamicStatusEnum.DISPLAY);
        // 调用抽离出来的通用方法进行处理
        return handleDynamicPage(pageReqVO, wrapper);
    }

    /**
     * 管理后台 - 分页获取动态
     *
     * @param pageReqVO 分页条件
     * @return 动态详情列表
     */
    @Override
    public PageResult<DynamicDetailVO> getAdminList(DynamicPageReqVO pageReqVO) {
        LambdaQueryWrapper<EasyDynamic> wrapper = new LambdaQueryWrapper<>();
        if (pageReqVO.getActivityId()!= null) {
            wrapper.eq(EasyDynamic::getActivityId, pageReqVO.getActivityId());
        }
        if (pageReqVO.getCircleId()!= null) {
            wrapper.eq(EasyDynamic::getCircleId, pageReqVO.getCircleId());
        }
        if (pageReqVO.getTopicId()!= null) {
            wrapper.eq(EasyDynamic::getTopicId, pageReqVO.getTopicId());
        }
        if (pageReqVO.getContent()!= null) {
            wrapper.like(EasyDynamic::getContent, pageReqVO.getContent());
        }
        if (pageReqVO.getUserId()!= null) {
            wrapper.eq(EasyDynamic::getUserId, pageReqVO.getUserId());
        }
        if (StringUtils.isNotBlank(pageReqVO.getStatus())) {
            wrapper.eq(EasyDynamic::getStatus, pageReqVO.getStatus());
        }
        // 调用抽离出来的通用方法进行处理
        return handleDynamicPage(pageReqVO, wrapper);
    }

    /**
     * 分页获取动态
     * @param pageReqVO 分页条件
     * @return 动态详情列表
     */
    @Override
    public PageResult<DynamicDetailVO> getList(DynamicPageReqVO pageReqVO) {
        LambdaQueryWrapper<EasyDynamic> wrapper = new LambdaQueryWrapper<>();
        if (pageReqVO.getActivityId()!= null) {
            wrapper.eq(EasyDynamic::getActivityId, pageReqVO.getActivityId());
        }
        if (pageReqVO.getCircleId()!= null) {
            wrapper.eq(EasyDynamic::getCircleId, pageReqVO.getCircleId());
        }
        if (pageReqVO.getTopicId()!= null) {
            wrapper.eq(EasyDynamic::getTopicId, pageReqVO.getTopicId());
        }
        if (pageReqVO.getContent()!= null) {
            wrapper.like(EasyDynamic::getContent, pageReqVO.getContent());
        }
        if (pageReqVO.getUserId()!= null) {
            wrapper.eq(EasyDynamic::getUserId, pageReqVO.getUserId());
        } else {
            wrapper.eq(EasyDynamic::getStatus, DynamicStatusEnum.DISPLAY);
        }
        if (StringUtils.isNotBlank(pageReqVO.getStatus())) {
            wrapper.eq(EasyDynamic::getStatus, pageReqVO.getStatus());
        }
        wrapper.orderByDesc(EasyDynamic::getCreateTime);
        // 调用抽离出来的通用方法进行处理
        return handleDynamicPage(pageReqVO, wrapper);
    }

    // 抽离出来的通用分页查询及数据处理方法
    private PageResult<DynamicDetailVO> handleDynamicPage(DynamicPageReqVO pageReqVO, LambdaQueryWrapper<EasyDynamic> queryWrapper) {
        // 获取基本的动态信息，进行分页查询
        Page<EasyDynamic> page = new Page<>(pageReqVO.getPageNo(), pageReqVO.getPageSize());
        Page<EasyDynamic> dynamicPage = dynamicMapper.selectPage(page, queryWrapper);
        // 如果为空直接返回
        if (dynamicPage.getTotal() == 0) {
            return PageResult.empty();
        }
        // 获取发布动态的用户信息
        List<EasyDynamic> records = dynamicPage.getRecords();
        List<DynamicDetailVO> dynamicDetailVOS = DynamicConvert.INSTANT.dynamicListToDetailVO(records);
        dynamicDetailVOS.forEach(this::fillDetail);
        return new PageResult<>(dynamicDetailVOS, dynamicPage.getPages());
    }

    public void fillDetail(DynamicDetailVO dynamicDetailVO) {
        //查找动态发布者的信息
        MemberDynamicInfoDTO memberDynamicInfoDTO = memberApi.getMemberById(dynamicDetailVO.getUserId());
        DynamicConvert.INSTANT.addDynamicInfoToDynamicDetailVO(dynamicDetailVO,memberDynamicInfoDTO);
        // TODO 这些都可以并发的执行
        //获取关联圈子信息
        if (dynamicDetailVO.getCircleId() != null){
            EasyCircle circle = circleService.getById(dynamicDetailVO.getCircleId());
            dynamicDetailVO.setCircleName(circle.getCircleName());
            dynamicDetailVO.setCircleCover(circle.getCoverImg());
        }
        //获取关联话题信息
        if (dynamicDetailVO.getTopicId() != null){
            EasyTopic topic = topicService.getById(dynamicDetailVO.getTopicId());
            dynamicDetailVO.setTopicName(topic.getTopicName());
        }
        //获取关联的活动信息
        if (dynamicDetailVO.getActivityId() != null){
            EasyActivity activity = activityService.getById(dynamicDetailVO.getActivityId());
            dynamicDetailVO.setActivityName(activity.getName());
            dynamicDetailVO.setActivityCover(activity.getCoverImageUrl().get(0));
        }
        //获取动态评论数
        Long likeCount = likeService.getLikeCount(dynamicDetailVO.getId());
        dynamicDetailVO.setLikeCount(likeCount);
        //获取动态评论数
        Long commentCount = commentService.getCommentCount(dynamicDetailVO.getId());
        dynamicDetailVO.setCommentCount(commentCount);
        //获取动态的附件，如何图片、视频等
        DynamicTypeHandler dynamicTypeHandler = dynamicHandlerFactory.getDynamicTypeHandler(dynamicDetailVO.getDynamicType());
        DynamicAppAttachmentVO dynamicAppAttachmentVO = dynamicTypeHandler.get(dynamicDetailVO.getId());
        dynamicDetailVO.setAttachment(dynamicAppAttachmentVO);
    }

}




