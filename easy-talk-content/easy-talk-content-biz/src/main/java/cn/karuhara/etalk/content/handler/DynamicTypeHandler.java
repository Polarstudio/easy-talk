package cn.karuhara.etalk.content.handler;

import cn.karuhara.etalk.content.controller.app.dynamic.vo.DynamicAppAttachmentVO;
import cn.karuhara.etalk.content.enums.DynamicTypeEnum;

/**
 * @Description: 动态附件的操作接口
 * @author: 轻语
*/
public interface DynamicTypeHandler {

    /**
     * 动态类型的枚举
     */
    DynamicTypeEnum getDynamicType();

    /**
     * 附件的插入操作
     */
    void sava(DynamicAppAttachmentVO attachmentVO);

    /**
     * 附件的查询操作
     */
    DynamicAppAttachmentVO get(Integer dynamicId);

    /**
     * 附件修改操作
     */
    void update(DynamicAppAttachmentVO attachmentVO);

    /**
     * 附件的删除操作
     */
    void delete(Integer dynamicId);
}
