package cn.karuhara.etalk.content.service.topic.impl;

import cn.karuhara.etalk.content.controller.admin.topic.vo.TopicBaseVO;
import cn.karuhara.etalk.content.controller.admin.topic.vo.TopicDeleteVO;
import cn.karuhara.etalk.content.controller.admin.topic.vo.TopicPageReqVO;
import cn.karuhara.etalk.content.controller.admin.topic.vo.TopicPageRespVO;
import cn.karuhara.etalk.content.controller.app.topic.vo.*;
import cn.karuhara.etalk.content.convert.TopicConvert;
import cn.karuhara.etalk.content.entity.dynamic.EasyDynamic;
import cn.karuhara.etalk.content.entity.topic.EasyTopicFollow;
import cn.karuhara.etalk.content.enums.TopicStatusEnum;
import cn.karuhara.etalk.content.mq.product.TopicProducer;
import cn.karuhara.etalk.content.service.dynamic.EasyDynamicService;
import cn.karuhara.etalk.content.service.topic.EasyTopicFollowService;
import cn.karuhara.etalk.framework.common.constants.GlobalConstant;
import cn.karuhara.etalk.framework.common.constants.KafkaStreamConstant;
import cn.karuhara.etalk.framework.common.constants.RedisConstant;
import cn.karuhara.etalk.framework.common.context.LoginContextHolder;
import cn.karuhara.etalk.framework.common.enums.GlobalCodeEnum;
import cn.karuhara.etalk.framework.common.exception.ServiceException;
import cn.karuhara.etalk.framework.common.model.mess.TopicStreamMess;
import cn.karuhara.etalk.framework.common.model.mess.UpdateTopicMess;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import cn.karuhara.etalk.fremawork.redis.core.MyRedisUtil;
import cn.karuhara.etalk.member.api.MemberApi;
import cn.karuhara.etalk.member.api.dto.MemberBaseInfoDTO;
import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.karuhara.etalk.content.entity.topic.EasyTopic;
import cn.karuhara.etalk.content.service.topic.EasyTopicService;
import cn.karuhara.etalk.content.mapper.topic.EasyTopicMapper;
import io.micrometer.common.util.StringUtils;
import jakarta.annotation.Resource;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
* @author atom
* @description 针对表【easy_topic(话题表)】的数据库操作Service实现
* @createDate 2024-11-18 16:38:17
*/
@Service
public class EasyTopicServiceImpl extends ServiceImpl<EasyTopicMapper, EasyTopic>
    implements EasyTopicService{

    @Resource
    private EasyTopicMapper easyTopicMapper;

    @Resource
    private EasyTopicFollowService easyTopicFollowService;

    @Resource
    private EasyDynamicService dynamicService;

    @Resource
    private EasyTopicFollowService followService;

    @Resource
    private MemberApi memberApi;

    @Resource
    private TopicProducer topicProducer;

    @Resource
    private MyRedisUtil myRedisUtil;

    @Resource
    private KafkaTemplate<String, String> kafkaTemplate;

    @Override
    public void addTopic(TopicBaseVO topicBaseVO) {
        //先判断是否存在同名话题
        LambdaQueryWrapper<EasyTopic> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyTopic::getTopicName, topicBaseVO.getTopicName());
        EasyTopic one = getOne(queryWrapper);
        if (one != null) {
            throw new ServiceException(GlobalCodeEnum.FAIL.getCode(), GlobalConstant.TOPIC_NAME_EXISTS);
        }
        EasyTopic easyTopic = TopicConvert.INSTANCE.addVOToTopic(topicBaseVO);
        //在管理后台创建的话题统一赋给id为1的官方账号
        easyTopic.setUserId(GlobalConstant.OFFICIAL_USER_ID);
        saveOrUpdate(easyTopic);
    }

    @Override
    public PageResult<TopicPageRespVO> getPageList(TopicPageReqVO topicPageReqVO) {
        LambdaQueryWrapper<EasyTopic> queryWrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(topicPageReqVO.getTopicName())) {
            queryWrapper.like(EasyTopic::getTopicName, topicPageReqVO.getTopicName());
        }
        if (StringUtils.isNotBlank(topicPageReqVO.getStatus())){
            queryWrapper.eq(EasyTopic::getStatus, topicPageReqVO.getStatus());
        }
        Page<EasyTopic> page = new Page<>(topicPageReqVO.getPageNo(), topicPageReqVO.getPageSize());
        IPage<EasyTopic> iPage = easyTopicMapper.selectPage(page, queryWrapper);
        if (iPage.getTotal() == 0) {
            return PageResult.empty();
        }
        List<EasyTopic> records = iPage.getRecords();
        //获取话题关注度
        List<TopicPageRespVO> pageRespVOS = new ArrayList<>();
        for (EasyTopic easyTopic : records) {
            TopicPageRespVO topicPageRespVO = TopicConvert.INSTANCE.topicToPageRespVO(easyTopic);
            topicPageRespVO.setFollowNum(easyTopic.getFollow());
            pageRespVOS.add(topicPageRespVO);
        }
        return new PageResult<>(pageRespVOS, iPage.getTotal());
    }

    @Override
    public void updateTopic(TopicBaseVO topicBaseVO) {
        LambdaQueryWrapper<EasyTopic> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyTopic::getTopicName, topicBaseVO.getTopicName());
        EasyTopic one = getOne(queryWrapper);
        if (one != null && !one.getId().equals(topicBaseVO.getId())) {
            throw new ServiceException(GlobalCodeEnum.FAIL.getCode(), GlobalConstant.TOPIC_NAME_EXISTS);
        }
        EasyTopic easyTopic = TopicConvert.INSTANCE.addVOToTopic(topicBaseVO);
        saveOrUpdate(easyTopic);
    }

    /**
     * 获取推荐的话题
     *
     * @return 话题的基本信息
     */
    @Override
    public List<TopicAppBaseVO> getRecommend() {
        //随机获取三条推荐的话题
        LambdaQueryWrapper<EasyTopic> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyTopic::getStatus, TopicStatusEnum.PUSH).last("LIMIT 4");
        List<EasyTopic> list = easyTopicMapper.selectList(queryWrapper);
        return TopicConvert.INSTANCE.topicToAppBaseVOList(list);
    }

    /**
     * 分页获取话题
     *
     * @param pageParam 分页参数
     * @return 分页结果
     */
    @Override
    public PageResult<TopicAppDetailVO> gteList(TopicAppPageReqVO pageParam) {
        LambdaQueryWrapper<EasyTopic> queryWrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(pageParam.getTopicName())) {
            queryWrapper.like(EasyTopic::getTopicName, pageParam.getTopicName());
        }
        if (pageParam.getUserId() != null){
            queryWrapper.eq(EasyTopic::getUserId, pageParam.getUserId());
        }else{
            queryWrapper.eq(EasyTopic::getStatus, TopicStatusEnum.PUSH).or()
                    .eq(EasyTopic::getStatus,TopicStatusEnum.NORMAL);
        }
        Page<EasyTopic> page = new Page<>(pageParam.getPageNo(), pageParam.getPageSize());
        IPage<EasyTopic> iPage = easyTopicMapper.selectPage(page, queryWrapper);
        if (iPage.getTotal() == 0) {
            return PageResult.empty();
        }
        List<EasyTopic> records = iPage.getRecords();
        List<TopicAppDetailVO> topicAppDetailVOS = TopicConvert.INSTANCE.topicToAppDetailVOList(records);
        topicAppDetailVOS.forEach(this::fillCircleCounts);
        return new PageResult<>(topicAppDetailVOS,iPage.getPages());
    }

    /**
     * APP端 - 创建或编辑话题
     *
     * @param topicAppBaseVO 话题信息
     */
    @Override
    public void appAddTopic(TopicAppBaseVO topicAppBaseVO) {
        //先判断判断用户是否登录
        Integer userId = LoginContextHolder.get();
        if (userId == null){
            throw new ServiceException(GlobalCodeEnum.LOGIN_AUTH);
        }
        //查找是否存在同名的话题
        LambdaQueryWrapper<EasyTopic> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyTopic::getTopicName, topicAppBaseVO.getTopicName());
        EasyTopic one = getOne(queryWrapper);
        if (one!= null && !one.getTopicName().equals(topicAppBaseVO.getTopicName())) {
            throw new ServiceException(GlobalCodeEnum.FAIL.getCode(), GlobalConstant.TOPIC_NAME_EXISTS);
        }
        EasyTopic easyTopic = TopicConvert.INSTANCE.appBaseVOToTopic(topicAppBaseVO);
        easyTopic.setUserId(userId);
        easyTopic.setStatus(TopicStatusEnum.AUDIT);
        saveOrUpdate(easyTopic);
    }

    /**
     * 获取用户关注的话题
     *
     * @return 话题分页信息
     */
    @Override
    public PageResult<TopicAppDetailVO> getFollowList(TopicAppPageReqVO pageParam) {
        //先判断判断用户是否登录
        Integer userId = LoginContextHolder.get();
        if (userId == null){
            return PageResult.empty();
        }
        //获取用户关注的话题的id
        LambdaQueryWrapper<EasyTopicFollow> followQueryWrapper = new LambdaQueryWrapper<>();
        followQueryWrapper.eq(EasyTopicFollow::getUserId,userId);
        List<EasyTopicFollow> list = followService.list(followQueryWrapper);
        List<Integer> collectId = list.stream().map(EasyTopicFollow::getTopicId).collect(Collectors.toList());
        if (collectId.isEmpty()){
            return PageResult.empty();
        }
        //分页查询用户关注的话题
        LambdaQueryWrapper<EasyTopic> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(EasyTopic::getId, collectId);
        Page<EasyTopic> page = new Page<>(pageParam.getPageNo(), pageParam.getPageSize());
        IPage<EasyTopic> iPage = easyTopicMapper.selectPage(page, queryWrapper);
        if (iPage.getTotal() == 0) {
            return PageResult.empty();
        }
        List<EasyTopic> records = iPage.getRecords();
        List<TopicAppDetailVO> topicAppDetailVOS = TopicConvert.INSTANCE.topicToAppDetailVOList(records);
        topicAppDetailVOS.forEach(this::fillCircleCounts);
        return new PageResult<>(topicAppDetailVOS,iPage.getPages());
    }

    /**
     * 获取话题的详情
     *
     * @param id 话题id
     * @return 话题详细信息
     */
    @Override
    public TopicAppDetailVO getDetail(Integer id) {
        EasyTopic topic = getById(id);
        if (topic == null) {
            return null;
        }
        TopicAppDetailVO detailVO = TopicConvert.INSTANCE.topicToAppDetailVO(topic);
        fillCircleCounts(detailVO);
        Integer score = computeScore(topic);
        detailVO.setHot(score);
        //增加话题的热度
        UpdateTopicMess mess = new UpdateTopicMess();
        mess.setTopicId(id);
        mess.setType(UpdateTopicMess.UpdateArticleType.VIEWS);
        mess.setAdd(1);
        //发送消息，数据聚合
        kafkaTemplate.send(KafkaStreamConstant.HOT_TOPIC_SCORE_TOPIC, JSON.toJSONString(mess));
        return detailVO;
    }

    /**
     * 获取话题的id和名称
     *
     * @return list
     */
    @Override
    public PageResult<TopicAppBriefVO> getTags(TopicAppPageReqVO pageReqVO) {
        LambdaQueryWrapper<EasyTopic> queryWrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(pageReqVO.getTopicName())) {
            queryWrapper.like(EasyTopic::getTopicName, pageReqVO.getTopicName());
        }
        Page<EasyTopic> page = new Page<>(pageReqVO.getPageNo(), pageReqVO.getPageSize());
        IPage<EasyTopic> iPage = easyTopicMapper.selectPage(page, queryWrapper);
        if (iPage.getTotal() == 0){
            return PageResult.empty();
        }
        List<EasyTopic> records = iPage.getRecords();
        List<TopicAppBriefVO> topicAppBriefVOS = TopicConvert.INSTANCE.topicsToAppBriefVOs(records);
        return new PageResult<>(topicAppBriefVOS,iPage.getPages());
    }

    /**
     * 管理后台 - 获取话题的id和名称
     *
     * @return
     */
    @Override
    public List<TopicAppBriefVO> getAdminTags() {
        List<EasyTopic> list = list();
        return TopicConvert.INSTANCE.topicsToAppBriefVOs(list);
    }

    /**
     * 删除话题
     *
     */
    @Override
    public void delete(TopicDeleteVO topicDeleteVO) {
        //取消关联动态
        dynamicService.canalRelevance(null,topicDeleteVO.getId(),null);
        //发送通知给用户
        topicProducer.deleteMessage(topicDeleteVO);
        //取消所有关注该话题的用户
        LambdaQueryWrapper<EasyTopicFollow> followQueryWrapper = new LambdaQueryWrapper<>();
        followQueryWrapper.eq(EasyTopicFollow::getTopicId,topicDeleteVO.getId());
        followService.remove(followQueryWrapper);
        //删除话题
        removeById(topicDeleteVO.getId());
    }

    /**
     * 更新话题的分值
     *
     * @param topicStreamMess 话题分值数据
     */
    @Override
    public void updateScore(TopicStreamMess topicStreamMess) {
        //1.跟新话题的行为，如关注、浏览等
        EasyTopic easyTopic = updateBehavior(topicStreamMess);
        //2.计算话题分值
        Integer score = computeScore(easyTopic);
        score *= 3;
        //3.更新话题的热度
        String buildKey = MyRedisUtil.buildKey(RedisConstant.TOPIC_HOT);
        myRedisUtil.updateZSetElement(buildKey,easyTopic.getId()+":"+easyTopic.getTopicName(),score);
    }

    /**
     * 获取热榜话题数据
     *
     * @return 话题数据
     */
    @Override
    public List<TopicHotVO> getHotTopic() {
        String buildKey = MyRedisUtil.buildKey(RedisConstant.TOPIC_HOT);
        Map<Object, Double> objectDoubleMap = myRedisUtil.zRevRangeWithScores(buildKey, 0, 29);
        List<TopicHotVO> list = new ArrayList<>();
        for (Map.Entry<Object, Double> entry : objectDoubleMap.entrySet()) {
            TopicHotVO topicHotVO = new TopicHotVO();
            String topic = (String) entry.getKey();
            String[] split = topic.split(":");
            topicHotVO.setId(Integer.parseInt(split[0]));
            topicHotVO.setTopicName(split[1]);
            topicHotVO.setScore(Objects.requireNonNull(entry.getValue()));
            list.add(topicHotVO);
        }
        return list;
    }

    public EasyTopic updateBehavior(TopicStreamMess topicStreamMess) {
        EasyTopic topic = getById(topicStreamMess.getTopicId());
        topic.setFollow(topic.getFollow()+topicStreamMess.getFollow());
        topic.setDynamic(topic.getDynamic()+topicStreamMess.getDynamic());
        topic.setViews(topic.getViews()+topicStreamMess.getView());
        updateById(topic);
        return topic;
    }

    private Integer computeScore(EasyTopic easyTopic) {
        int score = 0;
        if (easyTopic.getFollow() != null){
            score+= easyTopic.getFollow()* KafkaStreamConstant.HOT_TOPIC_FOLLOW_WEIGHT;
        }
        if (easyTopic.getViews() != null){
            score += easyTopic.getViews();
        }
        if (easyTopic.getDynamic() != null){
            score += easyTopic.getDynamic()* KafkaStreamConstant.HOT_TOPIC_DYNAMIC_WEIGHT;
        }
        return score;
    }


    //创建一个通用的方法来填充话题的动态和关注量
    private void fillCircleCounts(TopicAppDetailVO topicAppDetailVO) {
        //获取话题创建者的信息
        MemberBaseInfoDTO topicMemberInfo = memberApi.getMemberBaseInfo(topicAppDetailVO.getUserId());
        topicAppDetailVO.setAvatar(topicMemberInfo.getAvatar());
        topicAppDetailVO.setNikeName(topicMemberInfo.getNikeName());
    }
}




