package cn.karuhara.etalk.content.service.dynamic;

import cn.karuhara.etalk.content.controller.admin.dynamic.vo.CommentAdminDetailVO;
import cn.karuhara.etalk.content.controller.admin.dynamic.vo.CommentDeleteVO;
import cn.karuhara.etalk.content.controller.admin.dynamic.vo.CommentUpdateVO;
import cn.karuhara.etalk.content.controller.admin.dynamic.vo.CommonAdminPageReqVO;
import cn.karuhara.etalk.content.controller.app.dynamic.vo.CommentDetailVO;
import cn.karuhara.etalk.content.controller.app.dynamic.vo.CommentPageReqVO;
import cn.karuhara.etalk.content.controller.app.dynamic.vo.CommentPushVO;
import cn.karuhara.etalk.content.entity.dynamic.EasyDynamicComment;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author atom
* @description 针对表【easy_dynamic_comment(动态评论)】的数据库操作Service
* @createDate 2024-11-26 21:21:49
*/
public interface EasyDynamicCommentService extends IService<EasyDynamicComment> {

    /**
     * 获取动态的评论数
     * @param dynamicId 动态id
     * @return 评论数量
     */
    Long getCommentCount(Integer dynamicId);

    /**
     * 获取评论的列表
     * @param pageReqVO 分页参数
     * @return 分页数据
     */
    PageResult<CommentDetailVO> getList(CommentPageReqVO pageReqVO);

    /**
     * 发表评论
     * @param commentPushVO 评论参数
     */
    void push(CommentPushVO commentPushVO);

    /**
     * 删除评论
     */
    void delete(CommentDeleteVO detailVO);

    /**
     * 获取评论的列表
     * @param pageReqVO 分页参数
     * @return
     */
    PageResult<CommentAdminDetailVO> getCommentList(CommonAdminPageReqVO pageReqVO);

    /**
     * 修改评论
     */
    void updateComment(CommentUpdateVO updateVO);
}
