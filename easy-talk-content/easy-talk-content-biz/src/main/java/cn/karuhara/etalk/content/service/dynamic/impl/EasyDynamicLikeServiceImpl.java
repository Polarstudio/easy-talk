package cn.karuhara.etalk.content.service.dynamic.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.karuhara.etalk.content.entity.dynamic.EasyDynamicLike;
import cn.karuhara.etalk.content.service.dynamic.EasyDynamicLikeService;
import cn.karuhara.etalk.content.mapper.dynamic.EasyDynamicLikeMapper;
import org.springframework.stereotype.Service;

/**
* @author atom
* @description 针对表【easy_dynamic_like(动态点赞)】的数据库操作Service实现
* @createDate 2024-11-26 16:51:20
*/
@Service
public class EasyDynamicLikeServiceImpl extends ServiceImpl<EasyDynamicLikeMapper, EasyDynamicLike>
    implements EasyDynamicLikeService{

    @Override
    public Long getLikeCount(Integer dynamicId) {
        LambdaQueryWrapper<EasyDynamicLike> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyDynamicLike::getDynamicId, dynamicId);
        return count(queryWrapper);
    }

    /**
     * 点赞或取消点赞
     *
     * @param dynamicId 动态id
     * @param userId    用户id
     */
    @Override
    public void like(Integer dynamicId, Integer userId) {
        LambdaQueryWrapper<EasyDynamicLike> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyDynamicLike::getDynamicId, dynamicId);
        queryWrapper.eq(EasyDynamicLike::getUserId, userId);
        //先查询是否已经点赞
        EasyDynamicLike one = getOne(queryWrapper);
        if (one!=null){
            remove(queryWrapper);
        }else {
            EasyDynamicLike easyDynamicLike = new EasyDynamicLike();
            easyDynamicLike.setDynamicId(dynamicId);
            easyDynamicLike.setUserId(userId);
            saveOrUpdate(easyDynamicLike);
        }
    }

    /**
     * 查询用户是否点赞某个动态
     *
     * @param dynamicId 动态id
     * @param userId    用户id
     * @return 是否已经点赞
     */
    @Override
    public Boolean checkLike(Integer dynamicId, Integer userId) {
        LambdaQueryWrapper<EasyDynamicLike> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyDynamicLike::getDynamicId, dynamicId);
        queryWrapper.eq(EasyDynamicLike::getUserId, userId);
        EasyDynamicLike one = getOne(queryWrapper);
        return one != null;
    }
}




