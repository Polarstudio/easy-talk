package cn.karuhara.etalk.content.mapper.activity;

import cn.karuhara.etalk.content.entity.activity.EasyActivity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author atom
* @description 针对表【easy_activity(活动)】的数据库操作Mapper
* @createDate 2024-11-19 10:49:07
* @Entity cn.karuhara.etalk.content.entity.activity.EasyActivity
*/
public interface EasyActivityMapper extends BaseMapper<EasyActivity> {

}




