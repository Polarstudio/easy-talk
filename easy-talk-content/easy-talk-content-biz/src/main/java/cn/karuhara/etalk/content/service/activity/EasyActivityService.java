package cn.karuhara.etalk.content.service.activity;

import cn.karuhara.etalk.content.controller.admin.activity.vo.*;
import cn.karuhara.etalk.content.controller.app.activity.vo.*;
import cn.karuhara.etalk.content.entity.activity.EasyActivity;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author atom
* @description 针对表【easy_activity(活动)】的数据库操作Service
* @createDate 2024-11-19 10:49:07
*/
public interface EasyActivityService extends IService<EasyActivity> {

    /**
     * 新增活动
     * @param addReqVO 新增活动参数
     */
    void addActivity(ActivityAddReqVO addReqVO);

    /**
     * 管理端 - 分页获取活动
     * @param pageReqVO 分页参数
     * @return 返回的分页集合
     */
    PageResult<ActivityPageRespVO> getPageList(ActivityPageReqVO pageReqVO);

    /**
     * 更新活动
     * @param updateReqVO 活动实体
     */
    void updateActivity(ActivityUpdateReqVO updateReqVO);

    /**
     * App端 - 分页获取全部的活动
     * @param pageReqVO 分页参数
     * @return 结果集
     */
    PageResult<ActivityAppPageRespVO> getList(ActivityAppPageReqVO pageReqVO);

    /**
     * 分页获取用户参与的活动
     * @return 活动结果集
     */
    PageResult<ActivityAppPageRespVO> getAttendList(ActivityAppPageReqVO pageReqVO);

    /**
     * 获取活动的详细信息
     * @param id 活动id
     * @return 活动详细信息
     */
    ActivityAppDetailVO getDetail(Integer id);

    /**
     * 发布或修改活动
     * @param appAddVO 活动的参数
     */
    void addAppActivity(ActivityAppAddVO appAddVO);

    /**
     * 获取活动的tag信息
     * @param pageReqVO 分页参数
     * @return list
     */
    PageResult<ActivityAppBriefVO> getTags(ActivityAppPageReqVO pageReqVO);

    /**
     * 管理后台 - 获取活动的tag信息
     * @return
     */
    List<ActivityAppBriefVO> getAdminTags();

    /**
     */
    void delete(ActivityDeleteVO deleteVO);

    /**
     * 获取推荐的活动
     * @return 推荐的活动
     */
    ActivityAppBaseVO getRecommendActivity();
}
