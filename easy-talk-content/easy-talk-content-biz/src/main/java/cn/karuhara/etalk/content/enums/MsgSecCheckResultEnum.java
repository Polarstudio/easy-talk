package cn.karuhara.etalk.content.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Description: 审核结果枚举类
 * @author: 轻语
 */
@Getter
@AllArgsConstructor
public enum MsgSecCheckResultEnum {
    RISKY("risky", "屏蔽"),
    REVIEW("review", "复审"),
    PASS("pass", "通过");

    @EnumValue
    @JsonValue
    private final String code;

    private final String desc;
}
