package cn.karuhara.etalk.content.controller.admin.activity;

import cn.karuhara.etalk.content.controller.admin.activity.vo.*;
import cn.karuhara.etalk.content.controller.app.activity.vo.ActivityAppBriefVO;
import cn.karuhara.etalk.content.service.activity.EasyActivityService;
import cn.karuhara.etalk.framework.annotation.WeLog;
import cn.karuhara.etalk.framework.common.pojo.CommonResult;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "管理后台 - 活动模块")
@RestController
@RequestMapping("/content/activity/admin")
@Validated
public class ActivityController {

    @Resource
    private EasyActivityService service;

    @Resource
    private EasyActivityService activityService;

    @PostMapping
    @WeLog(description = "新增活动操作")
    @Operation(summary = "新增活动")
    public CommonResult<Void> add(@Valid @RequestBody ActivityAddReqVO addReqVO) {
        service.addActivity(addReqVO);
        return CommonResult.success();
    }

    @PostMapping("/page")
    @Operation(summary = "分页获取活动")
    public CommonResult<PageResult<ActivityPageRespVO>> getPageList(@Valid @RequestBody ActivityPageReqVO pageReqVO) {
        PageResult<ActivityPageRespVO> pageResult =  service.getPageList(pageReqVO);
        return CommonResult.success(pageResult);
    }

    @PutMapping
    @Operation(description = "更新活动")
    @WeLog(description = "更新活动操作")
    public CommonResult<Void> update(@Valid @RequestBody ActivityUpdateReqVO updateReqVO) {
        service.updateActivity(updateReqVO);
        return CommonResult.success();
    }

    @Operation(summary = "管理后台 - 获取活动的tag信息")
    @GetMapping("/list/tags")
    public CommonResult<List<ActivityAppBriefVO>> getTags(){
        List<ActivityAppBriefVO> list = activityService.getAdminTags();
        return CommonResult.success(list);
    }

    @Schema(description = "删除活动")
    @DeleteMapping
    public CommonResult<Void> delete(@RequestBody ActivityDeleteVO deleteVO){
        activityService.delete(deleteVO);
        return CommonResult.success();
    }
}
