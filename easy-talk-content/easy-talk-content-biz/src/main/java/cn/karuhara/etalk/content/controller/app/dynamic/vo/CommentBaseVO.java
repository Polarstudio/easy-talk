package cn.karuhara.etalk.content.controller.app.dynamic.vo;

import cn.karuhara.etalk.content.enums.CommentStatusEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;

@Schema(description = "动态评论的基本信息")
@Data
public class CommentBaseVO {
    @Schema(description = "评论id")
    private Integer id;

    @Schema(description = "评论者的id")
    private Integer userId;

    @Schema(description = "评论内容")
    private String content;

    @Schema(description = "状态：0 待审核、1 正常、2、驳回")
    private CommentStatusEnum status;

    @Schema(description = "评论时间")
    @JsonFormat(pattern = "MM-dd HH:mm", timezone = "GMT+8")
    private LocalDateTime date;
}
