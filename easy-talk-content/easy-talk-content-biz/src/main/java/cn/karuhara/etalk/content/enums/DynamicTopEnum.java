package cn.karuhara.etalk.content.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 动态置顶枚举类
 */
@Getter
@AllArgsConstructor
public enum DynamicTopEnum {
    TOP("0","不置顶"),
    NOT_TOP("1","置顶");

    @EnumValue
    @JsonValue
    private final String code;

    private final String desc;
}
