package cn.karuhara.etalk.content.entity.topic;

import cn.karuhara.etalk.content.enums.TopicStatusEnum;
import cn.karuhara.etalk.framework.mybatis.core.dataobject.BaseDO;
import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 话题表
 * @TableName easy_topic
 */
@EqualsAndHashCode(callSuper = true)
@TableName(value ="easy_topic")
@Data
public class EasyTopic extends BaseDO implements  Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 话题名称
     */
    private String topicName;

    /**
     * 话题描述
     */
    private String intro;

    /**
     * 创键者
     */
    private Integer userId;

    /**
     * 话题关注量
     */
    private Integer follow;

    /**
     * 话题动态量
     */
    private Integer dynamic;

    /**
     * 话题浏览量
     */
    private Integer views;

    /**
     * 状态
     */
    private TopicStatusEnum status;

    @TableLogic
    private Integer deleted;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}