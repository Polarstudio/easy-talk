package cn.karuhara.etalk.content.mapper.dynamic;

import cn.karuhara.etalk.content.entity.dynamic.EasyDynamicCommentLike;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
/**
* @author atom
* @description 针对表【easy_dynamic_comment_like】的数据库操作Mapper
* @createDate 2024-12-12 16:24:31
* @Entity cn.karuhara.etalk.content.entity/dynamic.EasyDynamicCommentLike
*/
public interface EasyDynamicCommentLikeMapper extends BaseMapper<EasyDynamicCommentLike> {

}




