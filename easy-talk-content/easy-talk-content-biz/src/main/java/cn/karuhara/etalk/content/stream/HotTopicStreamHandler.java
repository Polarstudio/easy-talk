package cn.karuhara.etalk.content.stream;

import cn.karuhara.etalk.framework.common.constants.KafkaStreamConstant;
import cn.karuhara.etalk.framework.common.model.mess.TopicStreamMess;
import cn.karuhara.etalk.framework.common.model.mess.UpdateTopicMess;
import com.alibaba.cloud.commons.lang.StringUtils;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Update;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.*;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.time.Duration;

@Component
@Slf4j
public class HotTopicStreamHandler {

    @Bean
    public KStream<String, String> hotTopicStream(StreamsBuilder streamsBuilder) {
        log.info("hotTopicStream start");
        //接收消息
        KStream<String, String> stream = streamsBuilder.stream(KafkaStreamConstant.HOT_TOPIC_SCORE_TOPIC);
        //聚合流式处理
        stream.map((key,value)->{
            UpdateTopicMess mess = JSONObject.parseObject(value, UpdateTopicMess.class);
            //重置消息的key:13   和  value: follow:1
            return new KeyValue<>(mess.getTopicId().toString(), mess.getType().name()+":"+mess.getAdd());
        })
                //按照文章id进行聚合
                .groupBy((key,value)->key)
                //时间窗口,这里设置1分钟
                .windowedBy(TimeWindows.of(Duration.ofSeconds(10)))
                .aggregate(new Initializer<String>() {
                    /**
                     * 初始方法，返回值是消息的value
                     * @return 聚合开始前的数据
                     */
                    @Override
                    public String apply() {
                        return "FOLLOW:0,DYNAMIC:0,VIEWS:0";
                    }
                }, (key, value, aggValue) -> {
                    if(StringUtils.isBlank(value)){
                        return aggValue;
                    }
                    String[] aggAry = aggValue.split(",");
                    int  fol= 0,dyn=0,vie=0;
                    for (String agg : aggAry) {
                        String[] split = agg.split(":");
                        switch (UpdateTopicMess.UpdateArticleType.valueOf(split[0])){
                            case FOLLOW:
                                fol = Integer.parseInt(split[1]);
                                break;
                            case DYNAMIC:
                                dyn = Integer.parseInt(split[1]);
                                break;
                            case VIEWS:
                                vie = Integer.parseInt(split[1]);
                                break;
                        }
                    }
                    /**
                     * 累加操作
                     */
                    String[] valAry = value.split(":");
                    switch (UpdateTopicMess.UpdateArticleType.valueOf(valAry[0])){
                        case FOLLOW:
                            fol += Integer.parseInt(valAry[1]);
                            break;
                        case DYNAMIC:
                            dyn += Integer.parseInt(valAry[1]);
                            break;
                        case VIEWS:
                            vie += Integer.parseInt(valAry[1]);
                            break;
                    }
                    String format = String.format("FOLLOW:%d,DYNAMIC:%d,VIEWS:%d", fol, dyn, vie);
                    System.out.println("文章的id:"+key);
                    System.out.println("当前时间窗口内的消息处理结果："+format);
                    return format;
                }, Materialized.as("hot-topic-stream-count-001"))
                .toStream()
                .map((key,value)->{
                    return new KeyValue<>(key.key(),formatObj(key.key(),value));
                })
                //发送消息
                .to(KafkaStreamConstant.HOT_TOPIC_INCR_HANDLE_TOPIC);
        log.info("hotTopicStream end");
        return stream;
    }

    /**
     * 格式化消息的value数据
     * @param topicId 话题id
     * @param value 聚合后的数据
     * @return 聚合结果的json字符串
     */
    public String formatObj(String topicId,String value){
        TopicStreamMess mess = new TopicStreamMess();
        mess.setTopicId(Long.valueOf(topicId));
        //FOLLOW:0,DYNAMIC:0,VIEWS:0
        String[] valAry = value.split(",");
        for (String val : valAry) {
            String[] split = val.split(":");
            switch (UpdateTopicMess.UpdateArticleType.valueOf(split[0])){
                case FOLLOW:
                    mess.setFollow(Integer.parseInt(split[1]));
                    break;
                case DYNAMIC:
                    mess.setDynamic(Integer.parseInt(split[1]));
                    break;
                case VIEWS:
                    mess.setView(Integer.parseInt(split[1]));
                    break;
            }
        }
        log.info("聚合消息处理之后的结果为:{}",JSONObject.toJSONString(mess));
        return JSONObject.toJSONString(mess);
    }
}
