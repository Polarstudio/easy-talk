package cn.karuhara.etalk.content.controller.app.activity.vo;

import cn.karuhara.etalk.content.enums.ActivityPushEnum;
import cn.karuhara.etalk.content.enums.ActivityStatusEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Schema(description = "活动的详细信息")
@Data
public class ActivityAppDetailVO extends ActivityAppBaseVO{

    @Schema(description = "活动简介",requiredMode = Schema.RequiredMode.REQUIRED,example = "一起来打排球吧！")
    @NotNull(message = "活动简介不能为空！")
    private String intro;

    @Schema(description = "介绍图 例如 活动的海报",requiredMode = Schema.RequiredMode.REQUIRED)
    private List<String> imgs;

    @Schema(description = "地点纬度  例如 39.90420000",requiredMode = Schema.RequiredMode.REQUIRED,example = "39.90420000")
    @NotNull(message = "地点纬度不能为空！")
    private Double latitude;

    @Schema(description = "地点经度  例如 116.40740000",requiredMode = Schema.RequiredMode.REQUIRED,example = "116.40740000")
    @NotNull(message = "地点经度不能为空！")
    private Double longitude;

    @Schema(description = "地点位置",requiredMode = Schema.RequiredMode.REQUIRED,example = "北京市朝阳区 XX 街道 XX 号")
    @NotNull(message = "地点位置不能为空！")
    private String address;

    @Schema(description = "创建人的昵称")
    private String nikeName;

    @Schema(description = "创建人的头像")
    private String avatar;

    @Schema(description = "活动的参与人数")
    private Long attendNum;

    @Schema(description = "用户认证名称")
    private String certificateName;
}
