package cn.karuhara.etalk.content.controller.admin.activity.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "分页获取活动的结果VO")
public class ActivityPageRespVO extends ActivityBaseVO{

    @Schema(description = "浏览")
    private Integer browse;

    @Schema(description = "活动参加人数")
    private long attendNum;

    @Schema(description = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

}
