package cn.karuhara.etalk.content.controller.app.topic.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "最简单的话题信息 - 提供给发布动态使用")
@Data
public class TopicAppBriefVO {

    @Schema(description = "Id",example = "1")
    private Integer id;

    @Schema(description = "话题名称",example = "轻语社怎么样")
    private String topicName;
}
