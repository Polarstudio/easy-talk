package cn.karuhara.etalk.content.service.circle;

import cn.karuhara.etalk.content.controller.admin.circle.vo.*;
import cn.karuhara.etalk.content.controller.app.circle.vo.*;
import cn.karuhara.etalk.content.entity.circle.EasyCircle;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author atom
* @description 针对表【easy_circle(圈子)】的数据库操作Service
* @createDate 2024-11-17 20:25:04
*/
public interface EasyCircleService extends IService<EasyCircle> {

    /**
     * 新增圈子
     * @param circleAddVO 新增圈子参数
     */
    void addCircle(CircleAddReqVO circleAddVO);

    /**
     * 分页获取圈子
     * @param circlePageReqVO 分页参数
     * @return 圈子列表结果
     */
    PageResult<CirclePageRespVO> getPageList(CirclePageReqVO circlePageReqVO);

    /**
     * 修改圈子
     * @param circleUpdateReqVO 修改圈子参数
     */
    void updateCircle(CircleUpdateReqVO circleUpdateReqVO);

    /**
     * APP端 - 获取圈子列表
     * @param appPageReqVO 分页参数
     * @return 圈子结果
     */
    PageResult<CircleAppDetailVO> getAppPageList(CircleAppPageReqVO appPageReqVO);

    /**
     * 获取推送的圈子信息
     * @return 圈子列表
     */
    List<CircleAppPushVO> getPushList();

    /**
     * 获取推荐圈子列表
     * @return 圈子列表
     */
    List<CircleAppRecommendVO> getRecommend();

    /**
     * 分页获取用户关注的圈子
     * @param appPageReqVO 分页参数
     * @return 圈子信息
     */
    PageResult<CircleAppDetailVO> getFollowList(CircleAppPageReqVO appPageReqVO);

    /**
     * 根据圈子id获取圈子的详细信息
     * @param circleId 圈子id
     * @return 圈子详细信息
     */
    CircleAppDetailVO getByCircleId(Integer circleId);

    /**
     *  用户端添加或编辑圈子
     * @param addReqVO 圈子信息
     */
    void addCircleApp(CircleAddReqVO addReqVO);

    /**
     * 获取话题的id和名称
     * @param appPageReqVO 分页参数
     * @return list
     */
    PageResult<CircleAppBriefVO> getTags(CircleAppPageReqVO appPageReqVO);

    /**
     * 获取用户加入的圈子的数量
     * @return 数量
     */
    Long getUserAttendCount();

    /**
     * 删除圈子
     */
    void delete(CircleDeleteVO deleteVO);

    /**
     * 管理后台 - 获取圈子的id和名称
     * @return
     */
    List<CircleAppBriefVO> getAdminTags();
}
