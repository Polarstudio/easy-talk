package cn.karuhara.etalk.content.service.dynamic.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.karuhara.etalk.content.entity.dynamic.EasyDynamicImg;
import cn.karuhara.etalk.content.service.dynamic.EasyDynamicImgService;
import cn.karuhara.etalk.content.mapper.dynamic.EasyDynamicImgMapper;
import org.springframework.stereotype.Service;

/**
* @author atom
* @description 针对表【easy_dynamic_img(动态图片)】的数据库操作Service实现
* @createDate 2024-11-26 21:21:49
*/
@Service
public class EasyDynamicImgServiceImpl extends ServiceImpl<EasyDynamicImgMapper, EasyDynamicImg>
    implements EasyDynamicImgService{

}




