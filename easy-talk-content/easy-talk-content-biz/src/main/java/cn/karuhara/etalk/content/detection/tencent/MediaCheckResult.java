package cn.karuhara.etalk.content.detection.tencent;

import com.alibaba.fastjson2.annotation.JSONField;
import lombok.Data;

@Data
public class MediaCheckResult {
    // 错误码
    @JSONField(name = "errcode")
    private int errCode;
    // 错误信息
    @JSONField(name = "errmsg")
    private String errMsg;
    // 唯一请求标识，标记单次请求，用于匹配异步推送结果
    @JSONField(name = "trace_id")
    private String traceId;
}
