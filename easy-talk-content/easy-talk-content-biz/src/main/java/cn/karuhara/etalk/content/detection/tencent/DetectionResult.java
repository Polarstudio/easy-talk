package cn.karuhara.etalk.content.detection.tencent;

import cn.karuhara.etalk.content.enums.MsgSecCheckResultEnum;
import lombok.Data;

/**
 * 审核结果
 */
@Data
public class DetectionResult {
    /**
     * 对应的内容id，如评论id，动态id
     */
    private Integer postId;
    /**
     * 审核结果
     */
    private MsgSecCheckResultEnum result;
}
