package cn.karuhara.etalk.content.service.dynamic;

import cn.karuhara.etalk.content.controller.app.dynamic.vo.CommentLikeVO;
import cn.karuhara.etalk.content.entity.dynamic.EasyDynamicCommentLike;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author atom
* @description 针对表【easy_dynamic_comment_like】的数据库操作Service
* @createDate 2024-12-12 16:24:31
*/
public interface EasyDynamicCommentLikeService extends IService<EasyDynamicCommentLike> {

    /**
     * 点赞或点踩评论
     * @param commentLikeVO 点赞或点踩评论
     */
    void like(CommentLikeVO commentLikeVO);
}
