package cn.karuhara.etalk.content.service.dynamic.impl;

import cn.karuhara.etalk.content.controller.app.dynamic.vo.CommentLikeVO;
import cn.karuhara.etalk.content.convert.CommentConvert;
import cn.karuhara.etalk.content.mapper.dynamic.EasyDynamicCommentLikeMapper;
import cn.karuhara.etalk.content.service.dynamic.EasyDynamicCommentLikeService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.karuhara.etalk.content.entity.dynamic.EasyDynamicCommentLike;
import org.springframework.stereotype.Service;

/**
* @author atom
* @description 针对表【easy_dynamic_comment_like】的数据库操作Service实现
* @createDate 2024-12-12 16:24:31
*/
@Service
public class EasyDynamicCommentLikeServiceImpl extends ServiceImpl<EasyDynamicCommentLikeMapper, EasyDynamicCommentLike>
    implements EasyDynamicCommentLikeService {

    /**
     * 点赞或点踩评论
     *
     * @param commentLikeVO 点赞或点踩评论
     */
    @Override
    public void like(CommentLikeVO commentLikeVO) {
        LambdaQueryWrapper<EasyDynamicCommentLike> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyDynamicCommentLike::getCommentId, commentLikeVO.getCommentId());
        queryWrapper.eq(EasyDynamicCommentLike::getUserId, commentLikeVO.getUserId());
        queryWrapper.eq(EasyDynamicCommentLike::getDynamicId, commentLikeVO.getDynamicId());
        queryWrapper.eq(EasyDynamicCommentLike::getType, commentLikeVO.getType());
        //先查询是否存在，如果存在则取消
        EasyDynamicCommentLike one = getOne(queryWrapper);
        if (one != null){
            remove(queryWrapper);
            return;
        }
        LambdaQueryWrapper<EasyDynamicCommentLike> wrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyDynamicCommentLike::getCommentId, commentLikeVO.getCommentId());
        queryWrapper.eq(EasyDynamicCommentLike::getUserId, commentLikeVO.getUserId());
        queryWrapper.eq(EasyDynamicCommentLike::getDynamicId, commentLikeVO.getDynamicId());
        remove(wrapper);
        EasyDynamicCommentLike commentLike = CommentConvert.INSTANT.likeVOToComment(commentLikeVO);
        save(commentLike);
    }
}




