package cn.karuhara.etalk.content.entity.dynamic;
import cn.karuhara.etalk.framework.mybatis.core.dataobject.BaseDO;
import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 动态视频
 * @TableName easy_dynamic_video
 */
@EqualsAndHashCode(callSuper = true)
@TableName(value ="easy_dynamic_video")
@Data
public class EasyDynamicVideo extends BaseDO implements Serializable {
    /**
     * ID
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 动态ID
     */
    private Integer dynamicId;

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 封面
     */
    private String videoCover;

    /**
     * 地址
     */
    private String videoUrl;

    /**
     * 高度
     */
    private Integer height;

    /**
     * 宽带
     */
    private Integer width;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}