package cn.karuhara.etalk.content.service.dynamic;

import cn.karuhara.etalk.content.entity.dynamic.EasyDynamicLike;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author atom
* @description 针对表【easy_dynamic_like(动态点赞)】的数据库操作Service
* @createDate 2024-11-26 16:51:20
*/
public interface EasyDynamicLikeService extends IService<EasyDynamicLike> {

    /**
     * 获取动态的点赞数量
     * @param dynamicId 动态ID
     * @return 点赞数量
     */
    Long getLikeCount(Integer dynamicId);

    /**
     * 点赞或取消点赞
     * @param dynamicId 动态id
     * @param userId 用户id
     */
    void like(Integer dynamicId, Integer userId);

    /**
     * 查询用户是否点赞某个动态
     * @param dynamicId 动态id
     * @param userId 用户id
     * @return 是否已经点赞
     */
    Boolean checkLike(Integer dynamicId, Integer userId);
}
