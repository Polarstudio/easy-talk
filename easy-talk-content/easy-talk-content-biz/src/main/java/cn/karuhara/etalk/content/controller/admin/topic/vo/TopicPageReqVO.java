package cn.karuhara.etalk.content.controller.admin.topic.vo;

import cn.karuhara.etalk.framework.common.pojo.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "分页获取话题操作")
public class TopicPageReqVO extends PageParam {

    @Schema(description = "话题名称",example = "轻语社怎么样")
    private String topicName;

    @Schema(description = "话题状态（0正常，1推送、2隐藏）",example = "0")
    private String status;

}
