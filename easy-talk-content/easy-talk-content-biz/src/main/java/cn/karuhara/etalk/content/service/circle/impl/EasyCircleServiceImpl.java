package cn.karuhara.etalk.content.service.circle.impl;

import cn.karuhara.etalk.content.controller.admin.circle.vo.*;
import cn.karuhara.etalk.content.controller.app.circle.vo.*;
import cn.karuhara.etalk.content.convert.CircleConvert;
import cn.karuhara.etalk.content.entity.circle.EasyCircle;
import cn.karuhara.etalk.content.entity.circle.EasyCircleFollow;
import cn.karuhara.etalk.content.entity.dynamic.EasyDynamic;
import cn.karuhara.etalk.content.enums.CircleStatusEnum;
import cn.karuhara.etalk.content.mapper.circle.EasyCircleMapper;
import cn.karuhara.etalk.content.mq.product.CircleProducer;
import cn.karuhara.etalk.content.service.circle.EasyCircleFollowService;
import cn.karuhara.etalk.content.service.circle.EasyCircleService;
import cn.karuhara.etalk.content.service.dynamic.EasyDynamicService;
import cn.karuhara.etalk.framework.common.constants.GlobalConstant;
import cn.karuhara.etalk.framework.common.context.LoginContextHolder;
import cn.karuhara.etalk.framework.common.enums.GlobalCodeEnum;
import cn.karuhara.etalk.framework.common.exception.ServiceException;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import cn.karuhara.etalk.member.api.MemberApi;
import cn.karuhara.etalk.member.api.dto.MemberBaseInfoDTO;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import jakarta.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
* @author atom
* @description 针对表【easy_circle(圈子)】的数据库操作Service实现
* @createDate 2024-11-17 20:25:04
*/
@Service
public class EasyCircleServiceImpl extends ServiceImpl<EasyCircleMapper, EasyCircle>
    implements EasyCircleService {

    @Resource
    private EasyCircleMapper circleMapper;

    @Resource
    private EasyDynamicService dynamicService;

    @Resource
    private EasyCircleFollowService followService;

    @Resource
    private MemberApi memberApi;


    @Resource
    private CircleProducer circleProducer;

    @Override
    public void addCircle(CircleAddReqVO circleAddVO) {
        //先判断是否存在同名的圈子
        EasyCircle byName = getByName(circleAddVO.getCircleName());
        if (byName != null) {
            throw new ServiceException(GlobalCodeEnum.FAIL.getCode(), GlobalConstant.CIRCLE_NAME_EXISTS);
        }
        EasyCircle easyCircle = CircleConvert.INSTANCE.addVOToCircle(circleAddVO);
        easyCircle.setUserId(1);
        save(easyCircle);
    }

    @Override
    public PageResult<CirclePageRespVO> getPageList(CirclePageReqVO circlePageReqVO) {
        LambdaQueryWrapper<EasyCircle> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(circlePageReqVO.getCircleName())){
            lambdaQueryWrapper.like(EasyCircle::getCircleName, circlePageReqVO.getCircleName());
        }
        if (StringUtils.isNotBlank(circlePageReqVO.getStatus())){
            lambdaQueryWrapper.eq(EasyCircle::getStatus, circlePageReqVO.getStatus());
        }
        Page<EasyCircle> page = new Page<>(circlePageReqVO.getPageNo(), circlePageReqVO.getPageSize());
        IPage<EasyCircle> pageResult = circleMapper.selectPage(page, lambdaQueryWrapper);
        if (pageResult.getTotal() == 0){
            return PageResult.empty();
        }
        List<EasyCircle> list = pageResult.getRecords();
        List<CirclePageRespVO> circlePageRespVOS = CircleConvert.INSTANCE.circleListToPageRespVOList(list);
        return new PageResult<>(circlePageRespVOS, pageResult.getPages());
    }

    @Override
    public void updateCircle(CircleUpdateReqVO circleUpdateReqVO) {
        //先判断是否存在同名的圈子
        EasyCircle byName = getByName(circleUpdateReqVO.getCircleName());
        if (byName != null && !Objects.equals(byName.getId(), circleUpdateReqVO.getId())) {
            throw new ServiceException(GlobalCodeEnum.FAIL.getCode(), GlobalConstant.CIRCLE_NAME_EXISTS);
        }
        EasyCircle easyCircle = CircleConvert.INSTANCE.updateVOToCircle(circleUpdateReqVO);
        updateById(easyCircle);
    }

    @Override
    public PageResult<CircleAppDetailVO> getAppPageList(CircleAppPageReqVO appPageReqVO) {
        LambdaQueryWrapper<EasyCircle> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(appPageReqVO.getCircleName())){
            lambdaQueryWrapper.like(EasyCircle::getCircleName,appPageReqVO.getCircleName());
        }
        if(appPageReqVO.getUserId() != null){
            lambdaQueryWrapper.eq(EasyCircle::getUserId,appPageReqVO.getUserId());
        }else {
            lambdaQueryWrapper.eq(EasyCircle::getStatus,CircleStatusEnum.NORMAL)
                    .or()
                    .eq(EasyCircle::getStatus,CircleStatusEnum.PUSH);
        }
        Page<EasyCircle> page = new Page<>(appPageReqVO.getPageNo(), appPageReqVO.getPageSize());
        IPage<EasyCircle> pageResult = circleMapper.selectPage(page, lambdaQueryWrapper);
        if (pageResult.getTotal() == 0){
            return PageResult.empty();
        }
        List<EasyCircle> records = pageResult.getRecords();
        List<CircleAppDetailVO> circleAppDetailVOS = CircleConvert.INSTANCE.circleListToAppCircleDetailVOs(records);
        circleAppDetailVOS.forEach(this::fillCircleCounts);
        return new PageResult<>(circleAppDetailVOS, pageResult.getPages());
    }

    @Override
    public List<CircleAppPushVO> getPushList() {
        LambdaQueryWrapper<EasyCircle> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(EasyCircle::getStatus, CircleStatusEnum.PUSH);
        List<EasyCircle> list = circleMapper.selectList(lambdaQueryWrapper);
        List<CircleAppPushVO> pushVOS = CircleConvert.INSTANCE.circleListToAppCirclePushVOList(list);
        pushVOS.forEach(circleAppPushVO -> {
            long dynamicCount = getDynamicCount(circleAppPushVO.getId());
            circleAppPushVO.setDynamicCount(dynamicCount);
            long memberCount = getMemberCount(circleAppPushVO.getId());
            circleAppPushVO.setMemberCount(memberCount);
        });
        return pushVOS;
    }

    @Override
    public List<CircleAppRecommendVO> getRecommend() {
        LambdaQueryWrapper<EasyCircle> queryWrapper = new LambdaQueryWrapper<>();
        //先按照权重来排序，在按浏览量来排序，并取4条
        queryWrapper.eq(EasyCircle::getStatus, CircleStatusEnum.PUSH);
        List<EasyCircle> circles = list(queryWrapper);
        List<CircleAppRecommendVO> recommendVOS = CircleConvert.INSTANCE.circleListToAppCircleRecommend(circles);
        recommendVOS.forEach(recommendVO -> {
            //获取圈子的动态数量
            LambdaQueryWrapper<EasyDynamic> dynamicWrapper1 = new LambdaQueryWrapper<>();
            dynamicWrapper1.eq(EasyDynamic::getCircleId,recommendVO.getId());
            long dynamicCount = dynamicService.count(dynamicWrapper1);
            recommendVO.setDynamicCount(dynamicCount);
        });
        return recommendVOS;
    }

    /**
     * 分页获取用户关注的圈子
     *
     * @param appPageReqVO 分页参数
     * @return 圈子信息
     */
    @Override
    public PageResult<CircleAppDetailVO> getFollowList(CircleAppPageReqVO appPageReqVO) {
        //先判断用户是否登录
        Integer userId = LoginContextHolder.get();
        if (userId == null){
            return PageResult.empty();
        }
        //先查询用户关注的圈子的id
        LambdaQueryWrapper<EasyCircleFollow> followLambdaQueryWrapper = new LambdaQueryWrapper<>();
        followLambdaQueryWrapper.eq(EasyCircleFollow::getUserId,userId);
        List<EasyCircleFollow> follows = followService.list(followLambdaQueryWrapper);
        List<Integer> circleIds = follows.stream().map(EasyCircleFollow::getCircleId).collect(Collectors.toList());
        if (circleIds.isEmpty()){
            return PageResult.empty();
        }
        //分页获取关注的圈子信息
        LambdaQueryWrapper<EasyCircle> queryWrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(appPageReqVO.getCircleName())){
            queryWrapper.eq(EasyCircle::getCircleName,appPageReqVO.getCircleName());
        }
        queryWrapper.in(EasyCircle::getId, circleIds);
        Page<EasyCircle> page = new Page<>(appPageReqVO.getPageNo(),appPageReqVO.getPageSize());
        Page<EasyCircle> pageResult = circleMapper.selectPage(page, queryWrapper);
        if (pageResult.getTotal() == 0){
            return PageResult.empty();
        }
        List<EasyCircle> circles = pageResult.getRecords();
        List<CircleAppDetailVO> circleAppDetailVOS = CircleConvert.INSTANCE.circleListToAppCircleDetailVOs(circles);
        circleAppDetailVOS.forEach(this::fillCircleCounts);
        return new PageResult<>(circleAppDetailVOS, pageResult.getPages());
    }

    /**
     * 根据圈子id获取圈子的详细信息
     *
     * @param circleId 圈子id
     * @return 圈子详细信息
     */
    @Override
    public CircleAppDetailVO getByCircleId(Integer circleId) {
        EasyCircle circle = getById(circleId);
        CircleAppDetailVO circleAppDetailVO = CircleConvert.INSTANCE.circleToDetailVO(circle);
        long dynamicCount = getDynamicCount(circleAppDetailVO.getId());
        circleAppDetailVO.setDynamicCount(dynamicCount);
        long memberCount = getMemberCount(circleAppDetailVO.getId());
        circleAppDetailVO.setMemberCount(memberCount);
        //获取创建者的信息
        MemberBaseInfoDTO memberBaseInfo = memberApi.getMemberBaseInfo(circle.getUserId());
        circleAppDetailVO.setNikeName(memberBaseInfo.getNikeName());
        circleAppDetailVO.setUserType(memberBaseInfo.getType());
        circleAppDetailVO.setAvatar(memberBaseInfo.getAvatar());
        //获取最后一条动态发布的时间
        LambdaQueryWrapper<EasyCircle> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyCircle::getId, circleId).orderByDesc(EasyCircle::getCreateTime);
        EasyCircle one = getOne(queryWrapper);
        circleAppDetailVO.setUpdateDynamicTime(one.getCreateTime());
        return circleAppDetailVO;
    }

    /**
     * 用户端添加或编辑圈子
     *
     * @param addReqVO 圈子信息
     */
    @Override
    public void addCircleApp(CircleAddReqVO addReqVO) {
        Integer userId = LoginContextHolder.get();
        if (userId == null){
            throw new ServiceException(GlobalCodeEnum.LOGIN_AUTH);
        }
        //先判断是否存在同名的圈子
//        EasyCircle byName = getByName(addReqVO.getCircleName());
//        if (byName != null && byName.getId() != addReqVO.getId()) {
//            throw new ServiceException(GlobalCodeEnum.FAIL.getCode(), GlobalConstant.CIRCLE_NAME_EXISTS);
//        }
        EasyCircle easyCircle = CircleConvert.INSTANCE.addVOToCircle(addReqVO);
        easyCircle.setUserId(userId);
        //无论新增还更改，都需要审核
        easyCircle.setStatus(CircleStatusEnum.REVIEWED);
        saveOrUpdate(easyCircle);
        //TODO 异步删除之前的封面
        if (addReqVO.getId() != null){
        }
    }

    /**
     * 获取话题的id和名称
     *
     * @param appPageReqVO 分页参数
     * @return list
     */
    @Override
    public PageResult<CircleAppBriefVO> getTags(CircleAppPageReqVO appPageReqVO) {
        LambdaQueryWrapper<EasyCircle> queryWrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(appPageReqVO.getCircleName())){
            queryWrapper.like(EasyCircle::getCircleName,appPageReqVO.getCircleName());
        }
        Page<EasyCircle> page = new Page<>(appPageReqVO.getPageNo(),appPageReqVO.getPageSize());
        Page<EasyCircle> pageResult = circleMapper.selectPage(page, queryWrapper);
        if (pageResult.getTotal() == 0){
            return PageResult.empty();
        }
        List<EasyCircle> circles = pageResult.getRecords();
        List<CircleAppBriefVO> circleAppBriefVOS = CircleConvert.INSTANCE.circleListToAppBriefVOList(circles);
        return new PageResult<>(circleAppBriefVOS, pageResult.getPages());
    }

    /**
     * 获取用户加入的圈子的数量
     *
     * @return 数量
     */
    @Override
    public Long getUserAttendCount() {
        Integer userId = LoginContextHolder.get();
        if (userId == null){
            throw new ServiceException(GlobalCodeEnum.LOGIN_AUTH);
        }
        LambdaQueryWrapper<EasyCircleFollow> followWrapper = new LambdaQueryWrapper<>();
        followWrapper.eq(EasyCircleFollow::getUserId,userId);
        List<EasyCircleFollow> follows = followService.list(followWrapper);
        return (long) follows.size();
    }

    /**
     * 删除圈子
     */
    @Override
    public void delete(CircleDeleteVO deleteVO) {
        //取消所有关联的动态
        dynamicService.canalRelevance(deleteVO.getId(),null,null);
        //发送通知给用户
        circleProducer.deleteMessage(deleteVO);
        //取消所有关注该圈子的用户
        LambdaQueryWrapper<EasyCircleFollow> followWrapper = new LambdaQueryWrapper<>();
        followWrapper.eq(EasyCircleFollow::getCircleId,deleteVO.getId());
        followService.remove(followWrapper);
        //删除圈子
        removeById(deleteVO.getId());
    }

    /**
     * 管理后台 - 获取圈子的id和名称
     *
     * @return
     */
    @Override
    public List<CircleAppBriefVO> getAdminTags() {
        List<EasyCircle> list = list();
        return CircleConvert.INSTANCE.circleListToAppBriefVOList(list);
    }

    /**
     * 根据圈子名称查找圈子
     * @param name 圈子名称
     */
    private EasyCircle getByName(String name) {
        LambdaQueryWrapper<EasyCircle> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(EasyCircle::getCircleName, name);
        return getOne(lambdaQueryWrapper);
    }

    //创建一个通用的方法来获取圈子的动态数量
    private long getDynamicCount(int circleId) {
        LambdaQueryWrapper<EasyDynamic> dynamicWrapper = new LambdaQueryWrapper<>();
        dynamicWrapper.eq(EasyDynamic::getCircleId, circleId);
        return dynamicService.count(dynamicWrapper);
    }

    //创建一个通用的方法来获取圈子的成员数量
    private long getMemberCount(int circleId) {
        LambdaQueryWrapper<EasyCircleFollow> followLambdaQueryWrapper = new LambdaQueryWrapper<>();
        followLambdaQueryWrapper.eq(EasyCircleFollow::getCircleId, circleId);
        return followService.count(followLambdaQueryWrapper);
    }

    //创建一个通用的方法来填充圈子的动态和成员数量
    private void fillCircleCounts(CircleAppDetailVO circleAppDetailVO) {
        long dynamicCount = getDynamicCount(circleAppDetailVO.getId());
        circleAppDetailVO.setDynamicCount(dynamicCount);
        long memberCount = getMemberCount(circleAppDetailVO.getId());
        circleAppDetailVO.setMemberCount(memberCount);
    }
}




