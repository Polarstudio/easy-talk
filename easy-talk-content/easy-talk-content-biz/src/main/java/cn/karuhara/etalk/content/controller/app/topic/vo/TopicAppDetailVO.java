package cn.karuhara.etalk.content.controller.app.topic.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Schema(description = "APP - 话题详细信息")
@Data
public class TopicAppDetailVO extends TopicAppBaseVO{

    @Schema(description = "话题创建人ID")
    private Integer userId;

    @Schema(description = "创建人头像")
    private String avatar;

    @Schema(description = "创建人的昵称")
    private String nikeName;

    @Schema(description = "创建时间")
    @JsonFormat(pattern = "yyyy/MM/dd HH:mm", timezone = "GMT+8")
    private LocalDateTime createTime;

    @Schema(description = "话题关注量")
    private Integer follow;

    @Schema(description = "话题动态数")
    private Integer dynamic;

    @Schema(description = "话题浏览量")
    private Integer views;

    @Schema(description = "话题热度")
    private Integer hot;
}
