package cn.karuhara.etalk.content.controller.app.dynamic.vo;

import cn.karuhara.etalk.content.enums.DynamicTopEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Schema(description = "动态详细信息")
@Data
public class DynamicDetailVO extends DynamicBaseVO {

    @Schema(description = "用户昵称")
    private String nikeName;

    @Schema(description = "用户头像")
    private String avatar;

    @Schema(description = "用户认证名")
    private String certificateName;

    @Schema(description = "用户省份")
    private String province;

    @Schema(description = "圈子名称")
    private String circleName;

    @Schema(description = "圈子的封面")
    private String circleCover;

    @Schema(description = "话题名称")
    private String topicName;

    @Schema(description = "活动名称")
    private String activityName;

    @Schema(description = "活动封面")
    private String activityCover;

    @Schema(description = "动态附件")
    private DynamicAppAttachmentVO attachment;

    @Schema(description = "动态点赞数")
    private Long likeCount;

    @Schema(description = "动态评论数")
    private Long commentCount;

    @Schema(description = "动态浏览量")
    private Integer browse;

    @Schema(description = "动态是否置顶")
    private DynamicTopEnum top;

    @Schema(description = "动态创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private LocalDateTime createTime;
}
