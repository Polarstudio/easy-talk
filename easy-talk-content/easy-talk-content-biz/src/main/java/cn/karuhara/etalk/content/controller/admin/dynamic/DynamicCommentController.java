package cn.karuhara.etalk.content.controller.admin.dynamic;

import cn.karuhara.etalk.content.controller.admin.dynamic.vo.CommentAdminDetailVO;
import cn.karuhara.etalk.content.controller.admin.dynamic.vo.CommentDeleteVO;
import cn.karuhara.etalk.content.controller.admin.dynamic.vo.CommentUpdateVO;
import cn.karuhara.etalk.content.controller.admin.dynamic.vo.CommonAdminPageReqVO;
import cn.karuhara.etalk.content.controller.app.dynamic.vo.CommentDetailVO;
import cn.karuhara.etalk.content.service.dynamic.EasyDynamicCommentService;
import cn.karuhara.etalk.framework.common.pojo.CommonResult;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

@Tag(name="管理后台 - 评论管理")
@RestController
@RequestMapping("/content/dynamic/admin/comment")
public class DynamicCommentController {

    @Resource
    private EasyDynamicCommentService commentService;

    @Operation(summary = "获取评论的列表")
    @PostMapping("/list")
    public CommonResult<PageResult<CommentAdminDetailVO>> comment(@RequestBody CommonAdminPageReqVO pageReqVO){
        PageResult<CommentAdminDetailVO> pageResult = commentService.getCommentList(pageReqVO);
        return CommonResult.success(pageResult);
    }

    @Operation(summary = "修改评论")
    @PutMapping
    public CommonResult<Void> update(@RequestBody CommentUpdateVO updateVO){
        commentService.updateComment(updateVO);
        return CommonResult.success();
    }

    @Operation(summary = "删除评论")
    @DeleteMapping
    public CommonResult<Void> delete(@RequestBody CommentDeleteVO detailVO){
        commentService.delete(detailVO);
        return CommonResult.success();
    }
}
