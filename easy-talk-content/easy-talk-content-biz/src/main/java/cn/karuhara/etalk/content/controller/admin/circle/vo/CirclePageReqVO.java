package cn.karuhara.etalk.content.controller.admin.circle.vo;

import cn.karuhara.etalk.framework.common.pojo.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "圈子分页参数")
public class CirclePageReqVO extends PageParam {

    @Schema(description = "圈子名称",example = "摄影")
    private String circleName;

    @Schema(description = "圈子状态(0正常、1推送、2隐藏)",examples = "0")
    private String status;

}
