package cn.karuhara.etalk.system.api;

import cn.karuhara.etalk.system.api.dto.SystemConfigDTO;
import cn.karuhara.etalk.system.api.enums.ApiConstant;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

@Tag(name = "RPC 服务 - 配置相关")
@FeignClient(name = ApiConstant.NAME ,contextId = "config", url = "http://localhost:39093")
@Component
public interface ConfigApi {

    String PREFIX = ApiConstant.PREFIX;

    @GetMapping(PREFIX+"/config/getSystemConfig")
    @Operation(summary = "获取系统配置")
    SystemConfigDTO getSystemConfig();
}
