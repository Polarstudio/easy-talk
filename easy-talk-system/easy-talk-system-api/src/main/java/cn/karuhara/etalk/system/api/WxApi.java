package cn.karuhara.etalk.system.api;

import cn.karuhara.etalk.system.api.dto.AccessToken;
import cn.karuhara.etalk.system.api.enums.ApiConstant;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

@Tag(name = "RPC 服务 - 微信相关")
@FeignClient(name = ApiConstant.NAME ,contextId = "wx", url = "http://localhost:39093")
@Component
public interface WxApi {

    String PREFIX = ApiConstant.PREFIX;

    @GetMapping(PREFIX+"/wx/getAccessToken")
    @Operation(summary = "获取小程序后台接口调用凭据")
    String getAccessToken();

}
