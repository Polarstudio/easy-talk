package cn.karuhara.etalk.system.handler.wx;

import cn.karuhara.etalk.system.enums.WxEvenTypeEnum;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 微信消息处理工厂
 */
@Component
public class WxMessageFactory implements InitializingBean {

    @Resource
    private List<WxMessageHandler> wxMessageHandlers;

    /**
     * 用于存储不同的处理器
     */
    private final Map<WxEvenTypeEnum, WxMessageHandler> wxMessageHandlerMap = new HashMap<>();

    /**
     * 根据枚举获取对应的处理器
     * @param evenTypeEnum 消息类型枚举
     * @return 消息处理器
     */
    public WxMessageHandler getWxMessageHandler(WxEvenTypeEnum evenTypeEnum) {
        return wxMessageHandlerMap.get(evenTypeEnum);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        for (WxMessageHandler messageHandler:wxMessageHandlers){
            wxMessageHandlerMap.put(messageHandler.getEvenType(), messageHandler);
        }
    }
}
