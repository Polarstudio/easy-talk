package cn.karuhara.etalk.system.service.config;

import cn.karuhara.etalk.system.controller.config.vo.SystemConfigVO;
import cn.karuhara.etalk.system.entity.config.EasyConfig;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author atom
* @description 针对表【easy_config(配置)】的数据库操作Service
* @createDate 2025-01-19 17:14:27
*/
public interface EasyConfigService extends IService<EasyConfig> {

    SystemConfigVO getConfig(String name);
}
