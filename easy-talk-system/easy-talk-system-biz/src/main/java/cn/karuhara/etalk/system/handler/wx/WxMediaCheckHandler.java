package cn.karuhara.etalk.system.handler.wx;

import cn.karuhara.etalk.system.enums.WxEvenTypeEnum;
import cn.karuhara.etalk.system.mq.product.DetectionMediaProducer;
import com.alibaba.fastjson2.JSONObject;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class WxMediaCheckHandler implements WxMessageHandler{

    @Resource
    private DetectionMediaProducer detectionMediaProducer;

    @Override
    public WxEvenTypeEnum getEvenType() {
        return WxEvenTypeEnum.WXA_MEDIA_CHECK;
    }

    @Override
    public String dealMsg(String message) {
        MediaCheckAsyncResult mediaCheckAsyncResult = JSONObject.parseObject(message, MediaCheckAsyncResult.class);
        log.info("mediaCheckAsyncResult: {}", mediaCheckAsyncResult);
        detectionMediaProducer.detectionMediaHandler(mediaCheckAsyncResult.getTraceId(),mediaCheckAsyncResult.getResult().getSuggest());
        return null;
    }
}
