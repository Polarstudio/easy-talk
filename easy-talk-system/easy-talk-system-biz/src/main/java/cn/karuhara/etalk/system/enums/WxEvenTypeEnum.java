package cn.karuhara.etalk.system.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 微信消息类型枚举
 */
@Getter
@AllArgsConstructor
public enum WxEvenTypeEnum {
    WXA_MEDIA_CHECK("wxa_media_check", "音视频内容安全识别");
    @EnumValue
    @JsonValue
    private final String code;

    private final String desc;
}
