package cn.karuhara.etalk.system.controller.wx;

import cn.karuhara.etalk.system.handler.wx.WxChatMsgNormal;
import cn.karuhara.etalk.system.handler.wx.WxMessageFactory;
import cn.karuhara.etalk.system.handler.wx.WxMessageHandler;
import cn.karuhara.etalk.system.utils.SHA1;
import com.alibaba.fastjson2.JSONObject;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Tag(name = "微信相关")
@RestController
@RequestMapping("/system/wx")
@Slf4j
public class WxController {
    @Resource
    private WxMessageFactory messageFactory;

    private static final String token = "adwidhaidwoaid";

    @GetMapping
    public String callback(@RequestParam("signature") String signature,
                           @RequestParam("timestamp") String timestamp,
                           @RequestParam("nonce") String nonce,
                           @RequestParam("echostr") String echostr) {
        log.info("get验签请求参数：signature:{}，timestamp:{}，nonce:{}，echostr:{}",
                signature, timestamp, nonce, echostr);
        String shaStr = SHA1.getSHA1(token, timestamp, nonce, "");
        if (signature.equals(shaStr)) {
            return echostr;
        }
        return "unknown";
    }

    @PostMapping
    public String callback(
            @RequestBody String requestBody,
            @RequestParam("signature") String signature,
            @RequestParam("timestamp") String timestamp,
            @RequestParam("nonce") String nonce,
            @RequestParam(value = "msg_signature", required = false) String msgSignature) {
        log.info("接收到微信消息：requestBody：{}", requestBody);
        WxChatMsgNormal wxChatMsgNormal = JSONObject.parseObject(requestBody, WxChatMsgNormal.class);
        if (!wxChatMsgNormal.getMsgType().equals("event")){
            return null;
        }
        //处理消息
        WxMessageHandler wxMessageHandler = messageFactory.getWxMessageHandler(wxChatMsgNormal.getEvent());
        return wxMessageHandler.dealMsg(requestBody);
    }
}
