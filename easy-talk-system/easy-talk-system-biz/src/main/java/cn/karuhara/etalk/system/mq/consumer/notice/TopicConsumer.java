package cn.karuhara.etalk.system.mq.consumer.notice;

import cn.karuhara.etalk.framework.kafka.core.Event;
import cn.karuhara.etalk.framework.kafka.core.MessageConstants;
import cn.karuhara.etalk.system.constants.NoticeConstants;
import cn.karuhara.etalk.system.entity.notice.EasyNotice;
import cn.karuhara.etalk.system.enums.NoticeTypeEnum;
import cn.karuhara.etalk.system.service.notice.EasyNoticeService;
import com.alibaba.fastjson2.JSONObject;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class TopicConsumer {

    @Resource
    private EasyNoticeService noticeService;

    @KafkaListener(topics = MessageConstants.TOPIC_DELETE_TOPIC)
    public void handlerDelete(ConsumerRecord record, Acknowledgment acknowledgment){
        Event event = JSONObject.parseObject(record.value().toString(), Event.class);
        log.info("收到一条消息：{}", event);
        //添加删除消息
        EasyNotice easyNotice = new EasyNotice();
        //标题
        easyNotice.setTitle(NoticeConstants.TOPIC_DELETE_TITLE);
        //通知类型
        easyNotice.setType(NoticeTypeEnum.TOPIC);
        //圈子名称
        String topicName = (String) event.getData().get(MessageConstants.TOPIC_NAME);
        //消息内容
        String content = event.getContent();
        //创建者
        Integer userId = event.getToUserId();
        //通知创建者,如果是用户自己删除的话不需要通知
        if (userId != null){
            easyNotice.setToId(userId);
            easyNotice.setContent("您创建的话题`"+topicName+"`已被删除，删除原因：`"+content+"`");
            noticeService.save(easyNotice);
        }
        //通知关注该话题的用户
        List<Integer> userIds = (List<Integer>) event.getData().get(MessageConstants.FOLLOW_IDS);
        userIds.remove(userId);
        if (!userIds.isEmpty()){
            List<EasyNotice> easyNotices = new ArrayList<>();
            userIds.forEach((followId) ->{
                EasyNotice notice = new EasyNotice();
                //标题
                notice.setTitle(NoticeConstants.TOPIC_DELETE_TITLE);
                //通知类型
                notice.setType(NoticeTypeEnum.TOPIC);
                //通知的用户
                notice.setToId(followId);
                notice.setContent("您关注的圈子`"+topicName+"`已被删除，删除原因：`"+content+"`");
                easyNotices.add(notice);
            });
            noticeService.saveBatch(easyNotices);
        }
        acknowledgment.acknowledge();
    }
}
