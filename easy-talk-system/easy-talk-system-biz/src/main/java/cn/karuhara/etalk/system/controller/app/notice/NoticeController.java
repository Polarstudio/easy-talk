package cn.karuhara.etalk.system.controller.app.notice;

import cn.karuhara.etalk.framework.common.pojo.CommonResult;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import cn.karuhara.etalk.system.controller.app.notice.vo.NoticeDetailVO;
import cn.karuhara.etalk.system.controller.app.notice.vo.NoticePageParam;
import cn.karuhara.etalk.system.entity.notice.EasyNotice;
import cn.karuhara.etalk.system.enums.NoticeStatusEnum;
import cn.karuhara.etalk.system.service.notice.EasyNoticeService;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

@Tag(name = "APP - 平台通知")
@RestController
@RequestMapping("/system/notice/app")
public class NoticeController {
    @Resource
    private EasyNoticeService noticeService;

    @Operation(summary = "获取通知列表")
    @PostMapping("/list")
    public CommonResult<PageResult<NoticeDetailVO>> list(@RequestBody NoticePageParam pageParam){
        PageResult<NoticeDetailVO> pageResult =  noticeService.getList(pageParam);
        return CommonResult.success(pageResult);
    }

    @Operation(summary = "删除通知")
    @DeleteMapping("/{noticeId}")
    public CommonResult<Void> delete(@PathVariable("noticeId") Integer noticeId){
        noticeService.removeById(noticeId);
        return CommonResult.success();
    }

    @Operation(summary = "读通知")
    @PutMapping("/{noticeId}")
    public CommonResult<Void> readNotice(@PathVariable("noticeId") Integer noticeId){
        LambdaUpdateWrapper<EasyNotice> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(EasyNotice::getId, noticeId).set(EasyNotice::getStatus, NoticeStatusEnum.READE.getCode());
        noticeService.update(updateWrapper);
        return CommonResult.success();
    }
}
