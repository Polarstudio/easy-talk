package cn.karuhara.etalk.system.service.notice.impl;

import cn.karuhara.etalk.framework.common.pojo.PageResult;
import cn.karuhara.etalk.system.controller.app.notice.vo.NoticePageParam;
import cn.karuhara.etalk.system.controller.app.notice.vo.NoticeDetailVO;
import cn.karuhara.etalk.system.convert.NoticeConvert;
import cn.karuhara.etalk.system.service.notice.EasyNoticeService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.karuhara.etalk.system.entity.notice.EasyNotice;
import cn.karuhara.etalk.system.mapper.notice.EasyNoticeMapper;
import jakarta.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author atom
* @description 针对表【easy_notice(通知)】的数据库操作Service实现
* @createDate 2024-12-19 09:21:29
*/
@Service
public class EasyNoticeServiceImpl extends ServiceImpl<EasyNoticeMapper, EasyNotice>
    implements EasyNoticeService {

    @Resource
    private EasyNoticeMapper noticeMapper;

    /**
     * 获取用户的通知
     *
     * @param pageParam 分页参数
     * @return 通知详情列表
     */
    @Override
    public PageResult<NoticeDetailVO> getList(NoticePageParam pageParam) {
        LambdaQueryWrapper<EasyNotice> wrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(pageParam.getStatus())){
            wrapper.eq(EasyNotice::getStatus, pageParam.getStatus());
        }
        if (StringUtils.isNotBlank(pageParam.getType())){
            wrapper.eq(EasyNotice::getType, pageParam.getType());
        }
        if (pageParam.getUserId() != null){
            wrapper.eq(EasyNotice::getToId, pageParam.getUserId());
        }
        //安照最新的消息进行排序
        wrapper.orderByDesc(EasyNotice::getCreateTime);
        //优先展示未读的通知
        wrapper.orderByDesc(EasyNotice::getStatus);
        Page<EasyNotice> page = new Page<>(pageParam.getPageNo(), pageParam.getPageSize());
        Page<EasyNotice> noticePage = noticeMapper.selectPage(page, wrapper);
        if (noticePage.getTotal() == 0){
            return PageResult.empty();
        }
        List<EasyNotice> notices = noticePage.getRecords();
        List<NoticeDetailVO> noticeDetailVOS = NoticeConvert.INSTANT.noticesToNoticeDetailVO(notices);
        return new PageResult<>(noticeDetailVOS,noticePage.getPages());
    }
}




