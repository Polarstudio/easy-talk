package cn.karuhara.etalk.system.mq.consumer.notice;

import cn.karuhara.etalk.framework.kafka.core.Event;
import cn.karuhara.etalk.framework.kafka.core.MessageConstants;
import cn.karuhara.etalk.system.constants.NoticeConstants;
import cn.karuhara.etalk.system.entity.notice.EasyNotice;
import cn.karuhara.etalk.system.enums.NoticeTypeEnum;
import cn.karuhara.etalk.system.service.notice.EasyNoticeService;
import com.alibaba.fastjson2.JSONObject;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class CertificateConsumer {
    @Resource
    private EasyNoticeService noticeService;

    @KafkaListener(topics = MessageConstants.CERTIFICATE_DELETE_TOPIC)
    public void handlerDelete(ConsumerRecord record, Acknowledgment acknowledgment){
        Event event = JSONObject.parseObject(record.value().toString(), Event.class);
        log.info("收到一条消息：{}", event);
        //添加删除消息
        EasyNotice easyNotice = new EasyNotice();
        //标题
        easyNotice.setTitle(NoticeConstants.CERTIFICATE_DELETE_TITLE);
        //通知类型
        easyNotice.setType(NoticeTypeEnum.SYSTEM);
        //认证名称
        String certificateName = (String) event.getData().get(MessageConstants.CERTIFICATE_NAME);
        //消息内容
        String content = event.getContent();
        //认证申请者
        Integer userId = event.getToUserId();
        if (userId!=null){
            easyNotice.setToId(userId);
            easyNotice.setContent("您申请的认证`"+certificateName+"`已被删除，删除原因：`"+content+"`");
            noticeService.save(easyNotice);
        }
        acknowledgment.acknowledge();
    }

    //处理审核的消息
    @KafkaListener(topics = MessageConstants.CERTIFICATE_HANDLE_TOPIC)
    public void handleCertificate(ConsumerRecord record, Acknowledgment acknowledgment){
        Event event = JSONObject.parseObject(record.value().toString(), Event.class);
        log.info("收到一条消息：{}", event);
        //添加删除消息
        EasyNotice easyNotice = new EasyNotice();
        //标题
        easyNotice.setTitle(NoticeConstants.CERTIFICATE_HANDLER_TITLE);
        //通知类型
        easyNotice.setType(NoticeTypeEnum.SYSTEM);
        //消息内容
        String content = event.getContent();
        //认证申请者
        Integer userId = event.getToUserId();
        easyNotice.setToId(userId);
        easyNotice.setContent(content);
        //获取跳转连接
        String contentUrl =(String) event.getData().get(MessageConstants.CONTENT_URL);
        easyNotice.setContentUrl(contentUrl);
        noticeService.save(easyNotice);
        acknowledgment.acknowledge();
    }
}
