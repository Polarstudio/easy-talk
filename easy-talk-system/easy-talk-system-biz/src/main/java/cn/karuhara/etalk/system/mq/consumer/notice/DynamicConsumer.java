package cn.karuhara.etalk.system.mq.consumer.notice;

import cn.karuhara.etalk.framework.kafka.core.Event;
import cn.karuhara.etalk.framework.kafka.core.MessageConstants;
import cn.karuhara.etalk.system.constants.NoticeConstants;
import cn.karuhara.etalk.system.entity.notice.EasyNotice;
import cn.karuhara.etalk.system.enums.NoticeTypeEnum;
import cn.karuhara.etalk.system.service.notice.EasyNoticeService;
import com.alibaba.fastjson2.JSONObject;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class DynamicConsumer {
    @Resource
    private EasyNoticeService noticeService;

    @KafkaListener(topics = MessageConstants.DYNAMIC_DELETE_TOPIC)
    public void deleteHandler(ConsumerRecord record, Acknowledgment acknowledgment){
        Event event = JSONObject.parseObject(record.value().toString(), Event.class);
        log.info("收到一条消息：{}", event);
        //添加删除消息
        EasyNotice easyNotice = new EasyNotice();
        //标题
        easyNotice.setTitle(NoticeConstants.DYNAMIC_DELETE_TITLE);
        //通知类型
        easyNotice.setType(NoticeTypeEnum.DYNAMIC);
        //动态内容名称
        String dynamic_content = (String) event.getData().get(MessageConstants.DYNAMIC_CONTENT);
        //消息内容
        String content = event.getContent();
        //创建者
        Integer userId = event.getToUserId();
        //通知圈子的创建者,如果是用户自己删除的话不需要通知
        if (userId!=null){
            easyNotice.setToId(userId);
            easyNotice.setContent("您发布的动态`"+dynamic_content+"`已被删除，删除原因：`"+content+"`");
            noticeService.save(easyNotice);
        }
        acknowledgment.acknowledge();
    }

    @KafkaListener(topics = MessageConstants.DYNAMIC_DETECTION_TOPIC)
    public void detectionHandler(ConsumerRecord record, Acknowledgment acknowledgment){
        Event event = JSONObject.parseObject(record.value().toString(), Event.class);
        //添加审核消息
        EasyNotice easyNotice = new EasyNotice();
        //标题
        easyNotice.setTitle(NoticeConstants.DYNAMIC_DETECTION_TITLE);
        //通知类型
        easyNotice.setType(NoticeTypeEnum.DYNAMIC);
        //通知的用户
        easyNotice.setToId(event.getToUserId());
        //跳转的连接
        easyNotice.setContentUrl(MessageConstants.DYNAMIC_URL+"?id="+event.getData().get(MessageConstants.POST_ID));
        //消息的内容
        String content = event.getContent();
        //动态内容
        String dynamicContent = (String) event.getData().get(MessageConstants.DYNAMIC_CONTENT);
        //审核的结果
        String detectionResult = (String) event.getData().get(MessageConstants.DETECTION_RESULT);
        if (detectionResult.equals("risky")){
            easyNotice.setContent("您发布的动态`"+dynamicContent+"`已被驳回，驳回原因："+content);
        }else if (detectionResult.equals("pass")){
            easyNotice.setContent("您发布的动态`"+dynamicContent+"`已通过审核");
        }
        noticeService.save(easyNotice);
        acknowledgment.acknowledge();
    }
}
