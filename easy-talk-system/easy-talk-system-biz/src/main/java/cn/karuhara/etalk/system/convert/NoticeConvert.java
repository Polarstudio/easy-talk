package cn.karuhara.etalk.system.convert;

import cn.karuhara.etalk.system.controller.app.notice.vo.NoticeDetailVO;
import cn.karuhara.etalk.system.entity.notice.EasyNotice;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface NoticeConvert {

    NoticeConvert INSTANT = Mappers.getMapper(NoticeConvert.class);

    List<NoticeDetailVO> noticesToNoticeDetailVO(List<EasyNotice> notices);
}
