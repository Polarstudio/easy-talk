package cn.karuhara.etalk.system.handler.wx;

import com.alibaba.fastjson2.annotation.JSONField;
import lombok.Data;
import java.util.List;

@Data
public class MediaCheckAsyncResult {
    // 小程序的username
    @JSONField(name = "ToUserName")
    private String toUserName;
    // 平台推送服务UserName
    @JSONField(name = "FromUserName")
    private String fromUserName;
    // 发送时间
    @JSONField(name = "CreateTime")
    private long createTime;
    // 默认为：event
    @JSONField(name = "MsgType")
    private String msgType;
    // 默认为：wxa_media_check
    @JSONField(name = "Event")
    private String event;
    // 小程序的appid
    @JSONField(name = "appid")
    private String appid;
    // 任务id
    @JSONField(name = "trace_id")
    private String traceId;
    // 可用于区分接口版本
    @JSONField(name = "version")
    private int version;
    // 详细检测结果
    @JSONField(name = "detail")
    private List<DetailResult> detail;
    // 综合结果
    @JSONField(name = "result")
    private Result result;
    // 错误码
    @JSONField(name = "errcode")
    private int errcode;
    // 错误信息
    @JSONField(name = "errmsg")
    private String errmsg;

    // 内部类，对应详细检测结果detail中的元素结构
    @Data
    public static class DetailResult {
        @JSONField(name = "strategy")
        private String strategy;
        @JSONField(name = "errcode")
        private int errcode;
        @JSONField(name = "suggest")
        private String suggest;
        @JSONField(name = "label")
        private int label;
        @JSONField(name = "prob")
        private int prob;
    }

    // 内部类，对应综合结果result的结构
    @Data
    public static class Result {
        @JSONField(name = "suggest")
        private String suggest;
        @JSONField(name = "label")
        private int label;
    }
}