package cn.karuhara.etalk.system.service.notice;

import cn.karuhara.etalk.framework.common.pojo.PageResult;
import cn.karuhara.etalk.system.controller.app.notice.vo.NoticeDetailVO;
import cn.karuhara.etalk.system.controller.app.notice.vo.NoticePageParam;
import cn.karuhara.etalk.system.entity.notice.EasyNotice;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author atom
* @description 针对表【easy_notice(通知)】的数据库操作Service
* @createDate 2024-12-19 09:21:29
*/
public interface EasyNoticeService extends IService<EasyNotice> {

    /**
     * 获取用户的通知
     * @param pageParam 分页参数
     * @return 通知详情列表
     */
    PageResult<NoticeDetailVO> getList(NoticePageParam pageParam);
}
