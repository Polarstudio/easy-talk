package cn.karuhara.etalk.system.controller.app.notice.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Schema(description = "App - 通知的详细信息")
@Data
public class NoticeDetailVO extends NoticeBaseVO{

    @Schema(description = "通知封面")
    private String coverImg;

}
