package cn.karuhara.etalk.system.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 消息状态枚举类
 */
@Getter
@AllArgsConstructor
public enum NoticeStatusEnum {
    UNREAD("0", "未读"),
    READE("1", "已读");

    @EnumValue
    @JsonValue
    private final String code;

    private final String desc;
}
