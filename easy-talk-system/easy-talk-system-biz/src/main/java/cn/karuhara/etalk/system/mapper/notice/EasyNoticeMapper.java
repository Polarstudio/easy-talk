package cn.karuhara.etalk.system.mapper.notice;

import cn.karuhara.etalk.system.entity.notice.EasyNotice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author atom
* @description 针对表【easy_notice(通知)】的数据库操作Mapper
* @createDate 2024-12-19 09:21:29
* @Entity cn.karuhara.etalk.system.entity.notice.EasyNotice
*/
public interface EasyNoticeMapper extends BaseMapper<EasyNotice> {

}




