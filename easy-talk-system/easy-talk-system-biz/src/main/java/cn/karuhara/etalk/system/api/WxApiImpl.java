package cn.karuhara.etalk.system.api;

import cn.karuhara.etalk.system.api.dto.AccessToken;
import cn.karuhara.etalk.system.service.wx.WxService;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.RestController;

@RestController // 提供 RESTful API 接口，给 Feign 调用
public class WxApiImpl implements WxApi{
    @Resource
    private WxService wxService;
    /**
     * @return
     */
    @Override
    public String getAccessToken() {
        return wxService.getAccessToken();
    }
}
