package cn.karuhara.etalk.system.mq.consumer.notice;

import cn.karuhara.etalk.framework.kafka.core.Event;

import cn.karuhara.etalk.framework.kafka.core.MessageConstants;
import cn.karuhara.etalk.system.constants.NoticeConstants;
import cn.karuhara.etalk.system.entity.notice.EasyNotice;
import cn.karuhara.etalk.system.enums.NoticeTypeEnum;
import cn.karuhara.etalk.system.service.notice.EasyNoticeService;
import com.alibaba.fastjson2.JSONObject;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class CircleConsumer {

    @Resource
    private EasyNoticeService noticeService;

    @KafkaListener(topics = MessageConstants.CIRCLE_DELETE_TOPIC)
    public void handlerDelete(ConsumerRecord record,Acknowledgment acknowledgment){
        Event event = JSONObject.parseObject(record.value().toString(), Event.class);
        log.info("收到一条消息：{}", event);
        //添加删除消息
        EasyNotice easyNotice = new EasyNotice();
        //标题
        easyNotice.setTitle(NoticeConstants.CIRCLE_DELETE_TOPIC);
        //通知类型
        easyNotice.setType(NoticeTypeEnum.CIRCLE);
        //圈子名称
        String circleName = (String) event.getData().get(MessageConstants.CIRCLE_NAME);
        //消息内容
        String content = event.getContent();
        //圈子创建者
        Integer userId = event.getToUserId();
        //通知圈子的创建者,如果是用户自己删除的话不需要通知
        if (event.getToUserId()!=null){
            easyNotice.setToId(userId);
            easyNotice.setContent("您创建的圈子`"+circleName+"`已被删除，删除原因：`"+content+"`");
            noticeService.save(easyNotice);
        }
        //通知关注该圈子的用户
        List<Integer> userIds = (List<Integer>) event.getData().get(MessageConstants.FOLLOW_IDS);
        userIds.remove(userId);
        if (!userIds.isEmpty()){
            List<EasyNotice> easyNotices = new ArrayList<>();
            userIds.forEach((followId) ->{
                EasyNotice notice = new EasyNotice();
                //标题
                notice.setTitle(NoticeConstants.CIRCLE_DELETE_TOPIC);
                //通知类型
                notice.setType(NoticeTypeEnum.CIRCLE);
                //通知的用户
                notice.setToId(followId);
                notice.setContent("您关注的圈子`"+circleName+"`已被删除，删除原因：`"+content+"`");
                easyNotices.add(notice);
            });
            noticeService.saveBatch(easyNotices);
        }
        acknowledgment.acknowledge();
    }
}
