package cn.karuhara.etalk.system.constants;


/**
 * @Description: 消息常量
 * @author: 轻语
*/
public class NoticeConstants {
    public static final String CIRCLE_DELETE_TOPIC = "圈子删除通知";

    public static final String TOPIC_DELETE_TITLE = "话题删除通知";

    public static final String DYNAMIC_DELETE_TITLE = "动态删除通知";

    public static final String ACTIVITY_DELETE_TITLE = "活动删除通知";

    public static final String COMMENT_DELETE_TITLE = "评论删除通知";

    public static final String CERTIFICATE_DELETE_TITLE = "认证删除通知";

    public static final String CERTIFICATE_HANDLER_TITLE = "认证处理通知";

    public static final String DYNAMIC_DETECTION_TITLE = "动态审核通知";

    public static final String COMMENT_DETECTION_TITLE = "评论审核通知";
}
