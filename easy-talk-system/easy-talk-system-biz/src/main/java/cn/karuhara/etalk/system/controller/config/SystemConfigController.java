package cn.karuhara.etalk.system.controller.config;

import cn.karuhara.etalk.framework.common.constants.RedisConstant;
import cn.karuhara.etalk.framework.common.pojo.CommonResult;
import cn.karuhara.etalk.fremawork.redis.core.MyRedisUtil;
import cn.karuhara.etalk.system.controller.config.vo.SystemConfigVO;
import cn.karuhara.etalk.system.convert.ConfigConvert;
import cn.karuhara.etalk.system.entity.config.EasyConfig;
import cn.karuhara.etalk.system.service.config.EasyConfigService;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.common.protocol.types.Field;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@Tag(name = "微信相关")
@RestController
@RequestMapping("/system/config")
@Slf4j
public class SystemConfigController {

    @Resource
    private EasyConfigService configService;

    @Resource
    private MyRedisUtil redisUtil;

    @Operation(description = "新增或修改配置")
    @PostMapping
    public CommonResult<Void> config(@RequestBody SystemConfigVO configVO){
        EasyConfig easyConfig = ConfigConvert.INSTANCE.voToEasyConfig(configVO);
        Map<String, String> map = new HashMap<>();
        map.put("miniProgramName", configVO.getMiniProgramName());
        map.put("appId", configVO.getAppId());
        map.put("secret", configVO.getSecret());
        map.put("tencentKey", configVO.getTencentKey());
        map.put("tencentName", configVO.getTencentName());
        String jsonString = JSONObject.toJSONString(map);
        easyConfig.setValue(jsonString);
        configService.saveOrUpdate(easyConfig);
        //更新redis
        String key = MyRedisUtil.buildKey(RedisConstant.SYSTEM_CONFIG,configVO.getName());
        redisUtil.set(key, easyConfig);
        return CommonResult.success();
    }

    @Operation(description = "获取系统的配置")
    @GetMapping("/{name}")
    private CommonResult<SystemConfigVO> getConfig(@PathVariable String name){
        return CommonResult.success(configService.getConfig(name));
    }
}
