package cn.karuhara.etalk.system.handler.wx;

import cn.karuhara.etalk.system.enums.WxEvenTypeEnum;

public interface WxMessageHandler {

    WxEvenTypeEnum getEvenType();

    String dealMsg(String message);
}
