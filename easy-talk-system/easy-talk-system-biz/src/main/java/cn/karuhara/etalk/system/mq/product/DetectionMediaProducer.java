package cn.karuhara.etalk.system.mq.product;

import cn.karuhara.etalk.framework.kafka.core.Event;
import cn.karuhara.etalk.framework.kafka.core.EventProducer;
import cn.karuhara.etalk.framework.kafka.core.MessageConstants;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

@Component
public class DetectionMediaProducer {
    @Resource
    private EventProducer eventProducer;

    public void detectionMediaHandler(String traceId,String suggest){
        Event event = new Event();
        event.setTopic(MessageConstants.DETECTION_MEDIA_TOPIC);
        event.setDataMap(MessageConstants.TRACE_ID,traceId);
        event.setDataMap(MessageConstants.DETECTION_SUGGEST,suggest);
        eventProducer.fireEvent(event);
    }
}
