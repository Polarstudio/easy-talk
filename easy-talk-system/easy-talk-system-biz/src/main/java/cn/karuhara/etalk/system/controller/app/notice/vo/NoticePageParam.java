package cn.karuhara.etalk.system.controller.app.notice.vo;

import cn.karuhara.etalk.framework.common.pojo.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Schema(description = "APP - 获取通知分页参数")
@Data
public class NoticePageParam extends PageParam {

    @Schema(description = "通知的类型")
    private String type;

    @Schema(description = "获取谁的通知")
    private Integer userId;

    @Schema(description = "通知的状态")
    private String status;
}
