package cn.karuhara.etalk.system.handler.wx;

import cn.karuhara.etalk.system.enums.WxEvenTypeEnum;
import com.alibaba.fastjson2.annotation.JSONField;
import lombok.Data;
/**
 * @Description: 微信通知推送类
 * @author: 轻语
*/
@Data
public class WxChatMsgNormal {
    @JSONField(name = "ToUserName")
    private String toUserName;

    @JSONField(name = "FromUserName")
    private String fromUserName;

    @JSONField(name = "CreateTime")
    private long createTime;

    @JSONField(name = "MsgType")
    private String msgType;

    @JSONField(name = "Event")
    private WxEvenTypeEnum event;
}