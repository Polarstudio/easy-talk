package cn.karuhara.etalk.system.api;

import cn.karuhara.etalk.system.api.dto.SystemConfigDTO;
import cn.karuhara.etalk.system.controller.config.vo.SystemConfigVO;
import cn.karuhara.etalk.system.service.config.EasyConfigService;
import jakarta.annotation.Resource;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.RestController;

@RestController // 提供 RESTful API 接口，给 Feign 调用
public class ConfigApiImpl implements ConfigApi {
    @Resource
    private EasyConfigService configService;
    /**
     * @return
     */
    @Override
    public SystemConfigDTO getSystemConfig() {
        SystemConfigDTO systemConfigDTO = new SystemConfigDTO();
        SystemConfigVO config = configService.getConfig("app");
        BeanUtils.copyProperties(config, systemConfigDTO);
        return systemConfigDTO;
    }
}
