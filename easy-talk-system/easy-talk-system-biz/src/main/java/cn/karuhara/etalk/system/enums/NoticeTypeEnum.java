package cn.karuhara.etalk.system.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 消息类型枚举枚举
 */
@Getter
@AllArgsConstructor
public enum NoticeTypeEnum {
    SYSTEM("0", "系统"),
    DYNAMIC("1", "动态"),
    CIRCLE("2", "圈子"),
    TOPIC("3", "话题"),
    ACTIVITY("4", "活动"),
    COMMENT("5", "评论");

    @EnumValue
    @JsonValue
    private final String code;

    private final String desc;
}
