package cn.karuhara.etalk.system.entity.notice;

import cn.karuhara.etalk.system.enums.NoticeStatusEnum;
import cn.karuhara.etalk.system.enums.NoticeTypeEnum;
import com.baomidou.mybatisplus.annotation.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Data;

/**
 * 通知
 * @TableName easy_notice
 */
@TableName(value ="easy_notice")
@Data
public class EasyNotice implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 标题
     */
    private String title;

    /**
     * 内容
     */
    private String content;

    /**
     * 发起人ID
     */
    private Integer formId;

    /**
     * 接受人ID
     */
    private Integer toId;

    /**
     * 内容跳转地址
     */
    private String contentUrl;

    /**
     * 类型：0 系统、1 动态、2 圈子、3 话题、4活动、5评论
     */
    private NoticeTypeEnum type;

    /**
     * 通知状态 0未读 1已读
     */
    private NoticeStatusEnum status;

    /**
     * 额外参数
     */
    private String extras;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}