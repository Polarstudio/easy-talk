package cn.karuhara.etalk.system.service.wx;

import cn.karuhara.etalk.framework.common.constants.RedisConstant;
import cn.karuhara.etalk.framework.common.enums.GlobalCodeEnum;
import cn.karuhara.etalk.framework.common.exception.ServiceException;
import cn.karuhara.etalk.fremawork.redis.core.MyRedisUtil;
import cn.karuhara.etalk.system.constants.WxConstants;
import cn.karuhara.etalk.system.api.dto.AccessToken;
import cn.karuhara.etalk.system.entity.config.EasyConfig;
import cn.karuhara.etalk.system.service.config.EasyConfigService;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import jakarta.annotation.Resource;
import okhttp3.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class WxService {

    OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(15, TimeUnit.SECONDS)
            .build();
    @Resource
    private MyRedisUtil myRedisUtil;

    @Resource
    private EasyConfigService configService;

    //获取接口调用凭证
    public String getAccessToken() {
        //从redis中查询
        String accessKey = MyRedisUtil.buildKey(RedisConstant.WX_ACCESS_TOKEN);
        Object object = myRedisUtil.get(accessKey);
        if (object != null) {
            return JSON.toJSONString(object);
        }
        String key = MyRedisUtil.buildKey(RedisConstant.SYSTEM_CONFIG,"app");
        Object configObject = myRedisUtil.get(key);
        EasyConfig easyConfig;
        if (configObject ==null){
            LambdaQueryWrapper<EasyConfig> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(EasyConfig::getName, "app");
            easyConfig = configService.getOne(queryWrapper);
        }else {
            easyConfig = (EasyConfig) configObject;
        }
        Map map = JSONObject.parseObject(easyConfig.getValue(), Map.class);
        String appId = map.get("appId").toString();
        String secret = map.get("secret").toString();
        //如果不存在，则发起请求获取access_token
        HttpUrl.Builder urlBuilder =
                HttpUrl.parse(WxConstants.WX_BASE_URL+"/cgi-bin/token").newBuilder();
        urlBuilder.addQueryParameter("appid",appId);
        urlBuilder.addQueryParameter("secret",secret);
        urlBuilder.addQueryParameter("grant_type","client_credential");
        Request request = new Request.Builder().url(urlBuilder.build()).build();
        Call call = client.newCall(request);
        Response response;
        String responseString;
        try {
            response = call.execute();
            responseString = response.body().string();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        AccessToken accessToken = JSON.parseObject(responseString, AccessToken.class);
        if (StringUtils.isBlank(accessToken.getAccessToken())){
            throw new ServiceException(GlobalCodeEnum.FAIL);
        }
        //将查询到的access_token存入
        myRedisUtil.set(accessKey, accessToken.getAccessToken(), accessToken.getExpiresIn());
        return accessToken.getAccessToken();
    }
}
