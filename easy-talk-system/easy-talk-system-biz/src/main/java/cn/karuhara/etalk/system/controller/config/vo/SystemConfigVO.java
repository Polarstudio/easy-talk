package cn.karuhara.etalk.system.controller.config.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "系统配置")
@Data
public class SystemConfigVO {

    @Schema(description = "配置id")
    private Integer id;

    @Schema(description = "配置名称")
    private String name;

    @Schema(description = "小程序名称")
    private String miniProgramName;

    @Schema(description = "小程序APPID")
    private String appId;

    @Schema(description = "小程序SECRET")
    private String secret;

    @Schema(description = "腾讯地图key")
    private String tencentKey;

    @Schema(description = "腾讯名称")
    private String tencentName;
}
