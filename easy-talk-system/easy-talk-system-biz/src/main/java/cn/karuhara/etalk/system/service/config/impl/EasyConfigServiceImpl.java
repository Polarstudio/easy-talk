package cn.karuhara.etalk.system.service.config.impl;

import cn.karuhara.etalk.framework.common.constants.RedisConstant;
import cn.karuhara.etalk.fremawork.redis.core.MyRedisUtil;
import cn.karuhara.etalk.system.controller.config.vo.SystemConfigVO;
import cn.karuhara.etalk.system.convert.ConfigConvert;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.karuhara.etalk.system.entity.config.EasyConfig;
import cn.karuhara.etalk.system.service.config.EasyConfigService;
import cn.karuhara.etalk.system.mapper.config.EasyConfigMapper;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Map;

/**
* @author atom
* @description 针对表【easy_config(配置)】的数据库操作Service实现
* @createDate 2025-01-19 17:14:27
*/
@Service
public class EasyConfigServiceImpl extends ServiceImpl<EasyConfigMapper, EasyConfig>
    implements EasyConfigService{

    @Resource
    private MyRedisUtil redisUtil;

    @Resource
    private EasyConfigService configService;

    /**
     * @param name 配置名称
     */
    @Override
    public SystemConfigVO getConfig(String name) {
        String key = MyRedisUtil.buildKey(RedisConstant.SYSTEM_CONFIG,name);
        Object object = redisUtil.get(key);
        EasyConfig easyConfig;
        if (object ==null){
            LambdaQueryWrapper<EasyConfig> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(EasyConfig::getName, name);
            easyConfig = configService.getOne(queryWrapper);
        }else {
            easyConfig = (EasyConfig) object;
        }
        SystemConfigVO configVO = ConfigConvert.INSTANCE.easyConfigToVo(easyConfig);
        String value = easyConfig.getValue();
        Map map = JSONObject.parseObject(value, Map.class);
        configVO.setMiniProgramName(map.get("miniProgramName").toString());
        configVO.setAppId(map.get("appId").toString());
        configVO.setSecret(map.get("secret").toString());
        configVO.setTencentKey(map.get("tencentKey").toString());
        configVO.setTencentName(map.get("tencentName").toString());
        return configVO;
    }
}




