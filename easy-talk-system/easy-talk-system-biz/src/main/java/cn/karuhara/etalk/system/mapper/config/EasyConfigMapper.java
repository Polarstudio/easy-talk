package cn.karuhara.etalk.system.mapper.config;

import cn.karuhara.etalk.system.entity.config.EasyConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author atom
* @description 针对表【easy_config(配置)】的数据库操作Mapper
* @createDate 2025-01-19 17:14:27
* @Entity cn.karuhara.etalk.system.entity.EasyConfig
*/
public interface EasyConfigMapper extends BaseMapper<EasyConfig> {

}




