package cn.karuhara.etalk.system.controller.app.notice.vo;

import cn.karuhara.etalk.system.enums.NoticeStatusEnum;
import cn.karuhara.etalk.system.enums.NoticeTypeEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;

@Schema(description = "通知基本信息")
@Data
public class NoticeBaseVO {

    @Schema(description = "通知id")
    private Integer id;

    @Schema(description = "标题")
    private String title;

    @Schema(description = "内容")
    private String content;

    @Schema(description = "发起人ID")
    private Integer formId;

    @Schema(description = "接收人ID")
    private Integer toId;

    @Schema(description = "内容跳转地址")
    private String contentUrl;

    @Schema(description = "类型：0 系统、1 动态、2 圈子、3 话题、4活动、5评论")
    private NoticeTypeEnum type;

    @Schema(description = "通知状态 0未读 1已读")
    private NoticeStatusEnum status;

    @Schema(description = "额外参数")
    private String extras;

    @Schema(description = "创建时间")
    @JsonFormat(pattern = "yy-MM-dd", timezone = "GMT+8")
    private LocalDateTime createTime;
}
