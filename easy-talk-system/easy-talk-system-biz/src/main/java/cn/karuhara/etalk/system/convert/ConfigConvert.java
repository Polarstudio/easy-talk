package cn.karuhara.etalk.system.convert;

import cn.karuhara.etalk.system.controller.config.vo.SystemConfigVO;
import cn.karuhara.etalk.system.entity.config.EasyConfig;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ConfigConvert {
    ConfigConvert INSTANCE = Mappers.getMapper(ConfigConvert.class);

    EasyConfig voToEasyConfig(SystemConfigVO vo);

    SystemConfigVO easyConfigToVo(EasyConfig easyConfig);
}
