<p align="center">
  <img src="https://110.40.63.160:14573/down/1gWFkntkH97w.png" alt="轻语" width="400">
</p>


**一个基于Spring Cloud Alibaba、Spring Cloud、MyBatis-Plus、MySQL、Redis、SaToken、Kafka、Vue、Uniapp（目前仅适配微信小程序）等技术栈实现的前后端分离的社交平台，采用主流的互联网技术架构、简洁的UI设计，拥有完整的动态发布、评论、自动审核、搜索、活动、话题、圈子、用户认证等功能，完全满足正常的社交需求**👍。


## 一、项目介绍

### 项目演示

#### 小程序端

- 项目仓库（码云）：[https://gitee.com/Polarstudio/easy-talk-mp](https://gitee.com/Polarstudio/easy-talk-mp)

![小程序端1](https://easy-talk-bean.oss-cn-beijing.aliyuncs.com/intro1.png)

![小程序端2](https://easy-talk-bean.oss-cn-beijing.aliyuncs.com/intro2.png)

![小程序端3](https://easy-talk-bean.oss-cn-beijing.aliyuncs.com/intro3.png)

![小程序端4](https://easy-talk-bean.oss-cn-beijing.aliyuncs.com/intro4.png)

小程序端的相关配置都写在了easy-talk-mp项目的README.md 中，请注意查看⚠️。


#### 后台管理系统

- 项目仓库（码云）：[https://gitee.com/Polarstudio/easy-talk-admin](https://gitee.com/Polarstudio/easy-talk-admin)

![管理系统](https://easy-talk-bean.oss-cn-beijing.aliyuncs.com/admin.png)

admin 端部署写在了 easy-talk-admin 项目的 README.md 中，请注意查看⚠️。

#### 代码展示

![轻语社源码结构](https://easy-talk-bean.oss-cn-beijing.aliyuncs.com/cbf53e62-f505-4c14-ade2-5ab24b0f7552.png)


### 架构图

#### 系统架构图

![轻语系统架构图](https://easy-talk-bean.oss-cn-beijing.aliyuncs.com/jishujiagou.png)


#### 业务架构图

![轻语业务架构图](https://easy-talk-bean.oss-cn-beijing.aliyuncs.com/yewujiagou.png)

### 组织结构

```
easy-talk
├── easy-talk-auth -- 集成RBAC模型，负责管理后台的登录，系统的菜单、角色、管理员进行管理
├── easy-talk-content -- 平台内容服务，主要有动态、话题、圈子、活动、评论相关
├── easy-talk-content-framework -- 项目的通用框架，核心工具/自定义starter相关模块，通用的组件都放在这个模块
├── easy-talk-gateway --  网关服务，负责路由和鉴权
├── easy-talk-gateway-member -- 会员服务，对平台用户进行操作的服务
├── easy-talk-gateway-system -- 系统服务，底层的一些业务操作，通知、系统配置相关，以及对接微信的一些服务接口
```

### 技术选型

后端技术栈

|        技术        | 说明                                 | 官网                                                         |
| :----------------: | ------------------------------------ | ------------------------------------------------------------ |
| Spring & SpringMVC | Java全栈应用程序框架和WEB容器实现    | [https://spring.io/](https://spring.io/)                     |
|     SpringBoot     | Spring应用简化集成开发框架           | [https://spring.io/projects/spring-boot](https://spring.io/projects/spring-boot) |
|    SpringCloud     | 微服务解决方案                       | [https://spring.io/cloud](https://spring.io/cloud)           |
|       Mysql        | 关系型数据库                         | [https://www.mysql.com/](https://www.mysql.com/)             |
|    mybatis-plus    | 数据库orm框架                        | [https://baomidou.com/](https://baomidou.com/)               |
|       redis        | 内存数据存储                         | [https://redis.io](https://redis.io)                         |
|       kafka        | 消息队列                             | [https://kafka.apache.org/](https://kafka.apache.org/)       |
|       nacos        | 动态服务发现、配置管理和服务管理平台 | [https://nacos.io/](https://nacos.io/)                       |
|       minio        | 分布式文件系统                       | [https://www.minio.org.cn/](https://www.minio.org.cn/)       |
|      satoken       | 权限认证框架                         | [https://sa-token.cc/](https://sa-token.cc/)                 |
|       lombok       | Java语言增强库                       | [https://projectlombok.org](https://projectlombok.org)       |
|       guava        | google开源的java工具集               | [https://github.com/google/guava](https://github.com/google/guava) |
|      swagger       | API文档生成工具                      | [https://swagger.io](https://swagger.io)                     |
|     ip2region      | ip地址                               | [https://github.com/zoujingli/ip2region](https://github.com/zoujingli/ip2region) |
|     fastjson2      | 高性能的JSON库                       | [https://alibaba.github.io/fastjson2/](https://alibaba.github.io/fastjson2/) |
|     mapstruct      | 对象属性映射工具                     | [https://mapstruct.org/](https://mapstruct.org/)             |
|       okhttp       | HTTP客户端                           | [https://square.github.io/okhttp/](https://square.github.io/okhttp/) |

## 二、环境搭建

### 开发工具

|      工具      | 说明               | 官网                                                         |
| :------------: | ------------------ | ------------------------------------------------------------ |
|      IDEA      | java开发工具       | [https://www.jetbrains.com](https://www.jetbrains.com)       |
|    Webstorm    | web开发工具        | [https://www.jetbrains.com/webstorm](https://www.jetbrains.com/webstorm) |
|   Hbuilder X   | UniApp开发工具     | [https://www.dcloud.io/hbuilderx.html](https://www.dcloud.io/hbuilderx.html) |
| 微信开发者工具 | 小程序调试         | [https://developers.weixin.qq.com/miniprogram](https://developers.weixin.qq.com/miniprogram/dev/devtools/download.html) |
|     Chrome     | 浏览器             | [https://www.google.com/intl/zh-CN/chrome](https://www.google.com/intl/zh-CN/chrome) |
|    Postman     | API接口调试        | [https://www.postman.com](https://www.postman.com)           |
|    process     | 流程图、架构图绘制 | [https://www.processon.com/](https://www.processon.com/)     |
|    navicat     | 数据库连接工具     | [https://www.navicat.com](https://www.navicat.com)           |


### 开发环境

| 工具  | 版本 | 下载                                                         |
| :---: | :--- | ------------------------------------------------------------ |
|  jdk  | 17+  | [https://www.oracle.com/java/technologies/downloads/#java8](https://www.oracle.com/java/technologies/downloads/#java8) |
| mysql | 8.0+ | [https://www.mysql.com/downloads/](https://www.mysql.com/downloads/) |
| redis | 6.0+ | [https://redis.io/download/](https://redis.io/download/)     |

### 搭建步骤
推荐先部署后端 -> 管理后台 -> 小程序端
> [后端本地开发环境教程](https://gitee.com/Polarstudio/easy-talk/blob/master/doc/%E6%9C%AC%E5%9C%B0%E5%BC%80%E5%8F%91%E7%8E%AF%E5%A2%83%E9%85%8D%E7%BD%AE.md)
> [管理后台部署](https://gitee.com/Polarstudio/easy-talk-admin)
> [小程序端](https://gitee.com/Polarstudio/easy-talk-mp)
## 三、鸣谢

感谢开源社区的所有开发者，是你们无私贡献的优秀代码和技术方案，为我的项目提供了坚实的技术基础，让我能够站在巨人的肩膀上完成开发。特别是 Spring Cloud Alibaba、Spring Cloud、MyBatis-Plus 等框架的开发者，让我在技术选型和架构搭建上少走了许多弯路。

## 四、演示

因为小程序上线需要营业执照，所以没法上线给大家演示，只有体验版，扫描下面的二维码申请体验版，我会第一时间处理。

 <img src="https://easy-talk-bean.oss-cn-beijing.aliyuncs.com/ow62a5Mcl7aRfCgsqmPXTLtkmBKA%20%283%29.jpg" width = "246" height = "246" alt="图片名称" />

联系我：

 <img src="https://easy-talk-bean.oss-cn-beijing.aliyuncs.com/wxcode.png" width = "246" height = "344" alt="图片名称" />

## 五、许可证

Apache License 2.0

Copyright (c) 2024-2025 轻语（Bean.）