/*
 Navicat Premium Dump SQL

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 80012 (8.0.12)
 Source Host           : localhost:3306
 Source Schema         : easy_talk

 Target Server Type    : MySQL
 Target Server Version : 80012 (8.0.12)
 File Encoding         : 65001

 Date: 23/01/2025 14:32:04
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for easy_activity
-- ----------------------------
DROP TABLE IF EXISTS `easy_activity`;
CREATE TABLE `easy_activity`  (
  `id` int(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
  `cover_image_url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '活动封面',
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '名称',
  `intro` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '简介',
  `imgs` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '介绍图 例如 活动的海报',
  `user_id` int(11) NOT NULL COMMENT '活动创建人',
  `apply_start` datetime NULL DEFAULT NULL COMMENT '报名开始时间',
  `apply_end` datetime NULL DEFAULT NULL COMMENT '报名结束时间',
  `start_time` datetime NOT NULL COMMENT '开始时间',
  `end_time` datetime NOT NULL COMMENT '结束时间',
  `latitude` double NOT NULL COMMENT '地点经度',
  `longitude` double NOT NULL COMMENT '地点维度',
  `people_num` int(11) NULL DEFAULT -1 COMMENT '人数限制 (-1代表无人数限制)',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '地点位置 ',
  `place_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '地点名称',
  `browse` int(11) NULL DEFAULT 1 COMMENT '浏览',
  `push` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '首页推送：0 否、1 是',
  `status` enum('0','1','2','3','4','5') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '4' COMMENT '状态：0 报名中、1进行中、2 已结束、3已下线、4审核中、5驳回',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `deleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除（0未删除 1删除）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '活动' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of easy_activity
-- ----------------------------

-- ----------------------------
-- Table structure for easy_activity_user
-- ----------------------------
DROP TABLE IF EXISTS `easy_activity_user`;
CREATE TABLE `easy_activity_user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `activity_id` int(11) NOT NULL COMMENT '活动ID',
  `uuid` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '报名的唯一标识',
  `status` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '状态 0未参加 1已参加',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户参与活动' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of easy_activity_user
-- ----------------------------

-- ----------------------------
-- Table structure for easy_circle
-- ----------------------------
DROP TABLE IF EXISTS `easy_circle`;
CREATE TABLE `easy_circle`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `circle_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '名称',
  `user_id` int(11) NOT NULL COMMENT '创建者',
  `cover_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '圈子封面',
  `intro` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '简介',
  `status` enum('0','1','2','3','4') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '3' COMMENT '话题状态（0正常，1推送、2隐藏、3审核中、4驳回）',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `_id`(`id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '圈子' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of easy_circle
-- ----------------------------

-- ----------------------------
-- Table structure for easy_circle_follow
-- ----------------------------
DROP TABLE IF EXISTS `easy_circle_follow`;
CREATE TABLE `easy_circle_follow`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `circle_id` int(11) NOT NULL COMMENT '圈子ID',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '圈子用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of easy_circle_follow
-- ----------------------------

-- ----------------------------
-- Table structure for easy_clause
-- ----------------------------
DROP TABLE IF EXISTS `easy_clause`;
CREATE TABLE `easy_clause`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '标题',
  `content` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '内容',
  `status` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `create_time` bigint(20) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` bigint(20) NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '条款' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of easy_clause
-- ----------------------------

-- ----------------------------
-- Table structure for easy_config
-- ----------------------------
DROP TABLE IF EXISTS `easy_config`;
CREATE TABLE `easy_config`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '配置名称',
  `value` json NULL COMMENT '值',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '配置' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of easy_config
-- ----------------------------

-- ----------------------------
-- Table structure for easy_dynamic
-- ----------------------------
DROP TABLE IF EXISTS `easy_dynamic`;
CREATE TABLE `easy_dynamic`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '内容',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '位置',
  `address_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '位置名称',
  `circle_id` int(11) NULL DEFAULT NULL COMMENT '圈子ID',
  `topic_id` int(11) NULL DEFAULT NULL COMMENT '话题ID',
  `activity_id` int(11) NULL DEFAULT NULL COMMENT '活动ID',
  `browse` int(11) NULL DEFAULT 1 COMMENT '浏览',
  `top` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '置顶:   0不置顶、1置顶',
  `dynamic_type` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '类型：0 图文、1 视频',
  `status` enum('0','1','2','3','4') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '状态：0 草稿、1 待审核、2 展示、3 不展示、4 驳回、5 用户删除',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `latitude` double NULL DEFAULT NULL COMMENT '地点经度',
  `longitude` double NULL DEFAULT NULL COMMENT '地点维度',
  `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `_id`(`id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '动态' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of easy_dynamic
-- ----------------------------

-- ----------------------------
-- Table structure for easy_dynamic_comment
-- ----------------------------
DROP TABLE IF EXISTS `easy_dynamic_comment`;
CREATE TABLE `easy_dynamic_comment`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `dynamic_id` int(11) NOT NULL COMMENT '动态ID',
  `reply_comment_id` int(11) NULL DEFAULT NULL COMMENT '回复的评论ID',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '回复内容',
  `status` enum('0','1','2') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '状态：0 待审核、1 正常、2 驳回',
  `date` datetime NULL DEFAULT NULL COMMENT '评论时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `_dynamic_id`(`dynamic_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '动态评论' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of easy_dynamic_comment
-- ----------------------------

-- ----------------------------
-- Table structure for easy_dynamic_comment_like
-- ----------------------------
DROP TABLE IF EXISTS `easy_dynamic_comment_like`;
CREATE TABLE `easy_dynamic_comment_like`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dynamic_id` int(11) NULL DEFAULT NULL COMMENT '动态id',
  `comment_id` int(11) NULL DEFAULT NULL COMMENT '评论id',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `type` enum('0','1') CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '类型（0点赞 1 点踩）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '评论点赞表' ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of easy_dynamic_comment_like
-- ----------------------------

-- ----------------------------
-- Table structure for easy_dynamic_img
-- ----------------------------
DROP TABLE IF EXISTS `easy_dynamic_img`;
CREATE TABLE `easy_dynamic_img`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dynamic_id` int(11) NOT NULL COMMENT '动态ID',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `img_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '地址',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `_dynamic_id`(`dynamic_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '动态图片' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of easy_dynamic_img
-- ----------------------------

-- ----------------------------
-- Table structure for easy_dynamic_like
-- ----------------------------
DROP TABLE IF EXISTS `easy_dynamic_like`;
CREATE TABLE `easy_dynamic_like`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `dynamic_id` int(11) NOT NULL COMMENT '动态ID',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `_dynamic_id`(`dynamic_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '动态点赞' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of easy_dynamic_like
-- ----------------------------

-- ----------------------------
-- Table structure for easy_dynamic_nearby
-- ----------------------------
DROP TABLE IF EXISTS `easy_dynamic_nearby`;
CREATE TABLE `easy_dynamic_nearby`  (
  `id` int(11) NOT NULL,
  `dynamic_id` int(11) NOT NULL COMMENT '动态ID',
  `latitude` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '经度',
  `longitude` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '维度',
  `deleted` tinyint(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '附近动态表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of easy_dynamic_nearby
-- ----------------------------

-- ----------------------------
-- Table structure for easy_dynamic_video
-- ----------------------------
DROP TABLE IF EXISTS `easy_dynamic_video`;
CREATE TABLE `easy_dynamic_video`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dynamic_id` int(11) NOT NULL COMMENT '动态ID',
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `video_cover` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '封面',
  `video_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '地址',
  `height` int(11) NULL DEFAULT NULL COMMENT '宽',
  `width` int(11) NULL DEFAULT NULL COMMENT '高',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `_dynamic_id`(`dynamic_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '动态视频' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of easy_dynamic_video
-- ----------------------------

-- ----------------------------
-- Table structure for easy_notice
-- ----------------------------
DROP TABLE IF EXISTS `easy_notice`;
CREATE TABLE `easy_notice`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '标题',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '内容',
  `form_id` int(11) NULL DEFAULT NULL COMMENT '发起人ID',
  `to_id` int(11) NULL DEFAULT NULL COMMENT '接受人ID',
  `content_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '内容跳转地址',
  `type` enum('0','1','2','3','4','5') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '0' COMMENT '类型：0 系统、1 动态、2 圈子、3 话题、4活动、5评论',
  `status` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '0' COMMENT '通知状态 0未读 1已读',
  `extras` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT '额外参数',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '通知' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of easy_notice
-- ----------------------------

-- ----------------------------
-- Table structure for easy_topic
-- ----------------------------
DROP TABLE IF EXISTS `easy_topic`;
CREATE TABLE `easy_topic`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '话题名称',
  `intro` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL COMMENT '话题描述',
  `user_id` int(11) NOT NULL COMMENT '创键者',
  `follow` int(11) NULL DEFAULT 0 COMMENT '话题关注量',
  `dynamic` int(11) NULL DEFAULT 0 COMMENT '话题动态数',
  `views` int(11) NULL DEFAULT 0 COMMENT '话题浏览量',
  `status` enum('0','1','2','3','4') CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '3' COMMENT '话题状态（0正常，1推送、2隐藏、3审核中、4驳回）',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除（0未删除、1删除）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '话题表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of easy_topic
-- ----------------------------

-- ----------------------------
-- Table structure for easy_topic_follow
-- ----------------------------
DROP TABLE IF EXISTS `easy_topic_follow`;
CREATE TABLE `easy_topic_follow`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `topic_id` int(11) NOT NULL COMMENT '话题ID',
  `create_time` datetime NULL DEFAULT NULL COMMENT '关注时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '话题关注表' ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of easy_topic_follow
-- ----------------------------

-- ----------------------------
-- Table structure for easy_user
-- ----------------------------
DROP TABLE IF EXISTS `easy_user`;
CREATE TABLE `easy_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nike_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '名称',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '头像',
  `intro` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '简介',
  `gender` enum('0','1','2') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '2' COMMENT '性别(0男，1女、2未知)',
  `age` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '年龄',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'IP',
  `dependency` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'IP属地',
  `wx_openid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '微信openid',
  `type` enum('0','1','2') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '用户类型：0普通、1 认证、2官方',
  `certificate_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '认证名称',
  `status` enum('0','1','2','3','8') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态：0 正常、1审核中、2禁言、3拉黑、8 注销',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `_u_id`(`id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of easy_user
-- ----------------------------

-- ----------------------------
-- Table structure for easy_user_certificate
-- ----------------------------
DROP TABLE IF EXISTS `easy_user_certificate`;
CREATE TABLE `easy_user_certificate`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户Id',
  `certificate_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '认证名称',
  `material` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL COMMENT '认证资料',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  `status` enum('0','1','2') CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '0' COMMENT '认证状态（0待审核 1通过 2驳回）',
  `rejection` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '驳回原因',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '用户认证表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of easy_user_certificate
-- ----------------------------

-- ----------------------------
-- Table structure for easy_user_follow
-- ----------------------------
DROP TABLE IF EXISTS `easy_user_follow`;
CREATE TABLE `easy_user_follow`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `follow_id` int(11) NOT NULL COMMENT '关注ID',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '用户关注' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of easy_user_follow
-- ----------------------------

-- ----------------------------
-- Table structure for sys_admin
-- ----------------------------
DROP TABLE IF EXISTS `sys_admin`;
CREATE TABLE `sys_admin`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '账号',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '密码',
  `nike_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT '系统管理员' COMMENT '昵称',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT 'https://img.picgo.net/2024/11/01/vector-24c763b95ff8a5a7b.png' COMMENT '头像',
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '邮箱',
  `login_ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '登录ip',
  `status` enum('0','1') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '状态（0正常 1封禁）',
  `login_time` datetime NULL DEFAULT NULL COMMENT '登录时间',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `creator` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建者',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '系统管理员表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_admin
-- ----------------------------
INSERT INTO `sys_admin` VALUES (1, 'admin', '$2a$10$KwS9i3HlsavRXCg1Nn93ze1dRxc7mGHdwXh8hL1SmF.Krz2kGTCSa', '超级管理员', 'https://img.picgo.net/2024/11/01/vector-24c763b95ff8a5a7b.png', 'admin@admin.cn', '192.168.150.143', '0', '2025-01-19 16:28:52', '2024-11-05 14:40:00', '2025-01-19 16:28:52', NULL);

-- ----------------------------
-- Table structure for sys_admin_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_admin_role`;
CREATE TABLE `sys_admin_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) NOT NULL COMMENT '管理员ID',
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '管理员角色表' ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of sys_admin_role
-- ----------------------------
INSERT INTO `sys_admin_role` VALUES (1, 1, 1);

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` int(11) NULL DEFAULT 0 COMMENT '目录ID',
  `order_num` int(11) NULL DEFAULT 0 COMMENT '显示顺序',
  `path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '路由地址',
  `route_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '路由名称',
  `menu_type` enum('C','M') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'C' COMMENT '菜单类型（C目录 M菜单）',
  `status` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注',
  `creator` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建者',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '总览', 0, 0, '', '', 'C', '0', '', '', '2024-11-02 21:56:01', '2024-11-02 21:56:01', '', '1691684352');
INSERT INTO `sys_menu` VALUES (2, '系统', 0, 1, '', '', 'C', '0', '', '', '2024-11-02 21:58:37', '2024-11-05 21:42:56', '', '1691684352');
INSERT INTO `sys_menu` VALUES (3, '菜单管理', 2011, 1, '/system/menu', 'systemMenu', 'M', '0', 'system:menu:list', 'custom-fatrows', '2024-11-03 23:24:57', '2024-11-09 22:28:14', '菜单管理', NULL);
INSERT INTO `sys_menu` VALUES (4, '用户管理', 2011, 2, '/system/user', 'system-user', 'M', '0', 'system:user:list', 'custom-user-square', '2024-11-03 23:30:02', '2024-11-09 22:28:01', '用户管理', NULL);
INSERT INTO `sys_menu` VALUES (5, '角色管理', 2011, 3, '/system/role', 'SystemRole', 'M', '0', 'system:role:list', 'custom-users', '2024-11-05 14:46:04', '2024-11-09 22:28:31', '角色管理', NULL);
INSERT INTO `sys_menu` VALUES (6, '系统配置', 2011, 4, '/system/config', 'SystemConfig', 'M', '0', 'system:config:list', 'custom-cpu-charge', '2024-11-05 14:49:54', '2024-11-09 22:28:41', '系统配置', NULL);
INSERT INTO `sys_menu` VALUES (7, '协议管理', 2011, 5, '/system/agreement', 'SystemAgreement', 'M', '0', 'system:agreement:list', 'custom-table', '2024-11-05 14:50:44', '2024-11-09 22:28:56', '协议管理', NULL);
INSERT INTO `sys_menu` VALUES (8, '会员', 0, 2, '', '', 'C', '0', '', '', '2024-11-05 14:51:29', '2024-11-05 21:43:01', '', NULL);
INSERT INTO `sys_menu` VALUES (9, '会员列表', 2020, 1, '/member/manager', 'MemberManager', 'M', '0', 'member:manager:list', 'custom-users', '2024-11-05 14:53:53', '2024-11-09 22:29:56', '会员列表', NULL);
INSERT INTO `sys_menu` VALUES (10, '认证管理', 2020, 2, '/member/certificate', 'MemberCertificate', 'M', '0', 'member:cartificate:list', 'custom-register', '2024-11-05 14:56:36', '2024-12-17 17:22:08', '认证管理', NULL);
INSERT INTO `sys_menu` VALUES (11, '通知推送', 2020, 3, '/member/notification', 'MemberNotification', 'M', '0', 'member:notification:list', 'custom-send', '2024-11-05 14:57:28', '2024-11-09 22:30:19', '通知推送', NULL);
INSERT INTO `sys_menu` VALUES (12, '内容', 0, 4, '', '', 'C', '0', '', '', '2024-11-05 14:58:34', '2024-11-05 21:43:06', '', NULL);
INSERT INTO `sys_menu` VALUES (13, '动态管理', 2024, 1, '/content/dynamic', 'ContentDynamic', 'M', '0', 'content:dynamic:list', 'custom-dynamic', '2024-11-05 15:01:54', '2024-11-09 22:30:28', '动态管理', NULL);
INSERT INTO `sys_menu` VALUES (14, '圈子管理', 2024, 2, '/content/circle', 'ContentCircle', 'M', '0', 'content:circle', 'custom-colorpick', '2024-11-05 15:02:54', '2024-11-09 22:30:38', '圈子管理', NULL);
INSERT INTO `sys_menu` VALUES (15, '活动管理', 2024, 3, '/content/activity', 'ContentActivity', 'M', '0', 'content:activity', 'custom-activities', '2024-11-05 15:04:30', '2024-11-09 22:30:47', '活动管理', NULL);
INSERT INTO `sys_menu` VALUES (16, '话题管理', 2024, 4, '/content/topic', 'ContentTopic', 'M', '0', 'content:topic', 'custom-topic', '2024-11-05 15:05:27', '2024-11-09 22:30:58', '话题管理', NULL);
INSERT INTO `sys_menu` VALUES (17, '评论管理', 2024, 5, '/content/comment', 'ContentComment', 'M', '0', 'content:comment:list', 'custom-comment', '2024-11-05 15:06:33', '2024-11-09 22:31:08', '评论管理', NULL);
INSERT INTO `sys_menu` VALUES (18, '监控', 0, 5, '', '', 'C', '0', '', '', '2024-11-05 15:07:06', '2024-11-05 21:43:10', '', NULL);
INSERT INTO `sys_menu` VALUES (19, '操作日志', 2030, 0, '/monitoring/log', 'MonitoringLog', 'M', '0', 'monitoring:log:list', 'custom-sample', '2024-11-05 15:08:07', '2024-11-09 22:31:19', '操作日志', NULL);
INSERT INTO `sys_menu` VALUES (20, '数据面板', 2010, 0, '/dashboard/default', 'Default', 'M', '0', 'dashboard:default', 'custom-home-trend', '2024-11-05 22:32:43', '2024-11-09 22:31:30', '数据面板', NULL);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '角色名',
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '角色标识',
  `type` int(11) NULL DEFAULT 1 COMMENT '角色类型(0 超级管理员 1自定义管理员)',
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '备注',
  `sort` tinyint(4) NULL DEFAULT NULL COMMENT '排序',
  `status` enum('0','1') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' COMMENT '状态（0正常 1封禁）',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `creator` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建者',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'SuperAdmin', 0, '超级管理员', 0, '0', '2024-11-05 14:41:13', '2025-01-19 16:28:42', NULL);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `menu_id` int(11) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '角色菜单表' ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (1, 1, 1);
INSERT INTO `sys_role_menu` VALUES (2, 1, 2);
INSERT INTO `sys_role_menu` VALUES (3, 1, 3);
INSERT INTO `sys_role_menu` VALUES (4, 1, 4);
INSERT INTO `sys_role_menu` VALUES (5, 1, 5);
INSERT INTO `sys_role_menu` VALUES (6, 1, 6);
INSERT INTO `sys_role_menu` VALUES (7, 1, 7);
INSERT INTO `sys_role_menu` VALUES (8, 1, 8);
INSERT INTO `sys_role_menu` VALUES (9, 1, 9);
INSERT INTO `sys_role_menu` VALUES (10, 1, 10);
INSERT INTO `sys_role_menu` VALUES (11, 1, 11);
INSERT INTO `sys_role_menu` VALUES (12, 1, 12);
INSERT INTO `sys_role_menu` VALUES (13, 1, 13);
INSERT INTO `sys_role_menu` VALUES (14, 1, 14);
INSERT INTO `sys_role_menu` VALUES (15, 1, 15);
INSERT INTO `sys_role_menu` VALUES (16, 1, 16);
INSERT INTO `sys_role_menu` VALUES (17, 1, 17);
INSERT INTO `sys_role_menu` VALUES (18, 1, 18);
INSERT INTO `sys_role_menu` VALUES (19, 1, 19);
INSERT INTO `sys_role_menu` VALUES (20, 1, 20);

SET FOREIGN_KEY_CHECKS = 1;
