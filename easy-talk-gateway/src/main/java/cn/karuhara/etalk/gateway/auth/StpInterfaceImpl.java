package cn.karuhara.etalk.gateway.auth;

import cn.dev33.satoken.stp.StpInterface;
import cn.karuhara.etalk.framework.common.constants.RedisConstant;
import cn.karuhara.etalk.fremawork.redis.core.MyRedisUtil;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.TypeReference;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 自定义权限加载接口实现类
 */
@Component    // 保证此类被 SpringBoot 扫描，完成 Sa-Token 的自定义权限验证扩展
@Slf4j
public class StpInterfaceImpl implements StpInterface {

    @Resource
    MyRedisUtil myRedisUtil;
    /**
     * 返回一个账号所拥有的权限码集合
     */
    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        log.info("getPermission:{}",loginId);
        String permissionKey = MyRedisUtil.buildKey(RedisConstant.PERMISSIONS_PREFIX, (String) loginId);
        Object object = myRedisUtil.get(permissionKey);
        if (object == null) {
            return new ArrayList<>();
        }
        return JSON.parseObject(JSON.toJSONString(object), new TypeReference<ArrayList<String>>() {});
    }

    /**
     * 返回一个账号所拥有的角色标识集合 (权限与角色可分开校验)
     */
    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        log.info("getRoles:{}",loginId);
        String roleKey = MyRedisUtil.buildKey(RedisConstant.ROLES_PREFIX, (String) loginId);
        Object object = myRedisUtil.get(roleKey);
        if (object == null) {
            return new ArrayList<>();
        }
        return JSON.parseObject(JSON.toJSONString(object), new TypeReference<ArrayList<String>>() {});
    }
}
