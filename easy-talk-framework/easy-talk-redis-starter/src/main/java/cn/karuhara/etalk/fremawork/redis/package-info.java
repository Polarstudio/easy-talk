/**
 * @Description: 采用 Spring Data Redis 操作 Redis，封装操作工具类，底层使用 Redisson 作为客户端，并自定义序列化，避免出现某些序列化问题
 * @author: 轻语
*/
package cn.karuhara.etalk.fremawork.redis;