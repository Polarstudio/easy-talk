package cn.karuhara.etalk.fremawork.redis.core;

import cn.karuhara.etalk.framework.common.constants.RedisConstant;
import lombok.Setter;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @Description: 工具类，封装Redis常用操作
 * @author: 轻语
*/

@Setter
public class MyRedisUtil {

    private RedisTemplate<String, Object> redisTemplate;

    public MyRedisUtil(){}

    public static String buildKey(String prefix, String key){
        return RedisConstant.REDIS_PREFIX+":"+ prefix + ":" + key;
    }

    public static String buildKey(String key){
        return RedisConstant.REDIS_PREFIX + ":" + key;
    }
    /**
     * 保存属性
     *
     * @param time 超时时间（秒）
     */
    public void set(String key, Object value, long time) {
        redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
    }

    /**
     * 保存属性
     */
    public void set(String key, Object value) {
        redisTemplate.opsForValue().set(key, value);
    }

    /**
     * 获取属性
     */
    public Object get(String key) {
        return redisTemplate.opsForValue().get(key);
    }

    /**
     * 删除属性
     */
    public Boolean del(String key) {
        return redisTemplate.delete(key);
    }

    /**
     * 批量删除属性
     */
    public Long del(List<String> keys) {
        return redisTemplate.delete(keys);
    }

    /**
     * 设置过期时间
     */
    public Boolean expire(String key, long time) {
        return redisTemplate.expire(key, time, TimeUnit.SECONDS);
    }

    /**
     * 获取过期时间
     */
    public Long getExpire(String key) {
        return redisTemplate.getExpire(key, TimeUnit.SECONDS);
    }

    /**
     * 判断是否有该属性
     */
    public Boolean hasKey(String key) {
        return redisTemplate.hasKey(key);
    }

    /**
     * 按delta递增
     */
    public Long incr(String key, long delta) {
        return redisTemplate.opsForValue().increment(key, delta);
    }

    /**
     * 按delta递减
     */
    public Long decr(String key, long delta) {
        return redisTemplate.opsForValue().increment(key, -delta);
    }

    /**
     * 获取Hash结构中的属性
     */
    public Object hGet(String key, String hashKey) {
        return redisTemplate.opsForHash().get(key, hashKey);
    }

    /**
     * 向Hash结构中放入一个属性
     */
    public Boolean hSet(String key, String hashKey, Object value, long time) {
        redisTemplate.opsForHash().put(key, hashKey, value);
        return expire(key, time);
    }

    /**
     * 向Hash结构中放入一个属性
     */
    public void hSet(String key, String hashKey, Object value) {
        redisTemplate.opsForHash().put(key, hashKey, value);
    }

    /**
     * 直接获取整个Hash结构
     */
    public Map<Object, Object> hGetAll(String key) {
        return redisTemplate.opsForHash().entries(key);
    }

    /**
     * 直接设置整个Hash结构
     */
    public Boolean hSetAll(String key, Map<String, Object> map, long time) {
        redisTemplate.opsForHash().putAll(key, map);
        return expire(key, time);
    }

    /**
     * 直接设置整个Hash结构
     */
    public void hSetAll(String key, Map<String, ?> map) {
        redisTemplate.opsForHash().putAll(key, map);
    }

    /**
     * 删除Hash结构中的属性
     */
    public void hDel(String key, Object... hashKey) {
        redisTemplate.opsForHash().delete(key, hashKey);
    }

    /**
     * 判断Hash结构中是否有该属性
     */
    public Boolean hHasKey(String key, String hashKey) {
        return redisTemplate.opsForHash().hasKey(key, hashKey);
    }

    /**
     * Hash结构中属性递增
     */
    public Long hIncr(String key, String hashKey, Long delta) {
        return redisTemplate.opsForHash().increment(key, hashKey, delta);
    }

    /**
     * Hash结构中属性递减
     */
    public Long hDecr(String key, String hashKey, Long delta) {
        return redisTemplate.opsForHash().increment(key, hashKey, -delta);
    }

    /**
     * 获取Set结构
     */
    public Set<Object> sMembers(String key) {
        return redisTemplate.opsForSet().members(key);
    }

    /**
     * 向Set结构中添加属性
     */
    public Long sAdd(String key, Object... values) {
        return redisTemplate.opsForSet().add(key, values);
    }

    /**
     * 向Set结构中添加属性
     */
    public Long sAdd(String key, long time, Object... values) {
        Long count = redisTemplate.opsForSet().add(key, values);
        expire(key, time);
        return count;
    }

    /**
     * 是否为Set中的属性
     */
    public Boolean sIsMember(String key, Object value) {
        return redisTemplate.opsForSet().isMember(key, value);
    }

    /**
     * 获取Set结构的长度
     */
    public Long sSize(String key) {
        return redisTemplate.opsForSet().size(key);
    }

    /**
     * 删除Set结构中的属性
     */
    public Long sRemove(String key, Object... values) {
        return redisTemplate.opsForSet().remove(key, values);
    }

    /**
     * 获取List结构中的属性
     */
    public List<Object> lRange(String key, long start, long end) {
        return redisTemplate.opsForList().range(key, start, end);
    }

    /**
     * 获取List结构的长度
     */
    public Long lSize(String key) {
        return redisTemplate.opsForList().size(key);
    }

    /**
     * 根据索引获取List中的属性
     */
    public Object lIndex(String key, long index) {
        return redisTemplate.opsForList().index(key, index);
    }

    /**
     * 向List结构中添加属性
     */
    public Long lPush(String key, Object value) {
        return redisTemplate.opsForList().rightPush(key, value);
    }

    /**
     * 向List结构中添加属性
     */
    public Long lPush(String key, Object value, long time) {
        Long index = redisTemplate.opsForList().rightPush(key, value);
        expire(key, time);
        return index;
    }

    /**
     * 向List结构中批量添加属性
     */
    public Long lPushAll(String key, Object... values) {
        return redisTemplate.opsForList().rightPushAll(key, values);
    }

    /**
     * 向List结构中批量添加属性
     */
    public Long lPushAll(String key, Long time, Object... values) {
        Long count = redisTemplate.opsForList().rightPushAll(key, values);
        expire(key, time);
        return count;
    }

    /**
     * 从List结构中移除属性
     */
    public Long lRemove(String key, long count, Object value) {
        return redisTemplate.opsForList().remove(key, count, value);
    }

    // Sorted Set操作
    /**
     * 向Sorted Set中添加元素
     *
     * @param key   键
     * @param value 值
     * @param score 分数
     * @return 是否添加成功
     */
    public Boolean zAdd(String key, Object value, double score) {
        return redisTemplate.opsForZSet().add(key, value, score);
    }

    // 更新 Sorted Set 中的元素
    public void updateZSetElement(String key, Object value, double newScore) {
        Double currentScore = zScore(key, value);
        if (currentScore!= null) {
            //存在则跟新
            zIncrScore(key, value, newScore - currentScore);
        } else {
            //不存在则直接赋值
            zAdd(key, value, newScore);
        }
    }

    /**
     * 获取Sorted Set中元素的分数
     *
     * @param key   键
     * @param value 值
     * @return 元素的分数，如果不存在返回null
     */
    public Double zScore(String key, Object value) {
        return redisTemplate.opsForZSet().score(key, value);
    }

    /**
     * 获取Sorted Set中指定排名范围的元素（从小到大排序）
     *
     * @param key   键
     * @param start 起始排名（包含）
     * @param end   结束排名（包含）
     * @return 元素集合
     */
    public Set<Object> zRange(String key, long start, long end) {
        return redisTemplate.opsForZSet().range(key, start, end);
    }

    /**
     * 获取Sorted Set中指定排名范围的元素（从大到小排序）
     *
     * @param key   键
     * @param start 起始排名（包含）
     * @param end   结束排名（包含）
     * @return 元素集合
     */
    public Map<Object, Double> zRevRangeWithScores(String key, long start, long end) {
        Set<ZSetOperations.TypedTuple<Object>> tuples = redisTemplate.opsForZSet().reverseRangeWithScores(key, start, end);
        Map<Object, Double> result = new LinkedHashMap<>();
        for (ZSetOperations.TypedTuple<Object> tuple : tuples) {
            result.put(tuple.getValue(), tuple.getScore());
        }
        return result;
    }

    /**
     * 获取Sorted Set中指定分数范围的元素（从小到大排序）
     *
     * @param key     键
     * @param minScore 最小分数（包含）
     * @param maxScore 最大分数（包含）
     * @return 元素集合
     */
    public Set<Object> zRangeByScore(String key, double minScore, double maxScore) {
        return redisTemplate.opsForZSet().rangeByScore(key, minScore, maxScore);
    }

    /**
     * 获取Sorted Set中指定排名范围的元素及其分数（从小到大排序）
     *
     * @param key   键
     * @param start 起始排名（包含）
     * @param end   结束排名（包含）
     * @return 元素和分数的映射
     */
    public Map<Object, Double> zRangeWithScores(String key, long start, long end) {
        Set<ZSetOperations.TypedTuple<Object>> tuples = redisTemplate.opsForZSet().rangeWithScores(key, start, end);
        Map<Object, Double> result = new HashMap<>();
        for (ZSetOperations.TypedTuple<Object> tuple : tuples) {
            result.put(tuple.getValue(), tuple.getScore());
        }
        return result;
    }

    /**
     * 获取Sorted Set中指定分数范围的元素及其分数（从小到大排序）
     *
     * @param key     键
     * @param minScore 最小分数（包含）
     * @param maxScore 最大分数（包含）
     * @return 元素和分数的映射
     */
    public Map<Object, Double> zRangeByScoreWithScores(String key, double minScore, double maxScore) {
        Set<ZSetOperations.TypedTuple<Object>> tuples = redisTemplate.opsForZSet().rangeByScoreWithScores(key, minScore, maxScore);
        Map<Object, Double> result = new HashMap<>();
        for (ZSetOperations.TypedTuple<Object> tuple : tuples) {
            result.put(tuple.getValue(), tuple.getScore());
        }
        return result;
    }

    /**
     * 获取Sorted Set中元素的排名（从小到大排序）
     *
     * @param key   键
     * @param value 值
     * @return 元素的排名，如果不存在返回null
     */
    public Long zRank(String key, Object value) {
        return redisTemplate.opsForZSet().rank(key, value);
    }

    /**
     * 获取Sorted Set中元素的排名（从大到小排序）
     *
     * @param key   键
     * @param value 值
     * @return 元素的排名，如果不存在返回null
     */
    public Long zRevRank(String key, Object value) {
        return redisTemplate.opsForZSet().reverseRank(key, value);
    }

    /**
     * 增加Sorted Set中元素的分数
     *
     * @param key   键
     * @param value 值
     * @param delta 增加的分数
     * @return 增加后的分数
     */
    public Double zIncrScore(String key, Object value, double delta) {
        return redisTemplate.opsForZSet().incrementScore(key, value, delta);
    }

    /**
     * 从Sorted Set中移除元素
     *
     * @param key   键
     * @param values 值
     * @return 是否移除成功
     */
    public Long zRemove(String key, Object... values) {
        return redisTemplate.opsForZSet().remove(key, values);
    }

    /**
     * 从Sorted Set中移除指定排名范围的元素（从小到大排序）
     *
     * @param key   键
     * @param start 起始排名（包含）
     * @param end   结束排名（包含）
     * @return 移除的元素数量
     */
    public Long zRemoveRange(String key, long start, long end) {
        return redisTemplate.opsForZSet().removeRange(key, start, end);
    }

    /**
     * 从Sorted Set中移除指定分数范围的元素（从小到大排序）
     *
     * @param key     键
     * @param minScore 最小分数（包含）
     * @param maxScore 最大分数（包含）
     * @return 移除的元素数量
     */
    public Long zRemoveRangeByScore(String key, double minScore, double maxScore) {
        return redisTemplate.opsForZSet().removeRangeByScore(key, minScore, maxScore);
    }

    /**
     * 获取Sorted Set的大小
     *
     * @param key 键
     * @return Sorted Set的大小
     */
    public Long zSize(String key) {
        return redisTemplate.opsForZSet().size(key);
    }
}

