package cn.karuhara.etalk.framework.kafka.core;

import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
/**
 * @Description: 消息事件对象
 * @author: 轻语
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Event {
    //主题
    private String topic;
    //发送者
    private Integer userId;
    //接受者
    private Integer toUserId;
    //消息内容
    private String content;
    // 封装其他数据，用于扩展
    private Map<String,Object> data = new HashMap<>(16);

    public void setDataMap(String key, Object value) {
        this.data.put(key, value);
    }

}
