package cn.karuhara.etalk.framework.kafka.core;

import com.alibaba.fastjson2.JSONObject;
import jakarta.annotation.Resource;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class EventProducer {

    @Resource
    private KafkaTemplate kafkaTemplate;

    // 处理事件
    public void fireEvent(Event event){
        // 将事件发布到指定的主题(评论、点赞、关注)
        kafkaTemplate.send(event.getTopic(), JSONObject.toJSONString(event));
    }
}

