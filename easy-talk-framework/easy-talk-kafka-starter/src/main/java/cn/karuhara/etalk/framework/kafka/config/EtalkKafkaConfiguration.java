package cn.karuhara.etalk.framework.kafka.config;

import cn.karuhara.etalk.framework.kafka.core.EventProducer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description: Kafka 自动装配类
 * @author: 轻语
 */
@Configuration
public class EtalkKafkaConfiguration {

    @Bean
    public EventProducer eventProducer() {
        return new EventProducer();
    }
}
