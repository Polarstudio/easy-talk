package cn.karuhara.etalk.framework.kafka.core;

/**
 * @Description: 消息常量
 * @author: 轻语
*/
public class MessageConstants {
    /**
     * 内容违规
     */
    public static final String CONTENT_VIOLATIONS = "所发布内容含违规信息";
    /**
     * 审核结果
     */
    public static final String DETECTION_RESULT = "detection_result";
    /**
     * 删除圈子消息
     */
    public static final String CIRCLE_DELETE_TOPIC = "circle-delete-topic";
    /**
     * 删除话题信息
     */
    public static final String TOPIC_DELETE_TOPIC = "topic-delete-topic";
    /**
     * 删除动态消息
     */
    public static final String DYNAMIC_DELETE_TOPIC = "dynamic-delete-topic";
    /**
     * 审核动态消息
     */
    public static final String DYNAMIC_DETECTION_TOPIC = "dynamic-detection-topic";
    /**
     * 媒体审核通知
     */
    public static final String DETECTION_MEDIA_TOPIC = "detection-media-topic";
    /**
     * 删除活动
     */
    public static final String ACTIVITY_DELETE_TOPIC = "activity-delete-topic";
    /**
     * 删除评论
     */
    public static final String COMMENT_DELETE_TOPIC = "comment-delete-topic";
    /**
     * 审核认证
     */
    public static final String COMMENT_DETECTION_TOPIC = "comment-detection-topic";
    /**
     * 删除认证
     */
    public static final String CERTIFICATE_DELETE_TOPIC = "certificate-delete-topic";
    /**
     * 认证处理消息
     */
    public static final String CERTIFICATE_HANDLE_TOPIC = "certificate-handle-topic";
    /**
     * POST_ID
     */
    public static final String POST_ID = "post-id";
    /**
     * 动态id
     */
    public static final String DYNAMIC_ID = "dynamic-id";

    public static final String FOLLOW_IDS = "follow-ids";
    /**
     * 跳转地址
     */
    public static final String CONTENT_URL = "content-url";
    /**
     * 圈子名称
     */
    public static final String CIRCLE_NAME = "circle-name";
    /**
     * 话题名称
     */
    public static final String TOPIC_NAME = "topic-name";
    /**
     * 动态内容
     */
    public static final String DYNAMIC_CONTENT = "dynamic-content";
    /**
     * 评论内容
     */
    public static final String COMMENT_CONTENT = "comment-content";
    /**
     * 活动标题
     */
    public static final String ACTIVITY_NAME = "activity-name";
    /**
     * 认证名称
     */
    public static final String CERTIFICATE_NAME = "certificate-name";
    /**
     * 认证跳转地址
     */
    public static final String CERTIFICATE_URL = "/pagesUser/certificate/certificate";
    /**
     * 动态跳转地址
     */
    public static final String DYNAMIC_URL = "/pagesDynamic/detail/detail";

    /**
     * 审核的traceId
     */
    public static final String TRACE_ID = "trace-id";

    /**
     * 审核的建议
     */
    public static final String DETECTION_SUGGEST = "detection-suggest";
}
