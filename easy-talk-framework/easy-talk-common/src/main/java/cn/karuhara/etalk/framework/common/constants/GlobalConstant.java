package cn.karuhara.etalk.framework.common.constants;

public class GlobalConstant {

    public static String ACCOUNT_ALREADY_EXISTS = "账号已存在，或该邮箱已绑定账号";

    public static String MAIL_ALREADY_EXISTS = "该邮箱已存在";

    public static String USERNAME_ALREADY_EXISTS = "该账号已存在";

    public static String USERNAME_OR_PASSWORD_ERROR = "账号或密码错误";

    public static String LOGIN_USER = "login_user";

    public static Integer OFFICIAL_USER_ID = 1;

    public static String PHONE_ALREADY_EXISTS = "该手机号已存在";

    public static String CIRCLE_NAME_EXISTS =   "已存在同名的圈子！";

    public static String TOPIC_NAME_EXISTS = "已存在同名的话题！";

    public static String ACTIVITY_NAME_EXISTS = "已存在同名的活动!";
}
