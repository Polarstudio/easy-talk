package cn.karuhara.etalk.framework.common.constants;

public class RedisConstant {
    public static String REDIS_PREFIX = "easy-talk";

    public static String ROLES_PREFIX = "auth:roles";

    public static String PERMISSIONS_PREFIX = "auth:permissions";

    public static String USER_MENU_PREFIX = "auth:usermenu";

    public static String WX_ACCESS_TOKEN = "access_token";

    public static String DETECTION_MEDIA = "detection_media";

    public static String TRACE_ID = "trace_id";

    public static String POST_ID = "post_id";

    public static String TOPIC_HOT = "topic_hot";

    public static String SYSTEM_CONFIG = "system_config";
}
