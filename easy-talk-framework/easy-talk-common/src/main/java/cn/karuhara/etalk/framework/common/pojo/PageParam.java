package cn.karuhara.etalk.framework.common.pojo;

import lombok.Data;
import java.io.Serializable;
/**
 * @Description: 分页参数
 * @author: 轻语
*/
@Data
public class PageParam implements Serializable {

    //默认的分页参数
    private static final Integer PAGE_NO = 1;
    private static final Integer PAGE_SIZE = 10;

    //当前页
    private Integer pageNo = PAGE_NO;

    //每页数量
    private Integer pageSize = PAGE_SIZE;
    
    //如果传入的pageNo为空或小于1，则赋值默认值
    public Integer getPageNo(){
        if (pageNo == null || pageNo<1){
            pageNo = PAGE_NO;
        }
        return pageNo;
    }

    //如果传入的pageSize为空或小于1，则赋值默认值
    public Integer getPageSize(){
        if (pageSize == 0 || pageSize<1){
            pageSize = PAGE_SIZE;
        }
        return pageSize;
    }

}
