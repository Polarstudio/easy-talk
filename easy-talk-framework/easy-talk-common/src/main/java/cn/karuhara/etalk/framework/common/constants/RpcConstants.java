package cn.karuhara.etalk.framework.common.constants;

/**
 * RPC 相关的枚举
 */
public class RpcConstants {

    /**
     * RPC API 的前缀
     */
    public static final String RPC_API_PREFIX = "/rpc-api";

}
