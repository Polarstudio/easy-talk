package cn.karuhara.etalk.framework.common.enums;

import lombok.Getter;

@Getter
public enum GlobalCodeEnum {
	// 自定义结果编码和结果信息。
    SUCCESS(200,"操作成功"),
    FAIL(201,"操作失败，请稍后重试"),
    BAD_REQUEST(400, "请求参数不正确"),
    UNAUTHORIZED(401,"账号未登录"),
    FORBIDDEN(403,"没有该操作的权限"),
    NOT_FOUND(404,"请求未找到"),
    LOGIN_AUTH(405, "未登陆,请先登录"),
    SERVER_ERROR(500, "系统异常"),
    NOT_IMPLEMENTED(501,"功能未实现/未开启")
    // 自定义...
    ;
	
	// 结果编码
    private final Integer code;
	
	// 结果信息
    private final String message;

    GlobalCodeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
