package cn.karuhara.etalk.framework.common.pojo;

import cn.karuhara.etalk.framework.common.enums.GlobalCodeEnum;
import lombok.Data;

/**
 * @Description: 封装统一返回结果
 * @author: 轻语
*/
@Data
public class CommonResult<T> {

    // 状态码
    private Integer code;

    // 信息
    private String message;

    // 数据
    private T data;

    public CommonResult() {
    }

    // 通用的构造器
    public static <T> CommonResult<T> build(Integer code, String message, T resultData) {

        CommonResult<T> result = new CommonResult<>();
        if (resultData != null){
            result.setData(resultData);
        }
        result.setCode(code);
        result.setMessage(message);
        return result;
    }

    // 带有数据的成功方法
    public static CommonResult<Void> success(){
        CommonResult<Void> result = new CommonResult<>();
        result.setCode(GlobalCodeEnum.SUCCESS.getCode());
        result.setMessage(GlobalCodeEnum.SUCCESS.getMessage());
        return result;
    }

    // 无参默认成功的方法
    public static<T> CommonResult<T> success(T resultData){
        return build(GlobalCodeEnum.SUCCESS.getCode(), GlobalCodeEnum.SUCCESS.getMessage(), resultData);
    }

    // 无参默认失败的方法
    public static CommonResult<Void> error(){
        CommonResult<Void> result = new CommonResult<>();
        result.setCode(GlobalCodeEnum.FAIL.getCode());
        result.setMessage(GlobalCodeEnum.FAIL.getMessage());
        return result;
    }

    // 带有数据的失败方法
    public static<T> CommonResult<T> error(T resultData) {
        return build(GlobalCodeEnum.FAIL.getCode(), GlobalCodeEnum.FAIL.getMessage(), resultData);
    }

    public static<T> CommonResult<T> error(String message,T resultData) {
        return build(GlobalCodeEnum.FAIL.getCode(), message, resultData);
    }

    public static CommonResult<Void> error(Integer code , String message){
        return build(code,message,null);
    }
}
