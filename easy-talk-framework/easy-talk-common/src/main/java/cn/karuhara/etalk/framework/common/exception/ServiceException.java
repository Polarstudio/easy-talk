package cn.karuhara.etalk.framework.common.exception;

import cn.karuhara.etalk.framework.common.enums.GlobalCodeEnum;
import lombok.Getter;
/**
 * @Description: 业务异常 Exception
 * @author: 轻语
*/
@Getter
public class ServiceException extends RuntimeException{

    private Integer code;

    private String message;

    public ServiceException(){
        super();
    }

    public ServiceException(GlobalCodeEnum codeEnum) {
        this.code = codeEnum.getCode();
        this.message = codeEnum.getMessage();
    }
    
    public ServiceException(Integer code , String message){
        this.code = code;
        this.message = message;
    }

    public ServiceException(GlobalCodeEnum codeEnum, String message){
        this.code = codeEnum.getCode();
        this.message = message;
    }

}
