package cn.karuhara.etalk.framework.common.constants;

public class KafkaStreamConstant {

    public static final String HOT_TOPIC_SCORE_TOPIC="hot.topic.score.topic";

    public static final String HOT_TOPIC_INCR_HANDLE_TOPIC="hot.topic.incr.handle.topic";

    public static final Integer HOT_TOPIC_VIEWS_WEIGHT= 1;

    public static final Integer HOT_TOPIC_FOLLOW_WEIGHT = 3;

    public static final Integer HOT_TOPIC_DYNAMIC_WEIGHT= 5;
}
