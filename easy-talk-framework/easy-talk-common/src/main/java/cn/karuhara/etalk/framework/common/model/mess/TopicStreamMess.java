package cn.karuhara.etalk.framework.common.model.mess;

import lombok.Data;

@Data
public class TopicStreamMess {
    /**
     * 话题id
     */
    private Long topicId;
    /**
     * 浏览
     */
    private int view;
    /**
     * 发布相关的动态
     */
    private int dynamic;
    /**
     * 关注
     */
    private int follow;
}
