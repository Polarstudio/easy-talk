package cn.karuhara.etalk.framework.common.context;

/**
 * @Description: 用于保存请求上下文信息
 * @author: 轻语
*/
public class LoginContextHolder {

    /**
     * 存储用户名
     */
    private static final ThreadLocal<Integer> threadLocal = new ThreadLocal<>();

    public static void set(Integer login_user) {
        threadLocal.set(login_user);
    }

    public static Integer get() {
        return threadLocal.get();
    }

    public static void remove() {
        threadLocal.remove();
    }
    
}
