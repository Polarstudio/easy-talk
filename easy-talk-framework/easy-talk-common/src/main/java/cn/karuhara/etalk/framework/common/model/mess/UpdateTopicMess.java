package cn.karuhara.etalk.framework.common.model.mess;

import lombok.Data;

@Data
public class UpdateTopicMess {
    /**
     * 修改文章的字段类型
     */
    private UpdateArticleType type;
    /**
     * 文章ID
     */
    private Integer topicId;
    /**
     * 修改数据的增量，可为正负
     */
    private Integer add;

    public enum UpdateArticleType{
        DYNAMIC,FOLLOW,VIEWS;
    }
}
