package cn.karuhara.etalk.framework.mybatis.config;

import cn.karuhara.etalk.framework.mybatis.core.handler.MyMetaObjectHandler;
import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description: Mybatis-plus 自动装配类
 * @author: 轻语
*/
@Configuration
public class EtalkMybatisPlusAutoConfiguration {

    /**
     * 自动填充类
     * @return 自定义填充类
     */
    @Bean
    public MetaObjectHandler metaObject(){
        return new MyMetaObjectHandler();
    }

    /**
     * 添加分页插件
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
        return interceptor;
    }

}
