package cn.karuhara.etalk.framework.annotation;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Documented
public @interface WeLog {
    /**
     * 日志描述信息
     * @return
     */
    String description() default "";
}
