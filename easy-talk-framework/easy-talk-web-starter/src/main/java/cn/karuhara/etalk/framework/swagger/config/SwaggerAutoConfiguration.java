package cn.karuhara.etalk.framework.swagger.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

/**
 * @Description: Swagger 自动配置类
 * @author: 轻语
*/
@AutoConfiguration
@EnableConfigurationProperties(SwaggerProperties.class)
@ConditionalOnProperty(prefix = "springdoc.api-docs" , name = "enable" , havingValue = "true" , matchIfMissing = true) //false时禁用swagger
public class SwaggerAutoConfiguration {

    @Bean
    public OpenAPI openAPI(SwaggerProperties properties){
        return new OpenAPI()
                .info(buildInfo(properties));
    }

    /**
     * API 摘要信息
     */
    private Info buildInfo(SwaggerProperties properties) {
        return new Info()
                .title(properties.getTitle())
                .description(properties.getDescription())
                .version(properties.getVersion())
                .contact(new Contact().name(properties.getAuthor()).email(properties.getEmail()));
    }

}
