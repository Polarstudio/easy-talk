package cn.karuhara.etalk.framework.web.exception;

import cn.karuhara.etalk.framework.common.enums.GlobalCodeEnum;
import cn.karuhara.etalk.framework.common.exception.ServiceException;
import cn.karuhara.etalk.framework.common.pojo.CommonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    /**
     * 处理 SpringMVC 参数校验不正确
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public CommonResult<?> methodArgumentNotValidExceptionExceptionHandler(MethodArgumentNotValidException ex) {
        log.warn("[methodArgumentNotValidExceptionExceptionHandler] {}", ex.getMessage());
        FieldError fieldError = ex.getBindingResult().getFieldError();
        assert fieldError != null; // 断言，避免告警
        return CommonResult.error(GlobalCodeEnum.BAD_REQUEST.getCode(), GlobalCodeEnum.BAD_REQUEST.getMessage());
    }

    /**
     * 处理业务上的异常
     */
    @ExceptionHandler(ServiceException.class)
    public CommonResult<?> serviceExceptionHandler(ServiceException ex){
        log.warn("[ServiceExceptionHandler] {}",ex.getMessage());
        return CommonResult.error(ex.getCode(),ex.getMessage());
    }

//    /**
//     * 兜底处理一切异常
//     */
//    @ExceptionHandler(Exception.class)
//    public CommonResult<?> exceptionHandler(Exception ex){
//        log.warn("[ExceptionHandler] {}",ex.getMessage());
//        return CommonResult.error(GlobalCodeEnum.SERVER_ERROR);
//    }

}
