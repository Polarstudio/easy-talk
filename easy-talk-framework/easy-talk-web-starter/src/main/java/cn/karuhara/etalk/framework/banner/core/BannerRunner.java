package cn.karuhara.etalk.framework.banner.core;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;

/**
 * @Description: 项目启动成功后输出一些自定义的内容
 * @author: 轻语
*/
@Slf4j
public class BannerRunner implements ApplicationRunner {
    @Value("${spring.application.name}")
    private String serverName;
    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("{} 服务启动成功！",serverName);
    }
}
