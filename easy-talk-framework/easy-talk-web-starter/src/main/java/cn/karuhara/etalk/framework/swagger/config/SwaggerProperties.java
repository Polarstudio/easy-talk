package cn.karuhara.etalk.framework.swagger.config;

import jakarta.validation.constraints.NotEmpty;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @Description: Swagger 属性配置
 * @author: 轻语
*/
@ConfigurationProperties("etalk.swagger")
@Data
public class SwaggerProperties {

    /**
     * 标题
     */
    @NotEmpty(message = "Swagger 标题不能为空，请检查配置文件")
    private String title;

    /**
     * 描述
     */
    @NotEmpty(message = "Swagger 描述不能为空，请检查配置文件")
    private String description;

    /**
     * 作者
     */
    @NotEmpty(message = "作者不能为空")
    private String author;

    /**
     * 版本
     */
    @NotEmpty(message = "版本不能为空")
    private String version;

    /**
     * 邮箱
     */
    @NotEmpty(message = "邮箱不能为空")
    private String email;
}
