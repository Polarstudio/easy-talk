package cn.karuhara.etalk.framework.banner.config;

import cn.karuhara.etalk.framework.banner.core.BannerRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description: Banner自动配置的类
 * @author: 轻语
*/
@Configuration
public class BannerAutoConfiguration {
    @Bean
    public BannerRunner bannerRunner(){
        return new BannerRunner();
    }
}
