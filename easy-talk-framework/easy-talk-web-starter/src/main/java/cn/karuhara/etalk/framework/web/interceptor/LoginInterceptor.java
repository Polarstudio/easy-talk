package cn.karuhara.etalk.framework.web.interceptor;

import cn.karuhara.etalk.framework.common.constants.GlobalConstant;
import cn.karuhara.etalk.framework.common.context.LoginContextHolder;
import io.micrometer.common.util.StringUtils;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

/**
 * @Description: 拦截器，用于拦截请求头中的userId
 * @author: 轻语
*/
@Slf4j
@Component
public class LoginInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String login_user = request.getHeader(GlobalConstant.LOGIN_USER);
        if (StringUtils.isNotBlank(login_user)){
            LoginContextHolder.set(Integer.parseInt(login_user));
        }
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        LoginContextHolder.remove();
    }
}
