package cn.karuhara.etalk.framework.aspect.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @Description: 需要扫描自定义的AOP，加载到容器中，否则将不生效
 * @author: 轻语
*/
@Configuration
@ComponentScan("cn.karuhara.etalk.framework.aspect")
public class AspectAutoConfiguration {
}
