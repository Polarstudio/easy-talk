package cn.karuhara.etalk.framework.oss.config.minio;

import cn.karuhara.etalk.framework.oss.adapter.MinioStorageAdapter;
import cn.karuhara.etalk.framework.oss.adapter.StorageAdapter;
import cn.karuhara.etalk.framework.oss.util.MinioUtil;
import io.minio.MinioClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

/**
 * @Description: Minio配置类
 * @author: 轻语
*/
@AutoConfiguration
@EnableConfigurationProperties(MinioOSSProperties.class)
@ConditionalOnProperty(prefix = "oss", name = "type",havingValue = "minio", matchIfMissing = true)
@Slf4j
public class MinioOSSConfiguration {

    @Bean
    public MinioClient minioClient(MinioOSSProperties ossProperties) {
        log.info("Minio存储创建完成！");
        return MinioClient.builder()
                .endpoint(ossProperties.getEndpoint())
                .credentials(ossProperties.getAccessKey(), ossProperties.getSecretKey())
                .build();
    }
    @Bean
    public MinioUtil minioUtil(MinioClient minioClient) {
        MinioUtil minioUtil = new MinioUtil();
        minioUtil.setMinioClient(minioClient);
        return minioUtil;
    }

    @Bean
    public StorageAdapter storageAdapter(MinioUtil minioUtil, MinioOSSProperties ossProperties) {
        MinioStorageAdapter minioStorageAdapter = new MinioStorageAdapter();
        minioStorageAdapter.setMinioUtil(minioUtil);
        minioStorageAdapter.setProperties(ossProperties);
        return minioStorageAdapter;
    }

}
