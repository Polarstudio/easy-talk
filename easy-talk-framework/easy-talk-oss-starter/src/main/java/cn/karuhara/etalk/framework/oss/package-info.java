/**
 * @Description: 提供系统的分布式存储服务，目前提供了阿里云和minio，采用工厂+策略模式无缝切换存储服务
 * @author: 轻语
*/
package cn.karuhara.etalk.framework.oss;