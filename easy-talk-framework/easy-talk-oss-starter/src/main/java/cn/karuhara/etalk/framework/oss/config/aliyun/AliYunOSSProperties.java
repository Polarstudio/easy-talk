package cn.karuhara.etalk.framework.oss.config.aliyun;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "oss.aliyun")
public class AliYunOSSProperties {

    private String accessKey;

    private String accessSecret;

    private String endpoint;
}
