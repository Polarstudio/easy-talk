package cn.karuhara.etalk.framework.oss.config.minio;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "oss.minio")
public class MinioOSSProperties {
    private String endpoint;
    private String accessKey;
    private String secretKey;
}

