package cn.karuhara.etalk.framework.oss.config.aliyun;

import cn.karuhara.etalk.framework.oss.adapter.AliStorageAdapter;
import cn.karuhara.etalk.framework.oss.adapter.StorageAdapter;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import jakarta.annotation.Resource;
import lombok.SneakyThrows;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description: 阿里云oss配置类
 * @author: 轻语
*/
@Configuration
@EnableConfigurationProperties(AliYunOSSProperties.class)
@ConditionalOnProperty(prefix = "oss", name = "type",havingValue = "aliyun", matchIfMissing = true)
public class AliYunOSSConfiguration {

    @Resource
    private AliYunOSSProperties ossProperties;

    @Bean
    @SneakyThrows
    public OSS ossClient() {
        return new OSSClientBuilder().build(ossProperties.getEndpoint(),ossProperties.getAccessKey(),ossProperties.getAccessSecret());
    }

    @Bean
    public StorageAdapter storageAdapter() {
        return new AliStorageAdapter();
    }

}
