package cn.karuhara.etalk.member.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(description = "话题创建者信息")
public class MemberBaseInfoDTO {

    @Schema(description = "用户的id")
    private Integer id;

    @Schema(description = "用户昵称")
    private String nikeName;

    @Schema(description = "IP属地")
    private String dependency;

    @Schema(description = "用户的头像")
    private String avatar;

    @Schema(description = "用户的手机号")
    private String phone;

    @Schema(description = "用户类型")
    private String type;

    @Schema(description = "用户认证名称")
    private String certificateName;

    @Schema(description = "OpenId")
    private String wxOpenid;

}
