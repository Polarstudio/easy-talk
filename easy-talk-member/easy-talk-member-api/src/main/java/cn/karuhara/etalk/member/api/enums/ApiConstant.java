package cn.karuhara.etalk.member.api.enums;

import cn.karuhara.etalk.framework.common.constants.RpcConstants;

/**
 * API 相关常量
 */
public class ApiConstant {

    public static final String NAME = "easy-talk-member";

    public static final String PREFIX = RpcConstants.RPC_API_PREFIX +  "/member";
}
