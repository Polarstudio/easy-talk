package cn.karuhara.etalk.member.api.dto;

import lombok.Data;

@Data
public class MemberDynamicInfoDTO {

    /**
     * 昵称
     */
    private String nikeName;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 省
     */
    private String province;

    /**
     * 认证名
     */
    private String certificateName;

}
