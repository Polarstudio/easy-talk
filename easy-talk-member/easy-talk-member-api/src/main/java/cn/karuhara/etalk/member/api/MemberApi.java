package cn.karuhara.etalk.member.api;

import cn.karuhara.etalk.member.api.dto.MemberBaseInfoDTO;
import cn.karuhara.etalk.member.api.dto.MemberDynamicInfoDTO;
import cn.karuhara.etalk.member.api.enums.ApiConstant;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Tag(name = "RPC 服务 - 会员")
@FeignClient(name = ApiConstant.NAME , url = "http://localhost:39091")
@Component
public interface MemberApi {

    String PREFIX = ApiConstant.PREFIX;

    @GetMapping(PREFIX + "/getMemberById")
    @Operation(summary = "根据用户ID获取用户信息")
    MemberDynamicInfoDTO getMemberById(@RequestParam("userId") Integer userId);

    @GetMapping(PREFIX + "/getMemberInfo")
    @Operation(summary = "查找用户的基本信息")
    MemberBaseInfoDTO getMemberBaseInfo(@RequestParam("userId") Integer userId);

    @GetMapping(PREFIX + "/getMemberByPhone")
    @Operation(summary = "根据手机号获取用户")
    MemberBaseInfoDTO getMemberByPhone(@RequestParam("phone") String phone);

    @PostMapping(PREFIX + "/getByIds")
    @Operation(summary = "根据用户的id批量获取用户信息")
    List<MemberBaseInfoDTO> getByIds(@RequestBody List<Integer> ids);

    @GetMapping(PREFIX + "/getFollowIds")
    @Operation(summary = "获取所有关注的用户id")
    List<Integer> getFollowIds(@RequestParam("userId") Integer userId);
}
