package cn.karuhara.etalk.member.convert;

import cn.karuhara.etalk.member.api.dto.MemberBaseInfoDTO;
import cn.karuhara.etalk.member.api.dto.MemberDynamicInfoDTO;
import cn.karuhara.etalk.member.controller.admin.user.vo.UserAddReqVO;
import cn.karuhara.etalk.member.controller.admin.user.vo.UserPageRespVO;
import cn.karuhara.etalk.member.controller.app.user.vo.UserBaseVO;
import cn.karuhara.etalk.member.controller.app.user.vo.UserFollowDetailVO;
import cn.karuhara.etalk.member.controller.app.user.vo.UserLoginRespVO;
import cn.karuhara.etalk.member.entity.EasyUser;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface MemberUserConvert {

    MemberUserConvert INSTANCE = Mappers.getMapper(MemberUserConvert.class);

    EasyUser addVOToEasyUser(UserAddReqVO addReqVO);

    List<UserPageRespVO> userListToPageRespVOList(List<EasyUser> userList);

    UserLoginRespVO userToLoginRespVO(EasyUser easyUser);

    MemberDynamicInfoDTO userToMemberDynamicInfoDTO(EasyUser easyUser);

    MemberBaseInfoDTO userToMemberTopicInfoDTO(EasyUser easyUser);

    EasyUser baseVOToEasyUser(UserBaseVO baseVO);

    UserBaseVO userToUserBaseVO(EasyUser user);

    List<MemberBaseInfoDTO> usersToApiBaseDTOs(List<EasyUser> userList);

    List<UserFollowDetailVO> usersToFollowDetailVOList(List<EasyUser> userList);
}
