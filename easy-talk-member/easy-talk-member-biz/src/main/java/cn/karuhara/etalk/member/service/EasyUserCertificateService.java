package cn.karuhara.etalk.member.service;

import cn.karuhara.etalk.framework.common.pojo.PageResult;
import cn.karuhara.etalk.member.controller.admin.certificate.vo.UserCertificateDeleteVO;
import cn.karuhara.etalk.member.controller.admin.certificate.vo.UserCertificateDetailVO;
import cn.karuhara.etalk.member.controller.admin.certificate.vo.UserCertificatePageReqVO;
import cn.karuhara.etalk.member.controller.admin.certificate.vo.UserCertificateUpdateVO;
import cn.karuhara.etalk.member.controller.app.certificate.vo.CertificateAppBaseVO;
import cn.karuhara.etalk.member.controller.app.certificate.vo.CertificateAppDetailVO;
import cn.karuhara.etalk.member.entity.EasyUserCertificate;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author atom
* @description 针对表【easy_user_certificate(用户认证表)】的数据库操作Service
* @createDate 2024-11-15 11:33:20
*/
public interface EasyUserCertificateService extends IService<EasyUserCertificate> {

    /**
     * 获取认证列表
     * @param pageReqVO 分页参数
     * @return 分页列表
     */
    PageResult<UserCertificateDetailVO> getList(UserCertificatePageReqVO pageReqVO);

    /**
     * 修改认证信息
     * @param updateVO 修改参数
     */
    void edit(UserCertificateUpdateVO updateVO);

    /**
     * 申请或修改认证
     * @param certificateAppBaseVO 认证参数
     */
    void certificate(CertificateAppBaseVO certificateAppBaseVO);

    /**
     * 查询认证详细信息
     * @param userId 用户id
     * @return 认证详情信息
     */
    CertificateAppDetailVO getCertificateDetail(Integer userId);

    /**
     * 删除认证
     * @param deleteVO 删除的信息
     */
    void delete(UserCertificateDeleteVO deleteVO);
}
