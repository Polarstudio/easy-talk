package cn.karuhara.etalk.member.controller.app.certificate.vo;

import cn.karuhara.etalk.member.enums.UserCertificateEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Schema(description = "认证的详细信息")
@Data
public class CertificateAppDetailVO extends CertificateAppBaseVO {
    /**
     * 认证状态（0通过 1待审核 2驳回）
     */
    private UserCertificateEnum status;

    /**
     * 驳回原因
     */
    private String rejection;
}
