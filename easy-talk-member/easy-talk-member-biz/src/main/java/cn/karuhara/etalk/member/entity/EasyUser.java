package cn.karuhara.etalk.member.entity;

import cn.karuhara.etalk.framework.mybatis.core.dataobject.BaseDO;
import cn.karuhara.etalk.member.enums.MemberGenderEnum;
import cn.karuhara.etalk.member.enums.MemberStatusEnum;
import cn.karuhara.etalk.member.enums.MemberTypeEnum;
import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户
 * @TableName easy_user
 */
@EqualsAndHashCode(callSuper = true)
@TableName(value ="easy_user")
@Data
public class EasyUser extends BaseDO implements Serializable {
    /**
     * 用户Id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 昵称
     */
    private String nikeName;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 简介
     */
    private String intro;

    /**
     * 性别
     */
    private MemberGenderEnum gender;

    /**
     * 年龄
     */
    private String age;

    /**
     * 手机号
     */
    private String phone;

    /**
     * IP
     */
    private String ip;

    /**
     * IP属地
     */
    private String dependency;

    /**
     * 微信openid
     */
    private String wxOpenid;

    /**
     * 类型：1 普通、2 认证、3 官方
     */
    private MemberTypeEnum type;

    /**
     * 认证名
     */
    private String certificateName;

    /**
     * 状态：0 正常、1审核中、2禁言、3拉黑、8 注销
     */
    private MemberStatusEnum status;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}