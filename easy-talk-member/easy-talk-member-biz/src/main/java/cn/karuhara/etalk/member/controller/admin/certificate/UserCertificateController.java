package cn.karuhara.etalk.member.controller.admin.certificate;

import cn.karuhara.etalk.framework.common.pojo.CommonResult;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import cn.karuhara.etalk.member.controller.admin.certificate.vo.UserCertificateDeleteVO;
import cn.karuhara.etalk.member.controller.admin.certificate.vo.UserCertificateDetailVO;
import cn.karuhara.etalk.member.controller.admin.certificate.vo.UserCertificatePageReqVO;
import cn.karuhara.etalk.member.controller.admin.certificate.vo.UserCertificateUpdateVO;
import cn.karuhara.etalk.member.service.EasyUserCertificateService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

@Tag(name = "管理 - 认证管理")
@RestController
@RequestMapping("/member/admin/user/certificate")
public class UserCertificateController {

    @Resource
    private EasyUserCertificateService userCertificateService;

    @Operation(summary = "获取认证列表")
    @PostMapping("/page")
    public CommonResult<PageResult<UserCertificateDetailVO>> getList(@RequestBody UserCertificatePageReqVO pageReqVO){
        PageResult<UserCertificateDetailVO> pageResult = userCertificateService.getList(pageReqVO);
        return CommonResult.success(pageResult);
    }

    @Operation(summary = "修改认证信息")
    @PutMapping
    private CommonResult<Void> edit(@RequestBody UserCertificateUpdateVO updateVO){
        userCertificateService.edit(updateVO);
        return CommonResult.success();
    }

    @Operation(summary = "删除认证")
    @DeleteMapping
    private CommonResult<Void> remove(@RequestBody UserCertificateDeleteVO deleteVO){
        userCertificateService.delete(deleteVO);
        return CommonResult.success();
    }
}
