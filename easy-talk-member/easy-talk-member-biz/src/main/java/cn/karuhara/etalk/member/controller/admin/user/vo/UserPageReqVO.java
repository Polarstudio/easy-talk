package cn.karuhara.etalk.member.controller.admin.user.vo;

import cn.karuhara.etalk.framework.common.pojo.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Schema(description = "管理后台 - 分页请求会员列表")
@Data
public class UserPageReqVO extends PageParam {
    @Schema(description = "用户昵称", example = "easy-talk")
    private String nikeName;

    @Schema(description = "手机号", example = "15333333333")
    private String phone;

    @Schema(description = "用户类型,(0普通用户、1认证用户、2官方)", example = "0")
    private String  type;

    @Schema(description = "用户状态（0正常、1审核中、2禁言、3拉黑、8注销）", example = "0")
    private String status;
}
