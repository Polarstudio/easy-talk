package cn.karuhara.etalk.member.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
/**
 * @Description: 用户性别枚举类
 * @author: 轻语
*/
@Getter
@AllArgsConstructor
public enum MemberGenderEnum {
    MALE("0","男"),
    FEMALE("1","女"),
    UNKNOWN("2","未知");

    @EnumValue
    @JsonValue
    private final String code;

    private final String desc;
}
