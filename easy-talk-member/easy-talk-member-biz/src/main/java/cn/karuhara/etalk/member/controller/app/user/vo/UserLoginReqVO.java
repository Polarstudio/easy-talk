package cn.karuhara.etalk.member.controller.app.user.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Schema(description = "微信登录凭证")
@Data
public class UserLoginReqVO {

    @Schema(description = "用户登录凭证")
    @NotNull(message = "用户登录凭证不能为空！")
    private String loginCode;

    @Schema(description = "用户手机号凭证")
    private String phoneCode;
}
