package cn.karuhara.etalk.member.service;
import cn.karuhara.etalk.member.controller.app.user.vo.UserFollowVO;
import cn.karuhara.etalk.member.entity.EasyUserFollow;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author atom
 * @description 针对表【easy_user_follow(用户关注)】的数据库操作Service
 * @createDate 2024-11-15 11:33:20
 */
public interface EasyUserFollowService extends IService<EasyUserFollow> {

    /**
     * 获取用户的关注数据
     * @return
     */
    UserFollowVO getFollowCount(Integer userId);

    /**
     * 移除粉丝或关注
     * @param userId 用户id
     * @param followedId 关注的用户id
     */
    void removeFollow(Integer userId, Integer followedId);

    /**
     * 检查是否关注用户
     * @param userId 用户id
     * @param followId 查询是否关注的用户id
     * @return 是否关注
     */
    Boolean checkFollow(Integer userId, Integer followId);

    /**
     * 关注或取消关注用户
     * @param userId 用户
     * @param followId 关注或取关用户的id
     */
    void follow(Integer userId, Integer followId);
}
