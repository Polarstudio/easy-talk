package cn.karuhara.etalk.member.entity;

import cn.karuhara.etalk.framework.mybatis.core.dataobject.BaseDO;
import cn.karuhara.etalk.member.enums.UserCertificateEnum;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户认证表
 * @TableName easy_user_certificate
 */
@EqualsAndHashCode(callSuper = true)
@TableName(value ="easy_user_certificate",autoResultMap = true)
@Data
public class EasyUserCertificate extends BaseDO implements Serializable {

    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 用户Id
     */
    private Integer userId;

    /**
     * 认证名称
     */
    private String certificateName;

    /**
     * 认证资料
     */
    @TableField(typeHandler = JacksonTypeHandler.class)
    private List<String> material;

    /**
     * 认证状态（0通过 1待审核 2驳回）
     */
    private UserCertificateEnum status;

    /**
     * 备注
     */
    private String remark;

    /**
     * 驳回原因
     */
    private String rejection;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}