package cn.karuhara.etalk.member.service.impl;

import cn.karuhara.etalk.framework.common.context.LoginContextHolder;
import cn.karuhara.etalk.member.controller.app.user.vo.UserFollowVO;
import cn.karuhara.etalk.member.service.EasyUserFollowService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.karuhara.etalk.member.entity.EasyUserFollow;
import cn.karuhara.etalk.member.mapper.EasyUserFollowMapper;
import org.springframework.stereotype.Service;

/**
* @author atom
* @description 针对表【easy_user_follow(用户关注)】的数据库操作Service实现
* @createDate 2024-11-15 11:33:20
*/
@Service
public class EasyUserFollowServiceImpl extends ServiceImpl<EasyUserFollowMapper, EasyUserFollow>
    implements EasyUserFollowService {

    @Override
    public UserFollowVO getFollowCount(Integer userId) {
        UserFollowVO userFollowVO = new UserFollowVO();
        //1. 获取用户的关注量
        LambdaQueryWrapper<EasyUserFollow> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyUserFollow::getUserId, userId);
        long count = count(queryWrapper);
        userFollowVO.setFollow(count);
        //2. 获取用户的粉丝数
        LambdaQueryWrapper<EasyUserFollow> queryWrapper1 = new LambdaQueryWrapper<>();
        queryWrapper1.eq(EasyUserFollow::getFollowId,userId);
        long count1 = count(queryWrapper1);
        userFollowVO.setFollowed(count1);
        return userFollowVO;
    }

    /**
     * 移除粉丝或关注
     *
     * @param userId     用户id
     * @param followId 关注的用户id
     */
    @Override
    public void removeFollow(Integer userId, Integer followId) {
        LambdaQueryWrapper<EasyUserFollow> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyUserFollow::getUserId, userId);
        queryWrapper.eq(EasyUserFollow::getFollowId,followId);
        remove(queryWrapper);
    }

    /**
     * 检查是否关注用户
     *
     * @param userId   用户id
     * @param followId 查询是否关注的用户id
     * @return 是否关注
     */
    @Override
    public Boolean checkFollow(Integer userId, Integer followId) {
        LambdaQueryWrapper<EasyUserFollow> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyUserFollow::getUserId, userId);
        queryWrapper.eq(EasyUserFollow::getFollowId,followId);
        EasyUserFollow userFollow = getOne(queryWrapper);
        return userFollow!=null;
    }

    /**
     * 关注或取消关注用户
     *
     * @param userId   用户
     * @param followId 关注或取关用户的id
     */
    @Override
    public void follow(Integer userId, Integer followId) {
        LambdaQueryWrapper<EasyUserFollow> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyUserFollow::getUserId, userId);
        queryWrapper.eq(EasyUserFollow::getFollowId,followId);
        EasyUserFollow userFollow = getOne(queryWrapper);
        if (userFollow!=null) {
            remove(queryWrapper);
        }else {
            userFollow = new EasyUserFollow();
            userFollow.setUserId(userId);
            userFollow.setFollowId(followId);
            save(userFollow);
        }
    }
}




