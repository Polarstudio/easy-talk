package cn.karuhara.etalk.member.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
/**
 * @Description: 用户类型枚举
 * @author: 轻语
*/
@Getter
@AllArgsConstructor
public enum MemberTypeEnum{

    NORMAL("0","普通"),
    CERTIFICATE("1","认证"),
    OFFICIAL("2","官方")
    ;

    //记一次坑：如果数据库的字段为枚举类型，那么这里必须是String类型，否则会报错
    @EnumValue
    @JsonValue
    private final String code;

    private final String desc;
}
