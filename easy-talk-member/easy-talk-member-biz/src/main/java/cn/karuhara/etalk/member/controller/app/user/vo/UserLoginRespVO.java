package cn.karuhara.etalk.member.controller.app.user.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Schema(description = "登录用户的详细信息")
@Data
public class UserLoginRespVO extends UserBaseVO{

    @Schema(description = "用户的token")
    private String token;

}
