package cn.karuhara.etalk.member.constants;

import org.apache.commons.lang3.RandomStringUtils;

public class MemberConstant {

    //微信登录的基缔地址
    public static final String WX_BASE_URL = "https://api.weixin.qq.com";

    //注册用户的默认头像
    public static final String DEFAULT_AVATAR = "https://www.helloimg.com/i/2024/11/25/674495cf092ec.jpg";

    //注册用户的默认年龄
    public static final String DEFAULT_AGE = "暂不展示";

    //默认简介
    public static final String DEFAULT_INTRO = "此人还没有简介哦~";

}
