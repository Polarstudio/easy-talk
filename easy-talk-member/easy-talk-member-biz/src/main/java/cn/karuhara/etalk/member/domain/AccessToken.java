package cn.karuhara.etalk.member.domain;

import com.alibaba.fastjson2.annotation.JSONField;
import lombok.Data;

/**
 * 微信接口调用凭证
 */
@Data
public class AccessToken {
    // 获取到的凭证
    @JSONField(name = "access_token")
    private String accessToken;
    // 凭证有效时间，单位：秒。目前是7200秒之内的值。
    @JSONField(name = "expires_in")
    private String expiresIn;
}
