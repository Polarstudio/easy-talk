package cn.karuhara.etalk.member.controller.app.user;

import cn.karuhara.etalk.framework.common.context.LoginContextHolder;
import cn.karuhara.etalk.framework.common.enums.GlobalCodeEnum;
import cn.karuhara.etalk.framework.common.exception.ServiceException;
import cn.karuhara.etalk.framework.common.pojo.CommonResult;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import cn.karuhara.etalk.member.controller.app.user.vo.*;
import cn.karuhara.etalk.member.convert.MemberUserConvert;
import cn.karuhara.etalk.member.entity.EasyUser;
import cn.karuhara.etalk.member.service.EasyUserFollowService;
import cn.karuhara.etalk.member.service.EasyUserService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@Tag(name = "APP - 用户")
@RequestMapping("/member/app")
@Validated
public class UserAppController {
    @Resource
    private EasyUserService service;

    @Resource
    private EasyUserFollowService followService;

    @Operation(summary = "微信登录")
    @PostMapping("/login/wx")
    public CommonResult<UserLoginRespVO> wx(@Valid @RequestBody UserLoginReqVO userLoginReqVO, HttpServletRequest request) throws IOException {
        UserLoginRespVO userLoginRespVO = service.wxLogin(userLoginReqVO);
        String ip = request.getRemoteAddr();
        String dependency = service.refreshIp(userLoginRespVO.getId(), ip);
        userLoginRespVO.setDependency(dependency);
        return CommonResult.success(userLoginRespVO);
    }

    @Operation(summary = "刷新用户IP地址")
    @GetMapping("/refreshIp")
    public CommonResult<String> refreshIp(HttpServletRequest request) {
        Integer userId = LoginContextHolder.get();
        if (userId == null) {
            throw new ServiceException(GlobalCodeEnum.LOGIN_AUTH);
        }
        String ip = request.getRemoteAddr();
        String province = service.refreshIp(userId, ip);
        return CommonResult.success(province);
    }

    @Operation(summary = "获取用户的关注数据")
    @GetMapping("/userFollow/{userId}")
    public CommonResult<UserFollowVO> follow(@PathVariable Integer userId) {
        UserFollowVO userFollowVO = followService.getFollowCount(userId);
        return CommonResult.success(userFollowVO);
    }

    @Operation(summary = "修改个人信息")
    @PutMapping
    public CommonResult<Void> updateUser(@RequestBody UserBaseVO baseVO){
        EasyUser user = MemberUserConvert.INSTANCE.baseVOToEasyUser(baseVO);
        //TODO 先直接保存，获取加入审核功能
        service.updateById(user);
        return CommonResult.success();
    }

    @Operation(summary = "获取用户详情")
    @GetMapping("/{userId}")
    public CommonResult<UserBaseVO> refreshUser(@PathVariable Integer userId,HttpServletRequest request) {
        LambdaQueryWrapper<EasyUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyUser::getId, userId);
        EasyUser one = service.getOne(queryWrapper);
        UserBaseVO userBaseVO = MemberUserConvert.INSTANCE.userToUserBaseVO(one);
        String ip = request.getRemoteAddr();
        String dependency = service.refreshIp(userId, ip);
        userBaseVO.setDependency(dependency);
        return CommonResult.success(userBaseVO);
    }

    @Operation(summary = "获取用户粉丝或用户的关注")
    @PostMapping("/follow/list")
    public CommonResult<PageResult<UserFollowDetailVO>> follow(@RequestBody UserFollowPageReqVO pageReqVO) {
        PageResult<UserFollowDetailVO> pageResult = service.getFollow(pageReqVO);
        return CommonResult.success(pageResult);
    }

    @Operation(summary = "移除粉丝或关注")
    @DeleteMapping("/follow/remove/{userId}/{followId}")
    public CommonResult<Void> removeFollow(@PathVariable Integer userId,@PathVariable Integer followId){
        followService.removeFollow(userId,followId);
        return CommonResult.success();
    }

    @Operation(summary = "检查是否关注用户")
    @GetMapping("/{userId}/{followId}")
    public CommonResult<Boolean> checkFollow(@PathVariable Integer userId,@PathVariable Integer followId){
        Boolean b = followService.checkFollow(userId,followId);
        return CommonResult.success(b);
    }

    @Operation(summary = "关注或取消关注用户")
    @PutMapping("/follow/{userId}/{followId}")
    public CommonResult<Void> follow(@PathVariable Integer userId,@PathVariable Integer followId){
        followService.follow(userId,followId);
        return CommonResult.success();
    }
}
