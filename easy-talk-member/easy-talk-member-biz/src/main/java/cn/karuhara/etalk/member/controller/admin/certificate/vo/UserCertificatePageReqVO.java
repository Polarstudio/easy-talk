package cn.karuhara.etalk.member.controller.admin.certificate.vo;

import cn.karuhara.etalk.framework.common.pojo.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Schema(description = "管理后台 - 分页请求认证列表")
@Data
public class UserCertificatePageReqVO extends PageParam {
    @Schema(description = "认证名称")
    private String certificateName;

    @Schema(description = "认证状态（0通过 1待审核 2驳回）")
    private String status;
}
