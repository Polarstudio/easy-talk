package cn.karuhara.etalk.member.mapper;

import cn.karuhara.etalk.member.entity.EasyUserCertificate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author atom
* @description 针对表【easy_user_certificate(用户认证表)】的数据库操作Mapper
* @createDate 2024-11-15 11:33:20
* @Entity cn.karuhara.etalk.member.domain.EasyUserCertificate
*/
public interface EasyUserCertificateMapper extends BaseMapper<EasyUserCertificate> {

}




