package cn.karuhara.etalk.member.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;
/**
 * @Description: 用户状态枚举
 * @author: 轻语
*/
@Getter
@AllArgsConstructor
public enum MemberStatusEnum {

    NORMAL("0","正常"),
    UNDER_VIEW("1","审核中"),
    FORBIDDEN("2","禁言"),
    BLOCKED("3","拉黑"),
    CANCELLED("8","注销");

    @EnumValue
    @JsonValue
    private final String code;

    private final String desc;
}
