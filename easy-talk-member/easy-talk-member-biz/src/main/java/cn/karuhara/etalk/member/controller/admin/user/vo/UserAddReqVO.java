package cn.karuhara.etalk.member.controller.admin.user.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Description: 添加用户信息VO
 * @author: 轻语
*/
@EqualsAndHashCode(callSuper = true)
@Schema(description = "管理后台 - 添加会员")
@Data
public class UserAddReqVO extends UserBaseVO {
}
