package cn.karuhara.etalk.member.controller.admin.user.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Schema(description = "管理后台 - 用户详细VO")
@Data
public class UserPageRespVO extends UserBaseVO {

    @Schema(description = "国家",example = "中国")
    private String country;

    @Schema(description = "省",example = "福建")
    private String province;

    @Schema(description = "市",example = "福州")
    private String city;

    @Schema(description = "区",example = "高新区")
    private String district;

    @Schema(description = "认证名",example = "校ACM队")
    private String certificateName;

    @Schema(description = "创建时间")
    @JsonFormat(pattern = "yyyy/MM/dd HH:mm", timezone = "GMT+8")
    private LocalDateTime createTime;
}
