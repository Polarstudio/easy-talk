package cn.karuhara.etalk.member.domain;
import com.alibaba.fastjson2.annotation.JSONField;
import lombok.Data;
// 对应整体的响应结构类
@Data
public class PhoneNumber {
    // 错误码，用于标识接口调用等操作返回的错误状态码，不同的数值对应不同的错误情况
    private int errcode;
    // 错误信息，用于详细描述出现错误的具体原因，方便开发者排查问题
    private String errmsg;
    // 用户手机号信息对象，包含了手机号相关的各种详细信息
    @JSONField(name = "phone_info")
    private PhoneInfo phoneInfo;

    // 对应手机号信息的内部类，用来封装和手机号相关的具体属性
    @Data
    public static class PhoneInfo {
        // 用户绑定的手机号（国外手机号会有区号），是展示给用户或者用于识别用户账号关联手机号的完整号码形式
        private String phoneNumber;
        // 没有区号的手机号，通常在某些特定业务逻辑下，可能需要单独使用纯数字形式的手机号进行处理
        private String purePhoneNumber;
        // 区号，用于区分手机号所属的不同国家或地区，方便进行号码归属地相关的判断等操作
        private String countryCode;
        // 数据水印相关对象，包含了一些用于标识数据来源、操作时间等关键信息的数据标记
        private Watermark watermark;

        // 对应数据水印的内部类，用来封装数据水印相关的具体属性
        @Data
        public static class Watermark {
            // 用户获取手机号操作的时间戳，可用于记录操作的具体时间，便于后续进行时间顺序相关的统计、验证等操作
            private long timestamp;
            // 小程序appid，用于唯一标识小程序，区分不同小程序来源的数据等情况
            private String appid;
        }
    }
}