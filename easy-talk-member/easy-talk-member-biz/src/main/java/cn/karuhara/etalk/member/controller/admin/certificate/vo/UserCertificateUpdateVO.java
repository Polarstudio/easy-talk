package cn.karuhara.etalk.member.controller.admin.certificate.vo;

import cn.karuhara.etalk.member.enums.UserCertificateEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "修改认证信息")
@Data
public class UserCertificateUpdateVO {
    @Schema(description = "id")
    private Integer id;

    @Schema(description = "用户id")
    private Integer userId;

    @Schema(description = "认证名称")
    private String certificateName;

    @Schema(description = "备注")
    private String remark;

    @Schema(description = "认证状态（0通过 1待审核 2驳回）")
    private UserCertificateEnum status;

    @Schema(description = "驳回原因")
    private String rejection;
}
