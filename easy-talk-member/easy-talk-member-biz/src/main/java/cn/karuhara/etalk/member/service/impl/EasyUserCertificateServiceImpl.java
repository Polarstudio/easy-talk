package cn.karuhara.etalk.member.service.impl;

import cn.karuhara.etalk.framework.common.pojo.PageResult;
import cn.karuhara.etalk.member.controller.admin.certificate.vo.UserCertificateDeleteVO;
import cn.karuhara.etalk.member.controller.admin.certificate.vo.UserCertificateDetailVO;
import cn.karuhara.etalk.member.controller.admin.certificate.vo.UserCertificatePageReqVO;
import cn.karuhara.etalk.member.controller.admin.certificate.vo.UserCertificateUpdateVO;
import cn.karuhara.etalk.member.controller.app.certificate.vo.CertificateAppBaseVO;
import cn.karuhara.etalk.member.controller.app.certificate.vo.CertificateAppDetailVO;
import cn.karuhara.etalk.member.convert.UserCertificateConvert;
import cn.karuhara.etalk.member.entity.EasyUser;
import cn.karuhara.etalk.member.enums.MemberTypeEnum;
import cn.karuhara.etalk.member.enums.UserCertificateEnum;
import cn.karuhara.etalk.member.mq.product.CertificateProducer;
import cn.karuhara.etalk.member.service.EasyUserService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.karuhara.etalk.member.entity.EasyUserCertificate;
import cn.karuhara.etalk.member.service.EasyUserCertificateService;
import cn.karuhara.etalk.member.mapper.EasyUserCertificateMapper;
import jakarta.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author atom
* @description 针对表【easy_user_certificate(用户认证表)】的数据库操作Service实现
* @createDate 2024-11-15 11:33:20
*/
@Service
public class EasyUserCertificateServiceImpl extends ServiceImpl<EasyUserCertificateMapper, EasyUserCertificate>
    implements EasyUserCertificateService{

    @Resource
    private EasyUserCertificateMapper certificateMapper;

    @Resource
    private EasyUserService userService;

    @Resource
    private CertificateProducer certificateProducer;
    /**
     * 获取认证列表
     *
     * @param pageReqVO 分页参数
     * @return 分页列表
     */
    @Override
    public PageResult<UserCertificateDetailVO> getList(UserCertificatePageReqVO pageReqVO) {
        LambdaQueryWrapper<EasyUserCertificate> wrapper = new LambdaQueryWrapper<>();
        if(StringUtils.isNotBlank(pageReqVO.getCertificateName())){
            wrapper.like(EasyUserCertificate::getCertificateName, pageReqVO.getCertificateName());
        }
        if(StringUtils.isNotBlank(pageReqVO.getStatus())){
            wrapper.eq(EasyUserCertificate::getStatus, pageReqVO.getStatus());
        }
        Page<EasyUserCertificate> page = new Page<>(pageReqVO.getPageNo(), pageReqVO.getPageSize());
        Page<EasyUserCertificate> certificatePage = certificateMapper.selectPage(page, wrapper);
        if (certificatePage.getTotal() == 0){
            return PageResult.empty();
        }
        List<EasyUserCertificate> certificates = certificatePage.getRecords();
        List<UserCertificateDetailVO> userCertificateDetailVOS = UserCertificateConvert.INSTANT.certificatesToDetails(certificates);
        userCertificateDetailVOS.forEach(this::fillData);
        return new PageResult<>(userCertificateDetailVOS, certificatePage.getPages());
    }

    /**
     * 修改认证信息
     *
     * @param updateVO 修改参数
     */
    @Override
    public void edit(UserCertificateUpdateVO updateVO) {
        EasyUserCertificate certificate = UserCertificateConvert.INSTANT.updateVOToCertificate(updateVO);
        updateById(certificate);
        //如果通过了认证请求，则更新用户的认证状态
        if (certificate.getStatus() == UserCertificateEnum.PASS){
            EasyUser user = new EasyUser();
            user.setId(updateVO.getUserId());
            user.setCertificateName(certificate.getCertificateName());
            user.setType(MemberTypeEnum.CERTIFICATE);
            userService.updateById(user);
        }else {
            //驳回认证，更新用户认证状态
            EasyUser user = new EasyUser();
            user.setId(updateVO.getUserId());
            user.setCertificateName(null);
            user.setType(MemberTypeEnum.NORMAL);
            userService.updateById(user);
        }
        //通知通知用户认证的结果
        certificateProducer.handleMessage(updateVO);
    }

    /**
     * 用户申请或修改认证
     *
     * @param certificateAppBaseVO 认证参数
     */
    @Override
    public void certificate(CertificateAppBaseVO certificateAppBaseVO) {
        EasyUserCertificate certificate = UserCertificateConvert.INSTANT.baseVOToCertificate(certificateAppBaseVO);
        //用户申请或修改认证时都要审核
        certificate.setStatus(UserCertificateEnum.REVIEWED);
        saveOrUpdate(certificate);
    }

    /**
     * 查询认证详细信息
     *
     * @param userId 用户id
     * @return 认证详情信息
     */
    @Override
    public CertificateAppDetailVO getCertificateDetail(Integer userId) {
        //根据用户id去获取认证信息
        LambdaQueryWrapper<EasyUserCertificate> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(EasyUserCertificate::getUserId, userId);
        EasyUserCertificate certificate = certificateMapper.selectOne(wrapper);
        return UserCertificateConvert.INSTANT.certificateToAppDetailVO(certificate);
    }

    /**
     * 删除认证
     *
     * @param deleteVO 删除的信息
     */
    @Override
    public void delete(UserCertificateDeleteVO deleteVO) {
        //通知用户认证被删除
        certificateProducer.deleteMessage(deleteVO);
        //先删除认证
        removeById(deleteVO.getId());
        //在跟新用户的认证信息
        EasyUser user = new EasyUser();
        user.setId(deleteVO.getUserId());
        user.setCertificateName("");
        user.setType(MemberTypeEnum.NORMAL);
        userService.updateById(user);
    }

    public void fillData(UserCertificateDetailVO userCertificateDetailVO) {
        //获取根据用户id获取用户信息
        EasyUser user = userService.getById(userCertificateDetailVO.getUserId());
        userCertificateDetailVO.setAvatar(user.getAvatar());
        userCertificateDetailVO.setNikeName(user.getNikeName());
    }
}




