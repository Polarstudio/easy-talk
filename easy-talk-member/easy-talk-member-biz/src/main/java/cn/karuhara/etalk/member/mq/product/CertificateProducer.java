package cn.karuhara.etalk.member.mq.product;

import cn.karuhara.etalk.framework.kafka.core.Event;
import cn.karuhara.etalk.framework.kafka.core.EventProducer;
import cn.karuhara.etalk.framework.kafka.core.MessageConstants;
import cn.karuhara.etalk.member.controller.admin.certificate.vo.UserCertificateDeleteVO;
import cn.karuhara.etalk.member.controller.admin.certificate.vo.UserCertificateUpdateVO;
import cn.karuhara.etalk.member.entity.EasyUserCertificate;
import cn.karuhara.etalk.member.enums.UserCertificateEnum;
import cn.karuhara.etalk.member.service.EasyUserCertificateService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Component;

@Component
public class CertificateProducer {
    @Resource
    private EventProducer eventProducer;

    @Resource
    private EasyUserCertificateService certificateService;

    public void deleteMessage(UserCertificateDeleteVO deleteVO){
        EasyUserCertificate certificate = certificateService.getById(deleteVO.getId());
        Event event = new Event();
        //设置消息类型
        event.setTopic(MessageConstants.CERTIFICATE_DELETE_TOPIC);
        //设置通知的用户
        if (deleteVO.getUserId()!=null){
            event.setToUserId(deleteVO.getUserId());
        }
        //设置消息的内容
        if (deleteVO.getReason()!=null){
            event.setContent(deleteVO.getReason());
        }
        //这是其它的额外扩展的参数
        event.setDataMap(MessageConstants.POST_ID,deleteVO.getId());
        event.setDataMap(MessageConstants.CERTIFICATE_NAME,certificate.getCertificateName());
        eventProducer.fireEvent(event);
    }

    //处理审核的消息
    public void handleMessage(UserCertificateUpdateVO updateVO){
        if (updateVO.getStatus() == UserCertificateEnum.REVIEWED){
            return;
        }
        Event event = new Event();
        event.setTopic(MessageConstants.CERTIFICATE_HANDLE_TOPIC);
        event.setToUserId(updateVO.getUserId());
        //如果通过
        if (updateVO.getStatus() == UserCertificateEnum.PASS){
            event.setContent("您申请认证的`"+updateVO.getCertificateName()+"`已通过认证！");
        }else {
            event.setContent("您申请认证的`"+updateVO.getCertificateName()+"`已被驳回！点我查看详情！");
            event.setDataMap(MessageConstants.CONTENT_URL,MessageConstants.CERTIFICATE_URL);
        }
        eventProducer.fireEvent(event);
    }

}