package cn.karuhara.etalk.member.controller.app.user.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "用户粉丝信息")
@Data
public class UserFollowDetailVO {
    @Schema(description = "用户的id")
    private Integer id;

    @Schema(description = "用户昵称")
    private String nikeName;

    @Schema(description = "IP属地")
    private String dependency;

    @Schema(description = "用户的头像")
    private String avatar;

    @Schema(description = "用户类型")
    private String type;

    @Schema(description = "用户认证名称")
    private String certificateName;
}
