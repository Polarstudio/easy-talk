package cn.karuhara.etalk.member.mapper;

import cn.karuhara.etalk.member.entity.EasyUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author atom
* @description 针对表【easy_user(用户)】的数据库操作Mapper
* @createDate 2024-11-15 11:33:20
* @Entity cn.karuhara.etalk.member.domain.EasyUser
*/
public interface EasyUserMapper extends BaseMapper<EasyUser> {

}




