package cn.karuhara.etalk.member.controller.app.user.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "APP - 用户的数据")
@Data
public class UserFollowVO {
    @Schema(description = "用户的关注")
    private long follow;

    @Schema(description = "用户粉丝量")
    private long followed;
}
