package cn.karuhara.etalk.member.controller.app.user.vo;

import cn.karuhara.etalk.framework.common.pojo.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Schema(description = "请求粉丝的列表参数")
@Data
public class UserFollowPageReqVO extends PageParam {
    @Schema(description = "请求类型 0获取粉丝 1获取关注")
    private Integer type;

    @Schema(description = "用户Id")
    private Integer userId;

    @Schema(description = "用户昵称")
    private String nikeName;
}
