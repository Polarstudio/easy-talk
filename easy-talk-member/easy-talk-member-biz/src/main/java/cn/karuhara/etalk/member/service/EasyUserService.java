package cn.karuhara.etalk.member.service;

import cn.karuhara.etalk.framework.common.pojo.PageResult;
import cn.karuhara.etalk.member.controller.admin.user.vo.UserAddReqVO;
import cn.karuhara.etalk.member.controller.admin.user.vo.UserPageReqVO;
import cn.karuhara.etalk.member.controller.admin.user.vo.UserPageRespVO;
import cn.karuhara.etalk.member.controller.app.user.vo.UserFollowDetailVO;
import cn.karuhara.etalk.member.controller.app.user.vo.UserFollowPageReqVO;
import cn.karuhara.etalk.member.controller.app.user.vo.UserLoginReqVO;
import cn.karuhara.etalk.member.controller.app.user.vo.UserLoginRespVO;
import cn.karuhara.etalk.member.entity.EasyUser;
import com.baomidou.mybatisplus.extension.service.IService;

import java.io.IOException;

/**
* @author atom
* @description 针对表【easy_user(用户)】的数据库操作Service
* @createDate 2024-11-15 11:33:20
*/
public interface EasyUserService extends IService<EasyUser> {

    /**
     * 添加用户
     * @param userAddReqVO 要添加的用户信息
     */
    void addUser(UserAddReqVO userAddReqVO);

    /**
     * 分页获取用户列表
     * @param userPageReqVO 分页请求参数
     * @return 分页用户列表
     */
    PageResult<UserPageRespVO> pageList(UserPageReqVO userPageReqVO);

    /**
     * 修改用户信息
     * @param userAddReqVO 用户信息
     */
    void updateMember(UserAddReqVO userAddReqVO);

    /**
     * 微信登录
     * @param userLoginReqVO 微信登录凭证
     * @return 用户详细信息
     */
    UserLoginRespVO wxLogin(UserLoginReqVO userLoginReqVO) throws IOException;


    /**
     * 刷新Ip
     */
    String refreshIp(Integer userId, String ip);

    /**
     * 获取用户粉丝或用户的关注
     * @return
     */
    PageResult<UserFollowDetailVO> getFollow(UserFollowPageReqVO pageReqVO);
}
