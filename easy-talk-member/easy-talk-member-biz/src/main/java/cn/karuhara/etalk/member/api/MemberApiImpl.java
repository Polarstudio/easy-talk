package cn.karuhara.etalk.member.api;

import cn.karuhara.etalk.member.api.dto.MemberBaseInfoDTO;
import cn.karuhara.etalk.member.api.dto.MemberDynamicInfoDTO;
import cn.karuhara.etalk.member.convert.MemberUserConvert;
import cn.karuhara.etalk.member.entity.EasyUser;
import cn.karuhara.etalk.member.entity.EasyUserFollow;
import cn.karuhara.etalk.member.service.EasyUserFollowService;
import cn.karuhara.etalk.member.service.EasyUserService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import jakarta.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController // 提供 RESTful API 接口，给 Feign 调用
@Validated
public class MemberApiImpl implements MemberApi{

    @Resource
    private EasyUserService userService;

    @Resource
    private EasyUserFollowService followService;

    @Override
    public MemberDynamicInfoDTO getMemberById(Integer userId) {
        EasyUser user = userService.getById(userId);
        return MemberUserConvert.INSTANCE.userToMemberDynamicInfoDTO(user);
    }

    /**
     *
     * @param userId 用户id
     * @return 用户信息
     */
    @Override
    public MemberBaseInfoDTO getMemberBaseInfo(Integer userId) {
        EasyUser user = userService.getById(userId);
        return MemberUserConvert.INSTANCE.userToMemberTopicInfoDTO(user);
    }

    /**
     * @param phone 手机号
     * @return 用户信息
     */
    @Override
    public MemberBaseInfoDTO getMemberByPhone(String phone) {
        LambdaQueryWrapper<EasyUser> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(EasyUser::getPhone, phone);
        EasyUser user = userService.getOne(wrapper);
        return MemberUserConvert.INSTANCE.userToMemberTopicInfoDTO(user);
    }

    /**
     * @param ids 用户id集合
     * @return 用户信息列表
     */
    @Override
    public List<MemberBaseInfoDTO> getByIds(List<Integer> ids) {
        LambdaQueryWrapper<EasyUser> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(EasyUser::getId, ids);
        List<EasyUser> users = userService.list(wrapper);
        return MemberUserConvert.INSTANCE.usersToApiBaseDTOs(users);
    }

    /**
     * @param userId 用户id
     * @return 关注的用户ids
     */
    @Override
    public List<Integer> getFollowIds(Integer userId) {
        LambdaQueryWrapper<EasyUserFollow> followWrapper = new LambdaQueryWrapper<>();
        followWrapper.eq(EasyUserFollow::getUserId, userId);
        List<EasyUserFollow> follows = followService.list(followWrapper);
        return follows.stream().map(EasyUserFollow::getFollowId).collect(Collectors.toList());
    }

}
