package cn.karuhara.etalk.member.controller.test;

import cn.karuhara.etalk.framework.common.pojo.CommonResult;
import cn.karuhara.etalk.framework.oss.adapter.StorageAdapter;
import cn.karuhara.etalk.member.utils.IpUtil;
import cn.karuhara.etalk.system.api.ConfigApi;
import cn.karuhara.etalk.system.api.dto.SystemConfigDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/test")
@Tag(name = "测试控制器")
public class TestController {

    @Resource
    private StorageAdapter storageAdapter;

    @Resource
    private ConfigApi configApi;

    @PostMapping("/upload")
    @Operation(summary = "测试上传文件")
    public CommonResult<String> upload(@RequestPart MultipartFile uploadFile,String bucket,String objectName){
        storageAdapter.uploadFile(uploadFile,bucket,objectName);
        objectName = objectName + "/" + uploadFile.getOriginalFilename();
        return CommonResult.success(storageAdapter.getUrl(bucket, objectName));
    }

    @Operation(summary = "IP库测试")
    @GetMapping("/ip")
    public CommonResult<String> ip(@RequestParam String ip , HttpServletRequest request){
        boolean b = IpUtil.checkIp(request.getRemoteAddr());
        String ipRegion = IpUtil.getIpRegion(ip);
        return CommonResult.success(ipRegion);
    }
}
