package cn.karuhara.etalk.member.controller.admin.certificate.vo;
import cn.karuhara.etalk.member.enums.UserCertificateEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Schema(description = "认证详情VO")
@Data
public class UserCertificateDetailVO {

    @Schema(description = "id")
    private Integer id;

    @Schema(description = "用户Id")
    private Integer userId;

    @Schema(description = "用户昵称")
    private String nikeName;

    @Schema(description = "用户头像")
    private String avatar;

    @Schema(description = "认证名称")
    private String certificateName;

    @Schema(description = "认证资料")
    private List<String> material;

    @Schema(description = "认证状态（0通过 1待审核 2驳回）")
    private UserCertificateEnum status;

    @Schema(description = "备注")
    private String remark;

    @Schema(description = "驳回原因")
    private String rejection;

    @Schema(description = "创建时间")
    @JsonFormat(pattern = "yyyy/MM/dd HH:mm", timezone = "GMT+8")
    private LocalDateTime createTime;
}
