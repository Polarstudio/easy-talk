package cn.karuhara.etalk.member.domain;

import com.alibaba.fastjson2.annotation.JSONField;
import lombok.Data;

/**
 * 微信登录获取用户凭证
 */
@Data
public class WxLoginCertificate {
    //会话密钥
    @JSONField(name = "session_key")
    private String sessionKey;
    //openid
    @JSONField(name = "openid")
    private String openId;
}
