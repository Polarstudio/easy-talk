package cn.karuhara.etalk.member.controller.app.certificate;

import cn.karuhara.etalk.framework.common.pojo.CommonResult;
import cn.karuhara.etalk.member.controller.app.certificate.vo.CertificateAppBaseVO;
import cn.karuhara.etalk.member.controller.app.certificate.vo.CertificateAppDetailVO;
import cn.karuhara.etalk.member.service.EasyUserCertificateService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@Tag(name = "APP - 用户认证接口")
@RequestMapping("/member/app/certificate")
@Validated
public class CertificateAppController {
    @Resource
    private EasyUserCertificateService certificateService;

    @PostMapping
    @Operation(summary = "申请或修改认证")
    public CommonResult<Void> certificate(@RequestBody CertificateAppBaseVO certificateAppBaseVO) {
        certificateService.certificate(certificateAppBaseVO);
        return CommonResult.success();
    }

    @Operation(summary = "查询认证详细信息")
    @GetMapping("/{userId}")
    public CommonResult<CertificateAppDetailVO> getCertificateDetail(@PathVariable Integer userId) {
        CertificateAppDetailVO detailVO = certificateService.getCertificateDetail(userId);
        return CommonResult.success(detailVO);
    }
}
