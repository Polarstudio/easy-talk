package cn.karuhara.etalk.member.service.impl;

import cn.dev33.satoken.stp.StpUtil;
import cn.karuhara.etalk.framework.common.constants.GlobalConstant;
import cn.karuhara.etalk.framework.common.constants.RedisConstant;
import cn.karuhara.etalk.framework.common.enums.GlobalCodeEnum;
import cn.karuhara.etalk.framework.common.exception.ServiceException;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import cn.karuhara.etalk.fremawork.redis.core.MyRedisUtil;
import cn.karuhara.etalk.member.constants.MemberConstant;
import cn.karuhara.etalk.member.controller.admin.user.vo.UserAddReqVO;
import cn.karuhara.etalk.member.controller.admin.user.vo.UserPageReqVO;
import cn.karuhara.etalk.member.controller.admin.user.vo.UserPageRespVO;
import cn.karuhara.etalk.member.controller.app.user.vo.UserFollowDetailVO;
import cn.karuhara.etalk.member.controller.app.user.vo.UserFollowPageReqVO;
import cn.karuhara.etalk.member.controller.app.user.vo.UserLoginReqVO;
import cn.karuhara.etalk.member.controller.app.user.vo.UserLoginRespVO;
import cn.karuhara.etalk.member.convert.MemberUserConvert;
import cn.karuhara.etalk.member.domain.AccessToken;
import cn.karuhara.etalk.member.domain.PhoneNumber;
import cn.karuhara.etalk.member.domain.WxLoginCertificate;
import cn.karuhara.etalk.member.entity.EasyUserFollow;
import cn.karuhara.etalk.member.enums.MemberGenderEnum;
import cn.karuhara.etalk.member.enums.MemberStatusEnum;
import cn.karuhara.etalk.member.enums.MemberTypeEnum;
import cn.karuhara.etalk.member.service.EasyUserFollowService;
import cn.karuhara.etalk.member.utils.IpUtil;
import cn.karuhara.etalk.system.api.ConfigApi;
import cn.karuhara.etalk.system.api.dto.SystemConfigDTO;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.karuhara.etalk.member.entity.EasyUser;
import cn.karuhara.etalk.member.service.EasyUserService;
import cn.karuhara.etalk.member.mapper.EasyUserMapper;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.ini4j.Config;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
* @author atom
* @description 针对表【easy_user(用户)】的数据库操作Service实现
* @createDate 2024-11-15 11:33:20
*/
@Service
@Slf4j
public class EasyUserServiceImpl extends ServiceImpl<EasyUserMapper, EasyUser>
    implements EasyUserService{

    OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(15, TimeUnit.SECONDS)
            .build();

    @Resource
    private EasyUserMapper easyUserMapper;

    @Resource
    private EasyUserFollowService followService;

    @Resource
    private ConfigApi configApi;

    @Override
    public void addUser(UserAddReqVO userAddReqVO) {
        //注册前先检查是否手机号是否已经存在
        LambdaQueryWrapper<EasyUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyUser::getPhone, userAddReqVO.getPhone());
        EasyUser one = getOne(queryWrapper);
        if (one != null ){
            throw new ServiceException(GlobalCodeEnum.FAIL.getCode(),GlobalConstant.PHONE_ALREADY_EXISTS);
        }
        EasyUser easyUser = MemberUserConvert.INSTANCE.addVOToEasyUser(userAddReqVO);
        easyUserMapper.insert(easyUser);
    }

    @Override
    public PageResult<UserPageRespVO> pageList(UserPageReqVO pageReqVO) {
        LambdaQueryWrapper<EasyUser> queryWrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(pageReqVO.getPhone())) {
            queryWrapper.eq(EasyUser::getPhone, pageReqVO.getPhone());
        }
        if (StringUtils.isNotBlank(pageReqVO.getNikeName())){
            queryWrapper.like(EasyUser::getNikeName, pageReqVO.getNikeName());
        }
        if (StringUtils.isNotBlank(pageReqVO.getStatus())){
            queryWrapper.eq(EasyUser::getStatus,pageReqVO.getStatus());
        }
        if (StringUtils.isNotBlank(pageReqVO.getType())){
            queryWrapper.eq(EasyUser::getType,pageReqVO.getType());
        }
        Page<EasyUser> page = new Page<>(pageReqVO.getPageNo(), pageReqVO.getPageSize());
        IPage<EasyUser> iPage = easyUserMapper.selectPage(page, queryWrapper);
        if (iPage.getTotal() == 0){
            return PageResult.empty();
        }
        List<EasyUser> records = iPage.getRecords();
        List<UserPageRespVO> pageRespVOList = MemberUserConvert.INSTANCE.userListToPageRespVOList(records);
        return new PageResult<>(pageRespVOList,iPage.getPages());
    }

    @Override
    public void updateMember(UserAddReqVO userAddReqVO) {
        //注册前先检查是否手机号是否已经存在
        LambdaQueryWrapper<EasyUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyUser::getPhone, userAddReqVO.getPhone());
        EasyUser one = getOne(queryWrapper);
        if (one.getId() != userAddReqVO.getId()){
            throw new ServiceException(GlobalCodeEnum.FAIL.getCode(),GlobalConstant.PHONE_ALREADY_EXISTS);
        }
        EasyUser easyUser = MemberUserConvert.INSTANCE.addVOToEasyUser(userAddReqVO);
        updateById(easyUser);
    }

    @Override
    public UserLoginRespVO wxLogin(UserLoginReqVO userLoginReqVO) throws IOException {
        WxLoginCertificate loginCertificate = userAuth(userLoginReqVO.getLoginCode());
        //通过OpenId查询用户是否以及注册
        LambdaQueryWrapper<EasyUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(EasyUser::getWxOpenid,loginCertificate.getOpenId());
        EasyUser one = getOne(queryWrapper);
        if (one != null){
            return generationUserInfo(one);
        }
        //没有,则注册一个
        //1. 先获取接口调用凭据
        // AccessToken accessToken = getAccessToken();
        //TODO 获取手机号的功能需要微信小程序认证完成后才能使用
        //2. 获取用户的手机号
        ///PhoneNumber phoneNumber =  getUserPhone(userLoginReqVO.getPhoneCode(),accessToken);
        //3. 注册
        EasyUser easyUser = new EasyUser();
        easyUser.setWxOpenid(loginCertificate.getOpenId());
        //easyUser.setPhone(phoneNumber.getPhoneInfo().getPurePhoneNumber());
        //头像
        easyUser.setAvatar(MemberConstant.DEFAULT_AVATAR);
        //昵称
        easyUser.setNikeName("用户" + RandomStringUtils.randomAlphabetic(10));
        //生成简介
        easyUser.setIntro(MemberConstant.DEFAULT_INTRO);
        //年龄
        easyUser.setAge(MemberConstant.DEFAULT_AGE);
        save(easyUser);
        easyUser.setStatus(MemberStatusEnum.NORMAL);
        easyUser.setType(MemberTypeEnum.NORMAL);
        easyUser.setGender(MemberGenderEnum.UNKNOWN);
        return generationUserInfo(easyUser);
    }


    //生成用户的信息
    public UserLoginRespVO generationUserInfo(EasyUser easyUser){
        UserLoginRespVO userLoginRespVO = MemberUserConvert.INSTANCE.userToLoginRespVO(easyUser);
        StpUtil.login(easyUser.getId());
        userLoginRespVO.setToken(StpUtil.getTokenValue());
        return userLoginRespVO;
    }

    @Override
    public String refreshIp(Integer userId,String ip) {
        String ipRegion = IpUtil.getIpRegion(ip);
        EasyUser easyUser  = new EasyUser();
        easyUser.setId(userId);
        easyUser.setIp(ip);
        easyUser.setDependency(ipRegion);
        updateById(easyUser);
        return ipRegion;
    }

    /**
     * 获取用户粉丝或用户的关注
     *
     * @return
     */
    @Override
    public PageResult<UserFollowDetailVO> getFollow(UserFollowPageReqVO pageReqVO) {
        LambdaQueryWrapper<EasyUserFollow> queryWrapper = new LambdaQueryWrapper<>();
        if(pageReqVO.getType() == 0){
            queryWrapper.eq(EasyUserFollow::getFollowId,pageReqVO.getUserId());
        } else if (pageReqVO.getType()  == 1) {
            queryWrapper.eq(EasyUserFollow::getUserId,pageReqVO.getUserId());
        }
        //获取粉丝（关注）的id列表
        List<EasyUserFollow> follows = followService.list(queryWrapper);
        List<Integer> followIds;
        if (pageReqVO.getType() == 0){
            followIds = follows.stream().map(EasyUserFollow::getUserId).collect(Collectors.toList());
        }else {
            followIds = follows.stream().map(EasyUserFollow::getFollowId).collect(Collectors.toList());
        }
        if (followIds.isEmpty()){
            return PageResult.empty();
        }
        LambdaQueryWrapper<EasyUser> userQueryWrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(pageReqVO.getNikeName())){
            userQueryWrapper.eq(EasyUser::getNikeName,pageReqVO.getNikeName());
        }
        userQueryWrapper.in(EasyUser::getId, followIds);
        Page<EasyUser> page = new Page<>(pageReqVO.getPageNo(), pageReqVO.getPageSize());
        Page<EasyUser> easyUserPage = easyUserMapper.selectPage(page, userQueryWrapper);
        if (easyUserPage.getTotal() == 0){
            return PageResult.empty();
        }
        List<EasyUser> records = easyUserPage.getRecords();
        List<UserFollowDetailVO> userFollowDetailVOS = MemberUserConvert.INSTANCE.usersToFollowDetailVOList(records);
        return new PageResult<>(userFollowDetailVOS,page.getPages());
    }

    //获取用户凭证
    public WxLoginCertificate userAuth(String code) throws IOException {
        SystemConfigDTO systemConfig = configApi.getSystemConfig();
        HttpUrl.Builder urlBuilder =
                HttpUrl.parse(MemberConstant.WX_BASE_URL+"/sns/jscode2session").newBuilder();
        urlBuilder.addQueryParameter("appid",systemConfig.getAppId());
        urlBuilder.addQueryParameter("secret",systemConfig.getSecret());
        urlBuilder.addQueryParameter("js_code",code);
        urlBuilder.addQueryParameter("grant_type","authorization_code");
        Request request = new Request.Builder().url(urlBuilder.build()).build();
        Call call = client.newCall(request);
        Response response = call.execute();
        String responseString = response.body().string();
        WxLoginCertificate loginCertificate = JSON.parseObject(responseString, WxLoginCertificate.class);
        if (StringUtils.isBlank(loginCertificate.getOpenId())){
            throw new ServiceException(GlobalCodeEnum.FAIL);
        }
        return loginCertificate;
    }

    //获取接口调用凭证
    public AccessToken getAccessToken() throws IOException {
        HttpUrl.Builder urlBuilder =
                HttpUrl.parse(MemberConstant.WX_BASE_URL+"/cgi-bin/token").newBuilder();
        urlBuilder.addQueryParameter("appid","wxbf1ffe35d20d4ffa");
        urlBuilder.addQueryParameter("secret","c0d94ac417f4aa00f149855589510c22");
        urlBuilder.addQueryParameter("grant_type","client_credential");
        Request request = new Request.Builder().url(urlBuilder.build()).build();
        Call call = client.newCall(request);
        Response response = call.execute();
        String responseString = response.body().string();
        AccessToken accessToken = JSON.parseObject(responseString, AccessToken.class);
        if (StringUtils.isBlank(accessToken.getAccessToken())){
            throw new ServiceException(GlobalCodeEnum.FAIL);
        }
        return accessToken;
    }

    //获取用户的手机号信息
    private PhoneNumber getUserPhone(String phoneCode, AccessToken accessToken) throws IOException {
        //注意,这里的code必须是json的格式,不然就会请求失败!!!!!!
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code",phoneCode);
        RequestBody requestBody = RequestBody
                .create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
        Request request = new Request.Builder()
                .url(MemberConstant.WX_BASE_URL + "/wxa/business/getuserphonenumber?access_token="
                        +accessToken.getAccessToken())
                .post(requestBody)
                .build();
        Call call = client.newCall(request);
        Response response = call.execute();
        String responseString = response.body().string();
        PhoneNumber phoneNumber = JSON.parseObject(responseString, PhoneNumber.class);
        if (StringUtils.isNotBlank(phoneNumber.getPhoneInfo().getPurePhoneNumber())){
            throw new ServiceException(GlobalCodeEnum.FAIL);
        }
        return phoneNumber;
    }

}



