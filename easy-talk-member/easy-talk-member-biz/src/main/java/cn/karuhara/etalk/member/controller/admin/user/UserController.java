package cn.karuhara.etalk.member.controller.admin.user;

import cn.karuhara.etalk.framework.annotation.WeLog;
import cn.karuhara.etalk.framework.common.pojo.CommonResult;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import cn.karuhara.etalk.member.controller.admin.user.vo.UserAddReqVO;
import cn.karuhara.etalk.member.controller.admin.user.vo.UserPageReqVO;
import cn.karuhara.etalk.member.controller.admin.user.vo.UserPageRespVO;
import cn.karuhara.etalk.member.service.EasyUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Tag(name = "管理 - 用户管理")
@RestController
@RequestMapping("/member/admin/user")
@Validated
public class UserController {

    @Resource
    private EasyUserService userService;

    @PostMapping()
    @WeLog(description = "添加用户的操作")
    @Operation(summary = "新增会员")
    public CommonResult<Void> add(@Valid @RequestBody UserAddReqVO userAddReqVO){
        userService.addUser(userAddReqVO);
        return CommonResult.success();
    }

    @PostMapping("/page")
    @WeLog(description = "获取会员列表")
    @Operation(summary = "获取会员列表")
    public CommonResult<PageResult<UserPageRespVO>> page(@RequestBody UserPageReqVO userPageReqVO){
        PageResult<UserPageRespVO> pageRespVOList = userService.pageList(userPageReqVO);
        return CommonResult.success(pageRespVOList);
    }

    @PutMapping()
    @WeLog(description = "修改会员的操作")
    @Operation(summary = "修改会员信息")
    public CommonResult<Void> update(@Valid @RequestBody UserAddReqVO userAddReqVO){
        userService.updateMember(userAddReqVO);
        return CommonResult.success();
    }

}