package cn.karuhara.etalk.member.controller.admin.certificate.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "删除认证的vo")
@Data
public class UserCertificateDeleteVO {
    @Schema(description = "认证id")
    private Integer id;

    @Schema(description = "用户id，用于提醒用户认证已被删除")
    private Integer userId;

    @Schema(description = "删除原因")
    private String reason;
}
