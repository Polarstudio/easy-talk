package cn.karuhara.etalk.member.controller.app.certificate.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

@Schema(description = "认证基本信息")
@Data
public class CertificateAppBaseVO {

    @Schema(description = "id")
    private Integer id;

    @Schema(description = "用户Id")
    private Integer userId;

    @Schema(description = "认证名称")
    private String certificateName;

    @Schema(description = "认证资料")
    private List<String> material;

    @Schema(description = "备注信息")
    private String remark;
}
