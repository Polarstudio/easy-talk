package cn.karuhara.etalk.member.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum UserCertificateEnum {
    REVIEWED("0","待审核"),
    PASS("1","通过"),
    REJECT("2","驳回")
    ;

    //记一次坑：如果数据库的字段为枚举类型，那么这里必须是String类型，否则会报错
    @EnumValue
    @JsonValue
    private final String code;

    private final String desc;
}
