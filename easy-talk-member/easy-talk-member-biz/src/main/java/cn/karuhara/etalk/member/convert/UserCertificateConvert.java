package cn.karuhara.etalk.member.convert;

import cn.karuhara.etalk.member.controller.admin.certificate.vo.UserCertificateDetailVO;
import cn.karuhara.etalk.member.controller.admin.certificate.vo.UserCertificateUpdateVO;
import cn.karuhara.etalk.member.controller.app.certificate.vo.CertificateAppBaseVO;
import cn.karuhara.etalk.member.controller.app.certificate.vo.CertificateAppDetailVO;
import cn.karuhara.etalk.member.entity.EasyUserCertificate;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import java.util.List;
@Mapper
public interface UserCertificateConvert {
    UserCertificateConvert INSTANT = Mappers.getMapper(UserCertificateConvert.class);

    List<EasyUserCertificate> detailsToCertificates(List<UserCertificateDetailVO> detailVOS);

    List<UserCertificateDetailVO> certificatesToDetails(List<EasyUserCertificate> easyUserCertificate);

    EasyUserCertificate updateVOToCertificate(UserCertificateUpdateVO updateVO);

    EasyUserCertificate baseVOToCertificate(CertificateAppBaseVO baseVO);

    CertificateAppDetailVO certificateToAppDetailVO(EasyUserCertificate certificate);
}
