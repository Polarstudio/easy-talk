package cn.karuhara.etalk.member.controller.app.user.vo;

import cn.karuhara.etalk.member.enums.MemberGenderEnum;
import cn.karuhara.etalk.member.enums.MemberStatusEnum;
import cn.karuhara.etalk.member.enums.MemberTypeEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

/**
 * @Description: 会员用户 Base VO，提供给添加、修改、详细的子 VO 使用
 * @author: 轻语
*/
@Data
@Schema(description = "管理后台 - 基础用户VO")
public class UserBaseVO {

    @Schema(description = "用户Id",example = "1")
    private Integer id;

    @Schema(description = "用户昵称", requiredMode = Schema.RequiredMode.REQUIRED , example = "easy-talk")
    @NotNull(message = "用户昵称不能为空！")
    private String nikeName;

    @Schema(description = "用户头像", requiredMode = Schema.RequiredMode.REQUIRED , example = "http://www.karuhara.cn/avatr.png")
    @URL(message = "用户头像必须是url！")
    private String avatar;

    @Schema(description = "用户简介" , example = "此人很懒，没有简介")
    private String intro;

    @Schema(description = "性别(0男，1女、2未知)",requiredMode = Schema.RequiredMode.REQUIRED  , example = "2")
    @NotNull(message = "性别不能为空！")
    private MemberGenderEnum gender;

    @Schema(description = "年龄" ,requiredMode = Schema.RequiredMode.REQUIRED , example = "00后")
    private String age;

    @Schema(description = "用户手机号",requiredMode = Schema.RequiredMode.REQUIRED  , example = "15333333333")
    @NotNull(message = "手机号不能为空！")
    private String phone;

    @Schema(description = "类型（0普通、1认证、2官方）" ,requiredMode = Schema.RequiredMode.REQUIRED , example = "0")
    @NotNull(message = "用户类型不能为空！")
    private MemberTypeEnum type;

    @Schema(description = "认证名",example = "校ACM队")
    private String certificateName;

    @Schema(description = "IP属地")
    private String dependency;

    @Schema(description = "状态：0正常、1审核中、2禁言、3拉黑、8 注销" ,requiredMode = Schema.RequiredMode.REQUIRED , example = "0")
    @NotNull(message = "用户状态不能为空！")
    private MemberStatusEnum status;

}
