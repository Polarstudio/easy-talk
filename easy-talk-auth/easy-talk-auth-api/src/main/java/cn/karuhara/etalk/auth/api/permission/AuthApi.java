package cn.karuhara.etalk.auth.api.permission;

import cn.karuhara.etalk.auth.api.enums.ApiConstant;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = ApiConstant.NAME)
@Tag(name = "RPC 服务 - 权限")
public interface AuthApi {

    String PREFIX = ApiConstant.PREFIX + "/auth";

    @GetMapping(PREFIX+"/getRoleAndPermission")
    @Operation(summary = "获取用户的角色标识和权限标识并存入redis中")
    void getRoleAndPermission(@RequestParam("username") String username);


}
