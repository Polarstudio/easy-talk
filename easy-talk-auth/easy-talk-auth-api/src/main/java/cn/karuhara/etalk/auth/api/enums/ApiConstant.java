package cn.karuhara.etalk.auth.api.enums;

import cn.karuhara.etalk.framework.common.constants.RpcConstants;

/**
 * API 相关常量
 */
public class ApiConstant {

    public static final String NAME = "easy-talk-auth";

    public static final String PREFIX = RpcConstants.RPC_API_PREFIX +  "/auth";
}
