package cn.karuhara.etalk.auth.controller.role.vo;

import cn.karuhara.etalk.framework.common.pojo.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Schema(description = "条件+分页查询角色")
@Data
public class RolePageReqVO extends PageParam {

    @Schema(description = "角色名")
    private String roleName;

    @Schema(description = "状态（0正常 1封禁）")
    private String status;
}
