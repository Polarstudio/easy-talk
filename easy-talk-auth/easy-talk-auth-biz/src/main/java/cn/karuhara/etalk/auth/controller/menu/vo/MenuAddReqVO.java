package cn.karuhara.etalk.auth.controller.menu.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;
@Schema(description = "管理后台 - 添加菜单请求VO")
@Data
public class MenuAddReqVO {

    @Schema(description = "菜单名称")
    @NotEmpty(message = "菜单名称不能为空！")
    private String menuName;

    @Schema(description = "目录ID")
    private Long parentId;

    @Schema(description = "显示顺序")
    private Integer orderNum;

    @Schema(description = "路由地址")
    private String path;

    @Schema(description = "路由名称")
    private String routeName;

    @Schema(description = "菜单类型（C目录 M菜单）")
    private String menuType;

    @Schema(description = "菜单状态（0正常 1停用）")
    private String status;

    @Schema(description = "权限标识")
    private String perms;

    @Schema(description = "菜单图标")
    private String icon;

    @Schema(description = "备注")
    private String remark;
}
