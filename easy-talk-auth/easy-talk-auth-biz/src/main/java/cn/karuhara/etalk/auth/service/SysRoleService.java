package cn.karuhara.etalk.auth.service;

import cn.karuhara.etalk.auth.controller.role.vo.*;
import cn.karuhara.etalk.auth.entity.SysRole;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author atom
* @description 针对表【sys_role】的数据库操作Service
* @createDate 2024-10-28 23:36:35
*/
public interface SysRoleService extends IService<SysRole> {

    /**
     * 新增角色
     * @param addReqVO
     */
    void addRole(RoleAddReqVO addReqVO);

    /**
     * 条件+分页获取角色
     * @param rolePageReqVO
     * @return
     */
    PageResult<RoleRespVO> getPage(RolePageReqVO rolePageReqVO);

    /**
     * 修改角色
     * @param roleUpdateReqVO
     */
    void updateRole(RoleUpdateReqVO roleUpdateReqVO);

    /**
     * 删除角色
     * @param id 角色Id
     */
    void deleteRole(Integer id);

    /**
     * 获取角色id和名称
     * @return
     */
    List<RoleIdNameVO> getIdName();
}
