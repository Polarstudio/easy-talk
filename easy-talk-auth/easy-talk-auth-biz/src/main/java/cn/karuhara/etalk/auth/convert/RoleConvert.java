package cn.karuhara.etalk.auth.convert;

import cn.karuhara.etalk.auth.controller.role.vo.RoleAddReqVO;
import cn.karuhara.etalk.auth.controller.role.vo.RoleIdNameVO;
import cn.karuhara.etalk.auth.controller.role.vo.RoleRespVO;
import cn.karuhara.etalk.auth.controller.role.vo.RoleUpdateReqVO;
import cn.karuhara.etalk.auth.entity.SysRole;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface RoleConvert{

    RoleConvert INSTANCE = Mappers.getMapper(RoleConvert.class);

    SysRole addVOToRole(RoleAddReqVO addReqVO);

    List<RoleRespVO> roleListToRoleRespVOList(List<SysRole> roleList);

    SysRole updateToRole(RoleUpdateReqVO updateReqVO);

    List<RoleIdNameVO> roleToRoleIdNameVOList(List<SysRole> roleList);
}
