package cn.karuhara.etalk.auth.controller.user.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.List;

@Schema(description = "管理后台 - 添加系统用户VO")
@Data
public class UserUpdateReqVO {

    @Schema(description = "用户id")
    @NotNull(message = "用户ID不能为空")
    private Integer id;

    @Schema(description = "密码", requiredMode = Schema.RequiredMode.REQUIRED)
    private String password;

    @Schema(description = "昵称")
    private String nikeName;

    @Schema(description = "用户角色")
    private List<Integer> roleIds;

    @Schema(description = "头像")
    private String avatar;

    @Schema(description = "邮箱")
    @Email(message = "邮箱格式不正确")
    private String email;

    @Schema(description = "状态")
    private String status;
}
