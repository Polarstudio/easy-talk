package cn.karuhara.etalk.auth.controller.user;

import cn.karuhara.etalk.auth.controller.user.vo.*;
import cn.karuhara.etalk.auth.convert.MenuConvert;
import cn.karuhara.etalk.auth.entity.SysMenu;
import cn.karuhara.etalk.auth.service.SysAdminService;
import cn.karuhara.etalk.framework.annotation.WeLog;
import cn.karuhara.etalk.framework.common.pojo.CommonResult;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "管理后台 - 系统用户管理")
@RestController
@RequestMapping("/system/auth/admin")
@Slf4j
@Validated
public class SystemAdminController {

    @Resource
    private SysAdminService sysAdminService;

    @PostMapping("/login")
    @Operation(summary = "管理员登录")
    @WeLog(description = "系统用户登录操作")
    public CommonResult<UserLoginRespVO> login(@RequestBody @Valid UserLoginReqVO loginVO){
        UserLoginRespVO loginRespVO = sysAdminService.login(loginVO);
        return CommonResult.success(loginRespVO);
    }

    @Operation(summary = "添加系统用户")
    @PostMapping
    @WeLog(description = "添加系统用户操作")
    public CommonResult<Void> add(@RequestBody @Valid UserAddReqVO addReqVO){
        sysAdminService.add(addReqVO);
        return CommonResult.success();
    }

    @Operation(summary = "修改用户信息")
    @PutMapping
    @WeLog(description = "修改用户信息")
    public CommonResult<Void> update(@RequestBody UserUpdateReqVO updateReqVO){
        sysAdminService.updateUser(updateReqVO);
        return CommonResult.success();
    }

    @Operation(summary = "删除用户")
    @DeleteMapping
    @WeLog(description = "删除用户操作")
    public CommonResult<Void> delete(@Schema(description = "用户ID")
                                         @RequestParam("userId")
                                         @NotNull
                                         Integer userId){
        sysAdminService.delete(userId);
        return CommonResult.success();
    }

    @Operation(summary = "分页获取系统用户")
    @PostMapping("/page")
    @WeLog(description = "分页获取用户列表")
    public CommonResult<PageResult<UserRespVO>> getPage(@RequestBody @Valid UserPageReqVO pageReqVO){
        PageResult<UserRespVO> pageList = sysAdminService.getPageList(pageReqVO);
        return CommonResult.success(pageList);
    }

    @Operation(summary = "获取用户的菜单")
    @GetMapping("/menu")
    @WeLog(description = "获取用户菜单")
    public CommonResult<List<UserMenu>> getMenu(){
        List<SysMenu> sysMenus = sysAdminService.getUserMenu();
        List<UserMenu> userMenus = MenuConvert.INSTANCE.MenuToUserMenuList(sysMenus);
        return CommonResult.success(userMenus);
    }

}
