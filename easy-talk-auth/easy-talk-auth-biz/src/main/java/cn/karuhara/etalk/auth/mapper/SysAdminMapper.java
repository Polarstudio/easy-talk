package cn.karuhara.etalk.auth.mapper;

import cn.karuhara.etalk.auth.entity.SysAdmin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author atom
* @description 针对表【sys_admin】的数据库操作Mapper
* @createDate 2024-10-28 23:39:58
* @Entity cn.karuhara.etalk.auth.domain.SysAdmin
*/
public interface SysAdminMapper extends BaseMapper<SysAdmin> {

}




