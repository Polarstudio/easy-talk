package cn.karuhara.etalk.auth.controller.role;

import cn.karuhara.etalk.auth.controller.role.vo.*;
import cn.karuhara.etalk.auth.service.SysRoleService;
import cn.karuhara.etalk.framework.annotation.WeLog;
import cn.karuhara.etalk.framework.common.pojo.CommonResult;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "管理后台 - 角色管理")
@RestController
@RequestMapping("/system/auth/role")
public class RoleController {

    @Resource
    private SysRoleService service;

    @Operation(summary = "新增角色")
    @PostMapping
    @WeLog(description = "新增角色")
    public CommonResult<Void> add(@RequestBody RoleAddReqVO addReqVO) {
        service.addRole(addReqVO);
        return CommonResult.success();
    }

    @Operation(summary = "条件+分页查找角色")
    @PostMapping("/page")
    public CommonResult<PageResult<RoleRespVO>> getPages(@RequestBody RolePageReqVO rolePageReqVO) {
        PageResult<RoleRespVO> pageResult = service.getPage(rolePageReqVO);
        return CommonResult.success(pageResult);
    }

    @Operation(summary = "修改角色")
    @PutMapping
    @WeLog(description = "修改角色操作")
    public CommonResult<Void> update(@RequestBody RoleUpdateReqVO roleUpdateReqVO) {
        service.updateRole(roleUpdateReqVO);
        return CommonResult.success();
    }

    @Operation(summary = "删除角色")
    @DeleteMapping
    @WeLog(description = "删除角色操作")
    public CommonResult<Void> delete(@RequestParam("roleId") Integer id) {
        service.deleteRole(id);
        return CommonResult.success();
    }

    @Operation(summary = "获取角色Id以及对应的名称")
    @GetMapping("getIdName")
    public CommonResult<List<RoleIdNameVO>> getIdName() {
        List<RoleIdNameVO> roleIdNameVOS = service.getIdName();
        return CommonResult.success(roleIdNameVOS);
    }
}
