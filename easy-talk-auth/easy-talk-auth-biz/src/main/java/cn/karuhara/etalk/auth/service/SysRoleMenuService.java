package cn.karuhara.etalk.auth.service;

import cn.karuhara.etalk.auth.controller.menu.vo.MenuIdNameVO;
import cn.karuhara.etalk.auth.entity.SysMenu;
import cn.karuhara.etalk.auth.entity.SysRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author atom
* @description 针对表【sys_role_menu】的数据库操作Service
* @createDate 2024-10-28 23:36:35
*/
public interface SysRoleMenuService extends IService<SysRoleMenu> {

    /**
     * 批量插入角色-菜单映射
     * @param sysRoleMenus
     */
    void batchInsert(List<SysRoleMenu> sysRoleMenus);

    /**
     * 根据角色ID获取映射信息
     * @param roleId
     * @return
     */
    List<MenuIdNameVO> getMenuIdName(Integer roleId);

    /**
     * 根据角色的Id删除对应的映射信息
     * @param roleId 角色Id
     */
    void deleteByRoleIds(Integer roleId);

    /**
     * 根据角色Id获取对应的菜单ids
     */
    List<Integer> getMenuIdsByRoleId(Integer roleId);

    /**
     * 根据角色id获取菜单
     * @param roleId
     * @return
     */
    List<SysMenu> getMenuByRoleId(Integer roleId);
}
