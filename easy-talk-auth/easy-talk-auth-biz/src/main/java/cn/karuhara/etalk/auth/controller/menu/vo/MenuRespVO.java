package cn.karuhara.etalk.auth.controller.menu.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "管理后台，分页查询数据")
@Data
public class MenuRespVO {

    @Schema(description = "菜单ID")
    private Integer id;

    @Schema(description = "菜单名称")
    private String menuName;

    @Schema(description = "显示顺序")
    private Integer orderNum;

    @Schema(description = "父级目录")
    private Integer parentId;

    @Schema(description = "路由地址")
    private String path;

    @Schema(description = "路由名称")
    private String routeName;

    @Schema(description = "菜单类型（C目录 M菜单）")
    private Object menuType;

    @Schema(description = "菜单状态（0正常 1停用）")
    private Object status;

    @Schema(description = "权限标识")
    private String perms;

    @Schema(description = "菜单图标")
    private String icon;

    @Schema(description = "备注")
    private String remark;

}
