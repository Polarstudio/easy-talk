package cn.karuhara.etalk.auth.mapper;

import cn.karuhara.etalk.auth.controller.menu.vo.MenuIdNameVO;
import cn.karuhara.etalk.auth.entity.SysMenu;
import cn.karuhara.etalk.auth.entity.SysRoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
* @author atom
* @description 针对表【sys_role_menu】的数据库操作Mapper
* @createDate 2024-10-28 23:36:35
* @Entity cn.karuhara.etalk.auth.domain.SysRoleMenu
*/
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {

    List<MenuIdNameVO> getMenuIdName(@Param("roleId") Integer roleId);

    @Select("SELECT * FROM sys_menu,sys_role_menu WHERE sys_menu.id = sys_role_menu.menu_id AND sys_role_menu.role_id = #{roleId}")
    List<SysMenu> getMenuByRoleId(@Param("roleId") Integer roleId);
}




