package cn.karuhara.etalk.auth.service;

import cn.karuhara.etalk.auth.controller.user.vo.*;
import cn.karuhara.etalk.auth.entity.SysAdmin;
import cn.karuhara.etalk.auth.entity.SysMenu;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import com.baomidou.mybatisplus.extension.service.IService;
import jakarta.validation.Valid;

import java.util.List;

/**
* @author atom
* @description 针对表【sys_admin】的数据库操作Service
* @createDate 2024-10-28 23:39:58
*/
public interface SysAdminService extends IService<SysAdmin> {
    /**
     * 添加系统用户
     * @param addReqVO 添加信息
     */
    void add(@Valid UserAddReqVO addReqVO);

    /**
     * 用户登录
     * @param loginVO 用户登录VO,主题是账号和密码
     * @return UserLoginRespVO 返回用户信息，去掉敏感信息
     */
    UserLoginRespVO login(UserLoginReqVO loginVO);

    /**
     * 删除用户
     * @param userId 用户ID
     */
    void delete(Integer userId);

    /**
     * 条件+分页获取用户列表
     * @param pageReqVO 分页参数+条件参数
     * @return 分页信息
     */
    PageResult<UserRespVO> getPageList(UserPageReqVO pageReqVO);

    /**
     * 修改用户信息
     * @param updateReqVO 用户信息
     */
    void updateUser(UserUpdateReqVO updateReqVO);

    /**
     * 根据用户Id获取用户的菜单
     * @return
     */
    List<SysMenu> getUserMenu();

    SysAdmin getUser(String username);

    /**
     * 获取用户的角色标识和权限标识并存入redis中
     */
    void getUserRoleAndPermission(String username);
}
