package cn.karuhara.etalk.auth.controller.role.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.models.security.SecurityScheme;
import lombok.Data;

import java.util.List;

@Schema(description = "修改角色请求参数")
@Data
public class RoleUpdateReqVO {

    @Schema(description = "角色Id")
    private String id;

    @Schema(description = "角色名")
    private String roleName;

    @Schema(description = "角色标识")
    private String code;

    @Schema(description = "拥有的菜单id")
    private List<Integer> menus;

    @Schema(description = "角色类型(0 超级管理员 1自定义管理员)")
    private Integer type;

    @Schema(description = "备注")
    private String remark;

    @Schema(description = "排序")
    private Integer sort;

    @Schema(description = "状态（0正常 1封禁）")
    private String status;

}
