package cn.karuhara.etalk.auth.controller.user.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Schema(description = "管理后台 - 用户登录返回结果")
@Data
public class UserLoginRespVO extends UserRespVO{

    @Schema(description = "生成的用户Token，前端每次请求都要携带该token")
    private String token;

}
