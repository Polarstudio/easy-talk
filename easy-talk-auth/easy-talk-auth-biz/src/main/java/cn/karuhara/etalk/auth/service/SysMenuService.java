package cn.karuhara.etalk.auth.service;

import cn.karuhara.etalk.auth.controller.menu.vo.*;
import cn.karuhara.etalk.auth.entity.SysMenu;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author atom
* @description 针对表【sys_menu(菜单权限表)】的数据库操作Service
* @createDate 2024-10-28 23:36:35
*/
public interface SysMenuService extends IService<SysMenu> {

    /**
     * 添加菜单
     * @param addReqVO
     */
    void add(MenuAddReqVO addReqVO);

    /**
     * 添加+分页查询
     * @param pageReqVO 请求参数
     * @return 分页数据
     */
    PageResult<MenuRespVO> getPageList(MenuPageReqVO pageReqVO);

    /**
     * 获取所有的目录
     * @return
     */
    List<SysMenu> getCatalog();

    /**
     * 修改菜单信息
     * @param updateReqVO
     */
    void updateMenu(MenuUpdateReqVO updateReqVO);

    /**
     * 获取菜单Id+Name
     * @return
     */
    List<MenuIdNameVO> getMenuIdName();
}
