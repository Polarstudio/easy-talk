package cn.karuhara.etalk.auth.entity;

import cn.karuhara.etalk.framework.mybatis.core.dataobject.BaseDO;
import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 
 * @TableName sys_admin
 */
@EqualsAndHashCode(callSuper = true)
@TableName(value ="sys_admin")
@Data
public class SysAdmin extends BaseDO implements Serializable {
    /**
     * ID
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 账号
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 昵称
     */
    private String nikeName;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 登录ip
     */
    private String loginIp;

    /**
     * 状态（0正常 1封禁）
     */
    private Object status;

    /**
     * 登录时间
     */
    private LocalDateTime loginTime;

    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String creator;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}