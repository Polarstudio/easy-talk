package cn.karuhara.etalk.auth.controller.user.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "用户菜单")
@Data
public class UserMenu {

    @Schema(description = "菜单Id")
    private Integer id;
    /**
     * 菜单名称
     */
    @Schema(description = "菜单名称")
    private String menuName;

    /**
     * 路由地址
     */
    @Schema(description = "路由地址")
    private String path;

    /**
     * 路由名称
     */
    @Schema(description = "路由名称")
    private String routeName;

    /**
     * 菜单类型（C目录 M菜单）
     */
    @Schema(description = "菜单类型（C目录 M菜单）")
    private String menuType;

    /**
     * 菜单状态（0正常 1停用）
     */
    @Schema(description = "菜单状态（0正常 1停用）")
    private Object status;

    /**
     * 菜单图标
     */
    @Schema(description = "菜单图标")
    private String icon;
}
