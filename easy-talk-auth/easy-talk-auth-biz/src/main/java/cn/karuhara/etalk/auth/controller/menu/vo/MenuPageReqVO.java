package cn.karuhara.etalk.auth.controller.menu.vo;

import cn.karuhara.etalk.framework.common.pojo.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
@Schema(description = "管理后台 - 添加分页参数")
public class MenuPageReqVO extends PageParam {

    @Schema(description = "菜单名称")
    private String menuName;

    @Schema(description = "菜单类型")
    private String menuType;

    @Schema(description = "菜单状态")
    private String status;
}
