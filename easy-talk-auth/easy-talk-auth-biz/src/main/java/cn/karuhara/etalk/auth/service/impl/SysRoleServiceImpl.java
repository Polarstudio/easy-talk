package cn.karuhara.etalk.auth.service.impl;

import cn.karuhara.etalk.auth.controller.role.vo.*;
import cn.karuhara.etalk.auth.convert.RoleConvert;
import cn.karuhara.etalk.auth.entity.SysRoleMenu;
import cn.karuhara.etalk.auth.service.SysRoleMenuService;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.karuhara.etalk.auth.entity.SysRole;
import cn.karuhara.etalk.auth.service.SysRoleService;
import cn.karuhara.etalk.auth.mapper.SysRoleMapper;
import jakarta.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
* @author atom
* @description 针对表【sys_role】的数据库操作Service实现
* @createDate 2024-10-28 23:36:35
*/
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole>
    implements SysRoleService{

    @Resource
    private SysRoleMapper mapper;

    @Resource
    private SysRoleMenuService sysRoleMenuService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addRole(RoleAddReqVO addReqVO) {
        //先存入菜单的信息
        SysRole sysRole = RoleConvert.INSTANCE.addVOToRole(addReqVO);
        mapper.insert(sysRole);
        //将角色与其拥有的菜单相关联
        List<Integer> menus = addReqVO.getMenus();
        List<SysRoleMenu> sysRoleMenus = new ArrayList<>();
        menus.forEach(menuId->{
            SysRoleMenu sysRoleMenu = new SysRoleMenu();
            sysRoleMenu.setRoleId(sysRole.getId());
            sysRoleMenu.setMenuId(menuId);
            sysRoleMenus.add(sysRoleMenu);
        });
        sysRoleMenuService.saveBatch(sysRoleMenus);
    }

    @Override
    public PageResult<RoleRespVO> getPage(RolePageReqVO rolePageReqVO) {
        LambdaQueryWrapper<SysRole> queryWrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(rolePageReqVO.getRoleName())) {
            queryWrapper.like(SysRole::getRoleName, rolePageReqVO.getRoleName());
        }
        if (StringUtils.isNotBlank(rolePageReqVO.getStatus())){
            queryWrapper.eq(SysRole::getStatus,rolePageReqVO.getStatus());
        }
        Page<SysRole> page = new Page<>(rolePageReqVO.getPageNo(), rolePageReqVO.getPageSize());
        IPage<SysRole> pageList = mapper.selectPage(page, queryWrapper);
        if (pageList.getTotal() < 0){
            return PageResult.empty();
        }
        List<RoleRespVO> roleRespVOS = RoleConvert.INSTANCE.roleListToRoleRespVOList(pageList.getRecords());
        //根据角色Id获取对应的菜单信息
        roleRespVOS.forEach(roleRespVO->{
            List<Integer> menuIdsByRoleId = sysRoleMenuService.getMenuIdsByRoleId(roleRespVO.getId());
            roleRespVO.setMenus(menuIdsByRoleId);
        });
        return new PageResult<>(roleRespVOS, page.getTotal());
    }

    @Override
    public void updateRole(RoleUpdateReqVO roleUpdateReqVO) {
        SysRole sysRole = RoleConvert.INSTANCE.updateToRole(roleUpdateReqVO);
        //先更新角色的信息
        mapper.updateById(sysRole);
        //在更新对应的菜单权限
        sysRoleMenuService.deleteByRoleIds(sysRole.getId());
        List<Integer> menus = roleUpdateReqVO.getMenus();
        List<SysRoleMenu> sysRoleMenus = new ArrayList<>();
        menus.forEach(menuId->{
            SysRoleMenu sysRoleMenu = new SysRoleMenu();
            sysRoleMenu.setRoleId(sysRole.getId());
            sysRoleMenu.setMenuId(menuId);
            sysRoleMenus.add(sysRoleMenu);
        });
        sysRoleMenuService.saveBatch(sysRoleMenus);
    }

    @Override
    public void deleteRole(Integer id) {
        //先删除角色信息
        removeById(id);
        //在删除对应的角色菜单
        sysRoleMenuService.deleteByRoleIds(id);
    }

    @Override
    public List<RoleIdNameVO> getIdName() {
        List<SysRole> list = list();
        return RoleConvert.INSTANCE.roleToRoleIdNameVOList(list);
    }
}




