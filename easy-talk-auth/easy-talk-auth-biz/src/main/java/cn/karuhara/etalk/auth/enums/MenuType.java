package cn.karuhara.etalk.auth.enums;

import lombok.Getter;

@Getter
public enum MenuType {

    CATALOG("C","目录"),
    MENU("M","菜单");

    private final String type;

    private final String name;

    MenuType(String type,String name){
        this.type = type;
        this.name = name;
    }
}
