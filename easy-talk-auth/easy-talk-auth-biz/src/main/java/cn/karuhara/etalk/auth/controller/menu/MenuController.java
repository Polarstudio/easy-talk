package cn.karuhara.etalk.auth.controller.menu;

import cn.karuhara.etalk.auth.controller.menu.vo.*;
import cn.karuhara.etalk.auth.convert.MenuConvert;
import cn.karuhara.etalk.auth.entity.SysMenu;
import cn.karuhara.etalk.auth.service.SysMenuService;
import cn.karuhara.etalk.auth.service.SysRoleMenuService;
import cn.karuhara.etalk.framework.annotation.WeLog;
import cn.karuhara.etalk.framework.common.pojo.CommonResult;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "管理后台 - 菜单管理")
@RestController
@RequestMapping("/system/auth/menu")
public class MenuController {

    @Resource
    private SysMenuService sysMenuService;

    @Resource
    private SysRoleMenuService roleMenuService;

    @PostMapping
    @Operation(summary = "添加菜单")
    @WeLog(description = "添加菜单操作")
    public CommonResult<Void> add(@RequestBody @Valid MenuAddReqVO addReqVO){
        sysMenuService.add(addReqVO);
        return CommonResult.success();
    }

    @Operation(summary = "分页获取菜单")
    @PostMapping("/page")
    public CommonResult<PageResult<MenuRespVO>> getPage(@RequestBody MenuPageReqVO pageReqVO){
        PageResult<MenuRespVO> pageResult = sysMenuService.getPageList(pageReqVO);
        return CommonResult.success(pageResult);
    }

    @Operation(summary = "获取所有目录")
    @GetMapping("/getCatalog")
    public CommonResult<List<MenuCatalogRespVO>> getCatalog(){
        List<SysMenu> catalog = sysMenuService.getCatalog();
        List<MenuCatalogRespVO> catalogRespVOS = MenuConvert.INSTANCE.menuListToCatalogList(catalog);
        return CommonResult.success(catalogRespVOS);
    }

    @Operation(summary = "修改菜单信息")
    @PutMapping
    public CommonResult<Void> update(@RequestBody MenuUpdateReqVO updateReqVO){
        sysMenuService.updateMenu(updateReqVO);
        return CommonResult.success();
    }

    @Operation(summary = "删除菜单")
    @DeleteMapping
    @WeLog(description = "删除菜单操作")
    public CommonResult<Void> delete(@Schema(description = "菜单ID")
                                         @NotNull(message = "菜单Id不能为空")
                                     @RequestParam("menuId")
                                     Integer menuId
                                     ){
        sysMenuService.removeById(menuId);
        return CommonResult.success();
    }

    @Operation(summary = "获取所有菜单ID+Name")
    @GetMapping("/getIdName")
    public CommonResult<List<MenuIdNameVO>> getIdName(){
        List<MenuIdNameVO> menuIdName = sysMenuService.getMenuIdName();
        return CommonResult.success(menuIdName);
    }

}
