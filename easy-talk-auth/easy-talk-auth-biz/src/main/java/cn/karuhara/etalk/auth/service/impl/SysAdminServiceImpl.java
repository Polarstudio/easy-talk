package cn.karuhara.etalk.auth.service.impl;

import cn.dev33.satoken.stp.StpUtil;
import cn.karuhara.etalk.auth.controller.user.vo.*;
import cn.karuhara.etalk.auth.convert.UserConvert;
import cn.karuhara.etalk.auth.entity.SysAdminRole;
import cn.karuhara.etalk.auth.entity.SysMenu;
import cn.karuhara.etalk.auth.entity.SysRole;
import cn.karuhara.etalk.auth.service.SysAdminRoleService;
import cn.karuhara.etalk.auth.service.SysMenuService;
import cn.karuhara.etalk.auth.service.SysRoleMenuService;
import cn.karuhara.etalk.framework.common.constants.GlobalConstant;
import cn.karuhara.etalk.framework.common.constants.RedisConstant;
import cn.karuhara.etalk.framework.common.context.LoginContextHolder;
import cn.karuhara.etalk.framework.common.enums.GlobalCodeEnum;
import cn.karuhara.etalk.framework.common.exception.ServiceException;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import cn.karuhara.etalk.fremawork.redis.core.MyRedisUtil;
import com.alibaba.fastjson2.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.karuhara.etalk.auth.entity.SysAdmin;
import cn.karuhara.etalk.auth.service.SysAdminService;
import cn.karuhara.etalk.auth.mapper.SysAdminMapper;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
* @author atom
* @description 针对表【sys_admin】的数据库操作Service实现
* @createDate 2024-10-28 23:39:58
*/
@Service
public class SysAdminServiceImpl extends ServiceImpl<SysAdminMapper, SysAdmin>
    implements SysAdminService{

    @Resource
    private SysAdminMapper sysAdminMapper;

    @Resource
    private SysAdminRoleService sysAdminRoleService;

    @Resource
    private SysRoleMenuService sysRoleMenuService;;

    @Resource
    private SysMenuService sysMenuService;

    @Resource
    private MyRedisUtil myRedisUtil;

    @Override
    public void add(UserAddReqVO addReqVO) {
        SysAdmin sysAdmin = new SysAdmin();
        BeanUtils.copyProperties(addReqVO,sysAdmin);
        //账号和邮箱不能重复，所以先判断是否重复
        LambdaQueryWrapper<SysAdmin> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(SysAdmin::getUsername,addReqVO.getUsername())
                .or()
                .eq(SysAdmin::getEmail,addReqVO.getEmail());
        boolean exists = sysAdminMapper.exists(lambdaQueryWrapper);
        if (exists){
            throw new ServiceException(GlobalCodeEnum.FAIL.getCode(), GlobalConstant.ACCOUNT_ALREADY_EXISTS);
        }
        //如果不存在，则添加用户
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encodePassword = encoder.encode(sysAdmin.getPassword());
        sysAdmin.setPassword(encodePassword);
        sysAdminMapper.insert(sysAdmin);
        //最后添加用户角色
        List<SysAdminRole> adminRoles = new ArrayList<>();
        addReqVO.getRoleIds().forEach(roleId ->{
            SysAdminRole sysAdminRole = new SysAdminRole();
            sysAdminRole.setRoleId(roleId);
            sysAdminRole.setAdminId(sysAdmin.getId());
            adminRoles.add(sysAdminRole);
        });
        sysAdminRoleService.saveBatch(adminRoles);
    }

    @Override
    public UserLoginRespVO login(UserLoginReqVO loginVO) {
        LambdaQueryWrapper<SysAdmin> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysAdmin::getUsername,loginVO.getUsername());
        SysAdmin sysAdmin = sysAdminMapper.selectOne(queryWrapper);
        //判断是否存在该用户或者密码是否正确
        if (sysAdmin == null || !BCrypt.checkpw(loginVO.getPassword(),sysAdmin.getPassword())){
            throw new ServiceException(GlobalCodeEnum.FAIL.getCode(),GlobalConstant.USERNAME_OR_PASSWORD_ERROR);
        }
        //获取登录IP
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        sysAdmin.setLoginIp(request.getRemoteAddr());
        LocalDateTime now = LocalDateTime.now();
        sysAdmin.setLoginTime(now);
        //更新用户数据库中对应的登录Ip和登录时间
        sysAdminMapper.updateById(sysAdmin);
        //生成返回信息
        UserLoginRespVO userLoginRespVO = new UserLoginRespVO();
        BeanUtils.copyProperties(sysAdmin,userLoginRespVO);
        //格式化时间，只显示到秒，不然会有多余的位数
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formattedDateTime = now.format(formatter);
        userLoginRespVO.setLoginTime(formattedDateTime);
        //生成Token
        StpUtil.login(sysAdmin.getId());
        String tokenValue = StpUtil.getTokenValue();
        userLoginRespVO.setToken(tokenValue);
        return userLoginRespVO;
    }

    @Override
    public void delete(Integer userId) {
        sysAdminMapper.deleteById(userId);
    }

    @Override
    public PageResult<UserRespVO> getPageList(UserPageReqVO pageReqVO) {
        Page<SysAdmin> page = new Page<>(pageReqVO.getPageNo(), pageReqVO.getPageSize());
        LambdaQueryWrapper<SysAdmin> queryWrapper = new LambdaQueryWrapper<>();
        //条件查询,模糊查询
        if (StringUtils.isNotBlank(pageReqVO.getUsername())){
            queryWrapper.like(SysAdmin::getUsername,pageReqVO.getUsername());
        }
        if (StringUtils.isNotBlank(pageReqVO.getEmail())){
            queryWrapper.like(SysAdmin::getEmail,pageReqVO.getEmail());
        }
        if (StringUtils.isNotBlank(pageReqVO.getStatus().toString())){
            queryWrapper.eq(SysAdmin::getStatus,pageReqVO.getStatus());
        }
        //开始查询
        Page<SysAdmin> sysAdminPage = sysAdminMapper.selectPage(page, queryWrapper);
        //如果查询结果为零直接返回
        if (sysAdminPage.getTotal() == 0){
            return PageResult.empty();
        }
        //否则做一波转换，去掉敏感词信息
        List<UserRespVO> userRespVOList = UserConvert.INSTANCE.sysAdminsToUserRespVOs(sysAdminPage.getRecords());
        //最后在获取用户的角色
        userRespVOList.forEach(userRespVO ->{
            List<Integer> roles = sysAdminRoleService.getRoleIdsByUserId(userRespVO.getId());
            userRespVO.setRoleIds(roles);
        });
        return new PageResult<>(userRespVOList,sysAdminPage.getTotal());
    }

    @Override
    public void updateUser(UserUpdateReqVO updateReqVO) {
        //新更新基本的信息
        SysAdmin sysAdmin = UserConvert.INSTANCE.updateVOToSysadmin(updateReqVO);
        if (StringUtils.isNotBlank(sysAdmin.getPassword())){
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            String newPassword = encoder.encode(sysAdmin.getPassword());
            sysAdmin.setPassword(newPassword);
        }
        sysAdminMapper.updateById(sysAdmin);
        //再删除对用的角色
        sysAdminRoleService.deleteByUserId(sysAdmin.getId());
        //最后添加用户角色
        List<SysAdminRole> adminRoles = new ArrayList<>();
        updateReqVO.getRoleIds().forEach(roleId ->{
            SysAdminRole sysAdminRole = new SysAdminRole();
            sysAdminRole.setRoleId(roleId);
            sysAdminRole.setAdminId(sysAdmin.getId());
            adminRoles.add(sysAdminRole);
        });
        sysAdminRoleService.saveBatch(adminRoles);
    }

    @Override
    public List<SysMenu> getUserMenu() {
        Integer user_login = LoginContextHolder.get();
        if (user_login == null){
            throw new ServiceException(GlobalCodeEnum.LOGIN_AUTH);
        }
        // 根据用户id获取对应的角色
        List<SysRole> roles = sysAdminRoleService.getUserRole(user_login);
        // 获取角色的id，根据id去获取对应的菜单
        List<Integer> roleIds = roles.stream().map(SysRole::getId).collect(Collectors.toList());
        List<SysMenu> menus = new ArrayList<>();
        roleIds.forEach(roleId -> {
            List<SysMenu> sysMenus = sysRoleMenuService.getMenuByRoleId(roleId);
            menus.addAll(sysMenus);
        });
        //做一波去重
        List<SysMenu> uniqueMenus = menus.stream().distinct().collect(Collectors.toList());
        // 获取所有的目录
        List<SysMenu> catalogs = sysMenuService.getCatalog();
        // 构建目录到其菜单的映射
        Map<Integer, List<SysMenu>> catalogMenuMap = new HashMap<>();
        for (SysMenu menu : uniqueMenus) {
            int parentId = menu.getParentId();
            if (!catalogMenuMap.containsKey(parentId)) {
                catalogMenuMap.put(parentId, new ArrayList<>());
            }
            catalogMenuMap.get(parentId).add(menu);
        }
        // 对目录进行排序
        List<SysMenu> sortedCatalogs = catalogs.stream()
                .sorted(Comparator.comparingInt(SysMenu::getOrderNum))
                .collect(Collectors.toList());
        // 将排序后的目录下的菜单进行排序，并添加到目录中，同时舍弃没有菜单的目录
        List<SysMenu> sortedMenuList = new ArrayList<>();
        for (SysMenu catalog : sortedCatalogs) {
            int catalogId = catalog.getId();
            if (catalogMenuMap.containsKey(catalogId) &&!catalogMenuMap.get(catalogId).isEmpty()) {
                sortedMenuList.add(catalog);
                List<SysMenu> menuList = catalogMenuMap.get(catalogId);
                List<SysMenu> sortedMenus = menuList.stream()
                        .sorted(Comparator.comparingInt(SysMenu::getOrderNum))
                        .collect(Collectors.toList());
                sortedMenuList.addAll(sortedMenus);
            }
        }
        //获取角色标识
        List<String> roleCode = roles.stream().map(SysRole::getCode).collect(Collectors.toList());
        //获取权限标识
        List<String> permissionCode = uniqueMenus.stream().map(SysMenu::getPerms).collect(Collectors.toList());
        String rolesKey = MyRedisUtil.buildKey(RedisConstant.ROLES_PREFIX, String.valueOf(user_login));
        //myRedisUtil.set(rolesKey, JSON.toJSONString(roleCode));
        String permissionKey = MyRedisUtil.buildKey(RedisConstant.PERMISSIONS_PREFIX, String.valueOf(user_login));
        //myRedisUtil.set(permissionKey,JSON.toJSONString(permissionCode));
        return sortedMenuList;
    }

    public void getUserRoleAndPermission(String username) {
        //根据用户名获取对应的用户
        SysAdmin user = getUser(username);
        if (user == null){
            throw new ServiceException(GlobalCodeEnum.LOGIN_AUTH);
        }
        // 根据用户id获取对应的角色
        List<SysRole> roles = sysAdminRoleService.getUserRole(user.getId());
        // 获取角色的id，根据id去获取对应的菜单
        List<Integer> roleIds = roles.stream().map(SysRole::getId).collect(Collectors.toList());
        List<SysMenu> menus = new ArrayList<>();
        roleIds.forEach(roleId -> {
            List<SysMenu> sysMenus = sysRoleMenuService.getMenuByRoleId(roleId);
            menus.addAll(sysMenus);
        });
        //做一波去重
        List<SysMenu> uniqueMenus = menus.stream().distinct().collect(Collectors.toList());
        //获取角色标识
        List<String> roleCode = roles.stream().map(SysRole::getCode).collect(Collectors.toList());
        //获取权限标识
        List<String> permissionCode = uniqueMenus.stream().map(SysMenu::getPerms).collect(Collectors.toList());
        String rolesKey = MyRedisUtil.buildKey(RedisConstant.ROLES_PREFIX, username);
        myRedisUtil.set(rolesKey,roleCode);
        String permissionKey = MyRedisUtil.buildKey(RedisConstant.PERMISSIONS_PREFIX, username);
        myRedisUtil.set(permissionKey,permissionCode);
    }

    @Override
    public SysAdmin getUser(String username) {
        LambdaQueryWrapper<SysAdmin> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysAdmin::getUsername,username);
        return sysAdminMapper.selectOne(queryWrapper);
    }
}




