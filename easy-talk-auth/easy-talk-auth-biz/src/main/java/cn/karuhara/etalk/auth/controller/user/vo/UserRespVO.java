package cn.karuhara.etalk.auth.controller.user.vo;

import cn.karuhara.etalk.auth.controller.role.vo.RoleIdNameVO;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Schema(description = "管理后台 - 分页获取用户返回结果")
@Data
public class UserRespVO {

    @Schema(description = "用户ID")
    private Integer id;

    @Schema(description = "用户名")
    private String username;

    @Schema(description = "昵称")
    private String nikeName;

    @Schema(description = "头像")
    private String avatar;

    @Schema(description = "邮箱")
    private String email;

    @Schema(description = "用户角色")
    private List<Integer> roleIds;

    @Schema(description = "登录IP")
    private String loginIp;

    @Schema(description = "状态")
    private Object status;

    @Schema(description = "登录时间")
    private String loginTime;

    @Schema(description = "创建时间")
    private LocalDateTime createTime;

    @Schema(description = "最后更新时间")
    private LocalDateTime updateTime;

    @Schema(description = "创建者")
    private String creator;
}
