package cn.karuhara.etalk.auth.service.impl;

import cn.karuhara.etalk.auth.controller.menu.vo.MenuIdNameVO;
import cn.karuhara.etalk.auth.entity.SysMenu;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.karuhara.etalk.auth.entity.SysRoleMenu;
import cn.karuhara.etalk.auth.service.SysRoleMenuService;
import cn.karuhara.etalk.auth.mapper.SysRoleMenuMapper;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
* @author atom
* @description 针对表【sys_role_menu】的数据库操作Service实现
* @createDate 2024-10-28 23:36:35
*/
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu>
    implements SysRoleMenuService{

    @Resource
    private SysRoleMenuMapper sysRoleMenuMapper;

    @Override
    @Transactional
    public void batchInsert(List<SysRoleMenu> sysRoleMenus) {
        saveBatch(sysRoleMenus);
    }

    @Override
    public List<MenuIdNameVO> getMenuIdName(Integer roleId) {
        return sysRoleMenuMapper.getMenuIdName(roleId);
    }

    @Override
    public void deleteByRoleIds(Integer roleId) {
        LambdaQueryWrapper<SysRoleMenu> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysRoleMenu::getRoleId, roleId);
        sysRoleMenuMapper.delete(wrapper);
    }

    @Override
    public List<Integer> getMenuIdsByRoleId(Integer roleId) {
        LambdaQueryWrapper<SysRoleMenu> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysRoleMenu::getRoleId, roleId);
        List<SysRoleMenu> sysRoleMenus = sysRoleMenuMapper.selectList(wrapper);
        List<Integer> menuIds = new ArrayList<>();
        for (SysRoleMenu sysRoleMenu : sysRoleMenus) {
            menuIds.add(sysRoleMenu.getMenuId());
        }
        return menuIds;
    }

    @Override
    public List<SysMenu> getMenuByRoleId(Integer roleId) {
        return sysRoleMenuMapper.getMenuByRoleId(roleId);
    }

}




