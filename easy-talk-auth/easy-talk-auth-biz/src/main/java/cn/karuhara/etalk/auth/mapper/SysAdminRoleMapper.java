package cn.karuhara.etalk.auth.mapper;

import cn.karuhara.etalk.auth.entity.SysAdminRole;
import cn.karuhara.etalk.auth.entity.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
* @author atom
* @description 针对表【sys_admin_role】的数据库操作Mapper
* @createDate 2024-10-28 23:36:35
* @Entity cn.karuhara.etalk.auth.domain.SysAdminRole
*/
public interface SysAdminRoleMapper extends BaseMapper<SysAdminRole> {

    @Select("SELECT * FROM sys_role LEFT JOIN sys_admin_role ON sys_admin_role.role_id = sys_role.id WHERE sys_admin_role.admin_id =#{userId}")
    List<SysRole> getUserRole(@Param("userId") Integer userId);
}




