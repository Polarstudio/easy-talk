package cn.karuhara.etalk.auth.mapper;

import cn.karuhara.etalk.auth.entity.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author atom
* @description 针对表【sys_menu(菜单权限表)】的数据库操作Mapper
* @createDate 2024-10-28 23:36:35
* @Entity cn.karuhara.etalk.auth.domain.SysMenu
*/
public interface SysMenuMapper extends BaseMapper<SysMenu> {

}




