package cn.karuhara.etalk.auth.convert;

import cn.karuhara.etalk.auth.controller.menu.vo.*;
import cn.karuhara.etalk.auth.controller.user.vo.UserMenu;
import cn.karuhara.etalk.auth.entity.SysMenu;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface MenuConvert {

    MenuConvert INSTANCE = Mappers.getMapper(MenuConvert.class);

    SysMenu addVOToMenu(MenuAddReqVO addReqVO);

    List<MenuRespVO> menuListToRespVOList(List<SysMenu> sysMenus);

    List<MenuCatalogRespVO> menuListToCatalogList(List<SysMenu> sysMenus);

    SysMenu updateVOToMenu(MenuUpdateReqVO updateReqVO);

    List<MenuIdNameVO> menuListToMenuIdNameList(List<SysMenu> sysMenus);

    List<UserMenu> MenuToUserMenuList(List<SysMenu> sysMenus);
}
