package cn.karuhara.etalk.auth.controller.menu.vo;

import lombok.Data;

@Data
public class MenuIdNameVO {

    private Integer id;

    private String menuName;

    private String menuType;
}
