package cn.karuhara.etalk.auth.service.impl;

import cn.karuhara.etalk.auth.controller.menu.vo.*;
import cn.karuhara.etalk.auth.convert.MenuConvert;
import cn.karuhara.etalk.auth.enums.MenuType;
import cn.karuhara.etalk.framework.common.pojo.PageResult;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.karuhara.etalk.auth.entity.SysMenu;
import cn.karuhara.etalk.auth.service.SysMenuService;
import cn.karuhara.etalk.auth.mapper.SysMenuMapper;
import jakarta.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author atom
* @description 针对表【sys_menu(菜单权限表)】的数据库操作Service实现
* @createDate 2024-10-28 23:36:35
*/
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu>
    implements SysMenuService{

    @Resource
    private SysMenuMapper sysMenuMapper;

    @Override
    public void add(MenuAddReqVO addReqVO) {
        SysMenu sysMenu = MenuConvert.INSTANCE.addVOToMenu(addReqVO);
        sysMenuMapper.insert(sysMenu);
    }

    @Override
    public PageResult<MenuRespVO> getPageList(MenuPageReqVO pageReqVO) {
        LambdaQueryWrapper<SysMenu> queryWrapper = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(pageReqVO.getMenuName())) {
            queryWrapper.like(SysMenu::getMenuName, pageReqVO.getMenuName());
        }
        if (StringUtils.isNotBlank(pageReqVO.getMenuType())){
            queryWrapper.eq(SysMenu::getMenuType,pageReqVO.getMenuType());
        }
        if (StringUtils.isNotBlank(pageReqVO.getStatus())){
            queryWrapper.eq(SysMenu::getStatus,pageReqVO.getStatus());
        }
        Page<SysMenu> page = new Page<>(pageReqVO.getPageNo(), pageReqVO.getPageSize());
        Page<SysMenu> menuPage = sysMenuMapper.selectPage(page, queryWrapper);
        if (menuPage.getTotal() == 0){
            return PageResult.empty();
        }
        List<MenuRespVO> menuRespVOS = MenuConvert.INSTANCE.menuListToRespVOList(menuPage.getRecords());
        return new PageResult(menuRespVOS,menuPage.getTotal());
    }

    @Override
    public List<SysMenu> getCatalog() {
        LambdaQueryWrapper<SysMenu> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysMenu::getMenuType, MenuType.CATALOG.getType());
        return sysMenuMapper.selectList(queryWrapper);
    }

    public List<MenuIdNameVO> getMenuIdName() {
        LambdaQueryWrapper<SysMenu> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysMenu::getMenuType, MenuType.MENU.getType());
        List<SysMenu> sysMenus = sysMenuMapper.selectList(queryWrapper);
        return MenuConvert.INSTANCE.menuListToMenuIdNameList(sysMenus);
    }

    @Override
    public void updateMenu(MenuUpdateReqVO updateReqVO) {
        SysMenu sysMenu = MenuConvert.INSTANCE.updateVOToMenu(updateReqVO);
        sysMenuMapper.updateById(sysMenu);
    }

}




