package cn.karuhara.etalk.auth.controller.user.vo;

import cn.karuhara.etalk.framework.common.pojo.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Schema(description = "管理后台 - 分页获取用户VO")
@Data
public class UserPageReqVO extends PageParam {

    @Schema(description = "用户账号，模糊匹配", example = "etalk")
    private String username;

    @Schema(description = "邮箱地址，模糊匹配", example = "etalk")
    private String email;

    @Schema(description = "展示状态,(0 正常 1 禁用)", example = "0")
    private Object status;

}
