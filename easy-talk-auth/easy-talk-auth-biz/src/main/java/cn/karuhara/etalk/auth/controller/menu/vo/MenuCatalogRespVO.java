package cn.karuhara.etalk.auth.controller.menu.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "目录")
@Data
public class MenuCatalogRespVO {

    @Schema(description = "目录ID")
    private Integer id;

    @Schema(description = "目录名称")
    private String menuName;

}
