package cn.karuhara.etalk.auth.service;

import cn.karuhara.etalk.auth.entity.SysAdminRole;
import cn.karuhara.etalk.auth.entity.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author atom
* @description 针对表【sys_admin_role】的数据库操作Service
* @createDate 2024-10-28 23:36:35
*/
public interface SysAdminRoleService extends IService<SysAdminRole> {

    /**
     * 根据用户Id获取对应的角色Id
     * @param id
     * @return
     */
    List<Integer> getRoleIdsByUserId(Integer id);

    /**
     * 根据用户Id删除对用的角色映射
     * @param id
     */
    void deleteByUserId(Integer id);

    /**
     * 根据用户id获取对应的角色
     * @param userId 角色id
     * @return 角色列表
     */
    List<SysRole> getUserRole(Integer userId);
}
