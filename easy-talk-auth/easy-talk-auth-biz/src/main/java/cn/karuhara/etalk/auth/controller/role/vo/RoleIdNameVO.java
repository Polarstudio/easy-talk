package cn.karuhara.etalk.auth.controller.role.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "角色的Id和名称")
@Data
public class RoleIdNameVO {

    private Integer id;

    private String roleName;
}
