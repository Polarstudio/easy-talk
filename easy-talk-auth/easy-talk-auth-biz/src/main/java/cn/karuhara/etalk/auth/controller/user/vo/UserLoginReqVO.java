package cn.karuhara.etalk.auth.controller.user.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Schema(description = "管理后台 - 账号密码登录 Request VO")
@Data
public class UserLoginReqVO {

    @Schema(description = "账号",requiredMode = Schema.RequiredMode.REQUIRED , example = "admin")
    @NotEmpty(message = "账号不能为空！")
    private String username;

    @Schema(description = "密码",requiredMode = Schema.RequiredMode.REQUIRED , example = "admin")
    @NotEmpty(message = "密码不能为空！")
    private String password;

}
