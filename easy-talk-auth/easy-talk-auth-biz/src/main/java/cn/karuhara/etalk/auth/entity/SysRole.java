package cn.karuhara.etalk.auth.entity;

import cn.karuhara.etalk.framework.mybatis.core.dataobject.BaseDO;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 
 * @TableName sys_role
 */
@EqualsAndHashCode(callSuper = true)
@TableName(value ="sys_role")
@Data
public class SysRole extends BaseDO implements Serializable {
    /**
     * ID
     */
    @TableId
    private Integer id;

    /**
     * 角色名
     */
    private String roleName;

    /**
     * 角色标识
     */
    private String code;

    /**
     * 角色类型(0 超级管理员 1自定义管理员)
     */
    private Integer type;

    /**
     * 备注
     */
    private String remark;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 状态（0正常 1封禁）
     */
    private String status;

    /**
     * 创建者
     */
    @TableField(fill = FieldFill.INSERT)
    private String creator;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}