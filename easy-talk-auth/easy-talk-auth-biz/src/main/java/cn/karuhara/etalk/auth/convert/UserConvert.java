package cn.karuhara.etalk.auth.convert;

import cn.karuhara.etalk.auth.controller.user.vo.UserRespVO;
import cn.karuhara.etalk.auth.controller.user.vo.UserUpdateReqVO;
import cn.karuhara.etalk.auth.entity.SysAdmin;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface UserConvert {

    UserConvert INSTANCE = Mappers.getMapper(UserConvert.class);

    List<UserRespVO> sysAdminsToUserRespVOs(List<SysAdmin> sysAdmins);

    SysAdmin updateVOToSysadmin(UserUpdateReqVO updateReqVO);
}
