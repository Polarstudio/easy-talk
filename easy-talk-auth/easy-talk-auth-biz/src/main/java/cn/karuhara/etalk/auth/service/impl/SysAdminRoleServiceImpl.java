package cn.karuhara.etalk.auth.service.impl;

import cn.karuhara.etalk.auth.entity.SysRole;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import cn.karuhara.etalk.auth.entity.SysAdminRole;
import cn.karuhara.etalk.auth.service.SysAdminRoleService;
import cn.karuhara.etalk.auth.mapper.SysAdminRoleMapper;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
* @author atom
* @description 针对表【sys_admin_role】的数据库操作Service实现
* @createDate 2024-10-28 23:36:35
*/
@Service
public class SysAdminRoleServiceImpl extends ServiceImpl<SysAdminRoleMapper, SysAdminRole>
    implements SysAdminRoleService{

    @Resource
    SysAdminRoleMapper sysAdminRoleMapper;

    @Override
    public List<Integer> getRoleIdsByUserId(Integer id) {
        LambdaQueryWrapper<SysAdminRole> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysAdminRole::getAdminId, id);
        List<SysAdminRole> adminRoles = sysAdminRoleMapper.selectList(queryWrapper);
        List<Integer> roleIds = new ArrayList<>();
        adminRoles.forEach(sysAdminRole -> {
            roleIds.add(sysAdminRole.getRoleId());
        });
        return roleIds;
    }

    @Override
    public void deleteByUserId(Integer id) {
        LambdaQueryWrapper<SysAdminRole> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysAdminRole::getAdminId, id);
        sysAdminRoleMapper.delete(queryWrapper);
    }

    @Override
    public List<SysRole> getUserRole(Integer userId) {
        return sysAdminRoleMapper.getUserRole(userId);
    }
}




